#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

extern uint64_t div64(uint64_t a, uint64_t b);

int main() {
  uint64_t old_a = 0;
  for(uint64_t a=1; a >= old_a; a += 931+UINT64_C(1)<<40) {
    old_a = a;
    printf("%" PRIu64 "\n", a);
    uint64_t old_b = 0;
    for(uint64_t b=1; b >= old_b; b += 777+UINT64_C(1)<<40) {
      old_b = b;
      //printf("%" PRIu64 "\n", b);
      uint64_t x = div64(a, b), y = a/b;
      if (x != y) {
	printf("%" PRIu64 " %" PRIu64 " %" PRIu64 " %" PRIu64 "\n", a, b, x, y);
	return 1;
      }
    }
  }
  return 0;
}
