We thank the reviewers for their insights and questions.

* Review 1
** In this paper the authors pretend to formally verify 32-bit and 64-bit integer division algorithms
I suppose you mean "claim", not "pretend".

** the authors rely on the Coq fappa tactic which call Gappa tool. From this point on, it takes a leap of faith to believe what comes next, that is, it is not possible to verify that data are true.
The paper contains a link to the full Coq development. It is possible for reviewers to recompile this development and check the properties proved in the FPDivision32/64 files.
Proofs using Gappa do not trust Gappa; Gappa produces a proof term checked by the Coq kernel. There is no leap of faith.

* Review 2
** I am a bit surprised by the correcting step at the end of the algorithms
I suppose you mean the computation of the remainder and the adjustment by -1 to the quotient if the remainder is negative.

We prove that the difference between the approximate quotient and the true quotient is less than one half.
We however don't know if the result is approximated from below or above, thus the need for the final adjustment.

We have tried the same algorithm with directing rounding, but this complicates some steps (some values need to be computed with both rounding directions) and anyway a final correcting step is still needed. The reason is that if we compute the quotient in over-approximation, then round it to the integer below, there are rare cases where the exact quotient is close to an integer but just below it, and the over-approximation causes it to go above the integer.

** it would still be interesting to compare the methodology and the proof effort of the authors' approach with respect to Harrison's one
Harrison's proof files are not available, as far as we know (in fact we contacted Harrison and he expressed some difficulties answering questions about that old series of papers).

** so close to C code that it should just be C code
We'll change it to be actual compilable C code.

** the "bs" variable is of type "double" each time; is that a typo?
Indeed, thanks for spotting it.

** changes in vocabulary (e.g. binary32)
We'll implemented the requested changes.

* Review 3
** Several "compiler builtins" are accompanied by "canned" assembly snippets with correctness proofs, which could be done for existing division approach.

Indeed. We would need to get a clearer specification of the stsud instruction used in the loop, and then do a proof of correctness of the loop and surroundings.

** It was quite surprising how big an impact instruction scheduling had in the evaluation -- nearly a 2x speedup!

The KV3 is a VLIW processor where all scheduling must be done inside the compiler.

** Why did you choose the particular input ranges for your test workloads?

We did not know what to take. In particular we don't have access to measurements of the typical sizes of operands to division operators.

** What is the speedup like for a common divisor across all inputs? Can the instruction scheduler hoist all the common ops outside the loop?

Indeed, our version of CompCert can hoist common divisor code out of the loop, e.g. with the optimization described here: https://hal.archives-ouvertes.fr/hal-03212087 (a newer code hoister, with more general capabilities, is currently being tested).

We'll include benchmarks with loop-hoisted code.

** The algorithms in this paper also work on some Arm and AMD64 platforms
These platforms have hardware integer division units; I don't quite understand what experiment is being suggested.

** What is the performance impact if you have to take the branches to avoid trapping on conversions to floating point (Alg 3)?
This is a bit of a complicated question. Our algorithm has a number of "special cases" due to large operands, divisor equal to one, etc., which may branch to "shortcuts". So it should be faster to use the branching version if these cases are frequent. However
- This breaks constant-time computation, which may be an issue for certain applications (e.g. inside cryptographic primitives). Granted, the hardware division units available in other architectures also break constant-time computation, since their execution latency typically depends on the number of bits in the quotient.
- Branches may hinder some compiler optimizations, typically those that operate over basic blocks; but they may still work if operating over superblocks.
- The cost of branches depends on the microarchitecture. On processors with branch predictors, typically these branches to exceptional cases should cost nothing. Unfortunately branch predictors make execution time harder to predict statically (since unrelated pieces of code may affect branch prediction), which is annoying for some hard real time applications, thus the KV3 does not have any.
