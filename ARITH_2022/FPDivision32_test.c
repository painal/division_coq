#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

extern uint32_t div32(uint32_t a, uint32_t b);

int main() {
  for(uint32_t a=0; a< 20000000; a += 1931) {
    printf("%" PRIu32 "\n", a);
    for(uint32_t b=1; b< 20000000; b += 1777) {
      if (div32(a, b) != a/b) {
	printf("%" PRIu32 "%" PRIu32 "\n", a, b);
	return 1;
      }
    }
  }
  return 0;
}
