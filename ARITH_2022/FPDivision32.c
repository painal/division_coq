#include <stdint.h>
#include <math.h>

uint32_t div32(uint32_t a, uint32_t b) {
  float bs = (float) b;
  double bd = (double) b;
  float invbs0 = 1.0f / bs;
  double invbd0 = (double) invbs0;
  double alpha = fma(-bd, invbd0, 1.0);
  double invbd = fma(alpha,invbd0,invbd0);
  double ad = (double) a;
  double qd = ad * invbd;
  // round to nearest, unsigned
  uint64_t q0 = __builtin_luround_ne(qd);
  int64_t r0 = a - b * q0;
  uint64_t ql = r0<0 ? q0-1 : q0;
  return (uint32_t) ql;
}
