#include <stdint.h>
#include <stdbool.h>
#include <math.h>

uint64_t div64(uint64_t a, uint64_t b) {
  if (b <= 1) return a;
  if ((int64_t) b < 0) // b >= 2^63
    return a >= b;
  double bd = (double) b;
  float bs = (float) bd;
  float invbs0 = 1.0f / bs;
  double invbd0 = (double) invbs0;
  double alpha = fma(-bd, invbd0, 1.0);
  double invbd = fma(alpha,invbd0,invbd0);
  double ad = (double) a;
  double q1d = ad * invbd0;
  // round to nearest, unsigned
  uint64_t q1 = __builtin_luround_ne(q1d);
  int64_t r1 = a - b*q1;
  double r1d = (double) r1;
  double q3d = r1d * invbd;
  // round to nearest, signed
  int64_t q3 = __builtin_lround_ne(q3d);
  int64_t r3 = r1 - b * q3;
  int64_t q2 = r3<0 ? q3-1 : q3;
  return q1 + q2;
}
