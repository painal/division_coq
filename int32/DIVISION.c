#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

uint32_t single_division(uint32_t a, uint32_t b)
{
	float inv_b_hat = 1.0f / (float) b; 
	double alpha = fma(b, -inv_b_hat, 1.0);	
	double inv_b_hat1 = fma(alpha, inv_b_hat, inv_b_hat);
	double quotient_hat1 = a * inv_b_hat1;

	uint64_t quotient = lround(quotient_hat1);
	int32_t remainder = a - quotient * b;
	if (remainder < 0)
	{
		quotient--;
		remainder = b - remainder;
	}

	return quotient;
}

int main() 
{
	return 0;
}

	
