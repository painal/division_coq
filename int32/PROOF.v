(* === Library imports === *)
From Coq Require Import Reals.
Require Import Coq.Numbers.NatInt.NZDiv.
Require Import Coq.Logic.ClassicalFacts.
Require Import Coq.QArith.QArith_base.
Require Import QArith.
Require Import BinInt.
Require Import Coq.ZArith.BinIntDef.
Require Import Gappa.Gappa_library.
From Gappa Require Import Gappa_tactic.
Require Import Psatz.
Require Import Lia.
Require Import Field.

Module Step_1.

Section Generated_by_Gappa1.
(* === Gappa proof: 
{b in [1, 4294967295] -> exact_alpha in [-1b-22,1b-22]} === *)

Variable _b : R.
Notation r3 := (Float1 (1)).
Notation r7 := ((rounding_float rndNE (24)%positive (-149)%Z) _b).
Notation r6 := ((r3 / r7)%R).
Notation _inv_b_hat := ((rounding_float rndNE (24)%positive (-149)%Z) r6).
Notation r4 := ((_b * _inv_b_hat)%R).
Notation _exact_alpha := ((r3 - r4)%R).
Notation r10 := ((- r3)%R).
Notation r12 := ((r3 / _b)%R).
Notation r11 := ((_inv_b_hat - r12)%R).
Notation r9 := ((r10 * r11)%R).
Notation r8 := ((r9 / r12)%R).
Hypothesis a1 : (_b <> 0)%R -> _exact_alpha = r8.
Lemma b1 : NZR _b -> _exact_alpha = r8.
 intros h0.
 apply a1.
 exact h0.
Qed.
Definition f1 := Float2 (1) (0).
Definition f2 := Float2 (4294967295) (0).
Definition i1 := makepairF f1 f2.
Notation p1 := (BND _b i1). (* BND(b, [1, 4.29497e+09]) *)
Definition f3 := Float2 (-1) (-22).
Definition f4 := Float2 (1) (-22).
Definition i2 := makepairF f3 f4.
Notation p2 := (BND _exact_alpha i2). (* BND(exact_alpha, [-2.38419e-07, 2.38419e-07]) *)
Definition s2 := (not p2).
Definition s1 := (p1 /\ s2).
Definition f5 := Float2 (1) (31).
Notation p3 := ((_b <= f5)%R). (* BND(b, [-inf, 2.14748e+09]) *)
Notation p4 := ((_exact_alpha <= f4)%R). (* BND(exact_alpha, [-inf, 2.38419e-07]) *)
Notation p5 := ((_exact_alpha <= f3)%R). (* BND(exact_alpha, [-inf, -2.38419e-07]) *)
Notation r13 := ((r6 - r12)%R).
Definition f6 := Float2 (-720576928222549575) (-90).
Definition f7 := Float2 (720576971172283977) (-114).
Definition i3 := makepairF f6 f7.
Notation p6 := (BND r13 i3). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-5.82077e-10, 3.46945e-17]) *)
Definition f8 := Float2 (-281474959933441) (-72).
Definition f9 := Float2 (576460786663163905) (-83).
Definition i4 := makepairF f8 f9.
Notation p7 := (REL r6 r12 i4). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Definition f10 := Float2 (-1) (-24).
Definition f11 := Float2 (1) (-24).
Definition i5 := makepairF f10 f11.
Notation p8 := (REL r7 _b i5). (* REL(float<24,-149,ne>(b), b, [-5.96046e-08, 5.96046e-08]) *)
Definition f12 := Float2 (1) (32).
Definition i6 := makepairF f1 f12.
Notation p9 := (ABS _b i6). (* ABS(b, [1, 4.29497e+09]) *)
Lemma l9 : s1 -> p1 (* BND(b, [1, 4.29497e+09]) *).
 intros h0.
 assert (h1 := h0).
 exact (proj1 h1).
Qed.
Notation p10 := (BND _b i6). (* BND(b, [1, 4.29497e+09]) *)
Lemma t1 : p10 -> p9.
 intros h0.
 refine (abs_of_bnd_p _b i6 i6 h0 _) ; finalize.
Qed.
Lemma l8 : s1 -> p9 (* ABS(b, [1, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l9 h0).
 apply t1. refine (subset _b i1 i6 h1 _) ; finalize.
Qed.
Lemma t2 : p9 -> p8.
 intros h0.
 refine (float_relative_ne _ _ _b i6 i5 h0 _) ; finalize.
Qed.
Lemma l7 : s1 -> p8 (* REL(float<24,-149,ne>(b), b, [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l8 h0).
 apply t2. exact h1.
Qed.
Notation p11 := (NZR _b). (* NZR(b) *)
Lemma t3 : p9 -> p11.
 intros h0.
 refine (nzr_of_abs _b i6 h0 _) ; finalize.
Qed.
Lemma l10 : s1 -> p11 (* NZR(b) *).
 intros h0.
 assert (h1 := l8 h0).
 apply t3. exact h1.
Qed.
Lemma t4 : p8 -> p11 -> p7.
 intros h0 h1.
 refine (inv_r r7 _b _ i5 i4 h0 h1 _) ; finalize.
Qed.
Lemma l6 : s1 -> p7 (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l7 h0).
 assert (h2 := l10 h0).
 apply t4. exact h1. exact h2.
Qed.
Definition f13 := Float2 (1) (-31).
Definition f14 := Float2 (720576928222549575) (-90).
Definition i7 := makepairF f13 f14.
Notation p12 := (BND r12 i7). (* BND(1 / b, [4.65661e-10, 5.82077e-10]) *)
Definition f15 := Float2 (1) (-32).
Definition i8 := makepairF f15 f14.
Notation p13 := (BND r12 i8). (* BND(1 / b, [2.32831e-10, 5.82077e-10]) *)
Notation p14 := (NZR r9). (* NZR(-1 * (inv_b_hat - 1 / b)) *)
Definition f16 := Float2 (1) (-53).
Definition i9 := makepairF f16 f1.
Notation p15 := (ABS r9 i9). (* ABS(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1]) *)
Definition f17 := Float2 (-864691162814875649) (-83).
Definition f18 := Float2 (-1) (-53).
Definition i10 := makepairF f17 f18.
Notation p16 := (BND r9 i10). (* BND(-1 * (inv_b_hat - 1 / b), [-8.9407e-08, -1.11022e-16]) *)
Definition f19 := Float2 (-1) (8).
Definition i11 := makepairF f19 f18.
Notation p17 := (BND r9 i11). (* BND(-1 * (inv_b_hat - 1 / b), [-256, -1.11022e-16]) *)
Notation p18 := (NZR r12). (* NZR(1 / b) *)
Notation p19 := (NZR r3). (* NZR(1) *)
Definition i12 := makepairF f1 f1.
Notation p20 := (ABS r3 i12). (* ABS(1, [1, 1]) *)
Notation p21 := (BND r3 i12). (* BND(1, [1, 1]) *)
Lemma t5 : p21.
 refine (constant1 _ i12 _) ; finalize.
Qed.
Lemma l20 : s1 -> p21 (* BND(1, [1, 1]) *).
 intros h0.
 apply t5.
Qed.
Lemma t6 : p21 -> p20.
 intros h0.
 refine (abs_of_bnd_p r3 i12 i12 h0 _) ; finalize.
Qed.
Lemma l19 : s1 -> p20 (* ABS(1, [1, 1]) *).
 intros h0.
 assert (h1 := l20 h0).
 apply t6. exact h1.
Qed.
Lemma t7 : p20 -> p19.
 intros h0.
 refine (nzr_of_abs r3 i12 h0 _) ; finalize.
Qed.
Lemma l18 : s1 -> p19 (* NZR(1) *).
 intros h0.
 assert (h1 := l19 h0).
 apply t7. exact h1.
Qed.
Lemma t8 : p19 -> p11 -> p18.
 intros h0 h1.
 refine (div_nzr r3 _b h0 h1) ; finalize.
Qed.
Lemma l17 : s1 -> p18 (* NZR(1 / b) *).
 intros h0.
 assert (h1 := l18 h0).
 assert (h2 := l10 h0).
 apply t8. exact h1. exact h2.
Qed.
Notation r14 := ((r8 * r12)%R).
Notation p22 := (BND r14 i11). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b) * (1 / b), [-256, -1.11022e-16]) *)
Definition i13 := makepairF f19 f3.
Notation p23 := (BND r8 i13). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-256, -2.38419e-07]) *)
Notation p24 := (BND _exact_alpha i13). (* BND(exact_alpha, [-256, -2.38419e-07]) *)
Lemma l24 : p5 -> p4 -> p3 -> s1 -> p5 (* BND(exact_alpha, [-inf, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := h0).
 exact (h4).
Qed.
Definition i14 := makepairF f19 f1.
Notation p25 := (BND _exact_alpha i14). (* BND(exact_alpha, [-256, 1]) *)
Notation r16 := ((_exact_alpha - r8)%R).
Notation r15 := ((r8 + r16)%R).
Notation p26 := (BND r15 i14). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b) + (exact_alpha - -1 * (inv_b_hat - 1 / b) / (1 / b)), [-256, 1]) *)
Notation p27 := (BND r8 i14). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-256, 1]) *)
Definition f20 := Float2 (-1) (32).
Definition i15 := makepairF f20 f1.
Notation p28 := (BND r8 i15). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-4.29497e+09, 1]) *)
Notation p29 := (BND _exact_alpha i15). (* BND(exact_alpha, [-4.29497e+09, 1]) *)
Definition i16 := makepairF f15 f12.
Notation p30 := (BND r4 i16). (* BND(b * inv_b_hat, [2.32831e-10, 4.29497e+09]) *)
Definition i17 := makepairF f15 f1.
Notation p31 := (BND _inv_b_hat i17). (* BND(inv_b_hat, [2.32831e-10, 1]) *)
Notation p32 := (BND r6 i17). (* BND(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *)
Notation p33 := (BND r7 i6). (* BND(float<24,-149,ne>(b), [1, 4.29497e+09]) *)
Lemma t9 : p10 -> p33.
 intros h0.
 refine (float_round_ne _ _ _b i6 i6 h0 _) ; finalize.
Qed.
Lemma l33 : s1 -> p33 (* BND(float<24,-149,ne>(b), [1, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l9 h0).
 apply t9. refine (subset _b i1 i6 h1 _) ; finalize.
Qed.
Lemma t10 : p21 -> p33 -> p32.
 intros h0 h1.
 refine (div_pp r3 r7 i12 i6 i17 h0 h1 _) ; finalize.
Qed.
Lemma l32 : s1 -> p32 (* BND(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l20 h0).
 assert (h2 := l33 h0).
 apply t10. exact h1. exact h2.
Qed.
Lemma t11 : p32 -> p31.
 intros h0.
 refine (float_round_ne _ _ r6 i17 i17 h0 _) ; finalize.
Qed.
Lemma l31 : s1 -> p31 (* BND(inv_b_hat, [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l32 h0).
 apply t11. exact h1.
Qed.
Lemma t12 : p10 -> p31 -> p30.
 intros h0 h1.
 refine (mul_pp _b _inv_b_hat i6 i17 i16 h0 h1 _) ; finalize.
Qed.
Lemma l30 : s1 -> p30 (* BND(b * inv_b_hat, [2.32831e-10, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l9 h0).
 assert (h2 := l31 h0).
 apply t12. refine (subset _b i1 i6 h1 _) ; finalize. exact h2.
Qed.
Lemma t13 : p21 -> p30 -> p29.
 intros h0 h1.
 refine (sub r3 r4 i12 i16 i15 h0 h1 _) ; finalize.
Qed.
Lemma l29 : s1 -> p29 (* BND(exact_alpha, [-4.29497e+09, 1]) *).
 intros h0.
 assert (h1 := l20 h0).
 assert (h2 := l30 h0).
 apply t13. exact h1. exact h2.
Qed.
Definition f21 := Float2 (0) (0).
Definition i18 := makepairF f21 f21.
Notation p34 := (REL _exact_alpha r8 i18). (* REL(exact_alpha, -1 * (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *)
Notation p35 := (_exact_alpha = r8). (* EQL(exact_alpha, -1 * (inv_b_hat - 1 / b) / (1 / b)) *)
Lemma t14 : p11 -> p35.
 intros h0.
 refine (b1 h0) ; finalize.
Qed.
Lemma l35 : s1 -> p35 (* EQL(exact_alpha, -1 * (inv_b_hat - 1 / b) / (1 / b)) *).
 intros h0.
 assert (h1 := l10 h0).
 apply t14. exact h1.
Qed.
Notation p36 := (REL r8 r8 i18). (* REL(-1 * (inv_b_hat - 1 / b) / (1 / b), -1 * (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *)
Lemma t15 : p36.
 refine (rel_refl r8 i18 _) ; finalize.
Qed.
Lemma l36 : s1 -> p36 (* REL(-1 * (inv_b_hat - 1 / b) / (1 / b), -1 * (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *).
 intros h0.
 apply t15.
Qed.
Lemma t16 : p35 -> p36 -> p34.
 intros h0 h1.
 refine (rel_rewrite_1 _exact_alpha r8 r8 i18 h0 h1) ; finalize.
Qed.
Lemma l34 : s1 -> p34 (* REL(exact_alpha, -1 * (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *).
 intros h0.
 assert (h1 := l35 h0).
 assert (h2 := l36 h0).
 apply t16. exact h1. exact h2.
Qed.
Lemma t17 : p29 -> p34 -> p28.
 intros h0 h1.
 refine (bnd_of_rel_bnd_o _exact_alpha r8 i15 i18 i15 h0 h1 _) ; finalize.
Qed.
Lemma l28 : s1 -> p28 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-4.29497e+09, 1]) *).
 intros h0.
 assert (h1 := l29 h0).
 assert (h2 := l34 h0).
 apply t17. exact h1. exact h2.
Qed.
Definition i19 := makepairF f19 f5.
Notation p37 := (BND r8 i19). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-256, 2.14748e+09]) *)
Definition f22 := Float2 (422212448288769) (-72).
Definition i20 := makepairF f17 f22.
Notation p38 := (BND r9 i20). (* BND(-1 * (inv_b_hat - 1 / b), [-8.9407e-08, 8.9407e-08]) *)
Definition f23 := Float2 (-1) (0).
Definition i21 := makepairF f23 f23.
Notation p39 := (BND r10 i21). (* BND(-1, [-1, -1]) *)
Lemma t18 : p21 -> p39.
 intros h0.
 refine (neg r3 i12 i21 h0 _) ; finalize.
Qed.
Lemma l39 : s1 -> p39 (* BND(-1, [-1, -1]) *).
 intros h0.
 assert (h1 := l20 h0).
 apply t18. exact h1.
Qed.
Definition f24 := Float2 (-422212448288769) (-72).
Definition f25 := Float2 (864691162814875649) (-83).
Definition i22 := makepairF f24 f25.
Notation p40 := (BND r11 i22). (* BND(inv_b_hat - 1 / b, [-8.9407e-08, 8.9407e-08]) *)
Notation r18 := ((_inv_b_hat - r6)%R).
Notation r17 := ((r18 + r13)%R).
Notation p41 := (BND r17 i22). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-8.9407e-08, 8.9407e-08]) *)
Definition f26 := Float2 (-1) (-25).
Definition f27 := Float2 (1) (-25).
Definition i23 := makepairF f26 f27.
Notation p42 := (BND r18 i23). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-2.98023e-08, 2.98023e-08]) *)
Notation p43 := (ABS r6 i17). (* ABS(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *)
Notation p44 := (ABS r7 i6). (* ABS(float<24,-149,ne>(b), [1, 4.29497e+09]) *)
Lemma t19 : p33 -> p44.
 intros h0.
 refine (abs_of_bnd_p r7 i6 i6 h0 _) ; finalize.
Qed.
Lemma l44 : s1 -> p44 (* ABS(float<24,-149,ne>(b), [1, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l33 h0).
 apply t19. exact h1.
Qed.
Lemma t20 : p20 -> p44 -> p43.
 intros h0 h1.
 refine (div_aa r3 r7 i12 i6 i17 h0 h1 _) ; finalize.
Qed.
Lemma l43 : s1 -> p43 (* ABS(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l19 h0).
 assert (h2 := l44 h0).
 apply t20. exact h1. exact h2.
Qed.
Lemma t21 : p43 -> p42.
 intros h0.
 refine (float_absolute_wide_ne _ _ r6 i17 i23 h0 _) ; finalize.
Qed.
Lemma l42 : s1 -> p42 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-2.98023e-08, 2.98023e-08]) *).
 intros h0.
 assert (h1 := l43 h0).
 apply t21. exact h1.
Qed.
Notation p45 := (BND r13 i4). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Notation p46 := (BND r12 i17). (* BND(1 / b, [2.32831e-10, 1]) *)
Lemma t22 : p21 -> p10 -> p46.
 intros h0 h1.
 refine (div_pp r3 _b i12 i6 i17 h0 h1 _) ; finalize.
Qed.
Lemma l46 : s1 -> p46 (* BND(1 / b, [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l20 h0).
 assert (h2 := l9 h0).
 apply t22. exact h1. refine (subset _b i1 i6 h2 _) ; finalize.
Qed.
Lemma t23 : p7 -> p46 -> p45.
 intros h0 h1.
 refine (error_of_rel_op r6 r12 i4 i17 i4 h0 h1 _) ; finalize.
Qed.
Lemma l45 : s1 -> p45 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l6 h0).
 assert (h2 := l46 h0).
 apply t23. exact h1. exact h2.
Qed.
Lemma t24 : p42 -> p45 -> p41.
 intros h0 h1.
 refine (add r18 r13 i23 i4 i22 h0 h1 _) ; finalize.
Qed.
Lemma l41 : s1 -> p41 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-8.9407e-08, 8.9407e-08]) *).
 intros h0.
 assert (h1 := l42 h0).
 assert (h2 := l45 h0).
 apply t24. exact h1. exact h2.
Qed.
Lemma t25 : p41 -> p40.
 intros h0.
 refine (sub_xals _ _ _ i22 h0) ; finalize.
Qed.
Lemma l40 : s1 -> p40 (* BND(inv_b_hat - 1 / b, [-8.9407e-08, 8.9407e-08]) *).
 intros h0.
 assert (h1 := l41 h0).
 apply t25. exact h1.
Qed.
Lemma t26 : p39 -> p40 -> p38.
 intros h0 h1.
 refine (mul_no r10 r11 i21 i22 i20 h0 h1 _) ; finalize.
Qed.
Lemma l38 : s1 -> p38 (* BND(-1 * (inv_b_hat - 1 / b), [-8.9407e-08, 8.9407e-08]) *).
 intros h0.
 assert (h1 := l39 h0).
 assert (h2 := l40 h0).
 apply t26. exact h1. exact h2.
Qed.
Definition i24 := makepairF f13 f1.
Notation p47 := (BND r12 i24). (* BND(1 / b, [4.65661e-10, 1]) *)
Definition i25 := makepairF f1 f5.
Notation p48 := (BND _b i25). (* BND(b, [1, 2.14748e+09]) *)
Lemma l49 : p3 -> s1 -> p3 (* BND(b, [-inf, 2.14748e+09]) *).
 intros h0 h1.
 assert (h2 := h0).
 exact (h2).
Qed.
Lemma l48 : p3 -> s1 -> p48 (* BND(b, [1, 2.14748e+09]) *).
 intros h0 h1.
 assert (h2 := l49 h0 h1).
 assert (h3 := l9 h1).
 apply intersect_hb with (1 := h2) (2 := h3). finalize.
Qed.
Lemma t27 : p21 -> p48 -> p47.
 intros h0 h1.
 refine (div_pp r3 _b i12 i25 i24 h0 h1 _) ; finalize.
Qed.
Lemma l47 : p3 -> s1 -> p47 (* BND(1 / b, [4.65661e-10, 1]) *).
 intros h0 h1.
 assert (h2 := l20 h1).
 assert (h3 := l48 h0 h1).
 apply t27. exact h2. exact h3.
Qed.
Definition f28 := Float2 (-1) (-23).
Definition i26 := makepairF f28 f1.
Notation p49 := (BND r9 i26). (* BND(-1 * (inv_b_hat - 1 / b), [-1.19209e-07, 1]) *)
Lemma t28 : p49 -> p47 -> p37.
 intros h0 h1.
 refine (div_op r9 r12 i26 i24 i19 h0 h1 _) ; finalize.
Qed.
Lemma l37 : p3 -> s1 -> p37 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-256, 2.14748e+09]) *).
 intros h0 h1.
 assert (h2 := l38 h1).
 assert (h3 := l47 h0 h1).
 apply t28. refine (subset r9 i20 i26 h2 _) ; finalize. exact h3.
Qed.
Lemma l27 : p3 -> s1 -> p27 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-256, 1]) *).
 intros h0 h1.
 assert (h2 := l28 h1).
 assert (h3 := l37 h0 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Notation p50 := (BND r16 i18). (* BND(exact_alpha - -1 * (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *)
Lemma t29 : p35 -> p50.
 intros h0.
 refine (sub_of_eql _exact_alpha r8 i18 h0 _) ; finalize.
Qed.
Lemma l50 : s1 -> p50 (* BND(exact_alpha - -1 * (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *).
 intros h0.
 assert (h1 := l35 h0).
 apply t29. exact h1.
Qed.
Lemma t30 : p27 -> p50 -> p26.
 intros h0 h1.
 refine (add r8 r16 i14 i18 i14 h0 h1 _) ; finalize.
Qed.
Lemma l26 : p3 -> s1 -> p26 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b) + (exact_alpha - -1 * (inv_b_hat - 1 / b) / (1 / b)), [-256, 1]) *).
 intros h0 h1.
 assert (h2 := l27 h0 h1).
 assert (h3 := l50 h1).
 apply t30. exact h2. exact h3.
Qed.
Lemma t31 : p26 -> p25.
 intros h0.
 refine (val_xabs _ _exact_alpha i14 h0) ; finalize.
Qed.
Lemma l25 : p3 -> s1 -> p25 (* BND(exact_alpha, [-256, 1]) *).
 intros h0 h1.
 assert (h2 := l26 h0 h1).
 apply t31. exact h2.
Qed.
Lemma l23 : p5 -> p4 -> p3 -> s1 -> p24 (* BND(exact_alpha, [-256, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l24 h0 h1 h2 h3).
 assert (h5 := l25 h2 h3).
 apply intersect_hb with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t32 : p24 -> p34 -> p23.
 intros h0 h1.
 refine (bnd_of_rel_bnd_n _exact_alpha r8 i13 i18 i13 h0 h1 _) ; finalize.
Qed.
Lemma l22 : p5 -> p4 -> p3 -> s1 -> p23 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-256, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l23 h0 h1 h2 h3).
 assert (h5 := l34 h3).
 apply t32. exact h4. exact h5.
Qed.
Lemma t33 : p23 -> p47 -> p22.
 intros h0 h1.
 refine (mul_np r8 r12 i13 i24 i11 h0 h1 _) ; finalize.
Qed.
Lemma l21 : p5 -> p4 -> p3 -> s1 -> p22 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b) * (1 / b), [-256, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t33. exact h4. exact h5.
Qed.
Lemma t34 : p18 -> p22 -> p17.
 intros h0 h1.
 refine (div_xilu r9 _ i11 h0 h1) ; finalize.
Qed.
Lemma l16 : p5 -> p4 -> p3 -> s1 -> p17 (* BND(-1 * (inv_b_hat - 1 / b), [-256, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l21 h0 h1 h2 h3).
 apply t34. exact h4. exact h5.
Qed.
Lemma l15 : p5 -> p4 -> p3 -> s1 -> p16 (* BND(-1 * (inv_b_hat - 1 / b), [-8.9407e-08, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l16 h0 h1 h2 h3).
 assert (h5 := l38 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition i27 := makepairF f23 f18.
Notation p51 := (BND r9 i27). (* BND(-1 * (inv_b_hat - 1 / b), [-1, -1.11022e-16]) *)
Lemma t35 : p51 -> p15.
 intros h0.
 refine (abs_of_bnd_n r9 i27 i9 h0 _) ; finalize.
Qed.
Lemma l14 : p5 -> p4 -> p3 -> s1 -> p15 (* ABS(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l15 h0 h1 h2 h3).
 apply t35. refine (subset r9 i10 i27 h4 _) ; finalize.
Qed.
Lemma t36 : p15 -> p14.
 intros h0.
 refine (nzr_of_abs r9 i9 h0 _) ; finalize.
Qed.
Lemma l13 : p5 -> p4 -> p3 -> s1 -> p14 (* NZR(-1 * (inv_b_hat - 1 / b)) *).
 intros h0 h1 h2 h3.
 assert (h4 := l14 h0 h1 h2 h3).
 apply t36. exact h4.
Qed.
Notation r19 := ((r9 / r8)%R).
Notation p52 := (BND r19 i8). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.32831e-10, 5.82077e-10]) *)
Definition f29 := Float2 (-720576928222549575) (-112).
Definition i28 := makepairF f29 f18.
Notation p53 := (BND r9 i28). (* BND(-1 * (inv_b_hat - 1 / b), [-1.38778e-16, -1.11022e-16]) *)
Definition f30 := Float2 (720576928222549575) (-112).
Definition i29 := makepairF f16 f30.
Notation p54 := (BND r11 i29). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.38778e-16]) *)
Definition f31 := Float2 (1) (-56).
Definition i30 := makepairF f31 f30.
Notation p55 := (BND r11 i30). (* BND(inv_b_hat - 1 / b, [1.38778e-17, 1.38778e-16]) *)
Definition f32 := Float2 (288230393331581953) (-81).
Definition i31 := makepairF f27 f32.
Notation p56 := (REL _inv_b_hat r12 i31). (* REL(inv_b_hat, 1 / b, [2.98023e-08, 1.19209e-07]) *)
Notation r20 := ((r11 / r12)%R).
Notation p57 := (BND r20 i31). (* BND((inv_b_hat - 1 / b) / (1 / b), [2.98023e-08, 1.19209e-07]) *)
Definition f33 := Float2 (-281474959933441) (-71).
Definition i32 := makepairF f33 f32.
Notation p58 := (BND r20 i32). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, 1.19209e-07]) *)
Notation p59 := (REL _inv_b_hat r12 i32). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, 1.19209e-07]) *)
Notation p60 := (REL _inv_b_hat r6 i5). (* REL(inv_b_hat, 1 / float<24,-149,ne>(b), [-5.96046e-08, 5.96046e-08]) *)
Lemma t37 : p43 -> p60.
 intros h0.
 refine (float_relative_ne _ _ r6 i17 i5 h0 _) ; finalize.
Qed.
Lemma l59 : s1 -> p60 (* REL(inv_b_hat, 1 / float<24,-149,ne>(b), [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l43 h0).
 apply t37. exact h1.
Qed.
Definition f34 := Float2 (288230393331581953) (-82).
Definition i33 := makepairF f8 f34.
Notation p61 := (REL r6 r12 i33). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Lemma t38 : p60 -> p61 -> p59.
 intros h0 h1.
 refine (compose _inv_b_hat r6 r12 i5 i33 i32 h0 h1 _) ; finalize.
Qed.
Lemma l58 : s1 -> p59 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l59 h0).
 assert (h2 := l6 h0).
 apply t38. exact h1. refine (rel_subset r6 r12 i4 i33 h2 _) ; finalize.
Qed.
Lemma t39 : p18 -> p59 -> p58.
 intros h0 h1.
 refine (bnd_of_nzr_rel _inv_b_hat r12 i32 h0 h1) ; finalize.
Qed.
Lemma l57 : s1 -> p58 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l17 h0).
 assert (h2 := l58 h0).
 apply t39. exact h1. exact h2.
Qed.
Definition i34 := makepairF f27 f5.
Notation p62 := (BND r20 i34). (* BND((inv_b_hat - 1 / b) / (1 / b), [2.98023e-08, 2.14748e+09]) *)
Notation p63 := (BND r11 i9). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1]) *)
Notation p64 := (NZR r10). (* NZR(-1) *)
Lemma t40 : p19 -> p64.
 intros h0.
 refine (neg_nzr r3 h0) ; finalize.
Qed.
Lemma l62 : s1 -> p64 (* NZR(-1) *).
 intros h0.
 assert (h1 := l18 h0).
 apply t40. exact h1.
Qed.
Notation r21 := ((r9 / r10)%R).
Notation p65 := (BND r21 i9). (* BND(-1 * (inv_b_hat - 1 / b) / -1, [1.11022e-16, 1]) *)
Lemma t41 : p51 -> p39 -> p65.
 intros h0 h1.
 refine (div_nn r9 r10 i27 i21 i9 h0 h1 _) ; finalize.
Qed.
Lemma l63 : p5 -> p4 -> p3 -> s1 -> p65 (* BND(-1 * (inv_b_hat - 1 / b) / -1, [1.11022e-16, 1]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l15 h0 h1 h2 h3).
 assert (h5 := l39 h3).
 apply t41. refine (subset r9 i10 i27 h4 _) ; finalize. exact h5.
Qed.
Lemma t42 : p64 -> p65 -> p63.
 intros h0 h1.
 refine (mul_xiru _ r11 i9 h0 h1) ; finalize.
Qed.
Lemma l61 : p5 -> p4 -> p3 -> s1 -> p63 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l62 h3).
 assert (h5 := l63 h0 h1 h2 h3).
 apply t42. exact h4. exact h5.
Qed.
Definition f35 := Float2 (720576842323088449) (-88).
Definition i35 := makepairF f13 f35.
Notation p66 := (BND r12 i35). (* BND(1 / b, [4.65661e-10, 2.32831e-09]) *)
Definition f36 := Float2 (1) (-34).
Definition i36 := makepairF f36 f35.
Notation p67 := (BND r12 i36). (* BND(1 / b, [5.82077e-11, 2.32831e-09]) *)
Notation p68 := (BND r19 i36). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [5.82077e-11, 2.32831e-09]) *)
Definition f37 := Float2 (-720576842323088449) (-110).
Definition i37 := makepairF f37 f18.
Notation p69 := (BND r9 i37). (* BND(-1 * (inv_b_hat - 1 / b), [-5.55112e-16, -1.11022e-16]) *)
Definition f38 := Float2 (720576842323088449) (-110).
Definition i38 := makepairF f16 f38.
Notation p70 := (BND r11 i38). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 5.55112e-16]) *)
Definition f39 := Float2 (1) (-58).
Definition i39 := makepairF f39 f38.
Notation p71 := (BND r11 i39). (* BND(inv_b_hat - 1 / b, [3.46945e-18, 5.55112e-16]) *)
Definition f40 := Float2 (1) (-27).
Definition i40 := makepairF f40 f32.
Notation p72 := (REL _inv_b_hat r12 i40). (* REL(inv_b_hat, 1 / b, [7.45058e-09, 1.19209e-07]) *)
Notation p73 := (BND r20 i40). (* BND((inv_b_hat - 1 / b) / (1 / b), [7.45058e-09, 1.19209e-07]) *)
Definition i41 := makepairF f40 f5.
Notation p74 := (BND r20 i41). (* BND((inv_b_hat - 1 / b) / (1 / b), [7.45058e-09, 2.14748e+09]) *)
Definition f41 := Float2 (720576756423637563) (-86).
Definition i42 := makepairF f13 f41.
Notation p75 := (BND r12 i42). (* BND(1 / b, [4.65661e-10, 9.31324e-09]) *)
Definition f42 := Float2 (1) (-36).
Definition i43 := makepairF f42 f41.
Notation p76 := (BND r12 i43). (* BND(1 / b, [1.45519e-11, 9.31324e-09]) *)
Notation p77 := (BND r19 i43). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.45519e-11, 9.31324e-09]) *)
Definition f43 := Float2 (-720576756423637563) (-108).
Definition i44 := makepairF f43 f18.
Notation p78 := (BND r9 i44). (* BND(-1 * (inv_b_hat - 1 / b), [-2.22045e-15, -1.11022e-16]) *)
Definition f44 := Float2 (720576756423637563) (-108).
Definition i45 := makepairF f16 f44.
Notation p79 := (BND r11 i45). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.22045e-15]) *)
Definition f45 := Float2 (1) (-60).
Definition i46 := makepairF f45 f44.
Notation p80 := (BND r11 i46). (* BND(inv_b_hat - 1 / b, [8.67362e-19, 2.22045e-15]) *)
Definition f46 := Float2 (1) (-29).
Definition i47 := makepairF f46 f32.
Notation p81 := (REL _inv_b_hat r12 i47). (* REL(inv_b_hat, 1 / b, [1.86265e-09, 1.19209e-07]) *)
Notation p82 := (BND r20 i47). (* BND((inv_b_hat - 1 / b) / (1 / b), [1.86265e-09, 1.19209e-07]) *)
Definition i48 := makepairF f46 f5.
Notation p83 := (BND r20 i48). (* BND((inv_b_hat - 1 / b) / (1 / b), [1.86265e-09, 2.14748e+09]) *)
Definition f47 := Float2 (720576670524196917) (-84).
Definition i49 := makepairF f13 f47.
Notation p84 := (BND r12 i49). (* BND(1 / b, [4.65661e-10, 3.72529e-08]) *)
Definition f48 := Float2 (1) (-38).
Definition i50 := makepairF f48 f47.
Notation p85 := (BND r12 i50). (* BND(1 / b, [3.63798e-12, 3.72529e-08]) *)
Notation p86 := (BND r19 i50). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [3.63798e-12, 3.72529e-08]) *)
Definition f49 := Float2 (-720576670524196917) (-106).
Definition i51 := makepairF f49 f18.
Notation p87 := (BND r9 i51). (* BND(-1 * (inv_b_hat - 1 / b), [-8.88179e-15, -1.11022e-16]) *)
Definition f50 := Float2 (720576670524196917) (-106).
Definition i52 := makepairF f16 f50.
Notation p88 := (BND r11 i52). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 8.88179e-15]) *)
Definition f51 := Float2 (1) (-62).
Definition i53 := makepairF f51 f50.
Notation p89 := (BND r11 i53). (* BND(inv_b_hat - 1 / b, [2.1684e-19, 8.88179e-15]) *)
Definition i54 := makepairF f13 f32.
Notation p90 := (REL _inv_b_hat r12 i54). (* REL(inv_b_hat, 1 / b, [4.65661e-10, 1.19209e-07]) *)
Notation p91 := (BND r20 i54). (* BND((inv_b_hat - 1 / b) / (1 / b), [4.65661e-10, 1.19209e-07]) *)
Definition i55 := makepairF f13 f5.
Notation p92 := (BND r20 i55). (* BND((inv_b_hat - 1 / b) / (1 / b), [4.65661e-10, 2.14748e+09]) *)
Definition f52 := Float2 (720576584624766511) (-82).
Definition i56 := makepairF f13 f52.
Notation p93 := (BND r12 i56). (* BND(1 / b, [4.65661e-10, 1.49012e-07]) *)
Definition f53 := Float2 (1) (-40).
Definition i57 := makepairF f53 f52.
Notation p94 := (BND r12 i57). (* BND(1 / b, [9.09495e-13, 1.49012e-07]) *)
Notation p95 := (BND r19 i57). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [9.09495e-13, 1.49012e-07]) *)
Definition f54 := Float2 (-720576584624766511) (-104).
Definition i58 := makepairF f54 f18.
Notation p96 := (BND r9 i58). (* BND(-1 * (inv_b_hat - 1 / b), [-3.55272e-14, -1.11022e-16]) *)
Definition f55 := Float2 (720576584624766511) (-104).
Definition i59 := makepairF f16 f55.
Notation p97 := (BND r11 i59). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 3.55272e-14]) *)
Definition f56 := Float2 (1) (-64).
Definition i60 := makepairF f56 f55.
Notation p98 := (BND r11 i60). (* BND(inv_b_hat - 1 / b, [5.42101e-20, 3.55272e-14]) *)
Definition f57 := Float2 (1) (-33).
Definition i61 := makepairF f57 f32.
Notation p99 := (REL _inv_b_hat r12 i61). (* REL(inv_b_hat, 1 / b, [1.16415e-10, 1.19209e-07]) *)
Notation p100 := (BND r20 i61). (* BND((inv_b_hat - 1 / b) / (1 / b), [1.16415e-10, 1.19209e-07]) *)
Definition i62 := makepairF f57 f5.
Notation p101 := (BND r20 i62). (* BND((inv_b_hat - 1 / b) / (1 / b), [1.16415e-10, 2.14748e+09]) *)
Definition f58 := Float2 (720576498725346345) (-80).
Definition i63 := makepairF f13 f58.
Notation p102 := (BND r12 i63). (* BND(1 / b, [4.65661e-10, 5.96047e-07]) *)
Definition f59 := Float2 (1) (-42).
Definition i64 := makepairF f59 f58.
Notation p103 := (BND r12 i64). (* BND(1 / b, [2.27374e-13, 5.96047e-07]) *)
Notation p104 := (BND r19 i64). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.27374e-13, 5.96047e-07]) *)
Definition f60 := Float2 (-720576498725346345) (-102).
Definition i65 := makepairF f60 f18.
Notation p105 := (BND r9 i65). (* BND(-1 * (inv_b_hat - 1 / b), [-1.42109e-13, -1.11022e-16]) *)
Definition f61 := Float2 (720576498725346345) (-102).
Definition i66 := makepairF f16 f61.
Notation p106 := (BND r11 i66). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.42109e-13]) *)
Definition f62 := Float2 (1) (-66).
Definition i67 := makepairF f62 f61.
Notation p107 := (BND r11 i67). (* BND(inv_b_hat - 1 / b, [1.35525e-20, 1.42109e-13]) *)
Definition f63 := Float2 (1) (-35).
Definition i68 := makepairF f63 f32.
Notation p108 := (REL _inv_b_hat r12 i68). (* REL(inv_b_hat, 1 / b, [2.91038e-11, 1.19209e-07]) *)
Notation p109 := (BND r20 i68). (* BND((inv_b_hat - 1 / b) / (1 / b), [2.91038e-11, 1.19209e-07]) *)
Definition i69 := makepairF f63 f5.
Notation p110 := (BND r20 i69). (* BND((inv_b_hat - 1 / b) / (1 / b), [2.91038e-11, 2.14748e+09]) *)
Definition f64 := Float2 (720576412825936419) (-78).
Definition i70 := makepairF f13 f64.
Notation p111 := (BND r12 i70). (* BND(1 / b, [4.65661e-10, 2.38419e-06]) *)
Definition f65 := Float2 (1) (-44).
Definition i71 := makepairF f65 f64.
Notation p112 := (BND r12 i71). (* BND(1 / b, [5.68434e-14, 2.38419e-06]) *)
Notation p113 := (BND r19 i71). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [5.68434e-14, 2.38419e-06]) *)
Definition f66 := Float2 (-720576412825936419) (-100).
Definition i72 := makepairF f66 f18.
Notation p114 := (BND r9 i72). (* BND(-1 * (inv_b_hat - 1 / b), [-5.68435e-13, -1.11022e-16]) *)
Definition f67 := Float2 (720576412825936419) (-100).
Definition i73 := makepairF f16 f67.
Notation p115 := (BND r11 i73). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 5.68435e-13]) *)
Definition f68 := Float2 (1) (-68).
Definition i74 := makepairF f68 f67.
Notation p116 := (BND r11 i74). (* BND(inv_b_hat - 1 / b, [3.38813e-21, 5.68435e-13]) *)
Definition f69 := Float2 (1) (-37).
Definition i75 := makepairF f69 f32.
Notation p117 := (REL _inv_b_hat r12 i75). (* REL(inv_b_hat, 1 / b, [7.27596e-12, 1.19209e-07]) *)
Notation p118 := (BND r20 i75). (* BND((inv_b_hat - 1 / b) / (1 / b), [7.27596e-12, 1.19209e-07]) *)
Definition i76 := makepairF f69 f5.
Notation p119 := (BND r20 i76). (* BND((inv_b_hat - 1 / b) / (1 / b), [7.27596e-12, 2.14748e+09]) *)
Definition f70 := Float2 (432345967954694175) (-75).
Definition i77 := makepairF f13 f70.
Notation p120 := (BND r12 i77). (* BND(1 / b, [4.65661e-10, 1.14441e-05]) *)
Definition f71 := Float2 (1) (-47).
Definition i78 := makepairF f71 f70.
Notation p121 := (BND r12 i78). (* BND(1 / b, [7.10543e-15, 1.14441e-05]) *)
Notation p122 := (BND r19 i78). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.14441e-05]) *)
Definition f72 := Float2 (-432345967954694175) (-97).
Definition i79 := makepairF f72 f18.
Notation p123 := (BND r9 i79). (* BND(-1 * (inv_b_hat - 1 / b), [-2.72849e-12, -1.11022e-16]) *)
Definition f73 := Float2 (432345967954694175) (-97).
Definition i80 := makepairF f16 f73.
Notation p124 := (BND r11 i80). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.72849e-12]) *)
Definition f74 := Float2 (-432345942184866333) (-74).
Definition i81 := makepairF f74 f73.
Notation p125 := (BND r11 i81). (* BND(inv_b_hat - 1 / b, [-2.28882e-05, 2.72849e-12]) *)
Definition f75 := Float2 (864691884369732665) (-75).
Definition i82 := makepairF f13 f75.
Notation p126 := (BND r12 i82). (* BND(1 / b, [4.65661e-10, 2.28882e-05]) *)
Definition f76 := Float2 (1) (-61).
Definition i83 := makepairF f76 f75.
Notation p127 := (BND r12 i83). (* BND(1 / b, [4.33681e-19, 2.28882e-05]) *)
Notation p128 := (BND r19 i83). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 2.28882e-05]) *)
Definition f77 := Float2 (-864691884369732665) (-97).
Definition i84 := makepairF f77 f18.
Notation p129 := (BND r9 i84). (* BND(-1 * (inv_b_hat - 1 / b), [-5.45697e-12, -1.11022e-16]) *)
Definition f78 := Float2 (864691884369732665) (-97).
Definition i85 := makepairF f16 f78.
Notation p130 := (BND r11 i85). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 5.45697e-12]) *)
Definition f79 := Float2 (-864691832830080053) (-74).
Definition i86 := makepairF f79 f78.
Notation p131 := (BND r11 i86). (* BND(inv_b_hat - 1 / b, [-4.57764e-05, 5.45697e-12]) *)
Definition f80 := Float2 (864691832830080053) (-74).
Definition i87 := makepairF f13 f80.
Notation p132 := (BND r12 i87). (* BND(1 / b, [4.65661e-10, 4.57764e-05]) *)
Definition i88 := makepairF f76 f80.
Notation p133 := (BND r12 i88). (* BND(1 / b, [4.33681e-19, 4.57764e-05]) *)
Notation p134 := (BND r19 i88). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 4.57764e-05]) *)
Definition f81 := Float2 (-864691832830080053) (-96).
Definition i89 := makepairF f81 f18.
Notation p135 := (BND r9 i89). (* BND(-1 * (inv_b_hat - 1 / b), [-1.09139e-11, -1.11022e-16]) *)
Definition f82 := Float2 (864691832830080053) (-96).
Definition i90 := makepairF f16 f82.
Notation p136 := (BND r11 i90). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.09139e-11]) *)
Definition f83 := Float2 (-864691781290430513) (-73).
Definition i91 := makepairF f83 f82.
Notation p137 := (BND r11 i91). (* BND(inv_b_hat - 1 / b, [-9.15528e-05, 1.09139e-11]) *)
Definition f84 := Float2 (864691781290430513) (-73).
Definition i92 := makepairF f13 f84.
Notation p138 := (BND r12 i92). (* BND(1 / b, [4.65661e-10, 9.15528e-05]) *)
Definition i93 := makepairF f76 f84.
Notation p139 := (BND r12 i93). (* BND(1 / b, [4.33681e-19, 9.15528e-05]) *)
Notation p140 := (BND r19 i93). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 9.15528e-05]) *)
Definition f85 := Float2 (-864691781290430513) (-95).
Definition i94 := makepairF f85 f18.
Notation p141 := (BND r9 i94). (* BND(-1 * (inv_b_hat - 1 / b), [-2.18279e-11, -1.11022e-16]) *)
Definition f86 := Float2 (864691781290430513) (-95).
Definition i95 := makepairF f16 f86.
Notation p142 := (BND r11 i95). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.18279e-11]) *)
Definition f87 := Float2 (-864691729750784045) (-72).
Definition i96 := makepairF f87 f86.
Notation p143 := (BND r11 i96). (* BND(inv_b_hat - 1 / b, [-0.000183106, 2.18279e-11]) *)
Definition f88 := Float2 (864691729750784045) (-72).
Definition i97 := makepairF f13 f88.
Notation p144 := (BND r12 i97). (* BND(1 / b, [4.65661e-10, 0.000183106]) *)
Definition i98 := makepairF f76 f88.
Notation p145 := (BND r12 i98). (* BND(1 / b, [4.33681e-19, 0.000183106]) *)
Notation p146 := (BND r19 i98). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.000183106]) *)
Definition f89 := Float2 (-864691729750784045) (-94).
Definition i99 := makepairF f89 f18.
Notation p147 := (BND r9 i99). (* BND(-1 * (inv_b_hat - 1 / b), [-4.36558e-11, -1.11022e-16]) *)
Definition f90 := Float2 (864691729750784045) (-94).
Definition i100 := makepairF f16 f90.
Notation p148 := (BND r11 i100). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 4.36558e-11]) *)
Definition f91 := Float2 (-864691678211140649) (-71).
Definition i101 := makepairF f91 f90.
Notation p149 := (BND r11 i101). (* BND(inv_b_hat - 1 / b, [-0.000366211, 4.36558e-11]) *)
Definition f92 := Float2 (864691678211140649) (-71).
Definition i102 := makepairF f13 f92.
Notation p150 := (BND r12 i102). (* BND(1 / b, [4.65661e-10, 0.000366211]) *)
Definition i103 := makepairF f76 f92.
Notation p151 := (BND r12 i103). (* BND(1 / b, [4.33681e-19, 0.000366211]) *)
Notation p152 := (BND r19 i103). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.000366211]) *)
Definition f93 := Float2 (-864691678211140649) (-93).
Definition i104 := makepairF f93 f18.
Notation p153 := (BND r9 i104). (* BND(-1 * (inv_b_hat - 1 / b), [-8.73115e-11, -1.11022e-16]) *)
Definition f94 := Float2 (864691678211140649) (-93).
Definition i105 := makepairF f16 f94.
Notation p154 := (BND r11 i105). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 8.73115e-11]) *)
Definition f95 := Float2 (-864691626671500325) (-70).
Definition i106 := makepairF f95 f94.
Notation p155 := (BND r11 i106). (* BND(inv_b_hat - 1 / b, [-0.000732422, 8.73115e-11]) *)
Definition f96 := Float2 (864691626671500325) (-70).
Definition i107 := makepairF f13 f96.
Notation p156 := (BND r12 i107). (* BND(1 / b, [4.65661e-10, 0.000732422]) *)
Definition i108 := makepairF f76 f96.
Notation p157 := (BND r12 i108). (* BND(1 / b, [4.33681e-19, 0.000732422]) *)
Notation p158 := (BND r19 i108). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.000732422]) *)
Definition f97 := Float2 (-864691626671500325) (-92).
Definition i109 := makepairF f97 f18.
Notation p159 := (BND r9 i109). (* BND(-1 * (inv_b_hat - 1 / b), [-1.74623e-10, -1.11022e-16]) *)
Definition f98 := Float2 (864691626671500325) (-92).
Definition i110 := makepairF f16 f98.
Notation p160 := (BND r11 i110). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.74623e-10]) *)
Definition f99 := Float2 (-864691575131863073) (-69).
Definition i111 := makepairF f99 f98.
Notation p161 := (BND r11 i111). (* BND(inv_b_hat - 1 / b, [-0.00146484, 1.74623e-10]) *)
Definition f100 := Float2 (864691575131863073) (-69).
Definition i112 := makepairF f13 f100.
Notation p162 := (BND r12 i112). (* BND(1 / b, [4.65661e-10, 0.00146484]) *)
Definition i113 := makepairF f76 f100.
Notation p163 := (BND r12 i113). (* BND(1 / b, [4.33681e-19, 0.00146484]) *)
Notation p164 := (BND r19 i113). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.00146484]) *)
Definition f101 := Float2 (-864691575131863073) (-91).
Definition i114 := makepairF f101 f18.
Notation p165 := (BND r9 i114). (* BND(-1 * (inv_b_hat - 1 / b), [-3.49246e-10, -1.11022e-16]) *)
Definition f102 := Float2 (864691575131863073) (-91).
Definition i115 := makepairF f16 f102.
Notation p166 := (BND r11 i115). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 3.49246e-10]) *)
Definition f103 := Float2 (-864691523592228893) (-68).
Definition i116 := makepairF f103 f102.
Notation p167 := (BND r11 i116). (* BND(inv_b_hat - 1 / b, [-0.00292969, 3.49246e-10]) *)
Definition f104 := Float2 (864691523592228893) (-68).
Definition i117 := makepairF f13 f104.
Notation p168 := (BND r12 i117). (* BND(1 / b, [4.65661e-10, 0.00292969]) *)
Definition i118 := makepairF f76 f104.
Notation p169 := (BND r12 i118). (* BND(1 / b, [4.33681e-19, 0.00292969]) *)
Notation p170 := (BND r19 i118). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.00292969]) *)
Definition f105 := Float2 (-864691523592228893) (-90).
Definition i119 := makepairF f105 f18.
Notation p171 := (BND r9 i119). (* BND(-1 * (inv_b_hat - 1 / b), [-6.98492e-10, -1.11022e-16]) *)
Definition f106 := Float2 (864691523592228893) (-90).
Definition i120 := makepairF f16 f106.
Notation p172 := (BND r11 i120). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 6.98492e-10]) *)
Definition f107 := Float2 (-864691472052597785) (-67).
Definition i121 := makepairF f107 f106.
Notation p173 := (BND r11 i121). (* BND(inv_b_hat - 1 / b, [-0.00585938, 6.98492e-10]) *)
Definition f108 := Float2 (864691472052597785) (-67).
Definition i122 := makepairF f13 f108.
Notation p174 := (BND r12 i122). (* BND(1 / b, [4.65661e-10, 0.00585938]) *)
Definition i123 := makepairF f76 f108.
Notation p175 := (BND r12 i123). (* BND(1 / b, [4.33681e-19, 0.00585938]) *)
Notation p176 := (BND r19 i123). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.00585938]) *)
Definition f109 := Float2 (-864691472052597785) (-89).
Definition i124 := makepairF f109 f18.
Notation p177 := (BND r9 i124). (* BND(-1 * (inv_b_hat - 1 / b), [-1.39698e-09, -1.11022e-16]) *)
Definition f110 := Float2 (864691472052597785) (-89).
Definition i125 := makepairF f16 f110.
Notation p178 := (BND r11 i125). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.39698e-09]) *)
Definition f111 := Float2 (-864691420512969749) (-66).
Definition i126 := makepairF f111 f110.
Notation p179 := (BND r11 i126). (* BND(inv_b_hat - 1 / b, [-0.0117188, 1.39698e-09]) *)
Definition f112 := Float2 (864691420512969749) (-66).
Definition i127 := makepairF f13 f112.
Notation p180 := (BND r12 i127). (* BND(1 / b, [4.65661e-10, 0.0117188]) *)
Definition i128 := makepairF f76 f112.
Notation p181 := (BND r12 i128). (* BND(1 / b, [4.33681e-19, 0.0117188]) *)
Notation p182 := (BND r19 i128). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.0117188]) *)
Definition f113 := Float2 (-864691420512969749) (-88).
Definition i129 := makepairF f113 f18.
Notation p183 := (BND r9 i129). (* BND(-1 * (inv_b_hat - 1 / b), [-2.79397e-09, -1.11022e-16]) *)
Definition f114 := Float2 (864691420512969749) (-88).
Definition i130 := makepairF f16 f114.
Notation p184 := (BND r11 i130). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.79397e-09]) *)
Definition f115 := Float2 (-864691368973344785) (-65).
Definition i131 := makepairF f115 f114.
Notation p185 := (BND r11 i131). (* BND(inv_b_hat - 1 / b, [-0.0234375, 2.79397e-09]) *)
Definition f116 := Float2 (864691368973344785) (-65).
Definition i132 := makepairF f13 f116.
Notation p186 := (BND r12 i132). (* BND(1 / b, [4.65661e-10, 0.0234375]) *)
Definition i133 := makepairF f76 f116.
Notation p187 := (BND r12 i133). (* BND(1 / b, [4.33681e-19, 0.0234375]) *)
Notation p188 := (BND r19 i133). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.0234375]) *)
Definition f117 := Float2 (-864691368973344785) (-87).
Definition i134 := makepairF f117 f18.
Notation p189 := (BND r9 i134). (* BND(-1 * (inv_b_hat - 1 / b), [-5.58794e-09, -1.11022e-16]) *)
Definition f118 := Float2 (864691368973344785) (-87).
Definition i135 := makepairF f16 f118.
Notation p190 := (BND r11 i135). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 5.58794e-09]) *)
Definition f119 := Float2 (-864691317433722893) (-64).
Definition i136 := makepairF f119 f118.
Notation p191 := (BND r11 i136). (* BND(inv_b_hat - 1 / b, [-0.046875, 5.58794e-09]) *)
Definition f120 := Float2 (864691317433722893) (-64).
Definition i137 := makepairF f13 f120.
Notation p192 := (BND r12 i137). (* BND(1 / b, [4.65661e-10, 0.046875]) *)
Definition i138 := makepairF f76 f120.
Notation p193 := (BND r12 i138). (* BND(1 / b, [4.33681e-19, 0.046875]) *)
Notation p194 := (BND r19 i138). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.046875]) *)
Definition f121 := Float2 (-864691317433722893) (-86).
Definition i139 := makepairF f121 f18.
Notation p195 := (BND r9 i139). (* BND(-1 * (inv_b_hat - 1 / b), [-1.11759e-08, -1.11022e-16]) *)
Definition f122 := Float2 (864691317433722893) (-86).
Definition i140 := makepairF f16 f122.
Notation p196 := (BND r11 i140). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.11759e-08]) *)
Definition f123 := Float2 (-864691265894104073) (-63).
Definition i141 := makepairF f123 f122.
Notation p197 := (BND r11 i141). (* BND(inv_b_hat - 1 / b, [-0.09375, 1.11759e-08]) *)
Definition f124 := Float2 (864691265894104073) (-63).
Definition i142 := makepairF f13 f124.
Notation p198 := (BND r12 i142). (* BND(1 / b, [4.65661e-10, 0.09375]) *)
Definition i143 := makepairF f76 f124.
Notation p199 := (BND r12 i143). (* BND(1 / b, [4.33681e-19, 0.09375]) *)
Notation p200 := (BND r19 i143). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.09375]) *)
Definition f125 := Float2 (-864691265894104073) (-85).
Definition i144 := makepairF f125 f18.
Notation p201 := (BND r9 i144). (* BND(-1 * (inv_b_hat - 1 / b), [-2.23517e-08, -1.11022e-16]) *)
Definition f126 := Float2 (864691265894104073) (-85).
Definition i145 := makepairF f16 f126.
Notation p202 := (BND r11 i145). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.23517e-08]) *)
Definition f127 := Float2 (-864691214354488325) (-62).
Definition i146 := makepairF f127 f126.
Notation p203 := (BND r11 i146). (* BND(inv_b_hat - 1 / b, [-0.1875, 2.23517e-08]) *)
Definition f128 := Float2 (864691214354488325) (-62).
Definition i147 := makepairF f13 f128.
Notation p204 := (BND r12 i147). (* BND(1 / b, [4.65661e-10, 0.1875]) *)
Definition i148 := makepairF f76 f128.
Notation p205 := (BND r12 i148). (* BND(1 / b, [4.33681e-19, 0.1875]) *)
Notation p206 := (BND r19 i148). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.1875]) *)
Definition f129 := Float2 (-864691214354488325) (-84).
Definition i149 := makepairF f129 f18.
Notation p207 := (BND r9 i149). (* BND(-1 * (inv_b_hat - 1 / b), [-4.47035e-08, -1.11022e-16]) *)
Definition f130 := Float2 (864691214354488325) (-84).
Definition i150 := makepairF f16 f130.
Notation p208 := (BND r11 i150). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 4.47035e-08]) *)
Definition f131 := Float2 (-864691162814875649) (-61).
Definition i151 := makepairF f131 f130.
Notation p209 := (BND r11 i151). (* BND(inv_b_hat - 1 / b, [-0.375, 4.47035e-08]) *)
Definition f132 := Float2 (864691162814875649) (-61).
Definition i152 := makepairF f13 f132.
Notation p210 := (BND r12 i152). (* BND(1 / b, [4.65661e-10, 0.375]) *)
Definition i153 := makepairF f76 f132.
Notation p211 := (BND r12 i153). (* BND(1 / b, [4.33681e-19, 0.375]) *)
Notation p212 := (BND r19 i153). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.375]) *)
Lemma t43 : p16 -> p23 -> p212.
 intros h0 h1.
 refine (div_nn r9 r8 i10 i13 i153 h0 h1 _) ; finalize.
Qed.
Lemma l210 : p5 -> p4 -> p3 -> s1 -> p212 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.375]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l15 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t43. exact h4. exact h5.
Qed.
Lemma t44 : p14 -> p18 -> p212 -> p211.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i153 h0 h1 h2) ; finalize.
Qed.
Lemma l209 : p5 -> p4 -> p3 -> s1 -> p211 (* BND(1 / b, [4.33681e-19, 0.375]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l210 h0 h1 h2 h3).
 apply t44. exact h4. exact h5. exact h6.
Qed.
Lemma l208 : p5 -> p4 -> p3 -> s1 -> p210 (* BND(1 / b, [4.65661e-10, 0.375]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l209 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition i154 := makepairF f23 f32.
Notation p213 := (REL _inv_b_hat r12 i154). (* REL(inv_b_hat, 1 / b, [-1, 1.19209e-07]) *)
Lemma t45 : p213 -> p210 -> p209.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i152 i151 h0 h1 _) ; finalize.
Qed.
Lemma l207 : p5 -> p4 -> p3 -> s1 -> p209 (* BND(inv_b_hat - 1 / b, [-0.375, 4.47035e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l208 h0 h1 h2 h3).
 apply t45. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l206 : p5 -> p4 -> p3 -> s1 -> p208 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 4.47035e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l207 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t46 : p39 -> p208 -> p207.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i150 i149 h0 h1 _) ; finalize.
Qed.
Lemma l205 : p5 -> p4 -> p3 -> s1 -> p207 (* BND(-1 * (inv_b_hat - 1 / b), [-4.47035e-08, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l206 h0 h1 h2 h3).
 apply t46. exact h4. exact h5.
Qed.
Lemma t47 : p207 -> p23 -> p206.
 intros h0 h1.
 refine (div_nn r9 r8 i149 i13 i148 h0 h1 _) ; finalize.
Qed.
Lemma l204 : p5 -> p4 -> p3 -> s1 -> p206 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.1875]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l205 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t47. exact h4. exact h5.
Qed.
Lemma t48 : p14 -> p18 -> p206 -> p205.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i148 h0 h1 h2) ; finalize.
Qed.
Lemma l203 : p5 -> p4 -> p3 -> s1 -> p205 (* BND(1 / b, [4.33681e-19, 0.1875]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l204 h0 h1 h2 h3).
 apply t48. exact h4. exact h5. exact h6.
Qed.
Lemma l202 : p5 -> p4 -> p3 -> s1 -> p204 (* BND(1 / b, [4.65661e-10, 0.1875]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l203 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t49 : p213 -> p204 -> p203.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i147 i146 h0 h1 _) ; finalize.
Qed.
Lemma l201 : p5 -> p4 -> p3 -> s1 -> p203 (* BND(inv_b_hat - 1 / b, [-0.1875, 2.23517e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l202 h0 h1 h2 h3).
 apply t49. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l200 : p5 -> p4 -> p3 -> s1 -> p202 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.23517e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l201 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t50 : p39 -> p202 -> p201.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i145 i144 h0 h1 _) ; finalize.
Qed.
Lemma l199 : p5 -> p4 -> p3 -> s1 -> p201 (* BND(-1 * (inv_b_hat - 1 / b), [-2.23517e-08, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l200 h0 h1 h2 h3).
 apply t50. exact h4. exact h5.
Qed.
Lemma t51 : p201 -> p23 -> p200.
 intros h0 h1.
 refine (div_nn r9 r8 i144 i13 i143 h0 h1 _) ; finalize.
Qed.
Lemma l198 : p5 -> p4 -> p3 -> s1 -> p200 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.09375]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l199 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t51. exact h4. exact h5.
Qed.
Lemma t52 : p14 -> p18 -> p200 -> p199.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i143 h0 h1 h2) ; finalize.
Qed.
Lemma l197 : p5 -> p4 -> p3 -> s1 -> p199 (* BND(1 / b, [4.33681e-19, 0.09375]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l198 h0 h1 h2 h3).
 apply t52. exact h4. exact h5. exact h6.
Qed.
Lemma l196 : p5 -> p4 -> p3 -> s1 -> p198 (* BND(1 / b, [4.65661e-10, 0.09375]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l197 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t53 : p213 -> p198 -> p197.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i142 i141 h0 h1 _) ; finalize.
Qed.
Lemma l195 : p5 -> p4 -> p3 -> s1 -> p197 (* BND(inv_b_hat - 1 / b, [-0.09375, 1.11759e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l196 h0 h1 h2 h3).
 apply t53. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l194 : p5 -> p4 -> p3 -> s1 -> p196 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.11759e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l195 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t54 : p39 -> p196 -> p195.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i140 i139 h0 h1 _) ; finalize.
Qed.
Lemma l193 : p5 -> p4 -> p3 -> s1 -> p195 (* BND(-1 * (inv_b_hat - 1 / b), [-1.11759e-08, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l194 h0 h1 h2 h3).
 apply t54. exact h4. exact h5.
Qed.
Lemma t55 : p195 -> p23 -> p194.
 intros h0 h1.
 refine (div_nn r9 r8 i139 i13 i138 h0 h1 _) ; finalize.
Qed.
Lemma l192 : p5 -> p4 -> p3 -> s1 -> p194 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.046875]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l193 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t55. exact h4. exact h5.
Qed.
Lemma t56 : p14 -> p18 -> p194 -> p193.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i138 h0 h1 h2) ; finalize.
Qed.
Lemma l191 : p5 -> p4 -> p3 -> s1 -> p193 (* BND(1 / b, [4.33681e-19, 0.046875]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l192 h0 h1 h2 h3).
 apply t56. exact h4. exact h5. exact h6.
Qed.
Lemma l190 : p5 -> p4 -> p3 -> s1 -> p192 (* BND(1 / b, [4.65661e-10, 0.046875]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l191 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t57 : p213 -> p192 -> p191.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i137 i136 h0 h1 _) ; finalize.
Qed.
Lemma l189 : p5 -> p4 -> p3 -> s1 -> p191 (* BND(inv_b_hat - 1 / b, [-0.046875, 5.58794e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l190 h0 h1 h2 h3).
 apply t57. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l188 : p5 -> p4 -> p3 -> s1 -> p190 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 5.58794e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l189 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t58 : p39 -> p190 -> p189.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i135 i134 h0 h1 _) ; finalize.
Qed.
Lemma l187 : p5 -> p4 -> p3 -> s1 -> p189 (* BND(-1 * (inv_b_hat - 1 / b), [-5.58794e-09, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l188 h0 h1 h2 h3).
 apply t58. exact h4. exact h5.
Qed.
Lemma t59 : p189 -> p23 -> p188.
 intros h0 h1.
 refine (div_nn r9 r8 i134 i13 i133 h0 h1 _) ; finalize.
Qed.
Lemma l186 : p5 -> p4 -> p3 -> s1 -> p188 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.0234375]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l187 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t59. exact h4. exact h5.
Qed.
Lemma t60 : p14 -> p18 -> p188 -> p187.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i133 h0 h1 h2) ; finalize.
Qed.
Lemma l185 : p5 -> p4 -> p3 -> s1 -> p187 (* BND(1 / b, [4.33681e-19, 0.0234375]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l186 h0 h1 h2 h3).
 apply t60. exact h4. exact h5. exact h6.
Qed.
Lemma l184 : p5 -> p4 -> p3 -> s1 -> p186 (* BND(1 / b, [4.65661e-10, 0.0234375]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l185 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t61 : p213 -> p186 -> p185.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i132 i131 h0 h1 _) ; finalize.
Qed.
Lemma l183 : p5 -> p4 -> p3 -> s1 -> p185 (* BND(inv_b_hat - 1 / b, [-0.0234375, 2.79397e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l184 h0 h1 h2 h3).
 apply t61. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l182 : p5 -> p4 -> p3 -> s1 -> p184 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.79397e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l183 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t62 : p39 -> p184 -> p183.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i130 i129 h0 h1 _) ; finalize.
Qed.
Lemma l181 : p5 -> p4 -> p3 -> s1 -> p183 (* BND(-1 * (inv_b_hat - 1 / b), [-2.79397e-09, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l182 h0 h1 h2 h3).
 apply t62. exact h4. exact h5.
Qed.
Lemma t63 : p183 -> p23 -> p182.
 intros h0 h1.
 refine (div_nn r9 r8 i129 i13 i128 h0 h1 _) ; finalize.
Qed.
Lemma l180 : p5 -> p4 -> p3 -> s1 -> p182 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.0117188]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l181 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t63. exact h4. exact h5.
Qed.
Lemma t64 : p14 -> p18 -> p182 -> p181.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i128 h0 h1 h2) ; finalize.
Qed.
Lemma l179 : p5 -> p4 -> p3 -> s1 -> p181 (* BND(1 / b, [4.33681e-19, 0.0117188]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l180 h0 h1 h2 h3).
 apply t64. exact h4. exact h5. exact h6.
Qed.
Lemma l178 : p5 -> p4 -> p3 -> s1 -> p180 (* BND(1 / b, [4.65661e-10, 0.0117188]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l179 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t65 : p213 -> p180 -> p179.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i127 i126 h0 h1 _) ; finalize.
Qed.
Lemma l177 : p5 -> p4 -> p3 -> s1 -> p179 (* BND(inv_b_hat - 1 / b, [-0.0117188, 1.39698e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l178 h0 h1 h2 h3).
 apply t65. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l176 : p5 -> p4 -> p3 -> s1 -> p178 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.39698e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l177 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t66 : p39 -> p178 -> p177.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i125 i124 h0 h1 _) ; finalize.
Qed.
Lemma l175 : p5 -> p4 -> p3 -> s1 -> p177 (* BND(-1 * (inv_b_hat - 1 / b), [-1.39698e-09, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l176 h0 h1 h2 h3).
 apply t66. exact h4. exact h5.
Qed.
Lemma t67 : p177 -> p23 -> p176.
 intros h0 h1.
 refine (div_nn r9 r8 i124 i13 i123 h0 h1 _) ; finalize.
Qed.
Lemma l174 : p5 -> p4 -> p3 -> s1 -> p176 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.00585938]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l175 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t67. exact h4. exact h5.
Qed.
Lemma t68 : p14 -> p18 -> p176 -> p175.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i123 h0 h1 h2) ; finalize.
Qed.
Lemma l173 : p5 -> p4 -> p3 -> s1 -> p175 (* BND(1 / b, [4.33681e-19, 0.00585938]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l174 h0 h1 h2 h3).
 apply t68. exact h4. exact h5. exact h6.
Qed.
Lemma l172 : p5 -> p4 -> p3 -> s1 -> p174 (* BND(1 / b, [4.65661e-10, 0.00585938]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l173 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t69 : p213 -> p174 -> p173.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i122 i121 h0 h1 _) ; finalize.
Qed.
Lemma l171 : p5 -> p4 -> p3 -> s1 -> p173 (* BND(inv_b_hat - 1 / b, [-0.00585938, 6.98492e-10]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l172 h0 h1 h2 h3).
 apply t69. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l170 : p5 -> p4 -> p3 -> s1 -> p172 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 6.98492e-10]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l171 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t70 : p39 -> p172 -> p171.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i120 i119 h0 h1 _) ; finalize.
Qed.
Lemma l169 : p5 -> p4 -> p3 -> s1 -> p171 (* BND(-1 * (inv_b_hat - 1 / b), [-6.98492e-10, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l170 h0 h1 h2 h3).
 apply t70. exact h4. exact h5.
Qed.
Lemma t71 : p171 -> p23 -> p170.
 intros h0 h1.
 refine (div_nn r9 r8 i119 i13 i118 h0 h1 _) ; finalize.
Qed.
Lemma l168 : p5 -> p4 -> p3 -> s1 -> p170 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.00292969]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l169 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t71. exact h4. exact h5.
Qed.
Lemma t72 : p14 -> p18 -> p170 -> p169.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i118 h0 h1 h2) ; finalize.
Qed.
Lemma l167 : p5 -> p4 -> p3 -> s1 -> p169 (* BND(1 / b, [4.33681e-19, 0.00292969]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l168 h0 h1 h2 h3).
 apply t72. exact h4. exact h5. exact h6.
Qed.
Lemma l166 : p5 -> p4 -> p3 -> s1 -> p168 (* BND(1 / b, [4.65661e-10, 0.00292969]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l167 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t73 : p213 -> p168 -> p167.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i117 i116 h0 h1 _) ; finalize.
Qed.
Lemma l165 : p5 -> p4 -> p3 -> s1 -> p167 (* BND(inv_b_hat - 1 / b, [-0.00292969, 3.49246e-10]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l166 h0 h1 h2 h3).
 apply t73. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l164 : p5 -> p4 -> p3 -> s1 -> p166 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 3.49246e-10]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l165 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t74 : p39 -> p166 -> p165.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i115 i114 h0 h1 _) ; finalize.
Qed.
Lemma l163 : p5 -> p4 -> p3 -> s1 -> p165 (* BND(-1 * (inv_b_hat - 1 / b), [-3.49246e-10, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l164 h0 h1 h2 h3).
 apply t74. exact h4. exact h5.
Qed.
Lemma t75 : p165 -> p23 -> p164.
 intros h0 h1.
 refine (div_nn r9 r8 i114 i13 i113 h0 h1 _) ; finalize.
Qed.
Lemma l162 : p5 -> p4 -> p3 -> s1 -> p164 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.00146484]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l163 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t75. exact h4. exact h5.
Qed.
Lemma t76 : p14 -> p18 -> p164 -> p163.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i113 h0 h1 h2) ; finalize.
Qed.
Lemma l161 : p5 -> p4 -> p3 -> s1 -> p163 (* BND(1 / b, [4.33681e-19, 0.00146484]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l162 h0 h1 h2 h3).
 apply t76. exact h4. exact h5. exact h6.
Qed.
Lemma l160 : p5 -> p4 -> p3 -> s1 -> p162 (* BND(1 / b, [4.65661e-10, 0.00146484]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l161 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t77 : p213 -> p162 -> p161.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i112 i111 h0 h1 _) ; finalize.
Qed.
Lemma l159 : p5 -> p4 -> p3 -> s1 -> p161 (* BND(inv_b_hat - 1 / b, [-0.00146484, 1.74623e-10]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l160 h0 h1 h2 h3).
 apply t77. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l158 : p5 -> p4 -> p3 -> s1 -> p160 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.74623e-10]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l159 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t78 : p39 -> p160 -> p159.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i110 i109 h0 h1 _) ; finalize.
Qed.
Lemma l157 : p5 -> p4 -> p3 -> s1 -> p159 (* BND(-1 * (inv_b_hat - 1 / b), [-1.74623e-10, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l158 h0 h1 h2 h3).
 apply t78. exact h4. exact h5.
Qed.
Lemma t79 : p159 -> p23 -> p158.
 intros h0 h1.
 refine (div_nn r9 r8 i109 i13 i108 h0 h1 _) ; finalize.
Qed.
Lemma l156 : p5 -> p4 -> p3 -> s1 -> p158 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.000732422]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l157 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t79. exact h4. exact h5.
Qed.
Lemma t80 : p14 -> p18 -> p158 -> p157.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i108 h0 h1 h2) ; finalize.
Qed.
Lemma l155 : p5 -> p4 -> p3 -> s1 -> p157 (* BND(1 / b, [4.33681e-19, 0.000732422]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l156 h0 h1 h2 h3).
 apply t80. exact h4. exact h5. exact h6.
Qed.
Lemma l154 : p5 -> p4 -> p3 -> s1 -> p156 (* BND(1 / b, [4.65661e-10, 0.000732422]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l155 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t81 : p213 -> p156 -> p155.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i107 i106 h0 h1 _) ; finalize.
Qed.
Lemma l153 : p5 -> p4 -> p3 -> s1 -> p155 (* BND(inv_b_hat - 1 / b, [-0.000732422, 8.73115e-11]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l154 h0 h1 h2 h3).
 apply t81. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l152 : p5 -> p4 -> p3 -> s1 -> p154 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 8.73115e-11]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l153 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t82 : p39 -> p154 -> p153.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i105 i104 h0 h1 _) ; finalize.
Qed.
Lemma l151 : p5 -> p4 -> p3 -> s1 -> p153 (* BND(-1 * (inv_b_hat - 1 / b), [-8.73115e-11, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l152 h0 h1 h2 h3).
 apply t82. exact h4. exact h5.
Qed.
Lemma t83 : p153 -> p23 -> p152.
 intros h0 h1.
 refine (div_nn r9 r8 i104 i13 i103 h0 h1 _) ; finalize.
Qed.
Lemma l150 : p5 -> p4 -> p3 -> s1 -> p152 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.000366211]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l151 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t83. exact h4. exact h5.
Qed.
Lemma t84 : p14 -> p18 -> p152 -> p151.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i103 h0 h1 h2) ; finalize.
Qed.
Lemma l149 : p5 -> p4 -> p3 -> s1 -> p151 (* BND(1 / b, [4.33681e-19, 0.000366211]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l150 h0 h1 h2 h3).
 apply t84. exact h4. exact h5. exact h6.
Qed.
Lemma l148 : p5 -> p4 -> p3 -> s1 -> p150 (* BND(1 / b, [4.65661e-10, 0.000366211]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l149 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t85 : p213 -> p150 -> p149.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i102 i101 h0 h1 _) ; finalize.
Qed.
Lemma l147 : p5 -> p4 -> p3 -> s1 -> p149 (* BND(inv_b_hat - 1 / b, [-0.000366211, 4.36558e-11]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l148 h0 h1 h2 h3).
 apply t85. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l146 : p5 -> p4 -> p3 -> s1 -> p148 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 4.36558e-11]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l147 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t86 : p39 -> p148 -> p147.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i100 i99 h0 h1 _) ; finalize.
Qed.
Lemma l145 : p5 -> p4 -> p3 -> s1 -> p147 (* BND(-1 * (inv_b_hat - 1 / b), [-4.36558e-11, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l146 h0 h1 h2 h3).
 apply t86. exact h4. exact h5.
Qed.
Lemma t87 : p147 -> p23 -> p146.
 intros h0 h1.
 refine (div_nn r9 r8 i99 i13 i98 h0 h1 _) ; finalize.
Qed.
Lemma l144 : p5 -> p4 -> p3 -> s1 -> p146 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 0.000183106]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l145 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t87. exact h4. exact h5.
Qed.
Lemma t88 : p14 -> p18 -> p146 -> p145.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i98 h0 h1 h2) ; finalize.
Qed.
Lemma l143 : p5 -> p4 -> p3 -> s1 -> p145 (* BND(1 / b, [4.33681e-19, 0.000183106]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l144 h0 h1 h2 h3).
 apply t88. exact h4. exact h5. exact h6.
Qed.
Lemma l142 : p5 -> p4 -> p3 -> s1 -> p144 (* BND(1 / b, [4.65661e-10, 0.000183106]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l143 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t89 : p213 -> p144 -> p143.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i97 i96 h0 h1 _) ; finalize.
Qed.
Lemma l141 : p5 -> p4 -> p3 -> s1 -> p143 (* BND(inv_b_hat - 1 / b, [-0.000183106, 2.18279e-11]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l142 h0 h1 h2 h3).
 apply t89. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l140 : p5 -> p4 -> p3 -> s1 -> p142 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.18279e-11]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l141 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t90 : p39 -> p142 -> p141.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i95 i94 h0 h1 _) ; finalize.
Qed.
Lemma l139 : p5 -> p4 -> p3 -> s1 -> p141 (* BND(-1 * (inv_b_hat - 1 / b), [-2.18279e-11, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l140 h0 h1 h2 h3).
 apply t90. exact h4. exact h5.
Qed.
Lemma t91 : p141 -> p23 -> p140.
 intros h0 h1.
 refine (div_nn r9 r8 i94 i13 i93 h0 h1 _) ; finalize.
Qed.
Lemma l138 : p5 -> p4 -> p3 -> s1 -> p140 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 9.15528e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l139 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t91. exact h4. exact h5.
Qed.
Lemma t92 : p14 -> p18 -> p140 -> p139.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i93 h0 h1 h2) ; finalize.
Qed.
Lemma l137 : p5 -> p4 -> p3 -> s1 -> p139 (* BND(1 / b, [4.33681e-19, 9.15528e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l138 h0 h1 h2 h3).
 apply t92. exact h4. exact h5. exact h6.
Qed.
Lemma l136 : p5 -> p4 -> p3 -> s1 -> p138 (* BND(1 / b, [4.65661e-10, 9.15528e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l137 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t93 : p213 -> p138 -> p137.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i92 i91 h0 h1 _) ; finalize.
Qed.
Lemma l135 : p5 -> p4 -> p3 -> s1 -> p137 (* BND(inv_b_hat - 1 / b, [-9.15528e-05, 1.09139e-11]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l136 h0 h1 h2 h3).
 apply t93. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l134 : p5 -> p4 -> p3 -> s1 -> p136 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.09139e-11]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l135 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t94 : p39 -> p136 -> p135.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i90 i89 h0 h1 _) ; finalize.
Qed.
Lemma l133 : p5 -> p4 -> p3 -> s1 -> p135 (* BND(-1 * (inv_b_hat - 1 / b), [-1.09139e-11, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l134 h0 h1 h2 h3).
 apply t94. exact h4. exact h5.
Qed.
Lemma t95 : p135 -> p23 -> p134.
 intros h0 h1.
 refine (div_nn r9 r8 i89 i13 i88 h0 h1 _) ; finalize.
Qed.
Lemma l132 : p5 -> p4 -> p3 -> s1 -> p134 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 4.57764e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l133 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t95. exact h4. exact h5.
Qed.
Lemma t96 : p14 -> p18 -> p134 -> p133.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i88 h0 h1 h2) ; finalize.
Qed.
Lemma l131 : p5 -> p4 -> p3 -> s1 -> p133 (* BND(1 / b, [4.33681e-19, 4.57764e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l132 h0 h1 h2 h3).
 apply t96. exact h4. exact h5. exact h6.
Qed.
Lemma l130 : p5 -> p4 -> p3 -> s1 -> p132 (* BND(1 / b, [4.65661e-10, 4.57764e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l131 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t97 : p213 -> p132 -> p131.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i87 i86 h0 h1 _) ; finalize.
Qed.
Lemma l129 : p5 -> p4 -> p3 -> s1 -> p131 (* BND(inv_b_hat - 1 / b, [-4.57764e-05, 5.45697e-12]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l130 h0 h1 h2 h3).
 apply t97. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. exact h5.
Qed.
Lemma l128 : p5 -> p4 -> p3 -> s1 -> p130 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 5.45697e-12]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l129 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t98 : p39 -> p130 -> p129.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i85 i84 h0 h1 _) ; finalize.
Qed.
Lemma l127 : p5 -> p4 -> p3 -> s1 -> p129 (* BND(-1 * (inv_b_hat - 1 / b), [-5.45697e-12, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l128 h0 h1 h2 h3).
 apply t98. exact h4. exact h5.
Qed.
Lemma t99 : p129 -> p23 -> p128.
 intros h0 h1.
 refine (div_nn r9 r8 i84 i13 i83 h0 h1 _) ; finalize.
Qed.
Lemma l126 : p5 -> p4 -> p3 -> s1 -> p128 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.33681e-19, 2.28882e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l127 h0 h1 h2 h3).
 assert (h5 := l22 h0 h1 h2 h3).
 apply t99. exact h4. exact h5.
Qed.
Lemma t100 : p14 -> p18 -> p128 -> p127.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i83 h0 h1 h2) ; finalize.
Qed.
Lemma l125 : p5 -> p4 -> p3 -> s1 -> p127 (* BND(1 / b, [4.33681e-19, 2.28882e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l126 h0 h1 h2 h3).
 apply t100. exact h4. exact h5. exact h6.
Qed.
Lemma l124 : p5 -> p4 -> p3 -> s1 -> p126 (* BND(1 / b, [4.65661e-10, 2.28882e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l125 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition f133 := Float2 (432345942184866333) (-74).
Definition i155 := makepairF f13 f133.
Notation p214 := (BND r12 i155). (* BND(1 / b, [4.65661e-10, 2.28882e-05]) *)
Lemma t101 : p213 -> p214 -> p125.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i154 i155 i81 h0 h1 _) ; finalize.
Qed.
Lemma l123 : p5 -> p4 -> p3 -> s1 -> p125 (* BND(inv_b_hat - 1 / b, [-2.28882e-05, 2.72849e-12]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l58 h3).
 assert (h5 := l124 h0 h1 h2 h3).
 apply t101. refine (rel_subset _inv_b_hat r12 i32 i154 h4 _) ; finalize. refine (subset r12 i82 i155 h5 _) ; finalize.
Qed.
Lemma l122 : p5 -> p4 -> p3 -> s1 -> p124 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.72849e-12]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l123 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t102 : p39 -> p124 -> p123.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i80 i79 h0 h1 _) ; finalize.
Qed.
Lemma l121 : p5 -> p4 -> p3 -> s1 -> p123 (* BND(-1 * (inv_b_hat - 1 / b), [-2.72849e-12, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l122 h0 h1 h2 h3).
 apply t102. exact h4. exact h5.
Qed.
Definition f134 := Float2 (-1) (-6).
Definition i156 := makepairF f134 f3.
Notation p215 := (BND r8 i156). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.015625, -2.38419e-07]) *)
Definition i157 := makepairF f134 f18.
Notation p216 := (BND r8 i157). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.015625, -1.11022e-16]) *)
Definition f135 := Float2 (-1) (-37).
Definition i158 := makepairF f135 f18.
Notation p217 := (BND r9 i158). (* BND(-1 * (inv_b_hat - 1 / b), [-7.27596e-12, -1.11022e-16]) *)
Lemma t103 : p217 -> p47 -> p216.
 intros h0 h1.
 refine (div_np r9 r12 i158 i24 i157 h0 h1 _) ; finalize.
Qed.
Lemma l212 : p5 -> p4 -> p3 -> s1 -> p216 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.015625, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l127 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t103. refine (subset r9 i84 i158 h4 _) ; finalize. exact h5.
Qed.
Lemma l211 : p5 -> p4 -> p3 -> s1 -> p215 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.015625, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l212 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t104 : p123 -> p215 -> p122.
 intros h0 h1.
 refine (div_nn r9 r8 i79 i156 i78 h0 h1 _) ; finalize.
Qed.
Lemma l120 : p5 -> p4 -> p3 -> s1 -> p122 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.14441e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l121 h0 h1 h2 h3).
 assert (h5 := l211 h0 h1 h2 h3).
 apply t104. exact h4. exact h5.
Qed.
Lemma t105 : p14 -> p18 -> p122 -> p121.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i78 h0 h1 h2) ; finalize.
Qed.
Lemma l119 : p5 -> p4 -> p3 -> s1 -> p121 (* BND(1 / b, [7.10543e-15, 1.14441e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l120 h0 h1 h2 h3).
 apply t105. exact h4. exact h5. exact h6.
Qed.
Lemma l118 : p5 -> p4 -> p3 -> s1 -> p120 (* BND(1 / b, [4.65661e-10, 1.14441e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l119 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition f136 := Float2 (1) (-16).
Definition i159 := makepairF f13 f136.
Notation p218 := (BND r12 i159). (* BND(1 / b, [4.65661e-10, 1.52588e-05]) *)
Lemma t106 : p63 -> p218 -> p119.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i159 i76 h0 h1 _) ; finalize.
Qed.
Lemma l117 : p5 -> p4 -> p3 -> s1 -> p119 (* BND((inv_b_hat - 1 / b) / (1 / b), [7.27596e-12, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l118 h0 h1 h2 h3).
 apply t106. exact h4. refine (subset r12 i77 i159 h5 _) ; finalize.
Qed.
Lemma l116 : p5 -> p4 -> p3 -> s1 -> p118 (* BND((inv_b_hat - 1 / b) / (1 / b), [7.27596e-12, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l117 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t107 : p18 -> p118 -> p117.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i75 h0 h1) ; finalize.
Qed.
Lemma l115 : p5 -> p4 -> p3 -> s1 -> p117 (* REL(inv_b_hat, 1 / b, [7.27596e-12, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l116 h0 h1 h2 h3).
 apply t107. exact h4. exact h5.
Qed.
Definition f137 := Float2 (22518011558632353) (-72).
Definition i160 := makepairF f13 f137.
Notation p219 := (BND r12 i160). (* BND(1 / b, [4.65661e-10, 4.76837e-06]) *)
Definition f138 := Float2 (1) (-45).
Definition i161 := makepairF f138 f137.
Notation p220 := (BND r12 i161). (* BND(1 / b, [2.84217e-14, 4.76837e-06]) *)
Notation p221 := (BND r19 i161). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.84217e-14, 4.76837e-06]) *)
Definition f139 := Float2 (-22518011558632353) (-94).
Definition i162 := makepairF f139 f18.
Notation p222 := (BND r9 i162). (* BND(-1 * (inv_b_hat - 1 / b), [-1.13687e-12, -1.11022e-16]) *)
Definition f140 := Float2 (22518011558632353) (-94).
Definition i163 := makepairF f16 f140.
Notation p223 := (BND r11 i163). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.13687e-12]) *)
Definition f141 := Float2 (-1) (-39).
Definition i164 := makepairF f141 f140.
Notation p224 := (BND r11 i164). (* BND(inv_b_hat - 1 / b, [-1.81899e-12, 1.13687e-12]) *)
Notation p225 := (BND r17 i164). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-1.81899e-12, 1.13687e-12]) *)
Definition f142 := Float2 (-1) (-40).
Definition f143 := Float2 (1) (-41).
Definition i165 := makepairF f142 f143.
Notation p226 := (BND r18 i165). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-9.09495e-13, 4.54747e-13]) *)
Definition f144 := Float2 (7) (-19).
Definition i166 := makepairF f13 f144.
Notation p227 := (ABS _inv_b_hat i166). (* ABS(inv_b_hat, [4.65661e-10, 1.33514e-05]) *)
Notation p228 := (BND _inv_b_hat i166). (* BND(inv_b_hat, [4.65661e-10, 1.33514e-05]) *)
Definition f145 := Float2 (1) (-4).
Definition i167 := makepairF f48 f145.
Notation p229 := (REL _inv_b_hat r12 i167). (* REL(inv_b_hat, 1 / b, [3.63798e-12, 0.0625]) *)
Definition f146 := Float2 (24019177015033295) (-92).
Definition i168 := makepairF f146 f145.
Notation p230 := (BND r20 i168). (* BND((inv_b_hat - 1 / b) / (1 / b), [4.85063e-12, 0.0625]) *)
Definition i169 := makepairF f16 f63.
Notation p231 := (BND r11 i169). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.91038e-11]) *)
Lemma t108 : p231 -> p126 -> p230.
 intros h0 h1.
 refine (div_pp r11 r12 i169 i82 i168 h0 h1 _) ; finalize.
Qed.
Lemma l224 : p5 -> p4 -> p3 -> s1 -> p230 (* BND((inv_b_hat - 1 / b) / (1 / b), [4.85063e-12, 0.0625]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l140 h0 h1 h2 h3).
 assert (h5 := l124 h0 h1 h2 h3).
 apply t108. refine (subset r11 i95 i169 h4 _) ; finalize. exact h5.
Qed.
Notation p232 := (BND r20 i167). (* BND((inv_b_hat - 1 / b) / (1 / b), [3.63798e-12, 0.0625]) *)
Lemma t109 : p18 -> p232 -> p229.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i167 h0 h1) ; finalize.
Qed.
Lemma l223 : p5 -> p4 -> p3 -> s1 -> p229 (* REL(inv_b_hat, 1 / b, [3.63798e-12, 0.0625]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l224 h0 h1 h2 h3).
 apply t109. exact h4. refine (subset r20 i168 i167 h5 _) ; finalize.
Qed.
Definition f147 := Float2 (13) (-20).
Definition i170 := makepairF f13 f147.
Notation p233 := (BND r12 i170). (* BND(1 / b, [4.65661e-10, 1.23978e-05]) *)
Lemma t110 : p233 -> p229 -> p228.
 intros h0 h1.
 refine (bnd_of_bnd_rel_p _inv_b_hat r12 i170 i167 i166 h0 h1 _) ; finalize.
Qed.
Lemma l222 : p5 -> p4 -> p3 -> s1 -> p228 (* BND(inv_b_hat, [4.65661e-10, 1.33514e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l118 h0 h1 h2 h3).
 assert (h5 := l223 h0 h1 h2 h3).
 apply t110. refine (subset r12 i77 i170 h4 _) ; finalize. exact h5.
Qed.
Lemma t111 : p228 -> p227.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat i166 i166 h0 _) ; finalize.
Qed.
Lemma l221 : p5 -> p4 -> p3 -> s1 -> p227 (* ABS(inv_b_hat, [4.65661e-10, 1.33514e-05]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l222 h0 h1 h2 h3).
 apply t111. exact h4.
Qed.
Lemma t112 : p227 -> p226.
 intros h0.
 refine (float_absolute_inv_ne _ _ _ i166 i165 h0 _) ; finalize.
Qed.
Lemma l220 : p5 -> p4 -> p3 -> s1 -> p226 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-9.09495e-13, 4.54747e-13]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l221 h0 h1 h2 h3).
 apply t112. exact h4.
Qed.
Definition f148 := Float2 (13510812303891361) (-94).
Definition i171 := makepairF f142 f148.
Notation p234 := (BND r13 i171). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-9.09495e-13, 6.82122e-13]) *)
Definition i172 := makepairF f10 f9.
Notation p235 := (REL r6 r12 i172). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Lemma t113 : p235 -> p120 -> p234.
 intros h0 h1.
 refine (error_of_rel_op r6 r12 i172 i77 i171 h0 h1 _) ; finalize.
Qed.
Lemma l225 : p5 -> p4 -> p3 -> s1 -> p234 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-9.09495e-13, 6.82122e-13]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l6 h3).
 assert (h5 := l118 h0 h1 h2 h3).
 apply t113. refine (rel_subset r6 r12 i4 i172 h4 _) ; finalize. exact h5.
Qed.
Lemma t114 : p226 -> p234 -> p225.
 intros h0 h1.
 refine (add r18 r13 i165 i171 i164 h0 h1 _) ; finalize.
Qed.
Lemma l219 : p5 -> p4 -> p3 -> s1 -> p225 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-1.81899e-12, 1.13687e-12]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l220 h0 h1 h2 h3).
 assert (h5 := l225 h0 h1 h2 h3).
 apply t114. exact h4. exact h5.
Qed.
Lemma t115 : p225 -> p224.
 intros h0.
 refine (sub_xals _ _ _ i164 h0) ; finalize.
Qed.
Lemma l218 : p5 -> p4 -> p3 -> s1 -> p224 (* BND(inv_b_hat - 1 / b, [-1.81899e-12, 1.13687e-12]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l219 h0 h1 h2 h3).
 apply t115. exact h4.
Qed.
Lemma l217 : p5 -> p4 -> p3 -> s1 -> p223 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.13687e-12]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l218 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t116 : p39 -> p223 -> p222.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i163 i162 h0 h1 _) ; finalize.
Qed.
Lemma l216 : p5 -> p4 -> p3 -> s1 -> p222 (* BND(-1 * (inv_b_hat - 1 / b), [-1.13687e-12, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l217 h0 h1 h2 h3).
 apply t116. exact h4. exact h5.
Qed.
Definition f149 := Float2 (-1) (-8).
Definition i173 := makepairF f149 f3.
Notation p236 := (BND r8 i173). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00390625, -2.38419e-07]) *)
Definition i174 := makepairF f149 f18.
Notation p237 := (BND r8 i174). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00390625, -1.11022e-16]) *)
Definition i175 := makepairF f141 f18.
Notation p238 := (BND r9 i175). (* BND(-1 * (inv_b_hat - 1 / b), [-1.81899e-12, -1.11022e-16]) *)
Lemma t117 : p238 -> p47 -> p237.
 intros h0 h1.
 refine (div_np r9 r12 i175 i24 i174 h0 h1 _) ; finalize.
Qed.
Lemma l227 : p5 -> p4 -> p3 -> s1 -> p237 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00390625, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l216 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t117. refine (subset r9 i162 i175 h4 _) ; finalize. exact h5.
Qed.
Lemma l226 : p5 -> p4 -> p3 -> s1 -> p236 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00390625, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l227 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t118 : p222 -> p236 -> p221.
 intros h0 h1.
 refine (div_nn r9 r8 i162 i173 i161 h0 h1 _) ; finalize.
Qed.
Lemma l215 : p5 -> p4 -> p3 -> s1 -> p221 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.84217e-14, 4.76837e-06]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l216 h0 h1 h2 h3).
 assert (h5 := l226 h0 h1 h2 h3).
 apply t118. exact h4. exact h5.
Qed.
Lemma t119 : p14 -> p18 -> p221 -> p220.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i161 h0 h1 h2) ; finalize.
Qed.
Lemma l214 : p5 -> p4 -> p3 -> s1 -> p220 (* BND(1 / b, [2.84217e-14, 4.76837e-06]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l215 h0 h1 h2 h3).
 apply t119. exact h4. exact h5. exact h6.
Qed.
Lemma l213 : p5 -> p4 -> p3 -> s1 -> p219 (* BND(1 / b, [4.65661e-10, 4.76837e-06]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l214 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t120 : p117 -> p219 -> p116.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i75 i160 i74 h0 h1 _) ; finalize.
Qed.
Lemma l114 : p5 -> p4 -> p3 -> s1 -> p116 (* BND(inv_b_hat - 1 / b, [3.38813e-21, 5.68435e-13]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l115 h0 h1 h2 h3).
 assert (h5 := l213 h0 h1 h2 h3).
 apply t120. exact h4. exact h5.
Qed.
Lemma l113 : p5 -> p4 -> p3 -> s1 -> p115 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 5.68435e-13]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l114 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t121 : p39 -> p115 -> p114.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i73 i72 h0 h1 _) ; finalize.
Qed.
Lemma l112 : p5 -> p4 -> p3 -> s1 -> p114 (* BND(-1 * (inv_b_hat - 1 / b), [-5.68435e-13, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l113 h0 h1 h2 h3).
 apply t121. exact h4. exact h5.
Qed.
Definition f150 := Float2 (-1) (-9).
Definition i176 := makepairF f150 f3.
Notation p239 := (BND r8 i176). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00195312, -2.38419e-07]) *)
Definition i177 := makepairF f150 f18.
Notation p240 := (BND r8 i177). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00195312, -1.11022e-16]) *)
Definition i178 := makepairF f142 f18.
Notation p241 := (BND r9 i178). (* BND(-1 * (inv_b_hat - 1 / b), [-9.09495e-13, -1.11022e-16]) *)
Lemma t122 : p241 -> p47 -> p240.
 intros h0 h1.
 refine (div_np r9 r12 i178 i24 i177 h0 h1 _) ; finalize.
Qed.
Lemma l229 : p5 -> p4 -> p3 -> s1 -> p240 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00195312, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l112 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t122. refine (subset r9 i72 i178 h4 _) ; finalize. exact h5.
Qed.
Lemma l228 : p5 -> p4 -> p3 -> s1 -> p239 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00195312, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l229 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t123 : p114 -> p239 -> p113.
 intros h0 h1.
 refine (div_nn r9 r8 i72 i176 i71 h0 h1 _) ; finalize.
Qed.
Lemma l111 : p5 -> p4 -> p3 -> s1 -> p113 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [5.68434e-14, 2.38419e-06]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l112 h0 h1 h2 h3).
 assert (h5 := l228 h0 h1 h2 h3).
 apply t123. exact h4. exact h5.
Qed.
Lemma t124 : p14 -> p18 -> p113 -> p112.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i71 h0 h1 h2) ; finalize.
Qed.
Lemma l110 : p5 -> p4 -> p3 -> s1 -> p112 (* BND(1 / b, [5.68434e-14, 2.38419e-06]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l111 h0 h1 h2 h3).
 apply t124. exact h4. exact h5. exact h6.
Qed.
Lemma l109 : p5 -> p4 -> p3 -> s1 -> p111 (* BND(1 / b, [4.65661e-10, 2.38419e-06]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l110 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition f151 := Float2 (1) (-18).
Definition i179 := makepairF f13 f151.
Notation p242 := (BND r12 i179). (* BND(1 / b, [4.65661e-10, 3.8147e-06]) *)
Lemma t125 : p63 -> p242 -> p110.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i179 i69 h0 h1 _) ; finalize.
Qed.
Lemma l108 : p5 -> p4 -> p3 -> s1 -> p110 (* BND((inv_b_hat - 1 / b) / (1 / b), [2.91038e-11, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l109 h0 h1 h2 h3).
 apply t125. exact h4. refine (subset r12 i70 i179 h5 _) ; finalize.
Qed.
Lemma l107 : p5 -> p4 -> p3 -> s1 -> p109 (* BND((inv_b_hat - 1 / b) / (1 / b), [2.91038e-11, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l108 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t126 : p18 -> p109 -> p108.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i68 h0 h1) ; finalize.
Qed.
Lemma l106 : p5 -> p4 -> p3 -> s1 -> p108 (* REL(inv_b_hat, 1 / b, [2.91038e-11, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l107 h0 h1 h2 h3).
 apply t126. exact h4. exact h5.
Qed.
Definition f152 := Float2 (360288227887820051) (-78).
Definition i180 := makepairF f13 f152.
Notation p243 := (BND r12 i180). (* BND(1 / b, [4.65661e-10, 1.19209e-06]) *)
Definition f153 := Float2 (1) (-43).
Definition i181 := makepairF f153 f152.
Notation p244 := (BND r12 i181). (* BND(1 / b, [1.13687e-13, 1.19209e-06]) *)
Notation p245 := (BND r19 i181). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.13687e-13, 1.19209e-06]) *)
Definition f154 := Float2 (-360288227887820051) (-100).
Definition i182 := makepairF f154 f18.
Notation p246 := (BND r9 i182). (* BND(-1 * (inv_b_hat - 1 / b), [-2.84217e-13, -1.11022e-16]) *)
Definition f155 := Float2 (360288227887820051) (-100).
Definition i183 := makepairF f16 f155.
Notation p247 := (BND r11 i183). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.84217e-13]) *)
Definition f156 := Float2 (1) (-67).
Definition i184 := makepairF f156 f155.
Notation p248 := (BND r11 i184). (* BND(inv_b_hat - 1 / b, [6.77626e-21, 2.84217e-13]) *)
Definition i185 := makepairF f42 f32.
Notation p249 := (REL _inv_b_hat r12 i185). (* REL(inv_b_hat, 1 / b, [1.45519e-11, 1.19209e-07]) *)
Notation p250 := (BND r20 i185). (* BND((inv_b_hat - 1 / b) / (1 / b), [1.45519e-11, 1.19209e-07]) *)
Definition i186 := makepairF f42 f5.
Notation p251 := (BND r20 i186). (* BND((inv_b_hat - 1 / b) / (1 / b), [1.45519e-11, 2.14748e+09]) *)
Definition f157 := Float2 (1) (-17).
Definition i187 := makepairF f13 f157.
Notation p252 := (BND r12 i187). (* BND(1 / b, [4.65661e-10, 7.62939e-06]) *)
Lemma t127 : p63 -> p252 -> p251.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i187 i186 h0 h1 _) ; finalize.
Qed.
Lemma l238 : p5 -> p4 -> p3 -> s1 -> p251 (* BND((inv_b_hat - 1 / b) / (1 / b), [1.45519e-11, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l213 h0 h1 h2 h3).
 apply t127. exact h4. refine (subset r12 i160 i187 h5 _) ; finalize.
Qed.
Lemma l237 : p5 -> p4 -> p3 -> s1 -> p250 (* BND((inv_b_hat - 1 / b) / (1 / b), [1.45519e-11, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l238 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t128 : p18 -> p250 -> p249.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i185 h0 h1) ; finalize.
Qed.
Lemma l236 : p5 -> p4 -> p3 -> s1 -> p249 (* REL(inv_b_hat, 1 / b, [1.45519e-11, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l237 h0 h1 h2 h3).
 apply t128. exact h4. exact h5.
Qed.
Lemma t129 : p249 -> p111 -> p248.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i185 i70 i184 h0 h1 _) ; finalize.
Qed.
Lemma l235 : p5 -> p4 -> p3 -> s1 -> p248 (* BND(inv_b_hat - 1 / b, [6.77626e-21, 2.84217e-13]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l236 h0 h1 h2 h3).
 assert (h5 := l109 h0 h1 h2 h3).
 apply t129. exact h4. exact h5.
Qed.
Lemma l234 : p5 -> p4 -> p3 -> s1 -> p247 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.84217e-13]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l235 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t130 : p39 -> p247 -> p246.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i183 i182 h0 h1 _) ; finalize.
Qed.
Lemma l233 : p5 -> p4 -> p3 -> s1 -> p246 (* BND(-1 * (inv_b_hat - 1 / b), [-2.84217e-13, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l234 h0 h1 h2 h3).
 apply t130. exact h4. exact h5.
Qed.
Definition f158 := Float2 (-1) (-10).
Definition i188 := makepairF f158 f3.
Notation p253 := (BND r8 i188). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000976562, -2.38419e-07]) *)
Definition i189 := makepairF f158 f18.
Notation p254 := (BND r8 i189). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000976562, -1.11022e-16]) *)
Definition f159 := Float2 (-1) (-41).
Definition i190 := makepairF f159 f18.
Notation p255 := (BND r9 i190). (* BND(-1 * (inv_b_hat - 1 / b), [-4.54747e-13, -1.11022e-16]) *)
Lemma t131 : p255 -> p47 -> p254.
 intros h0 h1.
 refine (div_np r9 r12 i190 i24 i189 h0 h1 _) ; finalize.
Qed.
Lemma l240 : p5 -> p4 -> p3 -> s1 -> p254 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000976562, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l233 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t131. refine (subset r9 i182 i190 h4 _) ; finalize. exact h5.
Qed.
Lemma l239 : p5 -> p4 -> p3 -> s1 -> p253 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000976562, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l240 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t132 : p246 -> p253 -> p245.
 intros h0 h1.
 refine (div_nn r9 r8 i182 i188 i181 h0 h1 _) ; finalize.
Qed.
Lemma l232 : p5 -> p4 -> p3 -> s1 -> p245 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.13687e-13, 1.19209e-06]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l233 h0 h1 h2 h3).
 assert (h5 := l239 h0 h1 h2 h3).
 apply t132. exact h4. exact h5.
Qed.
Lemma t133 : p14 -> p18 -> p245 -> p244.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i181 h0 h1 h2) ; finalize.
Qed.
Lemma l231 : p5 -> p4 -> p3 -> s1 -> p244 (* BND(1 / b, [1.13687e-13, 1.19209e-06]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l232 h0 h1 h2 h3).
 apply t133. exact h4. exact h5. exact h6.
Qed.
Lemma l230 : p5 -> p4 -> p3 -> s1 -> p243 (* BND(1 / b, [4.65661e-10, 1.19209e-06]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l231 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t134 : p108 -> p243 -> p107.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i68 i180 i67 h0 h1 _) ; finalize.
Qed.
Lemma l105 : p5 -> p4 -> p3 -> s1 -> p107 (* BND(inv_b_hat - 1 / b, [1.35525e-20, 1.42109e-13]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l106 h0 h1 h2 h3).
 assert (h5 := l230 h0 h1 h2 h3).
 apply t134. exact h4. exact h5.
Qed.
Lemma l104 : p5 -> p4 -> p3 -> s1 -> p106 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.42109e-13]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l105 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t135 : p39 -> p106 -> p105.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i66 i65 h0 h1 _) ; finalize.
Qed.
Lemma l103 : p5 -> p4 -> p3 -> s1 -> p105 (* BND(-1 * (inv_b_hat - 1 / b), [-1.42109e-13, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l104 h0 h1 h2 h3).
 apply t135. exact h4. exact h5.
Qed.
Definition f160 := Float2 (-1) (-11).
Definition i191 := makepairF f160 f3.
Notation p256 := (BND r8 i191). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000488281, -2.38419e-07]) *)
Definition i192 := makepairF f160 f18.
Notation p257 := (BND r8 i192). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000488281, -1.11022e-16]) *)
Definition f161 := Float2 (-1) (-42).
Definition i193 := makepairF f161 f18.
Notation p258 := (BND r9 i193). (* BND(-1 * (inv_b_hat - 1 / b), [-2.27374e-13, -1.11022e-16]) *)
Lemma t136 : p258 -> p47 -> p257.
 intros h0 h1.
 refine (div_np r9 r12 i193 i24 i192 h0 h1 _) ; finalize.
Qed.
Lemma l242 : p5 -> p4 -> p3 -> s1 -> p257 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000488281, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l103 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t136. refine (subset r9 i65 i193 h4 _) ; finalize. exact h5.
Qed.
Lemma l241 : p5 -> p4 -> p3 -> s1 -> p256 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000488281, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l242 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t137 : p105 -> p256 -> p104.
 intros h0 h1.
 refine (div_nn r9 r8 i65 i191 i64 h0 h1 _) ; finalize.
Qed.
Lemma l102 : p5 -> p4 -> p3 -> s1 -> p104 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.27374e-13, 5.96047e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l103 h0 h1 h2 h3).
 assert (h5 := l241 h0 h1 h2 h3).
 apply t137. exact h4. exact h5.
Qed.
Lemma t138 : p14 -> p18 -> p104 -> p103.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i64 h0 h1 h2) ; finalize.
Qed.
Lemma l101 : p5 -> p4 -> p3 -> s1 -> p103 (* BND(1 / b, [2.27374e-13, 5.96047e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l102 h0 h1 h2 h3).
 apply t138. exact h4. exact h5. exact h6.
Qed.
Lemma l100 : p5 -> p4 -> p3 -> s1 -> p102 (* BND(1 / b, [4.65661e-10, 5.96047e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l101 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition f162 := Float2 (1) (-20).
Definition i194 := makepairF f13 f162.
Notation p259 := (BND r12 i194). (* BND(1 / b, [4.65661e-10, 9.53674e-07]) *)
Lemma t139 : p63 -> p259 -> p101.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i194 i62 h0 h1 _) ; finalize.
Qed.
Lemma l99 : p5 -> p4 -> p3 -> s1 -> p101 (* BND((inv_b_hat - 1 / b) / (1 / b), [1.16415e-10, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l100 h0 h1 h2 h3).
 apply t139. exact h4. refine (subset r12 i63 i194 h5 _) ; finalize.
Qed.
Lemma l98 : p5 -> p4 -> p3 -> s1 -> p100 (* BND((inv_b_hat - 1 / b) / (1 / b), [1.16415e-10, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l99 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t140 : p18 -> p100 -> p99.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i61 h0 h1) ; finalize.
Qed.
Lemma l97 : p5 -> p4 -> p3 -> s1 -> p99 (* REL(inv_b_hat, 1 / b, [1.16415e-10, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l98 h0 h1 h2 h3).
 apply t140. exact h4. exact h5.
Qed.
Definition f163 := Float2 (180144135418763787) (-79).
Definition i195 := makepairF f13 f163.
Notation p260 := (BND r12 i195). (* BND(1 / b, [4.65661e-10, 2.98023e-07]) *)
Definition i196 := makepairF f143 f163.
Notation p261 := (BND r12 i196). (* BND(1 / b, [4.54747e-13, 2.98023e-07]) *)
Notation p262 := (BND r19 i196). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.54747e-13, 2.98023e-07]) *)
Definition f164 := Float2 (-180144135418763787) (-101).
Definition i197 := makepairF f164 f18.
Notation p263 := (BND r9 i197). (* BND(-1 * (inv_b_hat - 1 / b), [-7.10543e-14, -1.11022e-16]) *)
Definition f165 := Float2 (180144135418763787) (-101).
Definition i198 := makepairF f16 f165.
Notation p264 := (BND r11 i198). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 7.10543e-14]) *)
Definition f166 := Float2 (1) (-65).
Definition i199 := makepairF f166 f165.
Notation p265 := (BND r11 i199). (* BND(inv_b_hat - 1 / b, [2.71051e-20, 7.10543e-14]) *)
Definition i200 := makepairF f36 f32.
Notation p266 := (REL _inv_b_hat r12 i200). (* REL(inv_b_hat, 1 / b, [5.82077e-11, 1.19209e-07]) *)
Notation p267 := (BND r20 i200). (* BND((inv_b_hat - 1 / b) / (1 / b), [5.82077e-11, 1.19209e-07]) *)
Definition i201 := makepairF f36 f5.
Notation p268 := (BND r20 i201). (* BND((inv_b_hat - 1 / b) / (1 / b), [5.82077e-11, 2.14748e+09]) *)
Definition f167 := Float2 (1) (-19).
Definition i202 := makepairF f13 f167.
Notation p269 := (BND r12 i202). (* BND(1 / b, [4.65661e-10, 1.90735e-06]) *)
Lemma t141 : p63 -> p269 -> p268.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i202 i201 h0 h1 _) ; finalize.
Qed.
Lemma l251 : p5 -> p4 -> p3 -> s1 -> p268 (* BND((inv_b_hat - 1 / b) / (1 / b), [5.82077e-11, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l230 h0 h1 h2 h3).
 apply t141. exact h4. refine (subset r12 i180 i202 h5 _) ; finalize.
Qed.
Lemma l250 : p5 -> p4 -> p3 -> s1 -> p267 (* BND((inv_b_hat - 1 / b) / (1 / b), [5.82077e-11, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l251 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t142 : p18 -> p267 -> p266.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i200 h0 h1) ; finalize.
Qed.
Lemma l249 : p5 -> p4 -> p3 -> s1 -> p266 (* REL(inv_b_hat, 1 / b, [5.82077e-11, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l250 h0 h1 h2 h3).
 apply t142. exact h4. exact h5.
Qed.
Lemma t143 : p266 -> p102 -> p265.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i200 i63 i199 h0 h1 _) ; finalize.
Qed.
Lemma l248 : p5 -> p4 -> p3 -> s1 -> p265 (* BND(inv_b_hat - 1 / b, [2.71051e-20, 7.10543e-14]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l249 h0 h1 h2 h3).
 assert (h5 := l100 h0 h1 h2 h3).
 apply t143. exact h4. exact h5.
Qed.
Lemma l247 : p5 -> p4 -> p3 -> s1 -> p264 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 7.10543e-14]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l248 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t144 : p39 -> p264 -> p263.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i198 i197 h0 h1 _) ; finalize.
Qed.
Lemma l246 : p5 -> p4 -> p3 -> s1 -> p263 (* BND(-1 * (inv_b_hat - 1 / b), [-7.10543e-14, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l247 h0 h1 h2 h3).
 apply t144. exact h4. exact h5.
Qed.
Definition f168 := Float2 (-1) (-12).
Definition i203 := makepairF f168 f3.
Notation p270 := (BND r8 i203). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000244141, -2.38419e-07]) *)
Definition i204 := makepairF f168 f18.
Notation p271 := (BND r8 i204). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000244141, -1.11022e-16]) *)
Definition f169 := Float2 (-1) (-43).
Definition i205 := makepairF f169 f18.
Notation p272 := (BND r9 i205). (* BND(-1 * (inv_b_hat - 1 / b), [-1.13687e-13, -1.11022e-16]) *)
Lemma t145 : p272 -> p47 -> p271.
 intros h0 h1.
 refine (div_np r9 r12 i205 i24 i204 h0 h1 _) ; finalize.
Qed.
Lemma l253 : p5 -> p4 -> p3 -> s1 -> p271 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000244141, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l246 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t145. refine (subset r9 i197 i205 h4 _) ; finalize. exact h5.
Qed.
Lemma l252 : p5 -> p4 -> p3 -> s1 -> p270 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.000244141, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l253 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t146 : p263 -> p270 -> p262.
 intros h0 h1.
 refine (div_nn r9 r8 i197 i203 i196 h0 h1 _) ; finalize.
Qed.
Lemma l245 : p5 -> p4 -> p3 -> s1 -> p262 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.54747e-13, 2.98023e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l246 h0 h1 h2 h3).
 assert (h5 := l252 h0 h1 h2 h3).
 apply t146. exact h4. exact h5.
Qed.
Lemma t147 : p14 -> p18 -> p262 -> p261.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i196 h0 h1 h2) ; finalize.
Qed.
Lemma l244 : p5 -> p4 -> p3 -> s1 -> p261 (* BND(1 / b, [4.54747e-13, 2.98023e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l245 h0 h1 h2 h3).
 apply t147. exact h4. exact h5. exact h6.
Qed.
Lemma l243 : p5 -> p4 -> p3 -> s1 -> p260 (* BND(1 / b, [4.65661e-10, 2.98023e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l244 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t148 : p99 -> p260 -> p98.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i61 i195 i60 h0 h1 _) ; finalize.
Qed.
Lemma l96 : p5 -> p4 -> p3 -> s1 -> p98 (* BND(inv_b_hat - 1 / b, [5.42101e-20, 3.55272e-14]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l97 h0 h1 h2 h3).
 assert (h5 := l243 h0 h1 h2 h3).
 apply t148. exact h4. exact h5.
Qed.
Lemma l95 : p5 -> p4 -> p3 -> s1 -> p97 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 3.55272e-14]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l96 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t149 : p39 -> p97 -> p96.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i59 i58 h0 h1 _) ; finalize.
Qed.
Lemma l94 : p5 -> p4 -> p3 -> s1 -> p96 (* BND(-1 * (inv_b_hat - 1 / b), [-3.55272e-14, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l95 h0 h1 h2 h3).
 apply t149. exact h4. exact h5.
Qed.
Definition f170 := Float2 (-1) (-13).
Definition i206 := makepairF f170 f3.
Notation p273 := (BND r8 i206). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00012207, -2.38419e-07]) *)
Definition i207 := makepairF f170 f18.
Notation p274 := (BND r8 i207). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00012207, -1.11022e-16]) *)
Definition f171 := Float2 (-1) (-44).
Definition i208 := makepairF f171 f18.
Notation p275 := (BND r9 i208). (* BND(-1 * (inv_b_hat - 1 / b), [-5.68434e-14, -1.11022e-16]) *)
Lemma t150 : p275 -> p47 -> p274.
 intros h0 h1.
 refine (div_np r9 r12 i208 i24 i207 h0 h1 _) ; finalize.
Qed.
Lemma l255 : p5 -> p4 -> p3 -> s1 -> p274 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00012207, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l94 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t150. refine (subset r9 i58 i208 h4 _) ; finalize. exact h5.
Qed.
Lemma l254 : p5 -> p4 -> p3 -> s1 -> p273 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.00012207, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l255 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t151 : p96 -> p273 -> p95.
 intros h0 h1.
 refine (div_nn r9 r8 i58 i206 i57 h0 h1 _) ; finalize.
Qed.
Lemma l93 : p5 -> p4 -> p3 -> s1 -> p95 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [9.09495e-13, 1.49012e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l94 h0 h1 h2 h3).
 assert (h5 := l254 h0 h1 h2 h3).
 apply t151. exact h4. exact h5.
Qed.
Lemma t152 : p14 -> p18 -> p95 -> p94.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i57 h0 h1 h2) ; finalize.
Qed.
Lemma l92 : p5 -> p4 -> p3 -> s1 -> p94 (* BND(1 / b, [9.09495e-13, 1.49012e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l93 h0 h1 h2 h3).
 apply t152. exact h4. exact h5. exact h6.
Qed.
Lemma l91 : p5 -> p4 -> p3 -> s1 -> p93 (* BND(1 / b, [4.65661e-10, 1.49012e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l92 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition i209 := makepairF f13 f4.
Notation p276 := (BND r12 i209). (* BND(1 / b, [4.65661e-10, 2.38419e-07]) *)
Lemma t153 : p63 -> p276 -> p92.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i209 i55 h0 h1 _) ; finalize.
Qed.
Lemma l90 : p5 -> p4 -> p3 -> s1 -> p92 (* BND((inv_b_hat - 1 / b) / (1 / b), [4.65661e-10, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l91 h0 h1 h2 h3).
 apply t153. exact h4. refine (subset r12 i56 i209 h5 _) ; finalize.
Qed.
Lemma l89 : p5 -> p4 -> p3 -> s1 -> p91 (* BND((inv_b_hat - 1 / b) / (1 / b), [4.65661e-10, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l90 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t154 : p18 -> p91 -> p90.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i54 h0 h1) ; finalize.
Qed.
Lemma l88 : p5 -> p4 -> p3 -> s1 -> p90 (* REL(inv_b_hat, 1 / b, [4.65661e-10, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l89 h0 h1 h2 h3).
 apply t154. exact h4. exact h5.
Qed.
Definition f172 := Float2 (360288313787240217) (-82).
Definition i210 := makepairF f13 f172.
Notation p277 := (BND r12 i210). (* BND(1 / b, [4.65661e-10, 7.45059e-08]) *)
Definition f173 := Float2 (1) (-39).
Definition i211 := makepairF f173 f172.
Notation p278 := (BND r12 i211). (* BND(1 / b, [1.81899e-12, 7.45059e-08]) *)
Notation p279 := (BND r19 i211). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.81899e-12, 7.45059e-08]) *)
Definition f174 := Float2 (-360288313787240217) (-104).
Definition i212 := makepairF f174 f18.
Notation p280 := (BND r9 i212). (* BND(-1 * (inv_b_hat - 1 / b), [-1.77636e-14, -1.11022e-16]) *)
Definition f175 := Float2 (360288313787240217) (-104).
Definition i213 := makepairF f16 f175.
Notation p281 := (BND r11 i213). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.77636e-14]) *)
Definition f176 := Float2 (1) (-63).
Definition i214 := makepairF f176 f175.
Notation p282 := (BND r11 i214). (* BND(inv_b_hat - 1 / b, [1.0842e-19, 1.77636e-14]) *)
Definition i215 := makepairF f15 f32.
Notation p283 := (REL _inv_b_hat r12 i215). (* REL(inv_b_hat, 1 / b, [2.32831e-10, 1.19209e-07]) *)
Notation p284 := (BND r20 i215). (* BND((inv_b_hat - 1 / b) / (1 / b), [2.32831e-10, 1.19209e-07]) *)
Definition i216 := makepairF f15 f5.
Notation p285 := (BND r20 i216). (* BND((inv_b_hat - 1 / b) / (1 / b), [2.32831e-10, 2.14748e+09]) *)
Definition f177 := Float2 (1) (-21).
Definition i217 := makepairF f13 f177.
Notation p286 := (BND r12 i217). (* BND(1 / b, [4.65661e-10, 4.76837e-07]) *)
Lemma t155 : p63 -> p286 -> p285.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i217 i216 h0 h1 _) ; finalize.
Qed.
Lemma l264 : p5 -> p4 -> p3 -> s1 -> p285 (* BND((inv_b_hat - 1 / b) / (1 / b), [2.32831e-10, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l243 h0 h1 h2 h3).
 apply t155. exact h4. refine (subset r12 i195 i217 h5 _) ; finalize.
Qed.
Lemma l263 : p5 -> p4 -> p3 -> s1 -> p284 (* BND((inv_b_hat - 1 / b) / (1 / b), [2.32831e-10, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l264 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t156 : p18 -> p284 -> p283.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i215 h0 h1) ; finalize.
Qed.
Lemma l262 : p5 -> p4 -> p3 -> s1 -> p283 (* REL(inv_b_hat, 1 / b, [2.32831e-10, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l263 h0 h1 h2 h3).
 apply t156. exact h4. exact h5.
Qed.
Lemma t157 : p283 -> p93 -> p282.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i215 i56 i214 h0 h1 _) ; finalize.
Qed.
Lemma l261 : p5 -> p4 -> p3 -> s1 -> p282 (* BND(inv_b_hat - 1 / b, [1.0842e-19, 1.77636e-14]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l262 h0 h1 h2 h3).
 assert (h5 := l91 h0 h1 h2 h3).
 apply t157. exact h4. exact h5.
Qed.
Lemma l260 : p5 -> p4 -> p3 -> s1 -> p281 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.77636e-14]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l261 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t158 : p39 -> p281 -> p280.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i213 i212 h0 h1 _) ; finalize.
Qed.
Lemma l259 : p5 -> p4 -> p3 -> s1 -> p280 (* BND(-1 * (inv_b_hat - 1 / b), [-1.77636e-14, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l260 h0 h1 h2 h3).
 apply t158. exact h4. exact h5.
Qed.
Definition f178 := Float2 (-1) (-14).
Definition i218 := makepairF f178 f3.
Notation p287 := (BND r8 i218). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-6.10352e-05, -2.38419e-07]) *)
Definition i219 := makepairF f178 f18.
Notation p288 := (BND r8 i219). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-6.10352e-05, -1.11022e-16]) *)
Definition f179 := Float2 (-1) (-45).
Definition i220 := makepairF f179 f18.
Notation p289 := (BND r9 i220). (* BND(-1 * (inv_b_hat - 1 / b), [-2.84217e-14, -1.11022e-16]) *)
Lemma t159 : p289 -> p47 -> p288.
 intros h0 h1.
 refine (div_np r9 r12 i220 i24 i219 h0 h1 _) ; finalize.
Qed.
Lemma l266 : p5 -> p4 -> p3 -> s1 -> p288 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-6.10352e-05, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l259 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t159. refine (subset r9 i212 i220 h4 _) ; finalize. exact h5.
Qed.
Lemma l265 : p5 -> p4 -> p3 -> s1 -> p287 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-6.10352e-05, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l266 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t160 : p280 -> p287 -> p279.
 intros h0 h1.
 refine (div_nn r9 r8 i212 i218 i211 h0 h1 _) ; finalize.
Qed.
Lemma l258 : p5 -> p4 -> p3 -> s1 -> p279 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.81899e-12, 7.45059e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l259 h0 h1 h2 h3).
 assert (h5 := l265 h0 h1 h2 h3).
 apply t160. exact h4. exact h5.
Qed.
Lemma t161 : p14 -> p18 -> p279 -> p278.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i211 h0 h1 h2) ; finalize.
Qed.
Lemma l257 : p5 -> p4 -> p3 -> s1 -> p278 (* BND(1 / b, [1.81899e-12, 7.45059e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l258 h0 h1 h2 h3).
 apply t161. exact h4. exact h5. exact h6.
Qed.
Lemma l256 : p5 -> p4 -> p3 -> s1 -> p277 (* BND(1 / b, [4.65661e-10, 7.45059e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l257 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t162 : p90 -> p277 -> p89.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i54 i210 i53 h0 h1 _) ; finalize.
Qed.
Lemma l87 : p5 -> p4 -> p3 -> s1 -> p89 (* BND(inv_b_hat - 1 / b, [2.1684e-19, 8.88179e-15]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l88 h0 h1 h2 h3).
 assert (h5 := l256 h0 h1 h2 h3).
 apply t162. exact h4. exact h5.
Qed.
Lemma l86 : p5 -> p4 -> p3 -> s1 -> p88 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 8.88179e-15]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l87 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t163 : p39 -> p88 -> p87.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i52 i51 h0 h1 _) ; finalize.
Qed.
Lemma l85 : p5 -> p4 -> p3 -> s1 -> p87 (* BND(-1 * (inv_b_hat - 1 / b), [-8.88179e-15, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l86 h0 h1 h2 h3).
 apply t163. exact h4. exact h5.
Qed.
Definition f180 := Float2 (-1) (-15).
Definition i221 := makepairF f180 f3.
Notation p290 := (BND r8 i221). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-3.05176e-05, -2.38419e-07]) *)
Definition i222 := makepairF f180 f18.
Notation p291 := (BND r8 i222). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-3.05176e-05, -1.11022e-16]) *)
Definition f181 := Float2 (-1) (-46).
Definition i223 := makepairF f181 f18.
Notation p292 := (BND r9 i223). (* BND(-1 * (inv_b_hat - 1 / b), [-1.42109e-14, -1.11022e-16]) *)
Lemma t164 : p292 -> p47 -> p291.
 intros h0 h1.
 refine (div_np r9 r12 i223 i24 i222 h0 h1 _) ; finalize.
Qed.
Lemma l268 : p5 -> p4 -> p3 -> s1 -> p291 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-3.05176e-05, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l85 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t164. refine (subset r9 i51 i223 h4 _) ; finalize. exact h5.
Qed.
Lemma l267 : p5 -> p4 -> p3 -> s1 -> p290 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-3.05176e-05, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l268 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t165 : p87 -> p290 -> p86.
 intros h0 h1.
 refine (div_nn r9 r8 i51 i221 i50 h0 h1 _) ; finalize.
Qed.
Lemma l84 : p5 -> p4 -> p3 -> s1 -> p86 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [3.63798e-12, 3.72529e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l85 h0 h1 h2 h3).
 assert (h5 := l267 h0 h1 h2 h3).
 apply t165. exact h4. exact h5.
Qed.
Lemma t166 : p14 -> p18 -> p86 -> p85.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i50 h0 h1 h2) ; finalize.
Qed.
Lemma l83 : p5 -> p4 -> p3 -> s1 -> p85 (* BND(1 / b, [3.63798e-12, 3.72529e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l84 h0 h1 h2 h3).
 apply t166. exact h4. exact h5. exact h6.
Qed.
Lemma l82 : p5 -> p4 -> p3 -> s1 -> p84 (* BND(1 / b, [4.65661e-10, 3.72529e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l83 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition i224 := makepairF f13 f11.
Notation p293 := (BND r12 i224). (* BND(1 / b, [4.65661e-10, 5.96046e-08]) *)
Lemma t167 : p63 -> p293 -> p83.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i224 i48 h0 h1 _) ; finalize.
Qed.
Lemma l81 : p5 -> p4 -> p3 -> s1 -> p83 (* BND((inv_b_hat - 1 / b) / (1 / b), [1.86265e-09, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l82 h0 h1 h2 h3).
 apply t167. exact h4. refine (subset r12 i49 i224 h5 _) ; finalize.
Qed.
Lemma l80 : p5 -> p4 -> p3 -> s1 -> p82 (* BND((inv_b_hat - 1 / b) / (1 / b), [1.86265e-09, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l81 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t168 : p18 -> p82 -> p81.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i47 h0 h1) ; finalize.
Qed.
Lemma l79 : p5 -> p4 -> p3 -> s1 -> p81 (* REL(inv_b_hat, 1 / b, [1.86265e-09, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l80 h0 h1 h2 h3).
 apply t168. exact h4. exact h5.
Qed.
Definition f182 := Float2 (90072089184239495) (-82).
Definition i225 := makepairF f13 f182.
Notation p294 := (BND r12 i225). (* BND(1 / b, [4.65661e-10, 1.86265e-08]) *)
Definition i226 := makepairF f69 f182.
Notation p295 := (BND r12 i226). (* BND(1 / b, [7.27596e-12, 1.86265e-08]) *)
Notation p296 := (BND r19 i226). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.27596e-12, 1.86265e-08]) *)
Definition f183 := Float2 (-90072089184239495) (-104).
Definition i227 := makepairF f183 f18.
Notation p297 := (BND r9 i227). (* BND(-1 * (inv_b_hat - 1 / b), [-4.4409e-15, -1.11022e-16]) *)
Definition f184 := Float2 (90072089184239495) (-104).
Definition i228 := makepairF f16 f184.
Notation p298 := (BND r11 i228). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 4.4409e-15]) *)
Definition i229 := makepairF f76 f184.
Notation p299 := (BND r11 i229). (* BND(inv_b_hat - 1 / b, [4.33681e-19, 4.4409e-15]) *)
Definition f185 := Float2 (1) (-30).
Definition i230 := makepairF f185 f32.
Notation p300 := (REL _inv_b_hat r12 i230). (* REL(inv_b_hat, 1 / b, [9.31323e-10, 1.19209e-07]) *)
Notation p301 := (BND r20 i230). (* BND((inv_b_hat - 1 / b) / (1 / b), [9.31323e-10, 1.19209e-07]) *)
Definition i231 := makepairF f185 f5.
Notation p302 := (BND r20 i231). (* BND((inv_b_hat - 1 / b) / (1 / b), [9.31323e-10, 2.14748e+09]) *)
Definition f186 := Float2 (1) (-23).
Definition i232 := makepairF f13 f186.
Notation p303 := (BND r12 i232). (* BND(1 / b, [4.65661e-10, 1.19209e-07]) *)
Lemma t169 : p63 -> p303 -> p302.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i232 i231 h0 h1 _) ; finalize.
Qed.
Lemma l277 : p5 -> p4 -> p3 -> s1 -> p302 (* BND((inv_b_hat - 1 / b) / (1 / b), [9.31323e-10, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l256 h0 h1 h2 h3).
 apply t169. exact h4. refine (subset r12 i210 i232 h5 _) ; finalize.
Qed.
Lemma l276 : p5 -> p4 -> p3 -> s1 -> p301 (* BND((inv_b_hat - 1 / b) / (1 / b), [9.31323e-10, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l277 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t170 : p18 -> p301 -> p300.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i230 h0 h1) ; finalize.
Qed.
Lemma l275 : p5 -> p4 -> p3 -> s1 -> p300 (* REL(inv_b_hat, 1 / b, [9.31323e-10, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l276 h0 h1 h2 h3).
 apply t170. exact h4. exact h5.
Qed.
Lemma t171 : p300 -> p84 -> p299.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i230 i49 i229 h0 h1 _) ; finalize.
Qed.
Lemma l274 : p5 -> p4 -> p3 -> s1 -> p299 (* BND(inv_b_hat - 1 / b, [4.33681e-19, 4.4409e-15]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l275 h0 h1 h2 h3).
 assert (h5 := l82 h0 h1 h2 h3).
 apply t171. exact h4. exact h5.
Qed.
Lemma l273 : p5 -> p4 -> p3 -> s1 -> p298 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 4.4409e-15]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l274 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t172 : p39 -> p298 -> p297.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i228 i227 h0 h1 _) ; finalize.
Qed.
Lemma l272 : p5 -> p4 -> p3 -> s1 -> p297 (* BND(-1 * (inv_b_hat - 1 / b), [-4.4409e-15, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l273 h0 h1 h2 h3).
 apply t172. exact h4. exact h5.
Qed.
Definition f187 := Float2 (-1) (-16).
Definition i233 := makepairF f187 f3.
Notation p304 := (BND r8 i233). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-1.52588e-05, -2.38419e-07]) *)
Definition i234 := makepairF f187 f18.
Notation p305 := (BND r8 i234). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-1.52588e-05, -1.11022e-16]) *)
Definition f188 := Float2 (-1) (-47).
Definition i235 := makepairF f188 f18.
Notation p306 := (BND r9 i235). (* BND(-1 * (inv_b_hat - 1 / b), [-7.10543e-15, -1.11022e-16]) *)
Lemma t173 : p306 -> p47 -> p305.
 intros h0 h1.
 refine (div_np r9 r12 i235 i24 i234 h0 h1 _) ; finalize.
Qed.
Lemma l279 : p5 -> p4 -> p3 -> s1 -> p305 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-1.52588e-05, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l272 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t173. refine (subset r9 i227 i235 h4 _) ; finalize. exact h5.
Qed.
Lemma l278 : p5 -> p4 -> p3 -> s1 -> p304 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-1.52588e-05, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l279 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t174 : p297 -> p304 -> p296.
 intros h0 h1.
 refine (div_nn r9 r8 i227 i233 i226 h0 h1 _) ; finalize.
Qed.
Lemma l271 : p5 -> p4 -> p3 -> s1 -> p296 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.27596e-12, 1.86265e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l272 h0 h1 h2 h3).
 assert (h5 := l278 h0 h1 h2 h3).
 apply t174. exact h4. exact h5.
Qed.
Lemma t175 : p14 -> p18 -> p296 -> p295.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i226 h0 h1 h2) ; finalize.
Qed.
Lemma l270 : p5 -> p4 -> p3 -> s1 -> p295 (* BND(1 / b, [7.27596e-12, 1.86265e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l271 h0 h1 h2 h3).
 apply t175. exact h4. exact h5. exact h6.
Qed.
Lemma l269 : p5 -> p4 -> p3 -> s1 -> p294 (* BND(1 / b, [4.65661e-10, 1.86265e-08]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l270 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t176 : p81 -> p294 -> p80.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i47 i225 i46 h0 h1 _) ; finalize.
Qed.
Lemma l78 : p5 -> p4 -> p3 -> s1 -> p80 (* BND(inv_b_hat - 1 / b, [8.67362e-19, 2.22045e-15]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l79 h0 h1 h2 h3).
 assert (h5 := l269 h0 h1 h2 h3).
 apply t176. exact h4. exact h5.
Qed.
Lemma l77 : p5 -> p4 -> p3 -> s1 -> p79 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.22045e-15]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l78 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t177 : p39 -> p79 -> p78.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i45 i44 h0 h1 _) ; finalize.
Qed.
Lemma l76 : p5 -> p4 -> p3 -> s1 -> p78 (* BND(-1 * (inv_b_hat - 1 / b), [-2.22045e-15, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l77 h0 h1 h2 h3).
 apply t177. exact h4. exact h5.
Qed.
Definition f189 := Float2 (-1) (-17).
Definition i236 := makepairF f189 f3.
Notation p307 := (BND r8 i236). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-7.62939e-06, -2.38419e-07]) *)
Definition i237 := makepairF f189 f18.
Notation p308 := (BND r8 i237). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-7.62939e-06, -1.11022e-16]) *)
Definition f190 := Float2 (-1) (-48).
Definition i238 := makepairF f190 f18.
Notation p309 := (BND r9 i238). (* BND(-1 * (inv_b_hat - 1 / b), [-3.55271e-15, -1.11022e-16]) *)
Lemma t178 : p309 -> p47 -> p308.
 intros h0 h1.
 refine (div_np r9 r12 i238 i24 i237 h0 h1 _) ; finalize.
Qed.
Lemma l281 : p5 -> p4 -> p3 -> s1 -> p308 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-7.62939e-06, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l76 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t178. refine (subset r9 i44 i238 h4 _) ; finalize. exact h5.
Qed.
Lemma l280 : p5 -> p4 -> p3 -> s1 -> p307 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-7.62939e-06, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l281 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t179 : p78 -> p307 -> p77.
 intros h0 h1.
 refine (div_nn r9 r8 i44 i236 i43 h0 h1 _) ; finalize.
Qed.
Lemma l75 : p5 -> p4 -> p3 -> s1 -> p77 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.45519e-11, 9.31324e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l76 h0 h1 h2 h3).
 assert (h5 := l280 h0 h1 h2 h3).
 apply t179. exact h4. exact h5.
Qed.
Lemma t180 : p14 -> p18 -> p77 -> p76.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i43 h0 h1 h2) ; finalize.
Qed.
Lemma l74 : p5 -> p4 -> p3 -> s1 -> p76 (* BND(1 / b, [1.45519e-11, 9.31324e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l75 h0 h1 h2 h3).
 apply t180. exact h4. exact h5. exact h6.
Qed.
Lemma l73 : p5 -> p4 -> p3 -> s1 -> p75 (* BND(1 / b, [4.65661e-10, 9.31324e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l74 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition f191 := Float2 (1) (-26).
Definition i239 := makepairF f13 f191.
Notation p310 := (BND r12 i239). (* BND(1 / b, [4.65661e-10, 1.49012e-08]) *)
Lemma t181 : p63 -> p310 -> p74.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i239 i41 h0 h1 _) ; finalize.
Qed.
Lemma l72 : p5 -> p4 -> p3 -> s1 -> p74 (* BND((inv_b_hat - 1 / b) / (1 / b), [7.45058e-09, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l73 h0 h1 h2 h3).
 apply t181. exact h4. refine (subset r12 i42 i239 h5 _) ; finalize.
Qed.
Lemma l71 : p5 -> p4 -> p3 -> s1 -> p73 (* BND((inv_b_hat - 1 / b) / (1 / b), [7.45058e-09, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l72 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t182 : p18 -> p73 -> p72.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i40 h0 h1) ; finalize.
Qed.
Lemma l70 : p5 -> p4 -> p3 -> s1 -> p72 (* REL(inv_b_hat, 1 / b, [7.45058e-09, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l71 h0 h1 h2 h3).
 apply t182. exact h4. exact h5.
Qed.
Definition f192 := Float2 (360288399686680863) (-86).
Definition i240 := makepairF f13 f192.
Notation p311 := (BND r12 i240). (* BND(1 / b, [4.65661e-10, 4.65662e-09]) *)
Definition i241 := makepairF f63 f192.
Notation p312 := (BND r12 i241). (* BND(1 / b, [2.91038e-11, 4.65662e-09]) *)
Notation p313 := (BND r19 i241). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.91038e-11, 4.65662e-09]) *)
Definition f193 := Float2 (-360288399686680863) (-108).
Definition i242 := makepairF f193 f18.
Notation p314 := (BND r9 i242). (* BND(-1 * (inv_b_hat - 1 / b), [-1.11022e-15, -1.11022e-16]) *)
Definition f194 := Float2 (360288399686680863) (-108).
Definition i243 := makepairF f16 f194.
Notation p315 := (BND r11 i243). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.11022e-15]) *)
Definition f195 := Float2 (1) (-59).
Definition i244 := makepairF f195 f194.
Notation p316 := (BND r11 i244). (* BND(inv_b_hat - 1 / b, [1.73472e-18, 1.11022e-15]) *)
Definition f196 := Float2 (1) (-28).
Definition i245 := makepairF f196 f32.
Notation p317 := (REL _inv_b_hat r12 i245). (* REL(inv_b_hat, 1 / b, [3.72529e-09, 1.19209e-07]) *)
Notation p318 := (BND r20 i245). (* BND((inv_b_hat - 1 / b) / (1 / b), [3.72529e-09, 1.19209e-07]) *)
Definition i246 := makepairF f196 f5.
Notation p319 := (BND r20 i246). (* BND((inv_b_hat - 1 / b) / (1 / b), [3.72529e-09, 2.14748e+09]) *)
Definition i247 := makepairF f13 f27.
Notation p320 := (BND r12 i247). (* BND(1 / b, [4.65661e-10, 2.98023e-08]) *)
Lemma t183 : p63 -> p320 -> p319.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i247 i246 h0 h1 _) ; finalize.
Qed.
Lemma l290 : p5 -> p4 -> p3 -> s1 -> p319 (* BND((inv_b_hat - 1 / b) / (1 / b), [3.72529e-09, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l269 h0 h1 h2 h3).
 apply t183. exact h4. refine (subset r12 i225 i247 h5 _) ; finalize.
Qed.
Lemma l289 : p5 -> p4 -> p3 -> s1 -> p318 (* BND((inv_b_hat - 1 / b) / (1 / b), [3.72529e-09, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l290 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t184 : p18 -> p318 -> p317.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i245 h0 h1) ; finalize.
Qed.
Lemma l288 : p5 -> p4 -> p3 -> s1 -> p317 (* REL(inv_b_hat, 1 / b, [3.72529e-09, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l289 h0 h1 h2 h3).
 apply t184. exact h4. exact h5.
Qed.
Lemma t185 : p317 -> p75 -> p316.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i245 i42 i244 h0 h1 _) ; finalize.
Qed.
Lemma l287 : p5 -> p4 -> p3 -> s1 -> p316 (* BND(inv_b_hat - 1 / b, [1.73472e-18, 1.11022e-15]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l288 h0 h1 h2 h3).
 assert (h5 := l73 h0 h1 h2 h3).
 apply t185. exact h4. exact h5.
Qed.
Lemma l286 : p5 -> p4 -> p3 -> s1 -> p315 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.11022e-15]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l287 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t186 : p39 -> p315 -> p314.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i243 i242 h0 h1 _) ; finalize.
Qed.
Lemma l285 : p5 -> p4 -> p3 -> s1 -> p314 (* BND(-1 * (inv_b_hat - 1 / b), [-1.11022e-15, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l286 h0 h1 h2 h3).
 apply t186. exact h4. exact h5.
Qed.
Definition f197 := Float2 (-1) (-18).
Definition i248 := makepairF f197 f3.
Notation p321 := (BND r8 i248). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-3.8147e-06, -2.38419e-07]) *)
Definition i249 := makepairF f197 f18.
Notation p322 := (BND r8 i249). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-3.8147e-06, -1.11022e-16]) *)
Definition f198 := Float2 (-1) (-49).
Definition i250 := makepairF f198 f18.
Notation p323 := (BND r9 i250). (* BND(-1 * (inv_b_hat - 1 / b), [-1.77636e-15, -1.11022e-16]) *)
Lemma t187 : p323 -> p47 -> p322.
 intros h0 h1.
 refine (div_np r9 r12 i250 i24 i249 h0 h1 _) ; finalize.
Qed.
Lemma l292 : p5 -> p4 -> p3 -> s1 -> p322 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-3.8147e-06, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l285 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t187. refine (subset r9 i242 i250 h4 _) ; finalize. exact h5.
Qed.
Lemma l291 : p5 -> p4 -> p3 -> s1 -> p321 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-3.8147e-06, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l292 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t188 : p314 -> p321 -> p313.
 intros h0 h1.
 refine (div_nn r9 r8 i242 i248 i241 h0 h1 _) ; finalize.
Qed.
Lemma l284 : p5 -> p4 -> p3 -> s1 -> p313 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.91038e-11, 4.65662e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l285 h0 h1 h2 h3).
 assert (h5 := l291 h0 h1 h2 h3).
 apply t188. exact h4. exact h5.
Qed.
Lemma t189 : p14 -> p18 -> p313 -> p312.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i241 h0 h1 h2) ; finalize.
Qed.
Lemma l283 : p5 -> p4 -> p3 -> s1 -> p312 (* BND(1 / b, [2.91038e-11, 4.65662e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l284 h0 h1 h2 h3).
 apply t189. exact h4. exact h5. exact h6.
Qed.
Lemma l282 : p5 -> p4 -> p3 -> s1 -> p311 (* BND(1 / b, [4.65661e-10, 4.65662e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l283 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t190 : p72 -> p311 -> p71.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i40 i240 i39 h0 h1 _) ; finalize.
Qed.
Lemma l69 : p5 -> p4 -> p3 -> s1 -> p71 (* BND(inv_b_hat - 1 / b, [3.46945e-18, 5.55112e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l70 h0 h1 h2 h3).
 assert (h5 := l282 h0 h1 h2 h3).
 apply t190. exact h4. exact h5.
Qed.
Lemma l68 : p5 -> p4 -> p3 -> s1 -> p70 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 5.55112e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l69 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t191 : p39 -> p70 -> p69.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i38 i37 h0 h1 _) ; finalize.
Qed.
Lemma l67 : p5 -> p4 -> p3 -> s1 -> p69 (* BND(-1 * (inv_b_hat - 1 / b), [-5.55112e-16, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l68 h0 h1 h2 h3).
 apply t191. exact h4. exact h5.
Qed.
Definition f199 := Float2 (-1) (-19).
Definition i251 := makepairF f199 f3.
Notation p324 := (BND r8 i251). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-1.90735e-06, -2.38419e-07]) *)
Definition i252 := makepairF f199 f18.
Notation p325 := (BND r8 i252). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-1.90735e-06, -1.11022e-16]) *)
Definition f200 := Float2 (-1) (-50).
Definition i253 := makepairF f200 f18.
Notation p326 := (BND r9 i253). (* BND(-1 * (inv_b_hat - 1 / b), [-8.88178e-16, -1.11022e-16]) *)
Lemma t192 : p326 -> p47 -> p325.
 intros h0 h1.
 refine (div_np r9 r12 i253 i24 i252 h0 h1 _) ; finalize.
Qed.
Lemma l294 : p5 -> p4 -> p3 -> s1 -> p325 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-1.90735e-06, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l67 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t192. refine (subset r9 i37 i253 h4 _) ; finalize. exact h5.
Qed.
Lemma l293 : p5 -> p4 -> p3 -> s1 -> p324 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-1.90735e-06, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l294 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t193 : p69 -> p324 -> p68.
 intros h0 h1.
 refine (div_nn r9 r8 i37 i251 i36 h0 h1 _) ; finalize.
Qed.
Lemma l66 : p5 -> p4 -> p3 -> s1 -> p68 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [5.82077e-11, 2.32831e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l67 h0 h1 h2 h3).
 assert (h5 := l293 h0 h1 h2 h3).
 apply t193. exact h4. exact h5.
Qed.
Lemma t194 : p14 -> p18 -> p68 -> p67.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i36 h0 h1 h2) ; finalize.
Qed.
Lemma l65 : p5 -> p4 -> p3 -> s1 -> p67 (* BND(1 / b, [5.82077e-11, 2.32831e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l66 h0 h1 h2 h3).
 apply t194. exact h4. exact h5. exact h6.
Qed.
Lemma l64 : p5 -> p4 -> p3 -> s1 -> p66 (* BND(1 / b, [4.65661e-10, 2.32831e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l65 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition i254 := makepairF f13 f196.
Notation p327 := (BND r12 i254). (* BND(1 / b, [4.65661e-10, 3.72529e-09]) *)
Lemma t195 : p63 -> p327 -> p62.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i254 i34 h0 h1 _) ; finalize.
Qed.
Lemma l60 : p5 -> p4 -> p3 -> s1 -> p62 (* BND((inv_b_hat - 1 / b) / (1 / b), [2.98023e-08, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l64 h0 h1 h2 h3).
 apply t195. exact h4. refine (subset r12 i35 i254 h5 _) ; finalize.
Qed.
Lemma l56 : p5 -> p4 -> p3 -> s1 -> p57 (* BND((inv_b_hat - 1 / b) / (1 / b), [2.98023e-08, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l60 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t196 : p18 -> p57 -> p56.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i31 h0 h1) ; finalize.
Qed.
Lemma l55 : p5 -> p4 -> p3 -> s1 -> p56 (* REL(inv_b_hat, 1 / b, [2.98023e-08, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l56 h0 h1 h2 h3).
 apply t196. exact h4. exact h5.
Qed.
Definition f201 := Float2 (180144221318204433) (-87).
Definition i255 := makepairF f13 f201.
Notation p328 := (BND r12 i255). (* BND(1 / b, [4.65661e-10, 1.16415e-09]) *)
Definition i256 := makepairF f57 f201.
Notation p329 := (BND r12 i256). (* BND(1 / b, [1.16415e-10, 1.16415e-09]) *)
Notation p330 := (BND r19 i256). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.16415e-10, 1.16415e-09]) *)
Definition f202 := Float2 (-180144221318204433) (-109).
Definition i257 := makepairF f202 f18.
Notation p331 := (BND r9 i257). (* BND(-1 * (inv_b_hat - 1 / b), [-2.77556e-16, -1.11022e-16]) *)
Definition f203 := Float2 (180144221318204433) (-109).
Definition i258 := makepairF f16 f203.
Notation p332 := (BND r11 i258). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.77556e-16]) *)
Definition f204 := Float2 (1) (-57).
Definition i259 := makepairF f204 f203.
Notation p333 := (BND r11 i259). (* BND(inv_b_hat - 1 / b, [6.93889e-18, 2.77556e-16]) *)
Definition i260 := makepairF f191 f32.
Notation p334 := (REL _inv_b_hat r12 i260). (* REL(inv_b_hat, 1 / b, [1.49012e-08, 1.19209e-07]) *)
Notation p335 := (BND r20 i260). (* BND((inv_b_hat - 1 / b) / (1 / b), [1.49012e-08, 1.19209e-07]) *)
Definition i261 := makepairF f191 f5.
Notation p336 := (BND r20 i261). (* BND((inv_b_hat - 1 / b) / (1 / b), [1.49012e-08, 2.14748e+09]) *)
Definition i262 := makepairF f13 f40.
Notation p337 := (BND r12 i262). (* BND(1 / b, [4.65661e-10, 7.45058e-09]) *)
Lemma t197 : p63 -> p337 -> p336.
 intros h0 h1.
 refine (div_pp r11 r12 i9 i262 i261 h0 h1 _) ; finalize.
Qed.
Lemma l303 : p5 -> p4 -> p3 -> s1 -> p336 (* BND((inv_b_hat - 1 / b) / (1 / b), [1.49012e-08, 2.14748e+09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l61 h0 h1 h2 h3).
 assert (h5 := l282 h0 h1 h2 h3).
 apply t197. exact h4. refine (subset r12 i240 i262 h5 _) ; finalize.
Qed.
Lemma l302 : p5 -> p4 -> p3 -> s1 -> p335 (* BND((inv_b_hat - 1 / b) / (1 / b), [1.49012e-08, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l57 h3).
 assert (h5 := l303 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t198 : p18 -> p335 -> p334.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i260 h0 h1) ; finalize.
Qed.
Lemma l301 : p5 -> p4 -> p3 -> s1 -> p334 (* REL(inv_b_hat, 1 / b, [1.49012e-08, 1.19209e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l17 h3).
 assert (h5 := l302 h0 h1 h2 h3).
 apply t198. exact h4. exact h5.
Qed.
Lemma t199 : p334 -> p66 -> p333.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i260 i35 i259 h0 h1 _) ; finalize.
Qed.
Lemma l300 : p5 -> p4 -> p3 -> s1 -> p333 (* BND(inv_b_hat - 1 / b, [6.93889e-18, 2.77556e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l301 h0 h1 h2 h3).
 assert (h5 := l64 h0 h1 h2 h3).
 apply t199. exact h4. exact h5.
Qed.
Lemma l299 : p5 -> p4 -> p3 -> s1 -> p332 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 2.77556e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l300 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t200 : p39 -> p332 -> p331.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i258 i257 h0 h1 _) ; finalize.
Qed.
Lemma l298 : p5 -> p4 -> p3 -> s1 -> p331 (* BND(-1 * (inv_b_hat - 1 / b), [-2.77556e-16, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l299 h0 h1 h2 h3).
 apply t200. exact h4. exact h5.
Qed.
Definition f205 := Float2 (-1) (-20).
Definition i263 := makepairF f205 f3.
Notation p338 := (BND r8 i263). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-9.53674e-07, -2.38419e-07]) *)
Definition i264 := makepairF f205 f18.
Notation p339 := (BND r8 i264). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-9.53674e-07, -1.11022e-16]) *)
Definition f206 := Float2 (-1) (-51).
Definition i265 := makepairF f206 f18.
Notation p340 := (BND r9 i265). (* BND(-1 * (inv_b_hat - 1 / b), [-4.44089e-16, -1.11022e-16]) *)
Lemma t201 : p340 -> p47 -> p339.
 intros h0 h1.
 refine (div_np r9 r12 i265 i24 i264 h0 h1 _) ; finalize.
Qed.
Lemma l305 : p5 -> p4 -> p3 -> s1 -> p339 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-9.53674e-07, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l298 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t201. refine (subset r9 i257 i265 h4 _) ; finalize. exact h5.
Qed.
Lemma l304 : p5 -> p4 -> p3 -> s1 -> p338 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-9.53674e-07, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l305 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t202 : p331 -> p338 -> p330.
 intros h0 h1.
 refine (div_nn r9 r8 i257 i263 i256 h0 h1 _) ; finalize.
Qed.
Lemma l297 : p5 -> p4 -> p3 -> s1 -> p330 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.16415e-10, 1.16415e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l298 h0 h1 h2 h3).
 assert (h5 := l304 h0 h1 h2 h3).
 apply t202. exact h4. exact h5.
Qed.
Lemma t203 : p14 -> p18 -> p330 -> p329.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i256 h0 h1 h2) ; finalize.
Qed.
Lemma l296 : p5 -> p4 -> p3 -> s1 -> p329 (* BND(1 / b, [1.16415e-10, 1.16415e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l297 h0 h1 h2 h3).
 apply t203. exact h4. exact h5. exact h6.
Qed.
Lemma l295 : p5 -> p4 -> p3 -> s1 -> p328 (* BND(1 / b, [4.65661e-10, 1.16415e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l296 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t204 : p56 -> p328 -> p55.
 intros h0 h1.
 refine (error_of_rel_pp _inv_b_hat r12 i31 i255 i30 h0 h1 _) ; finalize.
Qed.
Lemma l54 : p5 -> p4 -> p3 -> s1 -> p55 (* BND(inv_b_hat - 1 / b, [1.38778e-17, 1.38778e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l55 h0 h1 h2 h3).
 assert (h5 := l295 h0 h1 h2 h3).
 apply t204. exact h4. exact h5.
Qed.
Lemma l53 : p5 -> p4 -> p3 -> s1 -> p54 (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.38778e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l54 h0 h1 h2 h3).
 assert (h5 := l61 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t205 : p39 -> p54 -> p53.
 intros h0 h1.
 refine (mul_np r10 r11 i21 i29 i28 h0 h1 _) ; finalize.
Qed.
Lemma l52 : p5 -> p4 -> p3 -> s1 -> p53 (* BND(-1 * (inv_b_hat - 1 / b), [-1.38778e-16, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l39 h3).
 assert (h5 := l53 h0 h1 h2 h3).
 apply t205. exact h4. exact h5.
Qed.
Definition f207 := Float2 (-1) (-21).
Definition i266 := makepairF f207 f3.
Notation p341 := (BND r8 i266). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-4.76837e-07, -2.38419e-07]) *)
Definition i267 := makepairF f207 f18.
Notation p342 := (BND r8 i267). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-4.76837e-07, -1.11022e-16]) *)
Definition f208 := Float2 (-1) (-52).
Definition i268 := makepairF f208 f18.
Notation p343 := (BND r9 i268). (* BND(-1 * (inv_b_hat - 1 / b), [-2.22045e-16, -1.11022e-16]) *)
Lemma t206 : p343 -> p47 -> p342.
 intros h0 h1.
 refine (div_np r9 r12 i268 i24 i267 h0 h1 _) ; finalize.
Qed.
Lemma l307 : p5 -> p4 -> p3 -> s1 -> p342 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-4.76837e-07, -1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l52 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply t206. refine (subset r9 i28 i268 h4 _) ; finalize. exact h5.
Qed.
Lemma l306 : p5 -> p4 -> p3 -> s1 -> p341 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-4.76837e-07, -2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l22 h0 h1 h2 h3).
 assert (h5 := l307 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t207 : p53 -> p341 -> p52.
 intros h0 h1.
 refine (div_nn r9 r8 i28 i266 i8 h0 h1 _) ; finalize.
Qed.
Lemma l51 : p5 -> p4 -> p3 -> s1 -> p52 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.32831e-10, 5.82077e-10]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l52 h0 h1 h2 h3).
 assert (h5 := l306 h0 h1 h2 h3).
 apply t207. exact h4. exact h5.
Qed.
Lemma t208 : p14 -> p18 -> p52 -> p13.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i8 h0 h1 h2) ; finalize.
Qed.
Lemma l12 : p5 -> p4 -> p3 -> s1 -> p13 (* BND(1 / b, [2.32831e-10, 5.82077e-10]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l13 h0 h1 h2 h3).
 assert (h5 := l17 h3).
 assert (h6 := l51 h0 h1 h2 h3).
 apply t208. exact h4. exact h5. exact h6.
Qed.
Lemma l11 : p5 -> p4 -> p3 -> s1 -> p12 (* BND(1 / b, [4.65661e-10, 5.82077e-10]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l12 h0 h1 h2 h3).
 assert (h5 := l47 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Definition i269 := makepairF f23 f9.
Notation p344 := (REL r6 r12 i269). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-1, 5.96046e-08]) *)
Lemma t209 : p344 -> p12 -> p6.
 intros h0 h1.
 refine (error_of_rel_op r6 r12 i269 i7 i3 h0 h1 _) ; finalize.
Qed.
Lemma l5 : p5 -> p4 -> p3 -> s1 -> p6 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-5.82077e-10, 3.46945e-17]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l6 h3).
 assert (h5 := l11 h0 h1 h2 h3).
 apply t209. refine (rel_subset r6 r12 i4 i269 h4 _) ; finalize. exact h5.
Qed.
Definition f209 := Float2 (1) (-54).
Definition i270 := makepairF f209 f16.
Notation p345 := (BND r13 i270). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [5.55112e-17, 1.11022e-16]) *)
Notation r23 := ((r6 - _inv_b_hat)%R).
Notation r22 := ((r23 + r11)%R).
Notation p346 := (BND r22 i270). (* BND(1 / float<24,-149,ne>(b) - inv_b_hat + (inv_b_hat - 1 / b), [5.55112e-17, 1.11022e-16]) *)
Definition f210 := Float2 (-1) (-54).
Definition f211 := Float2 (-5) (-57).
Definition i271 := makepairF f210 f211.
Notation p347 := (BND r23 i271). (* BND(1 / float<24,-149,ne>(b) - inv_b_hat, [-5.55112e-17, -3.46945e-17]) *)
Definition i272 := makepairF f208 f211.
Notation p348 := (BND r23 i272). (* BND(1 / float<24,-149,ne>(b) - inv_b_hat, [-2.22045e-16, -3.46945e-17]) *)
Notation r24 := ((r13 - r11)%R).
Notation p349 := (BND r24 i272). (* BND(1 / float<24,-149,ne>(b) - 1 / b - (inv_b_hat - 1 / b), [-2.22045e-16, -3.46945e-17]) *)
Definition f212 := Float2 (-11) (-57).
Definition f213 := Float2 (11) (-57).
Definition i273 := makepairF f212 f213.
Notation p350 := (BND r13 i273). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-7.63278e-17, 7.63278e-17]) *)
Definition f214 := Float2 (33) (-29).
Definition i274 := makepairF f10 f214.
Notation p351 := (REL r6 r12 i274). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 6.14673e-08]) *)
Definition f215 := Float2 (21) (-34).
Definition i275 := makepairF f13 f215.
Notation p352 := (BND r12 i275). (* BND(1 / b, [4.65661e-10, 1.22236e-09]) *)
Lemma t210 : p351 -> p352 -> p350.
 intros h0 h1.
 refine (error_of_rel_op r6 r12 i274 i275 i273 h0 h1 _) ; finalize.
Qed.
Lemma l313 : p5 -> p4 -> p3 -> s1 -> p350 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-7.63278e-17, 7.63278e-17]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l6 h3).
 assert (h5 := l295 h0 h1 h2 h3).
 apply t210. refine (rel_subset r6 r12 i4 i274 h4 _) ; finalize. refine (subset r12 i255 i275 h5 _) ; finalize.
Qed.
Definition f216 := Float2 (21) (-57).
Definition i276 := makepairF f16 f216.
Notation p353 := (BND r11 i276). (* BND(inv_b_hat - 1 / b, [1.11022e-16, 1.45717e-16]) *)
Lemma t211 : p350 -> p353 -> p349.
 intros h0 h1.
 refine (sub r13 r11 i273 i276 i272 h0 h1 _) ; finalize.
Qed.
Lemma l312 : p5 -> p4 -> p3 -> s1 -> p349 (* BND(1 / float<24,-149,ne>(b) - 1 / b - (inv_b_hat - 1 / b), [-2.22045e-16, -3.46945e-17]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l313 h0 h1 h2 h3).
 assert (h5 := l53 h0 h1 h2 h3).
 apply t211. exact h4. refine (subset r11 i29 i276 h5 _) ; finalize.
Qed.
Lemma t212 : p349 -> p348.
 intros h0.
 refine (sub_xars _ _ _ i272 h0) ; finalize.
Qed.
Lemma l311 : p5 -> p4 -> p3 -> s1 -> p348 (* BND(1 / float<24,-149,ne>(b) - inv_b_hat, [-2.22045e-16, -3.46945e-17]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l312 h0 h1 h2 h3).
 apply t212. exact h4.
Qed.
Definition i277 := makepairF f210 f16.
Notation p354 := (BND r23 i277). (* BND(1 / float<24,-149,ne>(b) - inv_b_hat, [-5.55112e-17, 1.11022e-16]) *)
Notation r26 := ((r6 - r6)%R).
Notation r25 := ((r26 - r18)%R).
Notation p355 := (BND r25 i277). (* BND(1 / float<24,-149,ne>(b) - 1 / float<24,-149,ne>(b) - (inv_b_hat - 1 / float<24,-149,ne>(b)), [-5.55112e-17, 1.11022e-16]) *)
Notation p356 := (BND r26 i18). (* BND(1 / float<24,-149,ne>(b) - 1 / float<24,-149,ne>(b), [0, 0]) *)
Lemma t213 : p356.
 refine (sub_refl _ i18 _) ; finalize.
Qed.
Lemma l316 : s1 -> p356 (* BND(1 / float<24,-149,ne>(b) - 1 / float<24,-149,ne>(b), [0, 0]) *).
 intros h0.
 apply t213.
Qed.
Definition i278 := makepairF f18 f209.
Notation p357 := (BND r18 i278). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-1.11022e-16, 5.55112e-17]) *)
Definition i279 := makepairF f13 f46.
Notation p358 := (ABS r6 i279). (* ABS(1 / float<24,-149,ne>(b), [4.65661e-10, 1.86265e-09]) *)
Notation p359 := (BND r6 i279). (* BND(1 / float<24,-149,ne>(b), [4.65661e-10, 1.86265e-09]) *)
Definition i280 := makepairF f21 f46.
Notation p360 := (BND r6 i280). (* BND(1 / float<24,-149,ne>(b), [-0, 1.86265e-09]) *)
Definition f217 := Float2 (3) (-31).
Definition i281 := makepairF f13 f217.
Notation p361 := (BND r12 i281). (* BND(1 / b, [4.65661e-10, 1.39698e-09]) *)
Definition f218 := Float2 (1) (-2).
Definition i282 := makepairF f23 f218.
Notation p362 := (REL r6 r12 i282). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-1, 0.25]) *)
Lemma t214 : p361 -> p362 -> p360.
 intros h0 h1.
 refine (bnd_of_bnd_rel_p r6 r12 i281 i282 i280 h0 h1 _) ; finalize.
Qed.
Lemma l320 : p5 -> p4 -> p3 -> s1 -> p360 (* BND(1 / float<24,-149,ne>(b), [-0, 1.86265e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l295 h0 h1 h2 h3).
 assert (h5 := l6 h3).
 apply t214. refine (subset r12 i255 i281 h4 _) ; finalize. refine (rel_subset r6 r12 i4 i282 h5 _) ; finalize.
Qed.
Notation p363 := (BND r6 i24). (* BND(1 / float<24,-149,ne>(b), [4.65661e-10, 1]) *)
Notation p364 := (BND r7 i25). (* BND(float<24,-149,ne>(b), [1, 2.14748e+09]) *)
Lemma t215 : p48 -> p364.
 intros h0.
 refine (float_round_ne _ _ _b i25 i25 h0 _) ; finalize.
Qed.
Lemma l322 : p3 -> s1 -> p364 (* BND(float<24,-149,ne>(b), [1, 2.14748e+09]) *).
 intros h0 h1.
 assert (h2 := l48 h0 h1).
 apply t215. exact h2.
Qed.
Lemma t216 : p21 -> p364 -> p363.
 intros h0 h1.
 refine (div_pp r3 r7 i12 i25 i24 h0 h1 _) ; finalize.
Qed.
Lemma l321 : p3 -> s1 -> p363 (* BND(1 / float<24,-149,ne>(b), [4.65661e-10, 1]) *).
 intros h0 h1.
 assert (h2 := l20 h1).
 assert (h3 := l322 h0 h1).
 apply t216. exact h2. exact h3.
Qed.
Lemma l319 : p5 -> p4 -> p3 -> s1 -> p359 (* BND(1 / float<24,-149,ne>(b), [4.65661e-10, 1.86265e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l320 h0 h1 h2 h3).
 assert (h5 := l321 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t217 : p359 -> p358.
 intros h0.
 refine (abs_of_bnd_p r6 i279 i279 h0 _) ; finalize.
Qed.
Lemma l318 : p5 -> p4 -> p3 -> s1 -> p358 (* ABS(1 / float<24,-149,ne>(b), [4.65661e-10, 1.86265e-09]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l319 h0 h1 h2 h3).
 apply t217. exact h4.
Qed.
Lemma t218 : p358 -> p357.
 intros h0.
 refine (float_absolute_wide_ne _ _ r6 i279 i278 h0 _) ; finalize.
Qed.
Lemma l317 : p5 -> p4 -> p3 -> s1 -> p357 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-1.11022e-16, 5.55112e-17]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l318 h0 h1 h2 h3).
 apply t218. exact h4.
Qed.
Lemma t219 : p356 -> p357 -> p355.
 intros h0 h1.
 refine (sub r26 r18 i18 i278 i277 h0 h1 _) ; finalize.
Qed.
Lemma l315 : p5 -> p4 -> p3 -> s1 -> p355 (* BND(1 / float<24,-149,ne>(b) - 1 / float<24,-149,ne>(b) - (inv_b_hat - 1 / float<24,-149,ne>(b)), [-5.55112e-17, 1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l316 h3).
 assert (h5 := l317 h0 h1 h2 h3).
 apply t219. exact h4. exact h5.
Qed.
Lemma t220 : p355 -> p354.
 intros h0.
 refine (sub_xars _ _ _ i277 h0) ; finalize.
Qed.
Lemma l314 : p5 -> p4 -> p3 -> s1 -> p354 (* BND(1 / float<24,-149,ne>(b) - inv_b_hat, [-5.55112e-17, 1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l315 h0 h1 h2 h3).
 apply t220. exact h4.
Qed.
Lemma l310 : p5 -> p4 -> p3 -> s1 -> p347 (* BND(1 / float<24,-149,ne>(b) - inv_b_hat, [-5.55112e-17, -3.46945e-17]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l311 h0 h1 h2 h3).
 assert (h5 := l314 h0 h1 h2 h3).
 apply intersect with (1 := h4) (2 := h5). finalize.
Qed.
Lemma t221 : p347 -> p353 -> p346.
 intros h0 h1.
 refine (add r23 r11 i271 i276 i270 h0 h1 _) ; finalize.
Qed.
Lemma l309 : p5 -> p4 -> p3 -> s1 -> p346 (* BND(1 / float<24,-149,ne>(b) - inv_b_hat + (inv_b_hat - 1 / b), [5.55112e-17, 1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l310 h0 h1 h2 h3).
 assert (h5 := l53 h0 h1 h2 h3).
 apply t221. exact h4. refine (subset r11 i29 i276 h5 _) ; finalize.
Qed.
Lemma t222 : p346 -> p345.
 intros h0.
 refine (sub_xals _ _ _ i270 h0) ; finalize.
Qed.
Lemma l308 : p5 -> p4 -> p3 -> s1 -> p345 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [5.55112e-17, 1.11022e-16]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l309 h0 h1 h2 h3).
 apply t222. exact h4.
Qed.
Lemma l4 : p5 -> p4 -> p3 -> s1 -> False.
 intros h0 h1 h2 h3.
 assert (h4 := l5 h0 h1 h2 h3).
 assert (h5 := l308 h0 h1 h2 h3).
 apply absurd_intersect with (1 := h4) (2 := h5). finalize.
Qed.
Notation p365 := ((f3 <= _exact_alpha)%R). (* BND(exact_alpha, [-2.38419e-07, inf]) *)
Lemma l324 : s1 -> s2.
 intros h0.
 assert (h1 := h0).
 exact (proj2 h1).
Qed.
Lemma l326 : p4 -> p3 -> s1 -> p4 (* BND(exact_alpha, [-inf, 2.38419e-07]) *).
 intros h0 h1 h2.
 assert (h3 := h0).
 exact (h3).
Qed.
Lemma l327 : p365 -> p4 -> p3 -> s1 -> p365 (* BND(exact_alpha, [-2.38419e-07, inf]) *).
 intros h0 h1 h2 h3.
 assert (h4 := h0).
 exact (h4).
Qed.
Lemma l325 : p365 -> p4 -> p3 -> s1 -> p2 (* BND(exact_alpha, [-2.38419e-07, 2.38419e-07]) *).
 intros h0 h1 h2 h3.
 assert (h4 := l326 h1 h2 h3).
 assert (h5 := l327 h0 h1 h2 h3).
 apply intersect_hh with (1 := h4) (2 := h5). finalize.
Qed.
Lemma l323 : p365 -> p4 -> p3 -> s1 -> False.
 intros h0 h1 h2 h3.
 assert (h4 := l324 h3).
 assert (h5 := l325 h0 h1 h2 h3).
 refine (simplify (Tatom false (Abnd 0%nat i2)) Tfalse (Abnd 0%nat i2) (List.cons _exact_alpha List.nil) h5 h4 _) ; finalize.
Qed.
Lemma l3 : p4 -> p3 -> s1 -> False.
 intros h0 h1 h2.
 apply (union _exact_alpha f3).
 intro h3. (* [-inf, -2.38419e-07] *)
 apply (l4 h3 h0 h1 h2).
 intro h3. (* [-2.38419e-07, inf] *)
 apply (l323 h3 h0 h1 h2).
Qed.
Notation p366 := ((f4 <= _exact_alpha)%R). (* BND(exact_alpha, [2.38419e-07, inf]) *)
Notation p367 := (BND r11 i27). (* BND(inv_b_hat - 1 / b, [-1, -1.11022e-16]) *)
Notation p368 := (BND r21 i27). (* BND(-1 * (inv_b_hat - 1 / b) / -1, [-1, -1.11022e-16]) *)
Notation p369 := (BND r9 i9). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1]) *)
Notation p370 := (BND r14 i9). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b) * (1 / b), [1.11022e-16, 1]) *)
Definition i283 := makepairF f4 f1.
Notation p371 := (BND r8 i283). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 1]) *)
Notation p372 := (BND _exact_alpha i283). (* BND(exact_alpha, [2.38419e-07, 1]) *)
Lemma l335 : p366 -> p3 -> s1 -> p366 (* BND(exact_alpha, [2.38419e-07, inf]) *).
 intros h0 h1 h2.
 assert (h3 := h0).
 exact (h3).
Qed.
Lemma l334 : p366 -> p3 -> s1 -> p372 (* BND(exact_alpha, [2.38419e-07, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l29 h2).
 assert (h4 := l335 h0 h1 h2).
 apply intersect_bh with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t223 : p372 -> p34 -> p371.
 intros h0 h1.
 refine (bnd_of_rel_bnd_p _exact_alpha r8 i283 i18 i283 h0 h1 _) ; finalize.
Qed.
Lemma l333 : p366 -> p3 -> s1 -> p371 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l334 h0 h1 h2).
 assert (h4 := l34 h2).
 apply t223. exact h3. exact h4.
Qed.
Lemma t224 : p371 -> p47 -> p370.
 intros h0 h1.
 refine (mul_pp r8 r12 i283 i24 i9 h0 h1 _) ; finalize.
Qed.
Lemma l332 : p366 -> p3 -> s1 -> p370 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b) * (1 / b), [1.11022e-16, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l333 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply t224. exact h3. exact h4.
Qed.
Lemma t225 : p18 -> p370 -> p369.
 intros h0 h1.
 refine (div_xilu r9 _ i9 h0 h1) ; finalize.
Qed.
Lemma l331 : p366 -> p3 -> s1 -> p369 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l332 h0 h1 h2).
 apply t225. exact h3. exact h4.
Qed.
Lemma t226 : p369 -> p39 -> p368.
 intros h0 h1.
 refine (div_pn r9 r10 i9 i21 i27 h0 h1 _) ; finalize.
Qed.
Lemma l330 : p366 -> p3 -> s1 -> p368 (* BND(-1 * (inv_b_hat - 1 / b) / -1, [-1, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l331 h0 h1 h2).
 assert (h4 := l39 h2).
 apply t226. exact h3. exact h4.
Qed.
Lemma t227 : p64 -> p368 -> p367.
 intros h0 h1.
 refine (mul_xiru _ r11 i27 h0 h1) ; finalize.
Qed.
Lemma l329 : p366 -> p3 -> s1 -> p367 (* BND(inv_b_hat - 1 / b, [-1, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l62 h2).
 assert (h4 := l330 h0 h1 h2).
 apply t227. exact h3. exact h4.
Qed.
Definition f219 := Float2 (-1407373272942007) (-104).
Definition f220 := Float2 (-1) (-55).
Definition i284 := makepairF f219 f220.
Notation p373 := (BND r11 i284). (* BND(inv_b_hat - 1 / b, [-6.93889e-17, -2.77556e-17]) *)
Definition i285 := makepairF f33 f10.
Notation p374 := (REL _inv_b_hat r12 i285). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -5.96046e-08]) *)
Notation p375 := (BND r20 i285). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -5.96046e-08]) *)
Definition f221 := Float2 (-1) (31).
Definition i286 := makepairF f221 f10.
Notation p376 := (BND r20 i286). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -5.96046e-08]) *)
Definition f222 := Float2 (351843360178495) (-78).
Definition i287 := makepairF f13 f222.
Notation p377 := (BND r12 i287). (* BND(1 / b, [4.65661e-10, 1.16415e-09]) *)
Definition i288 := makepairF f57 f222.
Notation p378 := (BND r12 i288). (* BND(1 / b, [1.16415e-10, 1.16415e-09]) *)
Lemma t228 : p369 -> p15.
 intros h0.
 refine (abs_of_bnd_p r9 i9 i9 h0 _) ; finalize.
Qed.
Lemma l343 : p366 -> p3 -> s1 -> p15 (* ABS(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l331 h0 h1 h2).
 apply t228. exact h3.
Qed.
Lemma t229 : p15 -> p14.
 intros h0.
 refine (nzr_of_abs r9 i9 h0 _) ; finalize.
Qed.
Lemma l342 : p366 -> p3 -> s1 -> p14 (* NZR(-1 * (inv_b_hat - 1 / b)) *).
 intros h0 h1 h2.
 assert (h3 := l343 h0 h1 h2).
 apply t229. exact h3.
Qed.
Notation p379 := (BND r19 i288). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.16415e-10, 1.16415e-09]) *)
Definition f223 := Float2 (351843360178495) (-100).
Definition i289 := makepairF f16 f223.
Notation p380 := (BND r9 i289). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.77555e-16]) *)
Definition f224 := Float2 (-351843360178495) (-100).
Definition i290 := makepairF f224 f18.
Notation p381 := (BND r11 i290). (* BND(inv_b_hat - 1 / b, [-2.77555e-16, -1.11022e-16]) *)
Definition f225 := Float2 (1) (-51).
Definition i291 := makepairF f224 f225.
Notation p382 := (BND r11 i291). (* BND(inv_b_hat - 1 / b, [-2.77555e-16, 4.44089e-16]) *)
Notation p383 := (BND r17 i291). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-2.77555e-16, 4.44089e-16]) *)
Definition f226 := Float2 (1) (-52).
Definition i292 := makepairF f18 f226.
Notation p384 := (BND r18 i292). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-1.11022e-16, 2.22045e-16]) *)
Notation p385 := (ABS r6 i254). (* ABS(1 / float<24,-149,ne>(b), [4.65661e-10, 3.72529e-09]) *)
Notation p386 := (BND r6 i254). (* BND(1 / float<24,-149,ne>(b), [4.65661e-10, 3.72529e-09]) *)
Definition i293 := makepairF f21 f196.
Notation p387 := (BND r6 i293). (* BND(1 / float<24,-149,ne>(b), [-0, 3.72529e-09]) *)
Definition f227 := Float2 (422211768812115) (-77).
Definition i294 := makepairF f13 f227.
Notation p388 := (BND r12 i294). (* BND(1 / b, [4.65661e-10, 2.79396e-09]) *)
Definition i295 := makepairF f36 f227.
Notation p389 := (BND r12 i295). (* BND(1 / b, [5.82077e-11, 2.79396e-09]) *)
Notation p390 := (BND r19 i295). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [5.82077e-11, 2.79396e-09]) *)
Definition f228 := Float2 (422211768812115) (-99).
Definition i296 := makepairF f16 f228.
Notation p391 := (BND r9 i296). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 6.66133e-16]) *)
Definition f229 := Float2 (-422211768812115) (-99).
Definition i297 := makepairF f229 f18.
Notation p392 := (BND r11 i297). (* BND(inv_b_hat - 1 / b, [-6.66133e-16, -1.11022e-16]) *)
Definition f230 := Float2 (-1) (-58).
Definition i298 := makepairF f229 f230.
Notation p393 := (BND r11 i298). (* BND(inv_b_hat - 1 / b, [-6.66133e-16, -3.46945e-18]) *)
Definition f231 := Float2 (-1) (-27).
Definition i299 := makepairF f33 f231.
Notation p394 := (REL _inv_b_hat r12 i299). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -7.45058e-09]) *)
Notation p395 := (BND r20 i299). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -7.45058e-09]) *)
Definition i300 := makepairF f221 f231.
Notation p396 := (BND r20 i300). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -7.45058e-09]) *)
Definition f232 := Float2 (844423638287363) (-76).
Definition i301 := makepairF f13 f232.
Notation p397 := (BND r12 i301). (* BND(1 / b, [4.65661e-10, 1.11759e-08]) *)
Definition i302 := makepairF f71 f232.
Notation p398 := (BND r12 i302). (* BND(1 / b, [7.10543e-15, 1.11759e-08]) *)
Notation p399 := (BND r19 i302). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.11759e-08]) *)
Definition f233 := Float2 (844423638287363) (-98).
Definition i303 := makepairF f16 f233.
Notation p400 := (BND r9 i303). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.66453e-15]) *)
Definition f234 := Float2 (-844423638287363) (-98).
Definition i304 := makepairF f234 f18.
Notation p401 := (BND r11 i304). (* BND(inv_b_hat - 1 / b, [-2.66453e-15, -1.11022e-16]) *)
Definition f235 := Float2 (-1) (-60).
Definition i305 := makepairF f234 f235.
Notation p402 := (BND r11 i305). (* BND(inv_b_hat - 1 / b, [-2.66453e-15, -8.67362e-19]) *)
Definition f236 := Float2 (-1) (-29).
Definition i306 := makepairF f33 f236.
Notation p403 := (REL _inv_b_hat r12 i306). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.86265e-09]) *)
Notation p404 := (BND r20 i306). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.86265e-09]) *)
Definition i307 := makepairF f221 f236.
Notation p405 := (BND r20 i307). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.86265e-09]) *)
Definition f237 := Float2 (211105934737627) (-72).
Definition i308 := makepairF f13 f237.
Notation p406 := (BND r12 i308). (* BND(1 / b, [4.65661e-10, 4.47034e-08]) *)
Definition i309 := makepairF f71 f237.
Notation p407 := (BND r12 i309). (* BND(1 / b, [7.10543e-15, 4.47034e-08]) *)
Notation p408 := (BND r19 i309). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 4.47034e-08]) *)
Definition f238 := Float2 (211105934737627) (-94).
Definition i310 := makepairF f16 f238.
Notation p409 := (BND r9 i310). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.06581e-14]) *)
Definition f239 := Float2 (-211105934737627) (-94).
Definition i311 := makepairF f239 f18.
Notation p410 := (BND r11 i311). (* BND(inv_b_hat - 1 / b, [-1.06581e-14, -1.11022e-16]) *)
Definition f240 := Float2 (-1) (-62).
Definition i312 := makepairF f239 f240.
Notation p411 := (BND r11 i312). (* BND(inv_b_hat - 1 / b, [-1.06581e-14, -2.1684e-19]) *)
Definition f241 := Float2 (-1) (-31).
Definition i313 := makepairF f33 f241.
Notation p412 := (REL _inv_b_hat r12 i313). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -4.65661e-10]) *)
Notation p413 := (BND r20 i313). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -4.65661e-10]) *)
Definition i314 := makepairF f221 f241.
Notation p414 := (BND r20 i314). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -4.65661e-10]) *)
Definition f242 := Float2 (844423839613665) (-72).
Definition i315 := makepairF f13 f242.
Notation p415 := (BND r12 i315). (* BND(1 / b, [4.65661e-10, 1.78814e-07]) *)
Definition i316 := makepairF f71 f242.
Notation p416 := (BND r12 i316). (* BND(1 / b, [7.10543e-15, 1.78814e-07]) *)
Notation p417 := (BND r19 i316). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.78814e-07]) *)
Definition f243 := Float2 (844423839613665) (-94).
Definition i317 := makepairF f16 f243.
Notation p418 := (BND r9 i317). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 4.26325e-14]) *)
Definition f244 := Float2 (-844423839613665) (-94).
Definition i318 := makepairF f244 f18.
Notation p419 := (BND r11 i318). (* BND(inv_b_hat - 1 / b, [-4.26325e-14, -1.11022e-16]) *)
Definition f245 := Float2 (-1) (-64).
Definition i319 := makepairF f244 f245.
Notation p420 := (BND r11 i319). (* BND(inv_b_hat - 1 / b, [-4.26325e-14, -5.42101e-20]) *)
Definition f246 := Float2 (-1) (-33).
Definition i320 := makepairF f33 f246.
Notation p421 := (REL _inv_b_hat r12 i320). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.16415e-10]) *)
Notation p422 := (BND r20 i320). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.16415e-10]) *)
Definition i321 := makepairF f221 f246.
Notation p423 := (BND r20 i321). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.16415e-10]) *)
Definition f247 := Float2 (422211970138417) (-69).
Definition i322 := makepairF f13 f247.
Notation p424 := (BND r12 i322). (* BND(1 / b, [4.65661e-10, 7.15255e-07]) *)
Definition i323 := makepairF f71 f247.
Notation p425 := (BND r12 i323). (* BND(1 / b, [7.10543e-15, 7.15255e-07]) *)
Notation p426 := (BND r19 i323). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 7.15255e-07]) *)
Definition f248 := Float2 (422211970138417) (-91).
Definition i324 := makepairF f16 f248.
Notation p427 := (BND r9 i324). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.7053e-13]) *)
Definition f249 := Float2 (-422211970138417) (-91).
Definition i325 := makepairF f249 f18.
Notation p428 := (BND r11 i325). (* BND(inv_b_hat - 1 / b, [-1.7053e-13, -1.11022e-16]) *)
Definition f250 := Float2 (-1) (-66).
Definition i326 := makepairF f249 f250.
Notation p429 := (BND r11 i326). (* BND(inv_b_hat - 1 / b, [-1.7053e-13, -1.35525e-20]) *)
Definition f251 := Float2 (-1) (-35).
Definition i327 := makepairF f33 f251.
Notation p430 := (REL _inv_b_hat r12 i327). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.91038e-11]) *)
Notation p431 := (BND r20 i327). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.91038e-11]) *)
Definition i328 := makepairF f221 f251.
Notation p432 := (BND r20 i328). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.91038e-11]) *)
Definition f252 := Float2 (844424040940015) (-68).
Definition i329 := makepairF f13 f252.
Notation p433 := (BND r12 i329). (* BND(1 / b, [4.65661e-10, 2.86102e-06]) *)
Definition i330 := makepairF f71 f252.
Notation p434 := (BND r12 i330). (* BND(1 / b, [7.10543e-15, 2.86102e-06]) *)
Notation p435 := (BND r19 i330). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 2.86102e-06]) *)
Definition f253 := Float2 (844424040940015) (-90).
Definition i331 := makepairF f16 f253.
Notation p436 := (BND r9 i331). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 6.8212e-13]) *)
Definition f254 := Float2 (-844424040940015) (-90).
Definition i332 := makepairF f254 f18.
Notation p437 := (BND r11 i332). (* BND(inv_b_hat - 1 / b, [-6.8212e-13, -1.11022e-16]) *)
Definition f255 := Float2 (-1) (-68).
Definition i333 := makepairF f254 f255.
Notation p438 := (BND r11 i333). (* BND(inv_b_hat - 1 / b, [-6.8212e-13, -3.38813e-21]) *)
Definition i334 := makepairF f33 f135.
Notation p439 := (REL _inv_b_hat r12 i334). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -7.27596e-12]) *)
Notation p440 := (BND r20 i334). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -7.27596e-12]) *)
Definition i335 := makepairF f221 f135.
Notation p441 := (BND r20 i335). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -7.27596e-12]) *)
Definition f256 := Float2 (105553017700401) (-63).
Definition i336 := makepairF f13 f256.
Notation p442 := (BND r12 i336). (* BND(1 / b, [4.65661e-10, 1.14441e-05]) *)
Definition i337 := makepairF f71 f256.
Notation p443 := (BND r12 i337). (* BND(1 / b, [7.10543e-15, 1.14441e-05]) *)
Notation p444 := (BND r19 i337). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.14441e-05]) *)
Definition f257 := Float2 (105553017700401) (-85).
Definition i338 := makepairF f16 f257.
Notation p445 := (BND r9 i338). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.72848e-12]) *)
Definition f258 := Float2 (-105553017700401) (-85).
Definition i339 := makepairF f258 f18.
Notation p446 := (BND r11 i339). (* BND(inv_b_hat - 1 / b, [-2.72848e-12, -1.11022e-16]) *)
Definition f259 := Float2 (-1) (-70).
Definition i340 := makepairF f258 f259.
Notation p447 := (BND r11 i340). (* BND(inv_b_hat - 1 / b, [-2.72848e-12, -8.47033e-22]) *)
Definition i341 := makepairF f33 f141.
Notation p448 := (REL _inv_b_hat r12 i341). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.81899e-12]) *)
Notation p449 := (BND r20 i341). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.81899e-12]) *)
Definition i342 := makepairF f221 f141.
Notation p450 := (BND r20 i342). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.81899e-12]) *)
Definition f260 := Float2 (844424242266413) (-64).
Definition i343 := makepairF f13 f260.
Notation p451 := (BND r12 i343). (* BND(1 / b, [4.65661e-10, 4.57763e-05]) *)
Definition f261 := Float2 (1) (-49).
Definition i344 := makepairF f261 f260.
Notation p452 := (BND r12 i344). (* BND(1 / b, [1.77636e-15, 4.57763e-05]) *)
Notation p453 := (BND r19 i344). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.77636e-15, 4.57763e-05]) *)
Definition f262 := Float2 (844424242266413) (-86).
Definition i345 := makepairF f16 f262.
Notation p454 := (BND r9 i345). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.09139e-11]) *)
Definition f263 := Float2 (-844424242266413) (-86).
Definition i346 := makepairF f263 f18.
Notation p455 := (BND r11 i346). (* BND(inv_b_hat - 1 / b, [-1.09139e-11, -1.11022e-16]) *)
Definition f264 := Float2 (-1) (-72).
Definition i347 := makepairF f263 f264.
Notation p456 := (BND r11 i347). (* BND(inv_b_hat - 1 / b, [-1.09139e-11, -2.11758e-22]) *)
Definition i348 := makepairF f33 f159.
Notation p457 := (REL _inv_b_hat r12 i348). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -4.54747e-13]) *)
Notation p458 := (BND r20 i348). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -4.54747e-13]) *)
Definition i349 := makepairF f221 f159.
Notation p459 := (BND r20 i349). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -4.54747e-13]) *)
Definition f265 := Float2 (422212171464815) (-61).
Definition i350 := makepairF f13 f265.
Notation p460 := (BND r12 i350). (* BND(1 / b, [4.65661e-10, 0.000183105]) *)
Definition i351 := makepairF f225 f265.
Notation p461 := (BND r12 i351). (* BND(1 / b, [4.44089e-16, 0.000183105]) *)
Notation p462 := (BND r19 i351). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.44089e-16, 0.000183105]) *)
Definition f266 := Float2 (422212171464815) (-83).
Definition i352 := makepairF f16 f266.
Notation p463 := (BND r9 i352). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 4.36557e-11]) *)
Definition f267 := Float2 (-422212171464815) (-83).
Definition i353 := makepairF f267 f18.
Notation p464 := (BND r11 i353). (* BND(inv_b_hat - 1 / b, [-4.36557e-11, -1.11022e-16]) *)
Definition f268 := Float2 (-1) (-73).
Definition i354 := makepairF f267 f268.
Notation p465 := (BND r11 i354). (* BND(inv_b_hat - 1 / b, [-4.36557e-11, -1.05879e-22]) *)
Definition i355 := makepairF f33 f161.
Notation p466 := (REL _inv_b_hat r12 i355). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.27374e-13]) *)
Notation p467 := (BND r20 i355). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.27374e-13]) *)
Definition i356 := makepairF f221 f161.
Notation p468 := (BND r20 i356). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.27374e-13]) *)
Definition f269 := Float2 (844424393261243) (-61).
Definition i357 := makepairF f13 f269.
Notation p469 := (BND r12 i357). (* BND(1 / b, [4.65661e-10, 0.000366211]) *)
Definition i358 := makepairF f16 f269.
Notation p470 := (BND r12 i358). (* BND(1 / b, [1.11022e-16, 0.000366211]) *)
Notation p471 := (BND r19 i358). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.000366211]) *)
Definition f270 := Float2 (844424393261243) (-83).
Definition i359 := makepairF f16 f270.
Notation p472 := (BND r9 i359). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 8.73114e-11]) *)
Definition f271 := Float2 (-844424393261243) (-83).
Definition i360 := makepairF f271 f18.
Notation p473 := (BND r11 i360). (* BND(inv_b_hat - 1 / b, [-8.73114e-11, -1.11022e-16]) *)
Definition f272 := Float2 (-1) (-75).
Definition i361 := makepairF f271 f272.
Notation p474 := (BND r11 i361). (* BND(inv_b_hat - 1 / b, [-8.73114e-11, -2.64698e-23]) *)
Definition i362 := makepairF f33 f171.
Notation p475 := (REL _inv_b_hat r12 i362). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -5.68434e-14]) *)
Notation p476 := (BND r20 i362). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -5.68434e-14]) *)
Definition i363 := makepairF f221 f171.
Notation p477 := (BND r20 i363). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -5.68434e-14]) *)
Definition f273 := Float2 (422212246962239) (-58).
Definition i364 := makepairF f13 f273.
Notation p478 := (BND r12 i364). (* BND(1 / b, [4.65661e-10, 0.00146484]) *)
Definition i365 := makepairF f16 f273.
Notation p479 := (BND r12 i365). (* BND(1 / b, [1.11022e-16, 0.00146484]) *)
Notation p480 := (BND r19 i365). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.00146484]) *)
Definition f274 := Float2 (422212246962239) (-80).
Definition i366 := makepairF f16 f274.
Notation p481 := (BND r9 i366). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 3.49246e-10]) *)
Definition f275 := Float2 (-422212246962239) (-80).
Definition i367 := makepairF f275 f18.
Notation p482 := (BND r11 i367). (* BND(inv_b_hat - 1 / b, [-3.49246e-10, -1.11022e-16]) *)
Definition f276 := Float2 (-1) (-77).
Definition i368 := makepairF f275 f276.
Notation p483 := (BND r11 i368). (* BND(inv_b_hat - 1 / b, [-3.49246e-10, -6.61744e-24]) *)
Definition i369 := makepairF f33 f181.
Notation p484 := (REL _inv_b_hat r12 i369). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.42109e-14]) *)
Notation p485 := (BND r20 i369). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.42109e-14]) *)
Definition i370 := makepairF f221 f181.
Notation p486 := (BND r20 i370). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.42109e-14]) *)
Definition f277 := Float2 (844424594587725) (-57).
Definition i371 := makepairF f13 f277.
Notation p487 := (BND r12 i371). (* BND(1 / b, [4.65661e-10, 0.00585937]) *)
Definition i372 := makepairF f16 f277.
Notation p488 := (BND r12 i372). (* BND(1 / b, [1.11022e-16, 0.00585937]) *)
Notation p489 := (BND r19 i372). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.00585937]) *)
Definition f278 := Float2 (844424594587725) (-79).
Definition i373 := makepairF f16 f278.
Notation p490 := (BND r9 i373). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.39698e-09]) *)
Definition f279 := Float2 (-844424594587725) (-79).
Definition i374 := makepairF f279 f18.
Notation p491 := (BND r11 i374). (* BND(inv_b_hat - 1 / b, [-1.39698e-09, -1.11022e-16]) *)
Definition f280 := Float2 (-1) (-79).
Definition i375 := makepairF f279 f280.
Notation p492 := (BND r11 i375). (* BND(inv_b_hat - 1 / b, [-1.39698e-09, -1.65436e-24]) *)
Definition i376 := makepairF f33 f190.
Notation p493 := (REL _inv_b_hat r12 i376). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -3.55271e-15]) *)
Notation p494 := (BND r20 i376). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -3.55271e-15]) *)
Definition i377 := makepairF f221 f190.
Notation p495 := (BND r20 i377). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -3.55271e-15]) *)
Definition f281 := Float2 (105553086906373) (-52).
Definition i378 := makepairF f13 f281.
Notation p496 := (BND r12 i378). (* BND(1 / b, [4.65661e-10, 0.0234375]) *)
Definition i379 := makepairF f16 f281.
Notation p497 := (BND r12 i379). (* BND(1 / b, [1.11022e-16, 0.0234375]) *)
Notation p498 := (BND r19 i379). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.0234375]) *)
Definition f282 := Float2 (105553086906373) (-74).
Definition i380 := makepairF f16 f282.
Notation p499 := (BND r9 i380). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 5.58793e-09]) *)
Definition f283 := Float2 (-105553086906373) (-74).
Definition i381 := makepairF f283 f18.
Notation p500 := (BND r11 i381). (* BND(inv_b_hat - 1 / b, [-5.58793e-09, -1.11022e-16]) *)
Definition f284 := Float2 (-1) (-81).
Definition i382 := makepairF f283 f284.
Notation p501 := (BND r11 i382). (* BND(inv_b_hat - 1 / b, [-5.58793e-09, -4.1359e-25]) *)
Definition i383 := makepairF f33 f200.
Notation p502 := (REL _inv_b_hat r12 i383). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -8.88178e-16]) *)
Notation p503 := (BND r20 i383). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -8.88178e-16]) *)
Definition i384 := makepairF f221 f200.
Notation p504 := (BND r20 i384). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -8.88178e-16]) *)
Definition f285 := Float2 (844424795914255) (-53).
Definition i385 := makepairF f13 f285.
Notation p505 := (BND r12 i385). (* BND(1 / b, [4.65661e-10, 0.09375]) *)
Definition i386 := makepairF f16 f285.
Notation p506 := (BND r12 i386). (* BND(1 / b, [1.11022e-16, 0.09375]) *)
Notation p507 := (BND r19 i386). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.09375]) *)
Definition f286 := Float2 (844424795914255) (-75).
Definition i387 := makepairF f16 f286.
Notation p508 := (BND r9 i387). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.23517e-08]) *)
Definition f287 := Float2 (-844424795914255) (-75).
Definition i388 := makepairF f287 f18.
Notation p509 := (BND r11 i388). (* BND(inv_b_hat - 1 / b, [-2.23517e-08, -1.11022e-16]) *)
Definition f288 := Float2 (-1) (-83).
Definition i389 := makepairF f287 f288.
Notation p510 := (BND r11 i389). (* BND(inv_b_hat - 1 / b, [-2.23517e-08, -1.03398e-25]) *)
Definition i390 := makepairF f33 f208.
Notation p511 := (REL _inv_b_hat r12 i390). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.22045e-16]) *)
Notation p512 := (BND r20 i390). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.22045e-16]) *)
Definition i391 := makepairF f221 f208.
Notation p513 := (BND r20 i391). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.22045e-16]) *)
Definition f289 := Float2 (422212448288769) (-50).
Definition i392 := makepairF f13 f289.
Notation p514 := (BND r12 i392). (* BND(1 / b, [4.65661e-10, 0.375]) *)
Definition i393 := makepairF f16 f289.
Notation p515 := (BND r12 i393). (* BND(1 / b, [1.11022e-16, 0.375]) *)
Notation p516 := (BND r19 i393). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.375]) *)
Definition i394 := makepairF f16 f22.
Notation p517 := (BND r9 i394). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 8.9407e-08]) *)
Lemma l482 : p366 -> p3 -> s1 -> p517 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 8.9407e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l38 h2).
 assert (h4 := l331 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t230 : p517 -> p371 -> p516.
 intros h0 h1.
 refine (div_pp r9 r8 i394 i283 i393 h0 h1 _) ; finalize.
Qed.
Lemma l481 : p366 -> p3 -> s1 -> p516 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.375]) *).
 intros h0 h1 h2.
 assert (h3 := l482 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t230. exact h3. exact h4.
Qed.
Lemma t231 : p14 -> p18 -> p516 -> p515.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i393 h0 h1 h2) ; finalize.
Qed.
Lemma l480 : p366 -> p3 -> s1 -> p515 (* BND(1 / b, [1.11022e-16, 0.375]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l481 h0 h1 h2).
 apply t231. exact h3. exact h4. exact h5.
Qed.
Lemma l479 : p366 -> p3 -> s1 -> p514 (* BND(1 / b, [4.65661e-10, 0.375]) *).
 intros h0 h1 h2.
 assert (h3 := l480 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition f290 := Float2 (1) (-1).
Definition i395 := makepairF f13 f290.
Notation p518 := (BND r12 i395). (* BND(1 / b, [4.65661e-10, 0.5]) *)
Lemma t232 : p367 -> p518 -> p513.
 intros h0 h1.
 refine (div_np r11 r12 i27 i395 i391 h0 h1 _) ; finalize.
Qed.
Lemma l478 : p366 -> p3 -> s1 -> p513 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.22045e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l479 h0 h1 h2).
 apply t232. exact h3. refine (subset r12 i392 i395 h4 _) ; finalize.
Qed.
Lemma l477 : p366 -> p3 -> s1 -> p512 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.22045e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l478 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t233 : p18 -> p512 -> p511.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i390 h0 h1) ; finalize.
Qed.
Lemma l476 : p366 -> p3 -> s1 -> p511 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.22045e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l477 h0 h1 h2).
 apply t233. exact h3. exact h4.
Qed.
Definition f291 := Float2 (844424846245895) (-52).
Definition i396 := makepairF f13 f291.
Notation p519 := (BND r12 i396). (* BND(1 / b, [4.65661e-10, 0.1875]) *)
Definition i397 := makepairF f16 f291.
Notation p520 := (BND r12 i397). (* BND(1 / b, [1.11022e-16, 0.1875]) *)
Notation p521 := (BND r19 i397). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.1875]) *)
Definition f292 := Float2 (844424846245895) (-74).
Definition i398 := makepairF f16 f292.
Notation p522 := (BND r9 i398). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 4.47035e-08]) *)
Definition f293 := Float2 (-844424846245895) (-74).
Definition i399 := makepairF f293 f18.
Notation p523 := (BND r11 i399). (* BND(inv_b_hat - 1 / b, [-4.47035e-08, -1.11022e-16]) *)
Definition f294 := Float2 (-1) (-84).
Definition i400 := makepairF f293 f294.
Notation p524 := (BND r11 i400). (* BND(inv_b_hat - 1 / b, [-4.47035e-08, -5.16988e-26]) *)
Definition i401 := makepairF f33 f18.
Notation p525 := (REL _inv_b_hat r12 i401). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.11022e-16]) *)
Notation p526 := (BND r20 i401). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.11022e-16]) *)
Definition i402 := makepairF f221 f18.
Notation p527 := (BND r20 i402). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.11022e-16]) *)
Lemma t234 : p367 -> p47 -> p527.
 intros h0 h1.
 refine (div_np r11 r12 i27 i24 i402 h0 h1 _) ; finalize.
Qed.
Lemma l491 : p366 -> p3 -> s1 -> p527 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply t234. exact h3. exact h4.
Qed.
Lemma l490 : p366 -> p3 -> s1 -> p526 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l491 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t235 : p18 -> p526 -> p525.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i401 h0 h1) ; finalize.
Qed.
Lemma l489 : p366 -> p3 -> s1 -> p525 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l490 h0 h1 h2).
 apply t235. exact h3. exact h4.
Qed.
Lemma t236 : p525 -> p514 -> p524.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i401 i392 i400 h0 h1 _) ; finalize.
Qed.
Lemma l488 : p366 -> p3 -> s1 -> p524 (* BND(inv_b_hat - 1 / b, [-4.47035e-08, -5.16988e-26]) *).
 intros h0 h1 h2.
 assert (h3 := l489 h0 h1 h2).
 assert (h4 := l479 h0 h1 h2).
 apply t236. exact h3. exact h4.
Qed.
Lemma l487 : p366 -> p3 -> s1 -> p523 (* BND(inv_b_hat - 1 / b, [-4.47035e-08, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l488 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t237 : p39 -> p523 -> p522.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i399 i398 h0 h1 _) ; finalize.
Qed.
Lemma l486 : p366 -> p3 -> s1 -> p522 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 4.47035e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l487 h0 h1 h2).
 apply t237. exact h3. exact h4.
Qed.
Lemma t238 : p522 -> p371 -> p521.
 intros h0 h1.
 refine (div_pp r9 r8 i398 i283 i397 h0 h1 _) ; finalize.
Qed.
Lemma l485 : p366 -> p3 -> s1 -> p521 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.1875]) *).
 intros h0 h1 h2.
 assert (h3 := l486 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t238. exact h3. exact h4.
Qed.
Lemma t239 : p14 -> p18 -> p521 -> p520.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i397 h0 h1 h2) ; finalize.
Qed.
Lemma l484 : p366 -> p3 -> s1 -> p520 (* BND(1 / b, [1.11022e-16, 0.1875]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l485 h0 h1 h2).
 apply t239. exact h3. exact h4. exact h5.
Qed.
Lemma l483 : p366 -> p3 -> s1 -> p519 (* BND(1 / b, [4.65661e-10, 0.1875]) *).
 intros h0 h1 h2.
 assert (h3 := l484 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t240 : p511 -> p519 -> p510.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i390 i396 i389 h0 h1 _) ; finalize.
Qed.
Lemma l475 : p366 -> p3 -> s1 -> p510 (* BND(inv_b_hat - 1 / b, [-2.23517e-08, -1.03398e-25]) *).
 intros h0 h1 h2.
 assert (h3 := l476 h0 h1 h2).
 assert (h4 := l483 h0 h1 h2).
 apply t240. exact h3. exact h4.
Qed.
Lemma l474 : p366 -> p3 -> s1 -> p509 (* BND(inv_b_hat - 1 / b, [-2.23517e-08, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l475 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t241 : p39 -> p509 -> p508.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i388 i387 h0 h1 _) ; finalize.
Qed.
Lemma l473 : p366 -> p3 -> s1 -> p508 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.23517e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l474 h0 h1 h2).
 apply t241. exact h3. exact h4.
Qed.
Lemma t242 : p508 -> p371 -> p507.
 intros h0 h1.
 refine (div_pp r9 r8 i387 i283 i386 h0 h1 _) ; finalize.
Qed.
Lemma l472 : p366 -> p3 -> s1 -> p507 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.09375]) *).
 intros h0 h1 h2.
 assert (h3 := l473 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t242. exact h3. exact h4.
Qed.
Lemma t243 : p14 -> p18 -> p507 -> p506.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i386 h0 h1 h2) ; finalize.
Qed.
Lemma l471 : p366 -> p3 -> s1 -> p506 (* BND(1 / b, [1.11022e-16, 0.09375]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l472 h0 h1 h2).
 apply t243. exact h3. exact h4. exact h5.
Qed.
Lemma l470 : p366 -> p3 -> s1 -> p505 (* BND(1 / b, [4.65661e-10, 0.09375]) *).
 intros h0 h1 h2.
 assert (h3 := l471 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition f295 := Float2 (1) (-3).
Definition i403 := makepairF f13 f295.
Notation p528 := (BND r12 i403). (* BND(1 / b, [4.65661e-10, 0.125]) *)
Lemma t244 : p367 -> p528 -> p504.
 intros h0 h1.
 refine (div_np r11 r12 i27 i403 i384 h0 h1 _) ; finalize.
Qed.
Lemma l469 : p366 -> p3 -> s1 -> p504 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -8.88178e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l470 h0 h1 h2).
 apply t244. exact h3. refine (subset r12 i385 i403 h4 _) ; finalize.
Qed.
Lemma l468 : p366 -> p3 -> s1 -> p503 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -8.88178e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l469 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t245 : p18 -> p503 -> p502.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i383 h0 h1) ; finalize.
Qed.
Lemma l467 : p366 -> p3 -> s1 -> p502 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -8.88178e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l468 h0 h1 h2).
 apply t245. exact h3. exact h4.
Qed.
Definition f296 := Float2 (422212372791309) (-53).
Definition i404 := makepairF f13 f296.
Notation p529 := (BND r12 i404). (* BND(1 / b, [4.65661e-10, 0.046875]) *)
Definition i405 := makepairF f16 f296.
Notation p530 := (BND r12 i405). (* BND(1 / b, [1.11022e-16, 0.046875]) *)
Notation p531 := (BND r19 i405). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.046875]) *)
Definition f297 := Float2 (422212372791309) (-75).
Definition i406 := makepairF f16 f297.
Notation p532 := (BND r9 i406). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.11759e-08]) *)
Definition f298 := Float2 (-422212372791309) (-75).
Definition i407 := makepairF f298 f18.
Notation p533 := (BND r11 i407). (* BND(inv_b_hat - 1 / b, [-1.11759e-08, -1.11022e-16]) *)
Definition f299 := Float2 (-1) (-82).
Definition i408 := makepairF f298 f299.
Notation p534 := (BND r11 i408). (* BND(inv_b_hat - 1 / b, [-1.11759e-08, -2.06795e-25]) *)
Definition i409 := makepairF f33 f206.
Notation p535 := (REL _inv_b_hat r12 i409). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -4.44089e-16]) *)
Notation p536 := (BND r20 i409). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -4.44089e-16]) *)
Definition i410 := makepairF f221 f206.
Notation p537 := (BND r20 i410). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -4.44089e-16]) *)
Definition i411 := makepairF f13 f218.
Notation p538 := (BND r12 i411). (* BND(1 / b, [4.65661e-10, 0.25]) *)
Lemma t246 : p367 -> p538 -> p537.
 intros h0 h1.
 refine (div_np r11 r12 i27 i411 i410 h0 h1 _) ; finalize.
Qed.
Lemma l500 : p366 -> p3 -> s1 -> p537 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -4.44089e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l483 h0 h1 h2).
 apply t246. exact h3. refine (subset r12 i396 i411 h4 _) ; finalize.
Qed.
Lemma l499 : p366 -> p3 -> s1 -> p536 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -4.44089e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l500 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t247 : p18 -> p536 -> p535.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i409 h0 h1) ; finalize.
Qed.
Lemma l498 : p366 -> p3 -> s1 -> p535 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -4.44089e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l499 h0 h1 h2).
 apply t247. exact h3. exact h4.
Qed.
Lemma t248 : p535 -> p505 -> p534.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i409 i385 i408 h0 h1 _) ; finalize.
Qed.
Lemma l497 : p366 -> p3 -> s1 -> p534 (* BND(inv_b_hat - 1 / b, [-1.11759e-08, -2.06795e-25]) *).
 intros h0 h1 h2.
 assert (h3 := l498 h0 h1 h2).
 assert (h4 := l470 h0 h1 h2).
 apply t248. exact h3. exact h4.
Qed.
Lemma l496 : p366 -> p3 -> s1 -> p533 (* BND(inv_b_hat - 1 / b, [-1.11759e-08, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l497 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t249 : p39 -> p533 -> p532.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i407 i406 h0 h1 _) ; finalize.
Qed.
Lemma l495 : p366 -> p3 -> s1 -> p532 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.11759e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l496 h0 h1 h2).
 apply t249. exact h3. exact h4.
Qed.
Lemma t250 : p532 -> p371 -> p531.
 intros h0 h1.
 refine (div_pp r9 r8 i406 i283 i405 h0 h1 _) ; finalize.
Qed.
Lemma l494 : p366 -> p3 -> s1 -> p531 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.046875]) *).
 intros h0 h1 h2.
 assert (h3 := l495 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t250. exact h3. exact h4.
Qed.
Lemma t251 : p14 -> p18 -> p531 -> p530.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i405 h0 h1 h2) ; finalize.
Qed.
Lemma l493 : p366 -> p3 -> s1 -> p530 (* BND(1 / b, [1.11022e-16, 0.046875]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l494 h0 h1 h2).
 apply t251. exact h3. exact h4. exact h5.
Qed.
Lemma l492 : p366 -> p3 -> s1 -> p529 (* BND(1 / b, [4.65661e-10, 0.046875]) *).
 intros h0 h1 h2.
 assert (h3 := l493 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t252 : p502 -> p529 -> p501.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i383 i404 i382 h0 h1 _) ; finalize.
Qed.
Lemma l466 : p366 -> p3 -> s1 -> p501 (* BND(inv_b_hat - 1 / b, [-5.58793e-09, -4.1359e-25]) *).
 intros h0 h1 h2.
 assert (h3 := l467 h0 h1 h2).
 assert (h4 := l492 h0 h1 h2).
 apply t252. exact h3. exact h4.
Qed.
Lemma l465 : p366 -> p3 -> s1 -> p500 (* BND(inv_b_hat - 1 / b, [-5.58793e-09, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l466 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t253 : p39 -> p500 -> p499.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i381 i380 h0 h1 _) ; finalize.
Qed.
Lemma l464 : p366 -> p3 -> s1 -> p499 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 5.58793e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l465 h0 h1 h2).
 apply t253. exact h3. exact h4.
Qed.
Lemma t254 : p499 -> p371 -> p498.
 intros h0 h1.
 refine (div_pp r9 r8 i380 i283 i379 h0 h1 _) ; finalize.
Qed.
Lemma l463 : p366 -> p3 -> s1 -> p498 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.0234375]) *).
 intros h0 h1 h2.
 assert (h3 := l464 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t254. exact h3. exact h4.
Qed.
Lemma t255 : p14 -> p18 -> p498 -> p497.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i379 h0 h1 h2) ; finalize.
Qed.
Lemma l462 : p366 -> p3 -> s1 -> p497 (* BND(1 / b, [1.11022e-16, 0.0234375]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l463 h0 h1 h2).
 apply t255. exact h3. exact h4. exact h5.
Qed.
Lemma l461 : p366 -> p3 -> s1 -> p496 (* BND(1 / b, [4.65661e-10, 0.0234375]) *).
 intros h0 h1 h2.
 assert (h3 := l462 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition f300 := Float2 (1) (-5).
Definition i412 := makepairF f13 f300.
Notation p539 := (BND r12 i412). (* BND(1 / b, [4.65661e-10, 0.03125]) *)
Lemma t256 : p367 -> p539 -> p495.
 intros h0 h1.
 refine (div_np r11 r12 i27 i412 i377 h0 h1 _) ; finalize.
Qed.
Lemma l460 : p366 -> p3 -> s1 -> p495 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -3.55271e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l461 h0 h1 h2).
 apply t256. exact h3. refine (subset r12 i378 i412 h4 _) ; finalize.
Qed.
Lemma l459 : p366 -> p3 -> s1 -> p494 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -3.55271e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l460 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t257 : p18 -> p494 -> p493.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i376 h0 h1) ; finalize.
Qed.
Lemma l458 : p366 -> p3 -> s1 -> p493 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -3.55271e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l459 h0 h1 h2).
 apply t257. exact h3. exact h4.
Qed.
Definition f301 := Float2 (844424644919353) (-56).
Definition i413 := makepairF f13 f301.
Notation p540 := (BND r12 i413). (* BND(1 / b, [4.65661e-10, 0.0117187]) *)
Definition i414 := makepairF f16 f301.
Notation p541 := (BND r12 i414). (* BND(1 / b, [1.11022e-16, 0.0117187]) *)
Notation p542 := (BND r19 i414). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.0117187]) *)
Definition f302 := Float2 (844424644919353) (-78).
Definition i415 := makepairF f16 f302.
Notation p543 := (BND r9 i415). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.79397e-09]) *)
Definition f303 := Float2 (-844424644919353) (-78).
Definition i416 := makepairF f303 f18.
Notation p544 := (BND r11 i416). (* BND(inv_b_hat - 1 / b, [-2.79397e-09, -1.11022e-16]) *)
Definition f304 := Float2 (-1) (-80).
Definition i417 := makepairF f303 f304.
Notation p545 := (BND r11 i417). (* BND(inv_b_hat - 1 / b, [-2.79397e-09, -8.27181e-25]) *)
Definition i418 := makepairF f33 f198.
Notation p546 := (REL _inv_b_hat r12 i418). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.77636e-15]) *)
Notation p547 := (BND r20 i418). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.77636e-15]) *)
Definition i419 := makepairF f221 f198.
Notation p548 := (BND r20 i419). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.77636e-15]) *)
Definition i420 := makepairF f13 f145.
Notation p549 := (BND r12 i420). (* BND(1 / b, [4.65661e-10, 0.0625]) *)
Lemma t258 : p367 -> p549 -> p548.
 intros h0 h1.
 refine (div_np r11 r12 i27 i420 i419 h0 h1 _) ; finalize.
Qed.
Lemma l509 : p366 -> p3 -> s1 -> p548 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.77636e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l492 h0 h1 h2).
 apply t258. exact h3. refine (subset r12 i404 i420 h4 _) ; finalize.
Qed.
Lemma l508 : p366 -> p3 -> s1 -> p547 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.77636e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l509 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t259 : p18 -> p547 -> p546.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i418 h0 h1) ; finalize.
Qed.
Lemma l507 : p366 -> p3 -> s1 -> p546 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.77636e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l508 h0 h1 h2).
 apply t259. exact h3. exact h4.
Qed.
Lemma t260 : p546 -> p496 -> p545.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i418 i378 i417 h0 h1 _) ; finalize.
Qed.
Lemma l506 : p366 -> p3 -> s1 -> p545 (* BND(inv_b_hat - 1 / b, [-2.79397e-09, -8.27181e-25]) *).
 intros h0 h1 h2.
 assert (h3 := l507 h0 h1 h2).
 assert (h4 := l461 h0 h1 h2).
 apply t260. exact h3. exact h4.
Qed.
Lemma l505 : p366 -> p3 -> s1 -> p544 (* BND(inv_b_hat - 1 / b, [-2.79397e-09, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l506 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t261 : p39 -> p544 -> p543.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i416 i415 h0 h1 _) ; finalize.
Qed.
Lemma l504 : p366 -> p3 -> s1 -> p543 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.79397e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l505 h0 h1 h2).
 apply t261. exact h3. exact h4.
Qed.
Lemma t262 : p543 -> p371 -> p542.
 intros h0 h1.
 refine (div_pp r9 r8 i415 i283 i414 h0 h1 _) ; finalize.
Qed.
Lemma l503 : p366 -> p3 -> s1 -> p542 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.0117187]) *).
 intros h0 h1 h2.
 assert (h3 := l504 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t262. exact h3. exact h4.
Qed.
Lemma t263 : p14 -> p18 -> p542 -> p541.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i414 h0 h1 h2) ; finalize.
Qed.
Lemma l502 : p366 -> p3 -> s1 -> p541 (* BND(1 / b, [1.11022e-16, 0.0117187]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l503 h0 h1 h2).
 apply t263. exact h3. exact h4. exact h5.
Qed.
Lemma l501 : p366 -> p3 -> s1 -> p540 (* BND(1 / b, [4.65661e-10, 0.0117187]) *).
 intros h0 h1 h2.
 assert (h3 := l502 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t264 : p493 -> p540 -> p492.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i376 i413 i375 h0 h1 _) ; finalize.
Qed.
Lemma l457 : p366 -> p3 -> s1 -> p492 (* BND(inv_b_hat - 1 / b, [-1.39698e-09, -1.65436e-24]) *).
 intros h0 h1 h2.
 assert (h3 := l458 h0 h1 h2).
 assert (h4 := l501 h0 h1 h2).
 apply t264. exact h3. exact h4.
Qed.
Lemma l456 : p366 -> p3 -> s1 -> p491 (* BND(inv_b_hat - 1 / b, [-1.39698e-09, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l457 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t265 : p39 -> p491 -> p490.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i374 i373 h0 h1 _) ; finalize.
Qed.
Lemma l455 : p366 -> p3 -> s1 -> p490 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.39698e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l456 h0 h1 h2).
 apply t265. exact h3. exact h4.
Qed.
Lemma t266 : p490 -> p371 -> p489.
 intros h0 h1.
 refine (div_pp r9 r8 i373 i283 i372 h0 h1 _) ; finalize.
Qed.
Lemma l454 : p366 -> p3 -> s1 -> p489 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.00585937]) *).
 intros h0 h1 h2.
 assert (h3 := l455 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t266. exact h3. exact h4.
Qed.
Lemma t267 : p14 -> p18 -> p489 -> p488.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i372 h0 h1 h2) ; finalize.
Qed.
Lemma l453 : p366 -> p3 -> s1 -> p488 (* BND(1 / b, [1.11022e-16, 0.00585937]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l454 h0 h1 h2).
 apply t267. exact h3. exact h4. exact h5.
Qed.
Lemma l452 : p366 -> p3 -> s1 -> p487 (* BND(1 / b, [4.65661e-10, 0.00585937]) *).
 intros h0 h1 h2.
 assert (h3 := l453 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition f305 := Float2 (1) (-7).
Definition i421 := makepairF f13 f305.
Notation p550 := (BND r12 i421). (* BND(1 / b, [4.65661e-10, 0.0078125]) *)
Lemma t268 : p367 -> p550 -> p486.
 intros h0 h1.
 refine (div_np r11 r12 i27 i421 i370 h0 h1 _) ; finalize.
Qed.
Lemma l451 : p366 -> p3 -> s1 -> p486 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.42109e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l452 h0 h1 h2).
 apply t268. exact h3. refine (subset r12 i371 i421 h4 _) ; finalize.
Qed.
Lemma l450 : p366 -> p3 -> s1 -> p485 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.42109e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l451 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t269 : p18 -> p485 -> p484.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i369 h0 h1) ; finalize.
Qed.
Lemma l449 : p366 -> p3 -> s1 -> p484 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.42109e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l450 h0 h1 h2).
 apply t269. exact h3. exact h4.
Qed.
Definition f306 := Float2 (211106136064025) (-56).
Definition i422 := makepairF f13 f306.
Notation p551 := (BND r12 i422). (* BND(1 / b, [4.65661e-10, 0.00292969]) *)
Definition i423 := makepairF f16 f306.
Notation p552 := (BND r12 i423). (* BND(1 / b, [1.11022e-16, 0.00292969]) *)
Notation p553 := (BND r19 i423). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.00292969]) *)
Definition f307 := Float2 (211106136064025) (-78).
Definition i424 := makepairF f16 f307.
Notation p554 := (BND r9 i424). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 6.98492e-10]) *)
Definition f308 := Float2 (-211106136064025) (-78).
Definition i425 := makepairF f308 f18.
Notation p555 := (BND r11 i425). (* BND(inv_b_hat - 1 / b, [-6.98492e-10, -1.11022e-16]) *)
Definition f309 := Float2 (-1) (-78).
Definition i426 := makepairF f308 f309.
Notation p556 := (BND r11 i426). (* BND(inv_b_hat - 1 / b, [-6.98492e-10, -3.30872e-24]) *)
Definition i427 := makepairF f33 f188.
Notation p557 := (REL _inv_b_hat r12 i427). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -7.10543e-15]) *)
Notation p558 := (BND r20 i427). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -7.10543e-15]) *)
Definition i428 := makepairF f221 f188.
Notation p559 := (BND r20 i428). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -7.10543e-15]) *)
Definition f310 := Float2 (1) (-6).
Definition i429 := makepairF f13 f310.
Notation p560 := (BND r12 i429). (* BND(1 / b, [4.65661e-10, 0.015625]) *)
Lemma t270 : p367 -> p560 -> p559.
 intros h0 h1.
 refine (div_np r11 r12 i27 i429 i428 h0 h1 _) ; finalize.
Qed.
Lemma l518 : p366 -> p3 -> s1 -> p559 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -7.10543e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l501 h0 h1 h2).
 apply t270. exact h3. refine (subset r12 i413 i429 h4 _) ; finalize.
Qed.
Lemma l517 : p366 -> p3 -> s1 -> p558 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -7.10543e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l518 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t271 : p18 -> p558 -> p557.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i427 h0 h1) ; finalize.
Qed.
Lemma l516 : p366 -> p3 -> s1 -> p557 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -7.10543e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l517 h0 h1 h2).
 apply t271. exact h3. exact h4.
Qed.
Lemma t272 : p557 -> p487 -> p556.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i427 i371 i426 h0 h1 _) ; finalize.
Qed.
Lemma l515 : p366 -> p3 -> s1 -> p556 (* BND(inv_b_hat - 1 / b, [-6.98492e-10, -3.30872e-24]) *).
 intros h0 h1 h2.
 assert (h3 := l516 h0 h1 h2).
 assert (h4 := l452 h0 h1 h2).
 apply t272. exact h3. exact h4.
Qed.
Lemma l514 : p366 -> p3 -> s1 -> p555 (* BND(inv_b_hat - 1 / b, [-6.98492e-10, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l515 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t273 : p39 -> p555 -> p554.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i425 i424 h0 h1 _) ; finalize.
Qed.
Lemma l513 : p366 -> p3 -> s1 -> p554 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 6.98492e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l514 h0 h1 h2).
 apply t273. exact h3. exact h4.
Qed.
Lemma t274 : p554 -> p371 -> p553.
 intros h0 h1.
 refine (div_pp r9 r8 i424 i283 i423 h0 h1 _) ; finalize.
Qed.
Lemma l512 : p366 -> p3 -> s1 -> p553 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.00292969]) *).
 intros h0 h1 h2.
 assert (h3 := l513 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t274. exact h3. exact h4.
Qed.
Lemma t275 : p14 -> p18 -> p553 -> p552.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i423 h0 h1 h2) ; finalize.
Qed.
Lemma l511 : p366 -> p3 -> s1 -> p552 (* BND(1 / b, [1.11022e-16, 0.00292969]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l512 h0 h1 h2).
 apply t275. exact h3. exact h4. exact h5.
Qed.
Lemma l510 : p366 -> p3 -> s1 -> p551 (* BND(1 / b, [4.65661e-10, 0.00292969]) *).
 intros h0 h1 h2.
 assert (h3 := l511 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t276 : p484 -> p551 -> p483.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i369 i422 i368 h0 h1 _) ; finalize.
Qed.
Lemma l448 : p366 -> p3 -> s1 -> p483 (* BND(inv_b_hat - 1 / b, [-3.49246e-10, -6.61744e-24]) *).
 intros h0 h1 h2.
 assert (h3 := l449 h0 h1 h2).
 assert (h4 := l510 h0 h1 h2).
 apply t276. exact h3. exact h4.
Qed.
Lemma l447 : p366 -> p3 -> s1 -> p482 (* BND(inv_b_hat - 1 / b, [-3.49246e-10, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l448 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t277 : p39 -> p482 -> p481.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i367 i366 h0 h1 _) ; finalize.
Qed.
Lemma l446 : p366 -> p3 -> s1 -> p481 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 3.49246e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l447 h0 h1 h2).
 apply t277. exact h3. exact h4.
Qed.
Lemma t278 : p481 -> p371 -> p480.
 intros h0 h1.
 refine (div_pp r9 r8 i366 i283 i365 h0 h1 _) ; finalize.
Qed.
Lemma l445 : p366 -> p3 -> s1 -> p480 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.00146484]) *).
 intros h0 h1 h2.
 assert (h3 := l446 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t278. exact h3. exact h4.
Qed.
Lemma t279 : p14 -> p18 -> p480 -> p479.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i365 h0 h1 h2) ; finalize.
Qed.
Lemma l444 : p366 -> p3 -> s1 -> p479 (* BND(1 / b, [1.11022e-16, 0.00146484]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l445 h0 h1 h2).
 apply t279. exact h3. exact h4. exact h5.
Qed.
Lemma l443 : p366 -> p3 -> s1 -> p478 (* BND(1 / b, [4.65661e-10, 0.00146484]) *).
 intros h0 h1 h2.
 assert (h3 := l444 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition f311 := Float2 (1) (-9).
Definition i430 := makepairF f13 f311.
Notation p561 := (BND r12 i430). (* BND(1 / b, [4.65661e-10, 0.00195312]) *)
Lemma t280 : p367 -> p561 -> p477.
 intros h0 h1.
 refine (div_np r11 r12 i27 i430 i363 h0 h1 _) ; finalize.
Qed.
Lemma l442 : p366 -> p3 -> s1 -> p477 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -5.68434e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l443 h0 h1 h2).
 apply t280. exact h3. refine (subset r12 i364 i430 h4 _) ; finalize.
Qed.
Lemma l441 : p366 -> p3 -> s1 -> p476 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -5.68434e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l442 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t281 : p18 -> p476 -> p475.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i362 h0 h1) ; finalize.
Qed.
Lemma l440 : p366 -> p3 -> s1 -> p475 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -5.68434e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l441 h0 h1 h2).
 apply t281. exact h3. exact h4.
Qed.
Definition f312 := Float2 (844424443592859) (-60).
Definition i431 := makepairF f13 f312.
Notation p562 := (BND r12 i431). (* BND(1 / b, [4.65661e-10, 0.000732421]) *)
Definition i432 := makepairF f16 f312.
Notation p563 := (BND r12 i432). (* BND(1 / b, [1.11022e-16, 0.000732421]) *)
Notation p564 := (BND r19 i432). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.000732421]) *)
Definition f313 := Float2 (844424443592859) (-82).
Definition i433 := makepairF f16 f313.
Notation p565 := (BND r9 i433). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.74623e-10]) *)
Definition f314 := Float2 (-844424443592859) (-82).
Definition i434 := makepairF f314 f18.
Notation p566 := (BND r11 i434). (* BND(inv_b_hat - 1 / b, [-1.74623e-10, -1.11022e-16]) *)
Definition f315 := Float2 (-1) (-76).
Definition i435 := makepairF f314 f315.
Notation p567 := (BND r11 i435). (* BND(inv_b_hat - 1 / b, [-1.74623e-10, -1.32349e-23]) *)
Definition i436 := makepairF f33 f179.
Notation p568 := (REL _inv_b_hat r12 i436). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.84217e-14]) *)
Notation p569 := (BND r20 i436). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.84217e-14]) *)
Definition i437 := makepairF f221 f179.
Notation p570 := (BND r20 i437). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.84217e-14]) *)
Definition f316 := Float2 (1) (-8).
Definition i438 := makepairF f13 f316.
Notation p571 := (BND r12 i438). (* BND(1 / b, [4.65661e-10, 0.00390625]) *)
Lemma t282 : p367 -> p571 -> p570.
 intros h0 h1.
 refine (div_np r11 r12 i27 i438 i437 h0 h1 _) ; finalize.
Qed.
Lemma l527 : p366 -> p3 -> s1 -> p570 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.84217e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l510 h0 h1 h2).
 apply t282. exact h3. refine (subset r12 i422 i438 h4 _) ; finalize.
Qed.
Lemma l526 : p366 -> p3 -> s1 -> p569 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.84217e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l527 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t283 : p18 -> p569 -> p568.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i436 h0 h1) ; finalize.
Qed.
Lemma l525 : p366 -> p3 -> s1 -> p568 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.84217e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l526 h0 h1 h2).
 apply t283. exact h3. exact h4.
Qed.
Lemma t284 : p568 -> p478 -> p567.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i436 i364 i435 h0 h1 _) ; finalize.
Qed.
Lemma l524 : p366 -> p3 -> s1 -> p567 (* BND(inv_b_hat - 1 / b, [-1.74623e-10, -1.32349e-23]) *).
 intros h0 h1 h2.
 assert (h3 := l525 h0 h1 h2).
 assert (h4 := l443 h0 h1 h2).
 apply t284. exact h3. exact h4.
Qed.
Lemma l523 : p366 -> p3 -> s1 -> p566 (* BND(inv_b_hat - 1 / b, [-1.74623e-10, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l524 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t285 : p39 -> p566 -> p565.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i434 i433 h0 h1 _) ; finalize.
Qed.
Lemma l522 : p366 -> p3 -> s1 -> p565 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.74623e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l523 h0 h1 h2).
 apply t285. exact h3. exact h4.
Qed.
Lemma t286 : p565 -> p371 -> p564.
 intros h0 h1.
 refine (div_pp r9 r8 i433 i283 i432 h0 h1 _) ; finalize.
Qed.
Lemma l521 : p366 -> p3 -> s1 -> p564 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.000732421]) *).
 intros h0 h1 h2.
 assert (h3 := l522 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t286. exact h3. exact h4.
Qed.
Lemma t287 : p14 -> p18 -> p564 -> p563.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i432 h0 h1 h2) ; finalize.
Qed.
Lemma l520 : p366 -> p3 -> s1 -> p563 (* BND(1 / b, [1.11022e-16, 0.000732421]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l521 h0 h1 h2).
 apply t287. exact h3. exact h4. exact h5.
Qed.
Lemma l519 : p366 -> p3 -> s1 -> p562 (* BND(1 / b, [4.65661e-10, 0.000732421]) *).
 intros h0 h1 h2.
 assert (h3 := l520 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t288 : p475 -> p562 -> p474.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i362 i431 i361 h0 h1 _) ; finalize.
Qed.
Lemma l439 : p366 -> p3 -> s1 -> p474 (* BND(inv_b_hat - 1 / b, [-8.73114e-11, -2.64698e-23]) *).
 intros h0 h1 h2.
 assert (h3 := l440 h0 h1 h2).
 assert (h4 := l519 h0 h1 h2).
 apply t288. exact h3. exact h4.
Qed.
Lemma l438 : p366 -> p3 -> s1 -> p473 (* BND(inv_b_hat - 1 / b, [-8.73114e-11, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l439 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t289 : p39 -> p473 -> p472.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i360 i359 h0 h1 _) ; finalize.
Qed.
Lemma l437 : p366 -> p3 -> s1 -> p472 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 8.73114e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l438 h0 h1 h2).
 apply t289. exact h3. exact h4.
Qed.
Lemma t290 : p472 -> p371 -> p471.
 intros h0 h1.
 refine (div_pp r9 r8 i359 i283 i358 h0 h1 _) ; finalize.
Qed.
Lemma l436 : p366 -> p3 -> s1 -> p471 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.11022e-16, 0.000366211]) *).
 intros h0 h1 h2.
 assert (h3 := l437 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply t290. exact h3. exact h4.
Qed.
Lemma t291 : p14 -> p18 -> p471 -> p470.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i358 h0 h1 h2) ; finalize.
Qed.
Lemma l435 : p366 -> p3 -> s1 -> p470 (* BND(1 / b, [1.11022e-16, 0.000366211]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l436 h0 h1 h2).
 apply t291. exact h3. exact h4. exact h5.
Qed.
Lemma l434 : p366 -> p3 -> s1 -> p469 (* BND(1 / b, [4.65661e-10, 0.000366211]) *).
 intros h0 h1 h2.
 assert (h3 := l435 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition f317 := Float2 (1) (-11).
Definition i439 := makepairF f13 f317.
Notation p572 := (BND r12 i439). (* BND(1 / b, [4.65661e-10, 0.000488281]) *)
Lemma t292 : p367 -> p572 -> p468.
 intros h0 h1.
 refine (div_np r11 r12 i27 i439 i356 h0 h1 _) ; finalize.
Qed.
Lemma l433 : p366 -> p3 -> s1 -> p468 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.27374e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l434 h0 h1 h2).
 apply t292. exact h3. refine (subset r12 i357 i439 h4 _) ; finalize.
Qed.
Lemma l432 : p366 -> p3 -> s1 -> p467 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.27374e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l433 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t293 : p18 -> p467 -> p466.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i355 h0 h1) ; finalize.
Qed.
Lemma l431 : p366 -> p3 -> s1 -> p466 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.27374e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l432 h0 h1 h2).
 apply t293. exact h3. exact h4.
Qed.
Lemma t294 : p466 -> p469 -> p465.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i355 i357 i354 h0 h1 _) ; finalize.
Qed.
Lemma l430 : p366 -> p3 -> s1 -> p465 (* BND(inv_b_hat - 1 / b, [-4.36557e-11, -1.05879e-22]) *).
 intros h0 h1 h2.
 assert (h3 := l431 h0 h1 h2).
 assert (h4 := l434 h0 h1 h2).
 apply t294. exact h3. exact h4.
Qed.
Lemma l429 : p366 -> p3 -> s1 -> p464 (* BND(inv_b_hat - 1 / b, [-4.36557e-11, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l430 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t295 : p39 -> p464 -> p463.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i353 i352 h0 h1 _) ; finalize.
Qed.
Lemma l428 : p366 -> p3 -> s1 -> p463 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 4.36557e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l429 h0 h1 h2).
 apply t295. exact h3. exact h4.
Qed.
Definition i440 := makepairF f4 f218.
Notation p573 := (BND r8 i440). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.25]) *)
Definition f318 := Float2 (-1) (-2).
Definition i441 := makepairF f318 f218.
Notation p574 := (BND r8 i441). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.25, 0.25]) *)
Notation p575 := (ABS r8 i440). (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.25]) *)
Definition i442 := makepairF f16 f218.
Notation p576 := (ABS r8 i442). (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 0.25]) *)
Definition i443 := makepairF f16 f57.
Notation p577 := (ABS r9 i443). (* ABS(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.16415e-10]) *)
Notation p578 := (BND r9 i443). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.16415e-10]) *)
Lemma t296 : p578 -> p577.
 intros h0.
 refine (abs_of_bnd_p r9 i443 i443 h0 _) ; finalize.
Qed.
Lemma l532 : p366 -> p3 -> s1 -> p577 (* ABS(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.16415e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l437 h0 h1 h2).
 apply t296. refine (subset r9 i359 i443 h3 _) ; finalize.
Qed.
Notation p579 := (ABS r12 i24). (* ABS(1 / b, [4.65661e-10, 1]) *)
Lemma t297 : p47 -> p579.
 intros h0.
 refine (abs_of_bnd_p r12 i24 i24 h0 _) ; finalize.
Qed.
Lemma l533 : p366 -> p3 -> s1 -> p579 (* ABS(1 / b, [4.65661e-10, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l47 h1 h2).
 apply t297. exact h3.
Qed.
Lemma t298 : p577 -> p579 -> p576.
 intros h0 h1.
 refine (div_aa r9 r12 i443 i24 i442 h0 h1 _) ; finalize.
Qed.
Lemma l531 : p366 -> p3 -> s1 -> p576 (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 0.25]) *).
 intros h0 h1 h2.
 assert (h3 := l532 h0 h1 h2).
 assert (h4 := l533 h0 h1 h2).
 apply t298. exact h3. exact h4.
Qed.
Notation p580 := (ABS r8 i283). (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 1]) *)
Lemma t299 : p371 -> p580.
 intros h0.
 refine (abs_of_bnd_p r8 i283 i283 h0 _) ; finalize.
Qed.
Lemma l534 : p366 -> p3 -> s1 -> p580 (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l333 h0 h1 h2).
 apply t299. exact h3.
Qed.
Lemma l530 : p366 -> p3 -> s1 -> p575 (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.25]) *).
 intros h0 h1 h2.
 assert (h3 := l531 h0 h1 h2).
 assert (h4 := l534 h0 h1 h2).
 apply intersect_aa with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t300 : p575 -> p574.
 intros h0.
 refine (bnd_of_abs r8 i440 i441 h0 _) ; finalize.
Qed.
Lemma l529 : p366 -> p3 -> s1 -> p574 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.25, 0.25]) *).
 intros h0 h1 h2.
 assert (h3 := l530 h0 h1 h2).
 apply t300. exact h3.
Qed.
Lemma l528 : p366 -> p3 -> s1 -> p573 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.25]) *).
 intros h0 h1 h2.
 assert (h3 := l529 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t301 : p463 -> p573 -> p462.
 intros h0 h1.
 refine (div_pp r9 r8 i352 i440 i351 h0 h1 _) ; finalize.
Qed.
Lemma l427 : p366 -> p3 -> s1 -> p462 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [4.44089e-16, 0.000183105]) *).
 intros h0 h1 h2.
 assert (h3 := l428 h0 h1 h2).
 assert (h4 := l528 h0 h1 h2).
 apply t301. exact h3. exact h4.
Qed.
Lemma t302 : p14 -> p18 -> p462 -> p461.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i351 h0 h1 h2) ; finalize.
Qed.
Lemma l426 : p366 -> p3 -> s1 -> p461 (* BND(1 / b, [4.44089e-16, 0.000183105]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l427 h0 h1 h2).
 apply t302. exact h3. exact h4. exact h5.
Qed.
Lemma l425 : p366 -> p3 -> s1 -> p460 (* BND(1 / b, [4.65661e-10, 0.000183105]) *).
 intros h0 h1 h2.
 assert (h3 := l426 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition f319 := Float2 (1) (-12).
Definition i444 := makepairF f13 f319.
Notation p581 := (BND r12 i444). (* BND(1 / b, [4.65661e-10, 0.000244141]) *)
Lemma t303 : p367 -> p581 -> p459.
 intros h0 h1.
 refine (div_np r11 r12 i27 i444 i349 h0 h1 _) ; finalize.
Qed.
Lemma l424 : p366 -> p3 -> s1 -> p459 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -4.54747e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l425 h0 h1 h2).
 apply t303. exact h3. refine (subset r12 i350 i444 h4 _) ; finalize.
Qed.
Lemma l423 : p366 -> p3 -> s1 -> p458 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -4.54747e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l424 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t304 : p18 -> p458 -> p457.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i348 h0 h1) ; finalize.
Qed.
Lemma l422 : p366 -> p3 -> s1 -> p457 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -4.54747e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l423 h0 h1 h2).
 apply t304. exact h3. exact h4.
Qed.
Definition f320 := Float2 (211106073149505) (-61).
Definition i445 := makepairF f13 f320.
Notation p582 := (BND r12 i445). (* BND(1 / b, [4.65661e-10, 9.15527e-05]) *)
Definition f321 := Float2 (1) (-50).
Definition i446 := makepairF f321 f320.
Notation p583 := (BND r12 i446). (* BND(1 / b, [8.88178e-16, 9.15527e-05]) *)
Notation p584 := (BND r19 i446). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [8.88178e-16, 9.15527e-05]) *)
Definition f322 := Float2 (211106073149505) (-83).
Definition i447 := makepairF f16 f322.
Notation p585 := (BND r9 i447). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.18279e-11]) *)
Definition f323 := Float2 (-211106073149505) (-83).
Definition i448 := makepairF f323 f18.
Notation p586 := (BND r11 i448). (* BND(inv_b_hat - 1 / b, [-2.18279e-11, -1.11022e-16]) *)
Definition i449 := makepairF f323 f268.
Notation p587 := (BND r11 i449). (* BND(inv_b_hat - 1 / b, [-2.18279e-11, -1.05879e-22]) *)
Lemma t305 : p466 -> p460 -> p587.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i355 i350 i449 h0 h1 _) ; finalize.
Qed.
Lemma l540 : p366 -> p3 -> s1 -> p587 (* BND(inv_b_hat - 1 / b, [-2.18279e-11, -1.05879e-22]) *).
 intros h0 h1 h2.
 assert (h3 := l431 h0 h1 h2).
 assert (h4 := l425 h0 h1 h2).
 apply t305. exact h3. exact h4.
Qed.
Lemma l539 : p366 -> p3 -> s1 -> p586 (* BND(inv_b_hat - 1 / b, [-2.18279e-11, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l540 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t306 : p39 -> p586 -> p585.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i448 i447 h0 h1 _) ; finalize.
Qed.
Lemma l538 : p366 -> p3 -> s1 -> p585 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.18279e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l539 h0 h1 h2).
 apply t306. exact h3. exact h4.
Qed.
Definition i450 := makepairF f4 f145.
Notation p588 := (BND r8 i450). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.0625]) *)
Definition f324 := Float2 (-1) (-4).
Definition i451 := makepairF f324 f145.
Notation p589 := (BND r8 i451). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.0625, 0.0625]) *)
Notation p590 := (ABS r8 i450). (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.0625]) *)
Definition i452 := makepairF f16 f145.
Notation p591 := (ABS r8 i452). (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 0.0625]) *)
Notation p592 := (ABS r9 i169). (* ABS(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.91038e-11]) *)
Notation p593 := (BND r9 i169). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.91038e-11]) *)
Lemma t307 : p593 -> p592.
 intros h0.
 refine (abs_of_bnd_p r9 i169 i169 h0 _) ; finalize.
Qed.
Lemma l545 : p366 -> p3 -> s1 -> p592 (* ABS(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.91038e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l538 h0 h1 h2).
 apply t307. refine (subset r9 i447 i169 h3 _) ; finalize.
Qed.
Lemma t308 : p592 -> p579 -> p591.
 intros h0 h1.
 refine (div_aa r9 r12 i169 i24 i452 h0 h1 _) ; finalize.
Qed.
Lemma l544 : p366 -> p3 -> s1 -> p591 (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 0.0625]) *).
 intros h0 h1 h2.
 assert (h3 := l545 h0 h1 h2).
 assert (h4 := l533 h0 h1 h2).
 apply t308. exact h3. exact h4.
Qed.
Lemma l543 : p366 -> p3 -> s1 -> p590 (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.0625]) *).
 intros h0 h1 h2.
 assert (h3 := l544 h0 h1 h2).
 assert (h4 := l534 h0 h1 h2).
 apply intersect_aa with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t309 : p590 -> p589.
 intros h0.
 refine (bnd_of_abs r8 i450 i451 h0 _) ; finalize.
Qed.
Lemma l542 : p366 -> p3 -> s1 -> p589 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.0625, 0.0625]) *).
 intros h0 h1 h2.
 assert (h3 := l543 h0 h1 h2).
 apply t309. exact h3.
Qed.
Lemma l541 : p366 -> p3 -> s1 -> p588 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.0625]) *).
 intros h0 h1 h2.
 assert (h3 := l542 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition i453 := makepairF f4 f295.
Notation p594 := (BND r8 i453). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.125]) *)
Lemma t310 : p585 -> p594 -> p584.
 intros h0 h1.
 refine (div_pp r9 r8 i447 i453 i446 h0 h1 _) ; finalize.
Qed.
Lemma l537 : p366 -> p3 -> s1 -> p584 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [8.88178e-16, 9.15527e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l538 h0 h1 h2).
 assert (h4 := l541 h0 h1 h2).
 apply t310. exact h3. refine (subset r8 i450 i453 h4 _) ; finalize.
Qed.
Lemma t311 : p14 -> p18 -> p584 -> p583.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i446 h0 h1 h2) ; finalize.
Qed.
Lemma l536 : p366 -> p3 -> s1 -> p583 (* BND(1 / b, [8.88178e-16, 9.15527e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l537 h0 h1 h2).
 apply t311. exact h3. exact h4. exact h5.
Qed.
Lemma l535 : p366 -> p3 -> s1 -> p582 (* BND(1 / b, [4.65661e-10, 9.15527e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l536 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t312 : p457 -> p582 -> p456.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i348 i445 i347 h0 h1 _) ; finalize.
Qed.
Lemma l421 : p366 -> p3 -> s1 -> p456 (* BND(inv_b_hat - 1 / b, [-1.09139e-11, -2.11758e-22]) *).
 intros h0 h1 h2.
 assert (h3 := l422 h0 h1 h2).
 assert (h4 := l535 h0 h1 h2).
 apply t312. exact h3. exact h4.
Qed.
Lemma l420 : p366 -> p3 -> s1 -> p455 (* BND(inv_b_hat - 1 / b, [-1.09139e-11, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l421 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t313 : p39 -> p455 -> p454.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i346 i345 h0 h1 _) ; finalize.
Qed.
Lemma l419 : p366 -> p3 -> s1 -> p454 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.09139e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l420 h0 h1 h2).
 apply t313. exact h3. exact h4.
Qed.
Lemma t314 : p454 -> p588 -> p453.
 intros h0 h1.
 refine (div_pp r9 r8 i345 i450 i344 h0 h1 _) ; finalize.
Qed.
Lemma l418 : p366 -> p3 -> s1 -> p453 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.77636e-15, 4.57763e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l419 h0 h1 h2).
 assert (h4 := l541 h0 h1 h2).
 apply t314. exact h3. exact h4.
Qed.
Lemma t315 : p14 -> p18 -> p453 -> p452.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i344 h0 h1 h2) ; finalize.
Qed.
Lemma l417 : p366 -> p3 -> s1 -> p452 (* BND(1 / b, [1.77636e-15, 4.57763e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l418 h0 h1 h2).
 apply t315. exact h3. exact h4. exact h5.
Qed.
Lemma l416 : p366 -> p3 -> s1 -> p451 (* BND(1 / b, [4.65661e-10, 4.57763e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l417 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition f325 := Float2 (1) (-14).
Definition i454 := makepairF f13 f325.
Notation p595 := (BND r12 i454). (* BND(1 / b, [4.65661e-10, 6.10352e-05]) *)
Lemma t316 : p367 -> p595 -> p450.
 intros h0 h1.
 refine (div_np r11 r12 i27 i454 i342 h0 h1 _) ; finalize.
Qed.
Lemma l415 : p366 -> p3 -> s1 -> p450 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.81899e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l416 h0 h1 h2).
 apply t316. exact h3. refine (subset r12 i343 i454 h4 _) ; finalize.
Qed.
Lemma l414 : p366 -> p3 -> s1 -> p449 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.81899e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l415 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t317 : p18 -> p449 -> p448.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i341 h0 h1) ; finalize.
Qed.
Lemma l413 : p366 -> p3 -> s1 -> p448 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.81899e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l414 h0 h1 h2).
 apply t317. exact h3. exact h4.
Qed.
Definition f326 := Float2 (844424191934809) (-65).
Definition i455 := makepairF f13 f326.
Notation p596 := (BND r12 i455). (* BND(1 / b, [4.65661e-10, 2.28882e-05]) *)
Definition f327 := Float2 (1) (-48).
Definition i456 := makepairF f327 f326.
Notation p597 := (BND r12 i456). (* BND(1 / b, [3.55271e-15, 2.28882e-05]) *)
Notation p598 := (BND r19 i456). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [3.55271e-15, 2.28882e-05]) *)
Definition f328 := Float2 (844424191934809) (-87).
Definition i457 := makepairF f16 f328.
Notation p599 := (BND r9 i457). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 5.45696e-12]) *)
Definition f329 := Float2 (-844424191934809) (-87).
Definition i458 := makepairF f329 f18.
Notation p600 := (BND r11 i458). (* BND(inv_b_hat - 1 / b, [-5.45696e-12, -1.11022e-16]) *)
Definition f330 := Float2 (-1) (-71).
Definition i459 := makepairF f329 f330.
Notation p601 := (BND r11 i459). (* BND(inv_b_hat - 1 / b, [-5.45696e-12, -4.23516e-22]) *)
Definition i460 := makepairF f33 f142.
Notation p602 := (REL _inv_b_hat r12 i460). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -9.09495e-13]) *)
Notation p603 := (BND r20 i460). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -9.09495e-13]) *)
Definition i461 := makepairF f221 f142.
Notation p604 := (BND r20 i461). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -9.09495e-13]) *)
Definition f331 := Float2 (1) (-13).
Definition i462 := makepairF f13 f331.
Notation p605 := (BND r12 i462). (* BND(1 / b, [4.65661e-10, 0.00012207]) *)
Lemma t318 : p367 -> p605 -> p604.
 intros h0 h1.
 refine (div_np r11 r12 i27 i462 i461 h0 h1 _) ; finalize.
Qed.
Lemma l554 : p366 -> p3 -> s1 -> p604 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -9.09495e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l535 h0 h1 h2).
 apply t318. exact h3. refine (subset r12 i445 i462 h4 _) ; finalize.
Qed.
Lemma l553 : p366 -> p3 -> s1 -> p603 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -9.09495e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l554 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t319 : p18 -> p603 -> p602.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i460 h0 h1) ; finalize.
Qed.
Lemma l552 : p366 -> p3 -> s1 -> p602 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -9.09495e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l553 h0 h1 h2).
 apply t319. exact h3. exact h4.
Qed.
Lemma t320 : p602 -> p451 -> p601.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i460 i343 i459 h0 h1 _) ; finalize.
Qed.
Lemma l551 : p366 -> p3 -> s1 -> p601 (* BND(inv_b_hat - 1 / b, [-5.45696e-12, -4.23516e-22]) *).
 intros h0 h1 h2.
 assert (h3 := l552 h0 h1 h2).
 assert (h4 := l416 h0 h1 h2).
 apply t320. exact h3. exact h4.
Qed.
Lemma l550 : p366 -> p3 -> s1 -> p600 (* BND(inv_b_hat - 1 / b, [-5.45696e-12, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l551 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t321 : p39 -> p600 -> p599.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i458 i457 h0 h1 _) ; finalize.
Qed.
Lemma l549 : p366 -> p3 -> s1 -> p599 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 5.45696e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l550 h0 h1 h2).
 apply t321. exact h3. exact h4.
Qed.
Definition i463 := makepairF f4 f310.
Notation p606 := (BND r8 i463). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.015625]) *)
Definition i464 := makepairF f134 f310.
Notation p607 := (BND r8 i464). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.015625, 0.015625]) *)
Notation p608 := (ABS r8 i463). (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.015625]) *)
Definition i465 := makepairF f16 f310.
Notation p609 := (ABS r8 i465). (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 0.015625]) *)
Definition i466 := makepairF f16 f69.
Notation p610 := (ABS r9 i466). (* ABS(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 7.27596e-12]) *)
Notation p611 := (BND r9 i466). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 7.27596e-12]) *)
Lemma t322 : p611 -> p610.
 intros h0.
 refine (abs_of_bnd_p r9 i466 i466 h0 _) ; finalize.
Qed.
Lemma l559 : p366 -> p3 -> s1 -> p610 (* ABS(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 7.27596e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l549 h0 h1 h2).
 apply t322. refine (subset r9 i457 i466 h3 _) ; finalize.
Qed.
Lemma t323 : p610 -> p579 -> p609.
 intros h0 h1.
 refine (div_aa r9 r12 i466 i24 i465 h0 h1 _) ; finalize.
Qed.
Lemma l558 : p366 -> p3 -> s1 -> p609 (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 0.015625]) *).
 intros h0 h1 h2.
 assert (h3 := l559 h0 h1 h2).
 assert (h4 := l533 h0 h1 h2).
 apply t323. exact h3. exact h4.
Qed.
Lemma l557 : p366 -> p3 -> s1 -> p608 (* ABS(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.015625]) *).
 intros h0 h1 h2.
 assert (h3 := l558 h0 h1 h2).
 assert (h4 := l534 h0 h1 h2).
 apply intersect_aa with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t324 : p608 -> p607.
 intros h0.
 refine (bnd_of_abs r8 i463 i464 h0 _) ; finalize.
Qed.
Lemma l556 : p366 -> p3 -> s1 -> p607 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-0.015625, 0.015625]) *).
 intros h0 h1 h2.
 assert (h3 := l557 h0 h1 h2).
 apply t324. exact h3.
Qed.
Lemma l555 : p366 -> p3 -> s1 -> p606 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.015625]) *).
 intros h0 h1 h2.
 assert (h3 := l556 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition i467 := makepairF f4 f300.
Notation p612 := (BND r8 i467). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 0.03125]) *)
Lemma t325 : p599 -> p612 -> p598.
 intros h0 h1.
 refine (div_pp r9 r8 i457 i467 i456 h0 h1 _) ; finalize.
Qed.
Lemma l548 : p366 -> p3 -> s1 -> p598 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [3.55271e-15, 2.28882e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l549 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t325. exact h3. refine (subset r8 i463 i467 h4 _) ; finalize.
Qed.
Lemma t326 : p14 -> p18 -> p598 -> p597.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i456 h0 h1 h2) ; finalize.
Qed.
Lemma l547 : p366 -> p3 -> s1 -> p597 (* BND(1 / b, [3.55271e-15, 2.28882e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l548 h0 h1 h2).
 apply t326. exact h3. exact h4. exact h5.
Qed.
Lemma l546 : p366 -> p3 -> s1 -> p596 (* BND(1 / b, [4.65661e-10, 2.28882e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l547 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t327 : p448 -> p596 -> p447.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i341 i455 i340 h0 h1 _) ; finalize.
Qed.
Lemma l412 : p366 -> p3 -> s1 -> p447 (* BND(inv_b_hat - 1 / b, [-2.72848e-12, -8.47033e-22]) *).
 intros h0 h1 h2.
 assert (h3 := l413 h0 h1 h2).
 assert (h4 := l546 h0 h1 h2).
 apply t327. exact h3. exact h4.
Qed.
Lemma l411 : p366 -> p3 -> s1 -> p446 (* BND(inv_b_hat - 1 / b, [-2.72848e-12, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l412 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t328 : p39 -> p446 -> p445.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i339 i338 h0 h1 _) ; finalize.
Qed.
Lemma l410 : p366 -> p3 -> s1 -> p445 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.72848e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l411 h0 h1 h2).
 apply t328. exact h3. exact h4.
Qed.
Lemma t329 : p445 -> p606 -> p444.
 intros h0 h1.
 refine (div_pp r9 r8 i338 i463 i337 h0 h1 _) ; finalize.
Qed.
Lemma l409 : p366 -> p3 -> s1 -> p444 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.14441e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l410 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t329. exact h3. exact h4.
Qed.
Lemma t330 : p14 -> p18 -> p444 -> p443.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i337 h0 h1 h2) ; finalize.
Qed.
Lemma l408 : p366 -> p3 -> s1 -> p443 (* BND(1 / b, [7.10543e-15, 1.14441e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l409 h0 h1 h2).
 apply t330. exact h3. exact h4. exact h5.
Qed.
Lemma l407 : p366 -> p3 -> s1 -> p442 (* BND(1 / b, [4.65661e-10, 1.14441e-05]) *).
 intros h0 h1 h2.
 assert (h3 := l408 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t331 : p367 -> p218 -> p441.
 intros h0 h1.
 refine (div_np r11 r12 i27 i159 i335 h0 h1 _) ; finalize.
Qed.
Lemma l406 : p366 -> p3 -> s1 -> p441 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -7.27596e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l407 h0 h1 h2).
 apply t331. exact h3. refine (subset r12 i336 i159 h4 _) ; finalize.
Qed.
Lemma l405 : p366 -> p3 -> s1 -> p440 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -7.27596e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l406 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t332 : p18 -> p440 -> p439.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i334 h0 h1) ; finalize.
Qed.
Lemma l404 : p366 -> p3 -> s1 -> p439 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -7.27596e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l405 h0 h1 h2).
 apply t332. exact h3. exact h4.
Qed.
Definition f332 := Float2 (422212045635805) (-66).
Definition i468 := makepairF f13 f332.
Notation p613 := (BND r12 i468). (* BND(1 / b, [4.65661e-10, 5.72204e-06]) *)
Definition i469 := makepairF f71 f332.
Notation p614 := (BND r12 i469). (* BND(1 / b, [7.10543e-15, 5.72204e-06]) *)
Notation p615 := (BND r19 i469). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 5.72204e-06]) *)
Definition f333 := Float2 (422212045635805) (-88).
Definition i470 := makepairF f16 f333.
Notation p616 := (BND r9 i470). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.36424e-12]) *)
Definition f334 := Float2 (-422212045635805) (-88).
Definition i471 := makepairF f334 f18.
Notation p617 := (BND r11 i471). (* BND(inv_b_hat - 1 / b, [-1.36424e-12, -1.11022e-16]) *)
Definition f335 := Float2 (-1) (-69).
Definition i472 := makepairF f334 f335.
Notation p618 := (BND r11 i472). (* BND(inv_b_hat - 1 / b, [-1.36424e-12, -1.69407e-21]) *)
Definition f336 := Float2 (-1) (-38).
Definition i473 := makepairF f33 f336.
Notation p619 := (REL _inv_b_hat r12 i473). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -3.63798e-12]) *)
Notation p620 := (BND r20 i473). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -3.63798e-12]) *)
Definition i474 := makepairF f221 f336.
Notation p621 := (BND r20 i474). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -3.63798e-12]) *)
Definition f337 := Float2 (1) (-15).
Definition i475 := makepairF f13 f337.
Notation p622 := (BND r12 i475). (* BND(1 / b, [4.65661e-10, 3.05176e-05]) *)
Lemma t333 : p367 -> p622 -> p621.
 intros h0 h1.
 refine (div_np r11 r12 i27 i475 i474 h0 h1 _) ; finalize.
Qed.
Lemma l568 : p366 -> p3 -> s1 -> p621 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -3.63798e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l546 h0 h1 h2).
 apply t333. exact h3. refine (subset r12 i455 i475 h4 _) ; finalize.
Qed.
Lemma l567 : p366 -> p3 -> s1 -> p620 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -3.63798e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l568 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t334 : p18 -> p620 -> p619.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i473 h0 h1) ; finalize.
Qed.
Lemma l566 : p366 -> p3 -> s1 -> p619 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -3.63798e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l567 h0 h1 h2).
 apply t334. exact h3. exact h4.
Qed.
Lemma t335 : p619 -> p442 -> p618.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i473 i336 i472 h0 h1 _) ; finalize.
Qed.
Lemma l565 : p366 -> p3 -> s1 -> p618 (* BND(inv_b_hat - 1 / b, [-1.36424e-12, -1.69407e-21]) *).
 intros h0 h1 h2.
 assert (h3 := l566 h0 h1 h2).
 assert (h4 := l407 h0 h1 h2).
 apply t335. exact h3. exact h4.
Qed.
Lemma l564 : p366 -> p3 -> s1 -> p617 (* BND(inv_b_hat - 1 / b, [-1.36424e-12, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l565 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t336 : p39 -> p617 -> p616.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i471 i470 h0 h1 _) ; finalize.
Qed.
Lemma l563 : p366 -> p3 -> s1 -> p616 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.36424e-12]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l564 h0 h1 h2).
 apply t336. exact h3. exact h4.
Qed.
Lemma t337 : p616 -> p606 -> p615.
 intros h0 h1.
 refine (div_pp r9 r8 i470 i463 i469 h0 h1 _) ; finalize.
Qed.
Lemma l562 : p366 -> p3 -> s1 -> p615 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 5.72204e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l563 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t337. exact h3. exact h4.
Qed.
Lemma t338 : p14 -> p18 -> p615 -> p614.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i469 h0 h1 h2) ; finalize.
Qed.
Lemma l561 : p366 -> p3 -> s1 -> p614 (* BND(1 / b, [7.10543e-15, 5.72204e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l562 h0 h1 h2).
 apply t338. exact h3. exact h4. exact h5.
Qed.
Lemma l560 : p366 -> p3 -> s1 -> p613 (* BND(1 / b, [4.65661e-10, 5.72204e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l561 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t339 : p439 -> p613 -> p438.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i334 i468 i333 h0 h1 _) ; finalize.
Qed.
Lemma l403 : p366 -> p3 -> s1 -> p438 (* BND(inv_b_hat - 1 / b, [-6.8212e-13, -3.38813e-21]) *).
 intros h0 h1 h2.
 assert (h3 := l404 h0 h1 h2).
 assert (h4 := l560 h0 h1 h2).
 apply t339. exact h3. exact h4.
Qed.
Lemma l402 : p366 -> p3 -> s1 -> p437 (* BND(inv_b_hat - 1 / b, [-6.8212e-13, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l403 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t340 : p39 -> p437 -> p436.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i332 i331 h0 h1 _) ; finalize.
Qed.
Lemma l401 : p366 -> p3 -> s1 -> p436 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 6.8212e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l402 h0 h1 h2).
 apply t340. exact h3. exact h4.
Qed.
Lemma t341 : p436 -> p606 -> p435.
 intros h0 h1.
 refine (div_pp r9 r8 i331 i463 i330 h0 h1 _) ; finalize.
Qed.
Lemma l400 : p366 -> p3 -> s1 -> p435 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 2.86102e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l401 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t341. exact h3. exact h4.
Qed.
Lemma t342 : p14 -> p18 -> p435 -> p434.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i330 h0 h1 h2) ; finalize.
Qed.
Lemma l399 : p366 -> p3 -> s1 -> p434 (* BND(1 / b, [7.10543e-15, 2.86102e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l400 h0 h1 h2).
 apply t342. exact h3. exact h4. exact h5.
Qed.
Lemma l398 : p366 -> p3 -> s1 -> p433 (* BND(1 / b, [4.65661e-10, 2.86102e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l399 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t343 : p367 -> p242 -> p432.
 intros h0 h1.
 refine (div_np r11 r12 i27 i179 i328 h0 h1 _) ; finalize.
Qed.
Lemma l397 : p366 -> p3 -> s1 -> p432 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.91038e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l398 h0 h1 h2).
 apply t343. exact h3. refine (subset r12 i329 i179 h4 _) ; finalize.
Qed.
Lemma l396 : p366 -> p3 -> s1 -> p431 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.91038e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l397 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t344 : p18 -> p431 -> p430.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i327 h0 h1) ; finalize.
Qed.
Lemma l395 : p366 -> p3 -> s1 -> p430 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.91038e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l396 h0 h1 h2).
 apply t344. exact h3. exact h4.
Qed.
Definition f338 := Float2 (844423990608423) (-69).
Definition i476 := makepairF f13 f338.
Notation p623 := (BND r12 i476). (* BND(1 / b, [4.65661e-10, 1.43051e-06]) *)
Definition i477 := makepairF f71 f338.
Notation p624 := (BND r12 i477). (* BND(1 / b, [7.10543e-15, 1.43051e-06]) *)
Notation p625 := (BND r19 i477). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.43051e-06]) *)
Definition f339 := Float2 (844423990608423) (-91).
Definition i478 := makepairF f16 f339.
Notation p626 := (BND r9 i478). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 3.4106e-13]) *)
Definition f340 := Float2 (-844423990608423) (-91).
Definition i479 := makepairF f340 f18.
Notation p627 := (BND r11 i479). (* BND(inv_b_hat - 1 / b, [-3.4106e-13, -1.11022e-16]) *)
Definition f341 := Float2 (-1) (-67).
Definition i480 := makepairF f340 f341.
Notation p628 := (BND r11 i480). (* BND(inv_b_hat - 1 / b, [-3.4106e-13, -6.77626e-21]) *)
Definition f342 := Float2 (-1) (-36).
Definition i481 := makepairF f33 f342.
Notation p629 := (REL _inv_b_hat r12 i481). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.45519e-11]) *)
Notation p630 := (BND r20 i481). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.45519e-11]) *)
Definition i482 := makepairF f221 f342.
Notation p631 := (BND r20 i482). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.45519e-11]) *)
Lemma t345 : p367 -> p252 -> p631.
 intros h0 h1.
 refine (div_np r11 r12 i27 i187 i482 h0 h1 _) ; finalize.
Qed.
Lemma l577 : p366 -> p3 -> s1 -> p631 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.45519e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l560 h0 h1 h2).
 apply t345. exact h3. refine (subset r12 i468 i187 h4 _) ; finalize.
Qed.
Lemma l576 : p366 -> p3 -> s1 -> p630 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.45519e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l577 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t346 : p18 -> p630 -> p629.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i481 h0 h1) ; finalize.
Qed.
Lemma l575 : p366 -> p3 -> s1 -> p629 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.45519e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l576 h0 h1 h2).
 apply t346. exact h3. exact h4.
Qed.
Lemma t347 : p629 -> p433 -> p628.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i481 i329 i480 h0 h1 _) ; finalize.
Qed.
Lemma l574 : p366 -> p3 -> s1 -> p628 (* BND(inv_b_hat - 1 / b, [-3.4106e-13, -6.77626e-21]) *).
 intros h0 h1 h2.
 assert (h3 := l575 h0 h1 h2).
 assert (h4 := l398 h0 h1 h2).
 apply t347. exact h3. exact h4.
Qed.
Lemma l573 : p366 -> p3 -> s1 -> p627 (* BND(inv_b_hat - 1 / b, [-3.4106e-13, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l574 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t348 : p39 -> p627 -> p626.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i479 i478 h0 h1 _) ; finalize.
Qed.
Lemma l572 : p366 -> p3 -> s1 -> p626 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 3.4106e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l573 h0 h1 h2).
 apply t348. exact h3. exact h4.
Qed.
Lemma t349 : p626 -> p606 -> p625.
 intros h0 h1.
 refine (div_pp r9 r8 i478 i463 i477 h0 h1 _) ; finalize.
Qed.
Lemma l571 : p366 -> p3 -> s1 -> p625 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.43051e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l572 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t349. exact h3. exact h4.
Qed.
Lemma t350 : p14 -> p18 -> p625 -> p624.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i477 h0 h1 h2) ; finalize.
Qed.
Lemma l570 : p366 -> p3 -> s1 -> p624 (* BND(1 / b, [7.10543e-15, 1.43051e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l571 h0 h1 h2).
 apply t350. exact h3. exact h4. exact h5.
Qed.
Lemma l569 : p366 -> p3 -> s1 -> p623 (* BND(1 / b, [4.65661e-10, 1.43051e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l570 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t351 : p430 -> p623 -> p429.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i327 i476 i326 h0 h1 _) ; finalize.
Qed.
Lemma l394 : p366 -> p3 -> s1 -> p429 (* BND(inv_b_hat - 1 / b, [-1.7053e-13, -1.35525e-20]) *).
 intros h0 h1 h2.
 assert (h3 := l395 h0 h1 h2).
 assert (h4 := l569 h0 h1 h2).
 apply t351. exact h3. exact h4.
Qed.
Lemma l393 : p366 -> p3 -> s1 -> p428 (* BND(inv_b_hat - 1 / b, [-1.7053e-13, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l394 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t352 : p39 -> p428 -> p427.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i325 i324 h0 h1 _) ; finalize.
Qed.
Lemma l392 : p366 -> p3 -> s1 -> p427 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.7053e-13]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l393 h0 h1 h2).
 apply t352. exact h3. exact h4.
Qed.
Lemma t353 : p427 -> p606 -> p426.
 intros h0 h1.
 refine (div_pp r9 r8 i324 i463 i323 h0 h1 _) ; finalize.
Qed.
Lemma l391 : p366 -> p3 -> s1 -> p426 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 7.15255e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l392 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t353. exact h3. exact h4.
Qed.
Lemma t354 : p14 -> p18 -> p426 -> p425.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i323 h0 h1 h2) ; finalize.
Qed.
Lemma l390 : p366 -> p3 -> s1 -> p425 (* BND(1 / b, [7.10543e-15, 7.15255e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l391 h0 h1 h2).
 apply t354. exact h3. exact h4. exact h5.
Qed.
Lemma l389 : p366 -> p3 -> s1 -> p424 (* BND(1 / b, [4.65661e-10, 7.15255e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l390 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t355 : p367 -> p259 -> p423.
 intros h0 h1.
 refine (div_np r11 r12 i27 i194 i321 h0 h1 _) ; finalize.
Qed.
Lemma l388 : p366 -> p3 -> s1 -> p423 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.16415e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l389 h0 h1 h2).
 apply t355. exact h3. refine (subset r12 i322 i194 h4 _) ; finalize.
Qed.
Lemma l387 : p366 -> p3 -> s1 -> p422 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.16415e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l388 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t356 : p18 -> p422 -> p421.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i320 h0 h1) ; finalize.
Qed.
Lemma l386 : p366 -> p3 -> s1 -> p421 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.16415e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l387 h0 h1 h2).
 apply t356. exact h3. exact h4.
Qed.
Definition f343 := Float2 (26388246560789) (-66).
Definition i483 := makepairF f13 f343.
Notation p632 := (BND r12 i483). (* BND(1 / b, [4.65661e-10, 3.57627e-07]) *)
Definition i484 := makepairF f71 f343.
Notation p633 := (BND r12 i484). (* BND(1 / b, [7.10543e-15, 3.57627e-07]) *)
Notation p634 := (BND r19 i484). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 3.57627e-07]) *)
Definition f344 := Float2 (26388246560789) (-88).
Definition i485 := makepairF f16 f344.
Notation p635 := (BND r9 i485). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 8.5265e-14]) *)
Definition f345 := Float2 (-26388246560789) (-88).
Definition i486 := makepairF f345 f18.
Notation p636 := (BND r11 i486). (* BND(inv_b_hat - 1 / b, [-8.5265e-14, -1.11022e-16]) *)
Definition f346 := Float2 (-1) (-65).
Definition i487 := makepairF f345 f346.
Notation p637 := (BND r11 i487). (* BND(inv_b_hat - 1 / b, [-8.5265e-14, -2.71051e-20]) *)
Definition f347 := Float2 (-1) (-34).
Definition i488 := makepairF f33 f347.
Notation p638 := (REL _inv_b_hat r12 i488). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -5.82077e-11]) *)
Notation p639 := (BND r20 i488). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -5.82077e-11]) *)
Definition i489 := makepairF f221 f347.
Notation p640 := (BND r20 i489). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -5.82077e-11]) *)
Lemma t357 : p367 -> p269 -> p640.
 intros h0 h1.
 refine (div_np r11 r12 i27 i202 i489 h0 h1 _) ; finalize.
Qed.
Lemma l586 : p366 -> p3 -> s1 -> p640 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -5.82077e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l569 h0 h1 h2).
 apply t357. exact h3. refine (subset r12 i476 i202 h4 _) ; finalize.
Qed.
Lemma l585 : p366 -> p3 -> s1 -> p639 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -5.82077e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l586 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t358 : p18 -> p639 -> p638.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i488 h0 h1) ; finalize.
Qed.
Lemma l584 : p366 -> p3 -> s1 -> p638 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -5.82077e-11]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l585 h0 h1 h2).
 apply t358. exact h3. exact h4.
Qed.
Lemma t359 : p638 -> p424 -> p637.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i488 i322 i487 h0 h1 _) ; finalize.
Qed.
Lemma l583 : p366 -> p3 -> s1 -> p637 (* BND(inv_b_hat - 1 / b, [-8.5265e-14, -2.71051e-20]) *).
 intros h0 h1 h2.
 assert (h3 := l584 h0 h1 h2).
 assert (h4 := l389 h0 h1 h2).
 apply t359. exact h3. exact h4.
Qed.
Lemma l582 : p366 -> p3 -> s1 -> p636 (* BND(inv_b_hat - 1 / b, [-8.5265e-14, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l583 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t360 : p39 -> p636 -> p635.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i486 i485 h0 h1 _) ; finalize.
Qed.
Lemma l581 : p366 -> p3 -> s1 -> p635 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 8.5265e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l582 h0 h1 h2).
 apply t360. exact h3. exact h4.
Qed.
Lemma t361 : p635 -> p606 -> p634.
 intros h0 h1.
 refine (div_pp r9 r8 i485 i463 i484 h0 h1 _) ; finalize.
Qed.
Lemma l580 : p366 -> p3 -> s1 -> p634 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 3.57627e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l581 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t361. exact h3. exact h4.
Qed.
Lemma t362 : p14 -> p18 -> p634 -> p633.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i484 h0 h1 h2) ; finalize.
Qed.
Lemma l579 : p366 -> p3 -> s1 -> p633 (* BND(1 / b, [7.10543e-15, 3.57627e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l580 h0 h1 h2).
 apply t362. exact h3. exact h4. exact h5.
Qed.
Lemma l578 : p366 -> p3 -> s1 -> p632 (* BND(1 / b, [4.65661e-10, 3.57627e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l579 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t363 : p421 -> p632 -> p420.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i320 i483 i319 h0 h1 _) ; finalize.
Qed.
Lemma l385 : p366 -> p3 -> s1 -> p420 (* BND(inv_b_hat - 1 / b, [-4.26325e-14, -5.42101e-20]) *).
 intros h0 h1 h2.
 assert (h3 := l386 h0 h1 h2).
 assert (h4 := l578 h0 h1 h2).
 apply t363. exact h3. exact h4.
Qed.
Lemma l384 : p366 -> p3 -> s1 -> p419 (* BND(inv_b_hat - 1 / b, [-4.26325e-14, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l385 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t364 : p39 -> p419 -> p418.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i318 i317 h0 h1 _) ; finalize.
Qed.
Lemma l383 : p366 -> p3 -> s1 -> p418 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 4.26325e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l384 h0 h1 h2).
 apply t364. exact h3. exact h4.
Qed.
Lemma t365 : p418 -> p606 -> p417.
 intros h0 h1.
 refine (div_pp r9 r8 i317 i463 i316 h0 h1 _) ; finalize.
Qed.
Lemma l382 : p366 -> p3 -> s1 -> p417 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.78814e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l383 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t365. exact h3. exact h4.
Qed.
Lemma t366 : p14 -> p18 -> p417 -> p416.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i316 h0 h1 h2) ; finalize.
Qed.
Lemma l381 : p366 -> p3 -> s1 -> p416 (* BND(1 / b, [7.10543e-15, 1.78814e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l382 h0 h1 h2).
 apply t366. exact h3. exact h4. exact h5.
Qed.
Lemma l380 : p366 -> p3 -> s1 -> p415 (* BND(1 / b, [4.65661e-10, 1.78814e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l381 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t367 : p367 -> p276 -> p414.
 intros h0 h1.
 refine (div_np r11 r12 i27 i209 i314 h0 h1 _) ; finalize.
Qed.
Lemma l379 : p366 -> p3 -> s1 -> p414 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -4.65661e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l380 h0 h1 h2).
 apply t367. exact h3. refine (subset r12 i315 i209 h4 _) ; finalize.
Qed.
Lemma l378 : p366 -> p3 -> s1 -> p413 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -4.65661e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l379 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t368 : p18 -> p413 -> p412.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i313 h0 h1) ; finalize.
Qed.
Lemma l377 : p366 -> p3 -> s1 -> p412 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -4.65661e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l378 h0 h1 h2).
 apply t368. exact h3. exact h4.
Qed.
Definition f348 := Float2 (844423789282085) (-73).
Definition i490 := makepairF f13 f348.
Notation p641 := (BND r12 i490). (* BND(1 / b, [4.65661e-10, 8.94068e-08]) *)
Definition i491 := makepairF f71 f348.
Notation p642 := (BND r12 i491). (* BND(1 / b, [7.10543e-15, 8.94068e-08]) *)
Notation p643 := (BND r19 i491). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 8.94068e-08]) *)
Definition f349 := Float2 (844423789282085) (-95).
Definition i492 := makepairF f16 f349.
Notation p644 := (BND r9 i492). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.13163e-14]) *)
Definition f350 := Float2 (-844423789282085) (-95).
Definition i493 := makepairF f350 f18.
Notation p645 := (BND r11 i493). (* BND(inv_b_hat - 1 / b, [-2.13163e-14, -1.11022e-16]) *)
Definition f351 := Float2 (-1) (-63).
Definition i494 := makepairF f350 f351.
Notation p646 := (BND r11 i494). (* BND(inv_b_hat - 1 / b, [-2.13163e-14, -1.0842e-19]) *)
Definition f352 := Float2 (-1) (-32).
Definition i495 := makepairF f33 f352.
Notation p647 := (REL _inv_b_hat r12 i495). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.32831e-10]) *)
Notation p648 := (BND r20 i495). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.32831e-10]) *)
Definition i496 := makepairF f221 f352.
Notation p649 := (BND r20 i496). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.32831e-10]) *)
Lemma t369 : p367 -> p286 -> p649.
 intros h0 h1.
 refine (div_np r11 r12 i27 i217 i496 h0 h1 _) ; finalize.
Qed.
Lemma l595 : p366 -> p3 -> s1 -> p649 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.32831e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l578 h0 h1 h2).
 apply t369. exact h3. refine (subset r12 i483 i217 h4 _) ; finalize.
Qed.
Lemma l594 : p366 -> p3 -> s1 -> p648 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.32831e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l595 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t370 : p18 -> p648 -> p647.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i495 h0 h1) ; finalize.
Qed.
Lemma l593 : p366 -> p3 -> s1 -> p647 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.32831e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l594 h0 h1 h2).
 apply t370. exact h3. exact h4.
Qed.
Lemma t371 : p647 -> p415 -> p646.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i495 i315 i494 h0 h1 _) ; finalize.
Qed.
Lemma l592 : p366 -> p3 -> s1 -> p646 (* BND(inv_b_hat - 1 / b, [-2.13163e-14, -1.0842e-19]) *).
 intros h0 h1 h2.
 assert (h3 := l593 h0 h1 h2).
 assert (h4 := l380 h0 h1 h2).
 apply t371. exact h3. exact h4.
Qed.
Lemma l591 : p366 -> p3 -> s1 -> p645 (* BND(inv_b_hat - 1 / b, [-2.13163e-14, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l592 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t372 : p39 -> p645 -> p644.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i493 i492 h0 h1 _) ; finalize.
Qed.
Lemma l590 : p366 -> p3 -> s1 -> p644 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.13163e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l591 h0 h1 h2).
 apply t372. exact h3. exact h4.
Qed.
Lemma t373 : p644 -> p606 -> p643.
 intros h0 h1.
 refine (div_pp r9 r8 i492 i463 i491 h0 h1 _) ; finalize.
Qed.
Lemma l589 : p366 -> p3 -> s1 -> p643 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 8.94068e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l590 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t373. exact h3. exact h4.
Qed.
Lemma t374 : p14 -> p18 -> p643 -> p642.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i491 h0 h1 h2) ; finalize.
Qed.
Lemma l588 : p366 -> p3 -> s1 -> p642 (* BND(1 / b, [7.10543e-15, 8.94068e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l589 h0 h1 h2).
 apply t374. exact h3. exact h4. exact h5.
Qed.
Lemma l587 : p366 -> p3 -> s1 -> p641 (* BND(1 / b, [4.65661e-10, 8.94068e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l588 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t375 : p412 -> p641 -> p411.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i313 i490 i312 h0 h1 _) ; finalize.
Qed.
Lemma l376 : p366 -> p3 -> s1 -> p411 (* BND(inv_b_hat - 1 / b, [-1.06581e-14, -2.1684e-19]) *).
 intros h0 h1 h2.
 assert (h3 := l377 h0 h1 h2).
 assert (h4 := l587 h0 h1 h2).
 apply t375. exact h3. exact h4.
Qed.
Lemma l375 : p366 -> p3 -> s1 -> p410 (* BND(inv_b_hat - 1 / b, [-1.06581e-14, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l376 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t376 : p39 -> p410 -> p409.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i311 i310 h0 h1 _) ; finalize.
Qed.
Lemma l374 : p366 -> p3 -> s1 -> p409 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.06581e-14]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l375 h0 h1 h2).
 apply t376. exact h3. exact h4.
Qed.
Lemma t377 : p409 -> p606 -> p408.
 intros h0 h1.
 refine (div_pp r9 r8 i310 i463 i309 h0 h1 _) ; finalize.
Qed.
Lemma l373 : p366 -> p3 -> s1 -> p408 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 4.47034e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l374 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t377. exact h3. exact h4.
Qed.
Lemma t378 : p14 -> p18 -> p408 -> p407.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i309 h0 h1 h2) ; finalize.
Qed.
Lemma l372 : p366 -> p3 -> s1 -> p407 (* BND(1 / b, [7.10543e-15, 4.47034e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l373 h0 h1 h2).
 apply t378. exact h3. exact h4. exact h5.
Qed.
Lemma l371 : p366 -> p3 -> s1 -> p406 (* BND(1 / b, [4.65661e-10, 4.47034e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l372 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t379 : p367 -> p293 -> p405.
 intros h0 h1.
 refine (div_np r11 r12 i27 i224 i307 h0 h1 _) ; finalize.
Qed.
Lemma l370 : p366 -> p3 -> s1 -> p405 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -1.86265e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l371 h0 h1 h2).
 apply t379. exact h3. refine (subset r12 i308 i224 h4 _) ; finalize.
Qed.
Lemma l369 : p366 -> p3 -> s1 -> p404 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -1.86265e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l370 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t380 : p18 -> p404 -> p403.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i306 h0 h1) ; finalize.
Qed.
Lemma l368 : p366 -> p3 -> s1 -> p403 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -1.86265e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l369 h0 h1 h2).
 apply t380. exact h3. exact h4.
Qed.
Definition f353 := Float2 (422211844309467) (-74).
Definition i497 := makepairF f13 f353.
Notation p650 := (BND r12 i497). (* BND(1 / b, [4.65661e-10, 2.23517e-08]) *)
Definition i498 := makepairF f71 f353.
Notation p651 := (BND r12 i498). (* BND(1 / b, [7.10543e-15, 2.23517e-08]) *)
Notation p652 := (BND r19 i498). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 2.23517e-08]) *)
Definition f354 := Float2 (422211844309467) (-96).
Definition i499 := makepairF f16 f354.
Notation p653 := (BND r9 i499). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 5.32906e-15]) *)
Definition f355 := Float2 (-422211844309467) (-96).
Definition i500 := makepairF f355 f18.
Notation p654 := (BND r11 i500). (* BND(inv_b_hat - 1 / b, [-5.32906e-15, -1.11022e-16]) *)
Definition f356 := Float2 (-1) (-61).
Definition i501 := makepairF f355 f356.
Notation p655 := (BND r11 i501). (* BND(inv_b_hat - 1 / b, [-5.32906e-15, -4.33681e-19]) *)
Definition f357 := Float2 (-1) (-30).
Definition i502 := makepairF f33 f357.
Notation p656 := (REL _inv_b_hat r12 i502). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -9.31323e-10]) *)
Notation p657 := (BND r20 i502). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -9.31323e-10]) *)
Definition i503 := makepairF f221 f357.
Notation p658 := (BND r20 i503). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -9.31323e-10]) *)
Lemma t381 : p367 -> p303 -> p658.
 intros h0 h1.
 refine (div_np r11 r12 i27 i232 i503 h0 h1 _) ; finalize.
Qed.
Lemma l604 : p366 -> p3 -> s1 -> p658 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -9.31323e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l587 h0 h1 h2).
 apply t381. exact h3. refine (subset r12 i490 i232 h4 _) ; finalize.
Qed.
Lemma l603 : p366 -> p3 -> s1 -> p657 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -9.31323e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l604 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t382 : p18 -> p657 -> p656.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i502 h0 h1) ; finalize.
Qed.
Lemma l602 : p366 -> p3 -> s1 -> p656 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -9.31323e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l603 h0 h1 h2).
 apply t382. exact h3. exact h4.
Qed.
Lemma t383 : p656 -> p406 -> p655.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i502 i308 i501 h0 h1 _) ; finalize.
Qed.
Lemma l601 : p366 -> p3 -> s1 -> p655 (* BND(inv_b_hat - 1 / b, [-5.32906e-15, -4.33681e-19]) *).
 intros h0 h1 h2.
 assert (h3 := l602 h0 h1 h2).
 assert (h4 := l371 h0 h1 h2).
 apply t383. exact h3. exact h4.
Qed.
Lemma l600 : p366 -> p3 -> s1 -> p654 (* BND(inv_b_hat - 1 / b, [-5.32906e-15, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l601 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t384 : p39 -> p654 -> p653.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i500 i499 h0 h1 _) ; finalize.
Qed.
Lemma l599 : p366 -> p3 -> s1 -> p653 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 5.32906e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l600 h0 h1 h2).
 apply t384. exact h3. exact h4.
Qed.
Lemma t385 : p653 -> p606 -> p652.
 intros h0 h1.
 refine (div_pp r9 r8 i499 i463 i498 h0 h1 _) ; finalize.
Qed.
Lemma l598 : p366 -> p3 -> s1 -> p652 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 2.23517e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l599 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t385. exact h3. exact h4.
Qed.
Lemma t386 : p14 -> p18 -> p652 -> p651.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i498 h0 h1 h2) ; finalize.
Qed.
Lemma l597 : p366 -> p3 -> s1 -> p651 (* BND(1 / b, [7.10543e-15, 2.23517e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l598 h0 h1 h2).
 apply t386. exact h3. exact h4. exact h5.
Qed.
Lemma l596 : p366 -> p3 -> s1 -> p650 (* BND(1 / b, [4.65661e-10, 2.23517e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l597 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t387 : p403 -> p650 -> p402.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i306 i497 i305 h0 h1 _) ; finalize.
Qed.
Lemma l367 : p366 -> p3 -> s1 -> p402 (* BND(inv_b_hat - 1 / b, [-2.66453e-15, -8.67362e-19]) *).
 intros h0 h1 h2.
 assert (h3 := l368 h0 h1 h2).
 assert (h4 := l596 h0 h1 h2).
 apply t387. exact h3. exact h4.
Qed.
Lemma l366 : p366 -> p3 -> s1 -> p401 (* BND(inv_b_hat - 1 / b, [-2.66453e-15, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l367 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t388 : p39 -> p401 -> p400.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i304 i303 h0 h1 _) ; finalize.
Qed.
Lemma l365 : p366 -> p3 -> s1 -> p400 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.66453e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l366 h0 h1 h2).
 apply t388. exact h3. exact h4.
Qed.
Lemma t389 : p400 -> p606 -> p399.
 intros h0 h1.
 refine (div_pp r9 r8 i303 i463 i302 h0 h1 _) ; finalize.
Qed.
Lemma l364 : p366 -> p3 -> s1 -> p399 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 1.11759e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l365 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t389. exact h3. exact h4.
Qed.
Lemma t390 : p14 -> p18 -> p399 -> p398.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i302 h0 h1 h2) ; finalize.
Qed.
Lemma l363 : p366 -> p3 -> s1 -> p398 (* BND(1 / b, [7.10543e-15, 1.11759e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l364 h0 h1 h2).
 apply t390. exact h3. exact h4. exact h5.
Qed.
Lemma l362 : p366 -> p3 -> s1 -> p397 (* BND(1 / b, [4.65661e-10, 1.11759e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l363 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t391 : p367 -> p310 -> p396.
 intros h0 h1.
 refine (div_np r11 r12 i27 i239 i300 h0 h1 _) ; finalize.
Qed.
Lemma l361 : p366 -> p3 -> s1 -> p396 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -7.45058e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l362 h0 h1 h2).
 apply t391. exact h3. refine (subset r12 i301 i239 h4 _) ; finalize.
Qed.
Lemma l360 : p366 -> p3 -> s1 -> p395 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -7.45058e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l361 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t392 : p18 -> p395 -> p394.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i299 h0 h1) ; finalize.
Qed.
Lemma l359 : p366 -> p3 -> s1 -> p394 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -7.45058e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l360 h0 h1 h2).
 apply t392. exact h3. exact h4.
Qed.
Definition f358 := Float2 (844423587955795) (-77).
Definition i504 := makepairF f13 f358.
Notation p659 := (BND r12 i504). (* BND(1 / b, [4.65661e-10, 5.58793e-09]) *)
Definition i505 := makepairF f71 f358.
Notation p660 := (BND r12 i505). (* BND(1 / b, [7.10543e-15, 5.58793e-09]) *)
Notation p661 := (BND r19 i505). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 5.58793e-09]) *)
Definition f359 := Float2 (844423587955795) (-99).
Definition i506 := makepairF f16 f359.
Notation p662 := (BND r9 i506). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.33227e-15]) *)
Definition f360 := Float2 (-844423587955795) (-99).
Definition i507 := makepairF f360 f18.
Notation p663 := (BND r11 i507). (* BND(inv_b_hat - 1 / b, [-1.33227e-15, -1.11022e-16]) *)
Definition f361 := Float2 (-1) (-59).
Definition i508 := makepairF f360 f361.
Notation p664 := (BND r11 i508). (* BND(inv_b_hat - 1 / b, [-1.33227e-15, -1.73472e-18]) *)
Definition f362 := Float2 (-1) (-28).
Definition i509 := makepairF f33 f362.
Notation p665 := (REL _inv_b_hat r12 i509). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -3.72529e-09]) *)
Notation p666 := (BND r20 i509). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -3.72529e-09]) *)
Definition i510 := makepairF f221 f362.
Notation p667 := (BND r20 i510). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -3.72529e-09]) *)
Lemma t393 : p367 -> p320 -> p667.
 intros h0 h1.
 refine (div_np r11 r12 i27 i247 i510 h0 h1 _) ; finalize.
Qed.
Lemma l613 : p366 -> p3 -> s1 -> p667 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -3.72529e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l596 h0 h1 h2).
 apply t393. exact h3. refine (subset r12 i497 i247 h4 _) ; finalize.
Qed.
Lemma l612 : p366 -> p3 -> s1 -> p666 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -3.72529e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l613 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t394 : p18 -> p666 -> p665.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i509 h0 h1) ; finalize.
Qed.
Lemma l611 : p366 -> p3 -> s1 -> p665 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -3.72529e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l612 h0 h1 h2).
 apply t394. exact h3. exact h4.
Qed.
Lemma t395 : p665 -> p397 -> p664.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i509 i301 i508 h0 h1 _) ; finalize.
Qed.
Lemma l610 : p366 -> p3 -> s1 -> p664 (* BND(inv_b_hat - 1 / b, [-1.33227e-15, -1.73472e-18]) *).
 intros h0 h1 h2.
 assert (h3 := l611 h0 h1 h2).
 assert (h4 := l362 h0 h1 h2).
 apply t395. exact h3. exact h4.
Qed.
Lemma l609 : p366 -> p3 -> s1 -> p663 (* BND(inv_b_hat - 1 / b, [-1.33227e-15, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l610 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t396 : p39 -> p663 -> p662.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i507 i506 h0 h1 _) ; finalize.
Qed.
Lemma l608 : p366 -> p3 -> s1 -> p662 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.33227e-15]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l609 h0 h1 h2).
 apply t396. exact h3. exact h4.
Qed.
Lemma t397 : p662 -> p606 -> p661.
 intros h0 h1.
 refine (div_pp r9 r8 i506 i463 i505 h0 h1 _) ; finalize.
Qed.
Lemma l607 : p366 -> p3 -> s1 -> p661 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [7.10543e-15, 5.58793e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l608 h0 h1 h2).
 assert (h4 := l555 h0 h1 h2).
 apply t397. exact h3. exact h4.
Qed.
Lemma t398 : p14 -> p18 -> p661 -> p660.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i505 h0 h1 h2) ; finalize.
Qed.
Lemma l606 : p366 -> p3 -> s1 -> p660 (* BND(1 / b, [7.10543e-15, 5.58793e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l607 h0 h1 h2).
 apply t398. exact h3. exact h4. exact h5.
Qed.
Lemma l605 : p366 -> p3 -> s1 -> p659 (* BND(1 / b, [4.65661e-10, 5.58793e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l606 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t399 : p394 -> p659 -> p393.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i299 i504 i298 h0 h1 _) ; finalize.
Qed.
Lemma l358 : p366 -> p3 -> s1 -> p393 (* BND(inv_b_hat - 1 / b, [-6.66133e-16, -3.46945e-18]) *).
 intros h0 h1 h2.
 assert (h3 := l359 h0 h1 h2).
 assert (h4 := l605 h0 h1 h2).
 apply t399. exact h3. exact h4.
Qed.
Lemma l357 : p366 -> p3 -> s1 -> p392 (* BND(inv_b_hat - 1 / b, [-6.66133e-16, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l358 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t400 : p39 -> p392 -> p391.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i297 i296 h0 h1 _) ; finalize.
Qed.
Lemma l356 : p366 -> p3 -> s1 -> p391 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 6.66133e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l357 h0 h1 h2).
 apply t400. exact h3. exact h4.
Qed.
Definition i511 := makepairF f4 f167.
Notation p668 := (BND r8 i511). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 1.90735e-06]) *)
Definition i512 := makepairF f16 f167.
Notation p669 := (BND r8 i512). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 1.90735e-06]) *)
Definition i513 := makepairF f16 f321.
Notation p670 := (BND r9 i513). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 8.88178e-16]) *)
Lemma t401 : p670 -> p47 -> p669.
 intros h0 h1.
 refine (div_pp r9 r12 i513 i24 i512 h0 h1 _) ; finalize.
Qed.
Lemma l615 : p366 -> p3 -> s1 -> p669 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 1.90735e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l356 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply t401. refine (subset r9 i296 i513 h3 _) ; finalize. exact h4.
Qed.
Lemma l614 : p366 -> p3 -> s1 -> p668 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 1.90735e-06]) *).
 intros h0 h1 h2.
 assert (h3 := l615 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t402 : p391 -> p668 -> p390.
 intros h0 h1.
 refine (div_pp r9 r8 i296 i511 i295 h0 h1 _) ; finalize.
Qed.
Lemma l355 : p366 -> p3 -> s1 -> p390 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [5.82077e-11, 2.79396e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l356 h0 h1 h2).
 assert (h4 := l614 h0 h1 h2).
 apply t402. exact h3. exact h4.
Qed.
Lemma t403 : p14 -> p18 -> p390 -> p389.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i295 h0 h1 h2) ; finalize.
Qed.
Lemma l354 : p366 -> p3 -> s1 -> p389 (* BND(1 / b, [5.82077e-11, 2.79396e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l355 h0 h1 h2).
 apply t403. exact h3. exact h4. exact h5.
Qed.
Lemma l353 : p366 -> p3 -> s1 -> p388 (* BND(1 / b, [4.65661e-10, 2.79396e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l354 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition f363 := Float2 (3) (-30).
Definition i514 := makepairF f13 f363.
Notation p671 := (BND r12 i514). (* BND(1 / b, [4.65661e-10, 2.79397e-09]) *)
Lemma t404 : p671 -> p362 -> p387.
 intros h0 h1.
 refine (bnd_of_bnd_rel_p r6 r12 i514 i282 i293 h0 h1 _) ; finalize.
Qed.
Lemma l352 : p366 -> p3 -> s1 -> p387 (* BND(1 / float<24,-149,ne>(b), [-0, 3.72529e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l353 h0 h1 h2).
 assert (h4 := l6 h2).
 apply t404. refine (subset r12 i294 i514 h3 _) ; finalize. refine (rel_subset r6 r12 i4 i282 h4 _) ; finalize.
Qed.
Lemma l351 : p366 -> p3 -> s1 -> p386 (* BND(1 / float<24,-149,ne>(b), [4.65661e-10, 3.72529e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l352 h0 h1 h2).
 assert (h4 := l321 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t405 : p386 -> p385.
 intros h0.
 refine (abs_of_bnd_p r6 i254 i254 h0 _) ; finalize.
Qed.
Lemma l350 : p366 -> p3 -> s1 -> p385 (* ABS(1 / float<24,-149,ne>(b), [4.65661e-10, 3.72529e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l351 h0 h1 h2).
 apply t405. exact h3.
Qed.
Lemma t406 : p385 -> p384.
 intros h0.
 refine (float_absolute_wide_ne _ _ r6 i254 i292 h0 _) ; finalize.
Qed.
Lemma l349 : p366 -> p3 -> s1 -> p384 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-1.11022e-16, 2.22045e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l350 h0 h1 h2).
 apply t406. exact h3.
Qed.
Definition f364 := Float2 (-211105871823167) (-100).
Definition i515 := makepairF f364 f226.
Notation p672 := (BND r13 i515). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-1.66533e-16, 2.22045e-16]) *)
Definition f365 := Float2 (5) (-26).
Definition i516 := makepairF f8 f365.
Notation p673 := (REL r6 r12 i516). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 7.45058e-08]) *)
Lemma t407 : p673 -> p388 -> p672.
 intros h0 h1.
 refine (error_of_rel_op r6 r12 i516 i294 i515 h0 h1 _) ; finalize.
Qed.
Lemma l616 : p366 -> p3 -> s1 -> p672 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-1.66533e-16, 2.22045e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l6 h2).
 assert (h4 := l353 h0 h1 h2).
 apply t407. refine (rel_subset r6 r12 i4 i516 h3 _) ; finalize. exact h4.
Qed.
Lemma t408 : p384 -> p672 -> p383.
 intros h0 h1.
 refine (add r18 r13 i292 i515 i291 h0 h1 _) ; finalize.
Qed.
Lemma l348 : p366 -> p3 -> s1 -> p383 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-2.77555e-16, 4.44089e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l349 h0 h1 h2).
 assert (h4 := l616 h0 h1 h2).
 apply t408. exact h3. exact h4.
Qed.
Lemma t409 : p383 -> p382.
 intros h0.
 refine (sub_xals _ _ _ i291 h0) ; finalize.
Qed.
Lemma l347 : p366 -> p3 -> s1 -> p382 (* BND(inv_b_hat - 1 / b, [-2.77555e-16, 4.44089e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l348 h0 h1 h2).
 apply t409. exact h3.
Qed.
Lemma l346 : p366 -> p3 -> s1 -> p381 (* BND(inv_b_hat - 1 / b, [-2.77555e-16, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l347 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t410 : p39 -> p381 -> p380.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i290 i289 h0 h1 _) ; finalize.
Qed.
Lemma l345 : p366 -> p3 -> s1 -> p380 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.77555e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l346 h0 h1 h2).
 apply t410. exact h3. exact h4.
Qed.
Definition i517 := makepairF f4 f162.
Notation p674 := (BND r8 i517). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 9.53674e-07]) *)
Definition i518 := makepairF f16 f162.
Notation p675 := (BND r8 i518). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 9.53674e-07]) *)
Definition i519 := makepairF f16 f225.
Notation p676 := (BND r9 i519). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 4.44089e-16]) *)
Lemma t411 : p676 -> p47 -> p675.
 intros h0 h1.
 refine (div_pp r9 r12 i519 i24 i518 h0 h1 _) ; finalize.
Qed.
Lemma l618 : p366 -> p3 -> s1 -> p675 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 9.53674e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l345 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply t411. refine (subset r9 i289 i519 h3 _) ; finalize. exact h4.
Qed.
Lemma l617 : p366 -> p3 -> s1 -> p674 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 9.53674e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l618 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t412 : p380 -> p674 -> p379.
 intros h0 h1.
 refine (div_pp r9 r8 i289 i517 i288 h0 h1 _) ; finalize.
Qed.
Lemma l344 : p366 -> p3 -> s1 -> p379 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.16415e-10, 1.16415e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l345 h0 h1 h2).
 assert (h4 := l617 h0 h1 h2).
 apply t412. exact h3. exact h4.
Qed.
Lemma t413 : p14 -> p18 -> p379 -> p378.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i288 h0 h1 h2) ; finalize.
Qed.
Lemma l341 : p366 -> p3 -> s1 -> p378 (* BND(1 / b, [1.16415e-10, 1.16415e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l344 h0 h1 h2).
 apply t413. exact h3. exact h4. exact h5.
Qed.
Lemma l340 : p366 -> p3 -> s1 -> p377 (* BND(1 / b, [4.65661e-10, 1.16415e-09]) *).
 intros h0 h1 h2.
 assert (h3 := l341 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Notation p677 := (BND r12 i279). (* BND(1 / b, [4.65661e-10, 1.86265e-09]) *)
Lemma t414 : p367 -> p677 -> p376.
 intros h0 h1.
 refine (div_np r11 r12 i27 i279 i286 h0 h1 _) ; finalize.
Qed.
Lemma l339 : p366 -> p3 -> s1 -> p376 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -5.96046e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l340 h0 h1 h2).
 apply t414. exact h3. refine (subset r12 i287 i279 h4 _) ; finalize.
Qed.
Lemma l338 : p366 -> p3 -> s1 -> p375 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -5.96046e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l339 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t415 : p18 -> p375 -> p374.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i285 h0 h1) ; finalize.
Qed.
Lemma l337 : p366 -> p3 -> s1 -> p374 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -5.96046e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l338 h0 h1 h2).
 apply t415. exact h3. exact h4.
Qed.
Definition f366 := Float2 (1407373356827991) (-81).
Definition i520 := makepairF f13 f366.
Notation p678 := (BND r12 i520). (* BND(1 / b, [4.65661e-10, 5.82076e-10]) *)
Definition i521 := makepairF f15 f366.
Notation p679 := (BND r12 i521). (* BND(1 / b, [2.32831e-10, 5.82076e-10]) *)
Notation p680 := (BND r19 i521). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.32831e-10, 5.82076e-10]) *)
Definition f367 := Float2 (1407373356827991) (-103).
Definition i522 := makepairF f16 f367.
Notation p681 := (BND r9 i522). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.38778e-16]) *)
Definition f368 := Float2 (-1407373356827991) (-103).
Definition i523 := makepairF f368 f18.
Notation p682 := (BND r11 i523). (* BND(inv_b_hat - 1 / b, [-1.38778e-16, -1.11022e-16]) *)
Definition f369 := Float2 (-1) (-56).
Definition i524 := makepairF f368 f369.
Notation p683 := (BND r11 i524). (* BND(inv_b_hat - 1 / b, [-1.38778e-16, -1.38778e-17]) *)
Definition i525 := makepairF f33 f26.
Notation p684 := (REL _inv_b_hat r12 i525). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.98023e-08]) *)
Notation p685 := (BND r20 i525). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.98023e-08]) *)
Definition i526 := makepairF f221 f26.
Notation p686 := (BND r20 i526). (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.98023e-08]) *)
Lemma t416 : p367 -> p327 -> p686.
 intros h0 h1.
 refine (div_np r11 r12 i27 i254 i526 h0 h1 _) ; finalize.
Qed.
Lemma l627 : p366 -> p3 -> s1 -> p686 (* BND((inv_b_hat - 1 / b) / (1 / b), [-2.14748e+09, -2.98023e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l353 h0 h1 h2).
 apply t416. exact h3. refine (subset r12 i294 i254 h4 _) ; finalize.
Qed.
Lemma l626 : p366 -> p3 -> s1 -> p685 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, -2.98023e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l627 h0 h1 h2).
 assert (h4 := l57 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t417 : p18 -> p685 -> p684.
 intros h0 h1.
 refine (rel_of_nzr_bnd _inv_b_hat r12 i525 h0 h1) ; finalize.
Qed.
Lemma l625 : p366 -> p3 -> s1 -> p684 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, -2.98023e-08]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l626 h0 h1 h2).
 apply t417. exact h3. exact h4.
Qed.
Lemma t418 : p684 -> p377 -> p683.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i525 i287 i524 h0 h1 _) ; finalize.
Qed.
Lemma l624 : p366 -> p3 -> s1 -> p683 (* BND(inv_b_hat - 1 / b, [-1.38778e-16, -1.38778e-17]) *).
 intros h0 h1 h2.
 assert (h3 := l625 h0 h1 h2).
 assert (h4 := l340 h0 h1 h2).
 apply t418. exact h3. exact h4.
Qed.
Lemma l623 : p366 -> p3 -> s1 -> p682 (* BND(inv_b_hat - 1 / b, [-1.38778e-16, -1.11022e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l624 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t419 : p39 -> p682 -> p681.
 intros h0 h1.
 refine (mul_nn r10 r11 i21 i523 i522 h0 h1 _) ; finalize.
Qed.
Lemma l622 : p366 -> p3 -> s1 -> p681 (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 1.38778e-16]) *).
 intros h0 h1 h2.
 assert (h3 := l39 h2).
 assert (h4 := l623 h0 h1 h2).
 apply t419. exact h3. exact h4.
Qed.
Definition i527 := makepairF f4 f177.
Notation p687 := (BND r8 i527). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 4.76837e-07]) *)
Definition i528 := makepairF f16 f177.
Notation p688 := (BND r8 i528). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 4.76837e-07]) *)
Definition i529 := makepairF f16 f226.
Notation p689 := (BND r9 i529). (* BND(-1 * (inv_b_hat - 1 / b), [1.11022e-16, 2.22045e-16]) *)
Lemma t420 : p689 -> p47 -> p688.
 intros h0 h1.
 refine (div_pp r9 r12 i529 i24 i528 h0 h1 _) ; finalize.
Qed.
Lemma l629 : p366 -> p3 -> s1 -> p688 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [1.11022e-16, 4.76837e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l622 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply t420. refine (subset r9 i522 i529 h3 _) ; finalize. exact h4.
Qed.
Lemma l628 : p366 -> p3 -> s1 -> p687 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 4.76837e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l629 h0 h1 h2).
 assert (h4 := l333 h0 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t421 : p681 -> p687 -> p680.
 intros h0 h1.
 refine (div_pp r9 r8 i522 i527 i521 h0 h1 _) ; finalize.
Qed.
Lemma l621 : p366 -> p3 -> s1 -> p680 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [2.32831e-10, 5.82076e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l622 h0 h1 h2).
 assert (h4 := l628 h0 h1 h2).
 apply t421. exact h3. exact h4.
Qed.
Lemma t422 : p14 -> p18 -> p680 -> p679.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i521 h0 h1 h2) ; finalize.
Qed.
Lemma l620 : p366 -> p3 -> s1 -> p679 (* BND(1 / b, [2.32831e-10, 5.82076e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l342 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l621 h0 h1 h2).
 apply t422. exact h3. exact h4. exact h5.
Qed.
Lemma l619 : p366 -> p3 -> s1 -> p678 (* BND(1 / b, [4.65661e-10, 5.82076e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l620 h0 h1 h2).
 assert (h4 := l47 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t423 : p374 -> p678 -> p373.
 intros h0 h1.
 refine (error_of_rel_np _inv_b_hat r12 i285 i520 i284 h0 h1 _) ; finalize.
Qed.
Lemma l336 : p366 -> p3 -> s1 -> p373 (* BND(inv_b_hat - 1 / b, [-6.93889e-17, -2.77556e-17]) *).
 intros h0 h1 h2.
 assert (h3 := l337 h0 h1 h2).
 assert (h4 := l619 h0 h1 h2).
 apply t423. exact h3. exact h4.
Qed.
Lemma l328 : p366 -> p3 -> s1 -> False.
 intros h0 h1 h2.
 assert (h3 := l329 h0 h1 h2).
 assert (h4 := l336 h0 h1 h2).
 apply absurd_intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma l2 : p3 -> s1 -> False.
 intros h0 h1.
 apply (union _exact_alpha f4).
 intro h2. (* [-inf, 2.38419e-07] *)
 apply (l3 h2 h0 h1).
 intro h2. (* [2.38419e-07, inf] *)
 apply (l328 h2 h0 h1).
Qed.
Notation p690 := ((f5 <= _b)%R). (* BND(b, [2.14748e+09, inf]) *)
Definition f370 := Float2 (288230359038951419) (-80).
Definition i530 := makepairF f370 f12.
Notation p691 := (BND r20 i530). (* BND((inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 4.29497e+09]) *)
Definition f371 := Float2 (4294967297) (-86).
Definition i531 := makepairF f371 f1.
Notation p692 := (BND r11 i531). (* BND(inv_b_hat - 1 / b, [5.55112e-17, 1]) *)
Notation p693 := (BND r21 i531). (* BND(-1 * (inv_b_hat - 1 / b) / -1, [5.55112e-17, 1]) *)
Definition f372 := Float2 (-4294967297) (-86).
Definition i532 := makepairF f23 f372.
Notation p694 := (BND r9 i532). (* BND(-1 * (inv_b_hat - 1 / b), [-1, -5.55112e-17]) *)
Notation p695 := (BND r14 i532). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b) * (1 / b), [-1, -5.55112e-17]) *)
Notation p696 := (BND _exact_alpha i266). (* BND(exact_alpha, [-4.76837e-07, -2.38419e-07]) *)
Lemma l639 : p5 -> p690 -> s1 -> p5 (* BND(exact_alpha, [-inf, -2.38419e-07]) *).
 intros h0 h1 h2.
 assert (h3 := h0).
 exact (h3).
Qed.
Definition f373 := Float2 (1152921435618938897) (-82).
Definition i533 := makepairF f207 f373.
Notation p697 := (BND _exact_alpha i533). (* BND(exact_alpha, [-4.76837e-07, 2.38419e-07]) *)
Notation p698 := (BND r8 i533). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-4.76837e-07, 2.38419e-07]) *)
Definition f374 := Float2 (-288230393331581953) (-112).
Definition f375 := Float2 (281474959933441) (-102).
Definition i534 := makepairF f374 f375.
Notation p699 := (BND r9 i534). (* BND(-1 * (inv_b_hat - 1 / b), [-5.55112e-17, 5.55111e-17]) *)
Definition f376 := Float2 (-281474959933441) (-102).
Definition f377 := Float2 (288230393331581953) (-112).
Definition i535 := makepairF f376 f377.
Notation p700 := (BND r11 i535). (* BND(inv_b_hat - 1 / b, [-5.55111e-17, 5.55112e-17]) *)
Definition f378 := Float2 (4294967297) (-64).
Definition i536 := makepairF f378 f13.
Notation p701 := (BND r12 i536). (* BND(1 / b, [2.32831e-10, 4.65661e-10]) *)
Definition i537 := makepairF f5 f2.
Notation p702 := (BND _b i537). (* BND(b, [2.14748e+09, 4.29497e+09]) *)
Lemma l646 : p690 -> s1 -> p690 (* BND(b, [2.14748e+09, inf]) *).
 intros h0 h1.
 assert (h2 := h0).
 exact (h2).
Qed.
Lemma l645 : p690 -> s1 -> p702 (* BND(b, [2.14748e+09, 4.29497e+09]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l646 h0 h1).
 apply intersect_bh with (1 := h2) (2 := h3). finalize.
Qed.
Lemma t424 : p21 -> p702 -> p701.
 intros h0 h1.
 refine (div_pp r3 _b i12 i537 i536 h0 h1 _) ; finalize.
Qed.
Lemma l644 : p690 -> s1 -> p701 (* BND(1 / b, [2.32831e-10, 4.65661e-10]) *).
 intros h0 h1.
 assert (h2 := l20 h1).
 assert (h3 := l645 h0 h1).
 apply t424. exact h2. exact h3.
Qed.
Definition i538 := makepairF f15 f13.
Notation p703 := (BND r12 i538). (* BND(1 / b, [2.32831e-10, 4.65661e-10]) *)
Lemma t425 : p59 -> p703 -> p700.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r12 i32 i538 i535 h0 h1 _) ; finalize.
Qed.
Lemma l643 : p690 -> s1 -> p700 (* BND(inv_b_hat - 1 / b, [-5.55111e-17, 5.55112e-17]) *).
 intros h0 h1.
 assert (h2 := l58 h1).
 assert (h3 := l644 h0 h1).
 apply t425. exact h2. refine (subset r12 i536 i538 h3 _) ; finalize.
Qed.
Lemma t426 : p39 -> p700 -> p699.
 intros h0 h1.
 refine (mul_no r10 r11 i21 i535 i534 h0 h1 _) ; finalize.
Qed.
Lemma l642 : p690 -> s1 -> p699 (* BND(-1 * (inv_b_hat - 1 / b), [-5.55112e-17, 5.55111e-17]) *).
 intros h0 h1.
 assert (h2 := l39 h1).
 assert (h3 := l643 h0 h1).
 apply t426. exact h2. exact h3.
Qed.
Definition i539 := makepairF f18 f375.
Notation p704 := (BND r9 i539). (* BND(-1 * (inv_b_hat - 1 / b), [-1.11022e-16, 5.55111e-17]) *)
Definition i540 := makepairF f378 f1.
Notation p705 := (BND r12 i540). (* BND(1 / b, [2.32831e-10, 1]) *)
Lemma t427 : p704 -> p705 -> p698.
 intros h0 h1.
 refine (div_op r9 r12 i539 i540 i533 h0 h1 _) ; finalize.
Qed.
Lemma l641 : p690 -> s1 -> p698 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-4.76837e-07, 2.38419e-07]) *).
 intros h0 h1.
 assert (h2 := l642 h0 h1).
 assert (h3 := l644 h0 h1).
 apply t427. refine (subset r9 i534 i539 h2 _) ; finalize. refine (subset r12 i536 i540 h3 _) ; finalize.
Qed.
Lemma t428 : p698 -> p34 -> p697.
 intros h0 h1.
 refine (bnd_of_bnd_rel_o _exact_alpha r8 i533 i18 i533 h0 h1 _) ; finalize.
Qed.
Lemma l640 : p690 -> s1 -> p697 (* BND(exact_alpha, [-4.76837e-07, 2.38419e-07]) *).
 intros h0 h1.
 assert (h2 := l641 h0 h1).
 assert (h3 := l34 h1).
 apply t428. exact h2. exact h3.
Qed.
Lemma l638 : p5 -> p690 -> s1 -> p696 (* BND(exact_alpha, [-4.76837e-07, -2.38419e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l639 h0 h1 h2).
 assert (h4 := l640 h1 h2).
 apply intersect_hb with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t429 : p696 -> p34 -> p341.
 intros h0 h1.
 refine (bnd_of_rel_bnd_n _exact_alpha r8 i266 i18 i266 h0 h1 _) ; finalize.
Qed.
Lemma l637 : p5 -> p690 -> s1 -> p341 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-4.76837e-07, -2.38419e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l638 h0 h1 h2).
 assert (h4 := l34 h2).
 apply t429. exact h3. exact h4.
Qed.
Definition i541 := makepairF f23 f3.
Notation p706 := (BND r8 i541). (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b), [-1, -2.38419e-07]) *)
Lemma t430 : p706 -> p705 -> p695.
 intros h0 h1.
 refine (mul_np r8 r12 i541 i540 i532 h0 h1 _) ; finalize.
Qed.
Lemma l636 : p5 -> p690 -> s1 -> p695 (* BND(-1 * (inv_b_hat - 1 / b) / (1 / b) * (1 / b), [-1, -5.55112e-17]) *).
 intros h0 h1 h2.
 assert (h3 := l637 h0 h1 h2).
 assert (h4 := l644 h1 h2).
 apply t430. refine (subset r8 i266 i541 h3 _) ; finalize. refine (subset r12 i536 i540 h4 _) ; finalize.
Qed.
Lemma t431 : p18 -> p695 -> p694.
 intros h0 h1.
 refine (div_xilu r9 _ i532 h0 h1) ; finalize.
Qed.
Lemma l635 : p5 -> p690 -> s1 -> p694 (* BND(-1 * (inv_b_hat - 1 / b), [-1, -5.55112e-17]) *).
 intros h0 h1 h2.
 assert (h3 := l17 h2).
 assert (h4 := l636 h0 h1 h2).
 apply t431. exact h3. exact h4.
Qed.
Lemma t432 : p694 -> p39 -> p693.
 intros h0 h1.
 refine (div_nn r9 r10 i532 i21 i531 h0 h1 _) ; finalize.
Qed.
Lemma l634 : p5 -> p690 -> s1 -> p693 (* BND(-1 * (inv_b_hat - 1 / b) / -1, [5.55112e-17, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l635 h0 h1 h2).
 assert (h4 := l39 h2).
 apply t432. exact h3. exact h4.
Qed.
Lemma t433 : p64 -> p693 -> p692.
 intros h0 h1.
 refine (mul_xiru _ r11 i531 h0 h1) ; finalize.
Qed.
Lemma l633 : p5 -> p690 -> s1 -> p692 (* BND(inv_b_hat - 1 / b, [5.55112e-17, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l62 h2).
 assert (h4 := l634 h0 h1 h2).
 apply t433. exact h3. exact h4.
Qed.
Definition f379 := Float2 (288230393331581953) (-90).
Definition i542 := makepairF f15 f379.
Notation p707 := (BND r12 i542). (* BND(1 / b, [2.32831e-10, 2.32831e-10]) *)
Definition i543 := makepairF f57 f379.
Notation p708 := (BND r12 i543). (* BND(1 / b, [1.16415e-10, 2.32831e-10]) *)
Definition i544 := makepairF f209 f1.
Notation p709 := (ABS r9 i544). (* ABS(-1 * (inv_b_hat - 1 / b), [5.55112e-17, 1]) *)
Definition i545 := makepairF f23 f210.
Notation p710 := (BND r9 i545). (* BND(-1 * (inv_b_hat - 1 / b), [-1, -5.55112e-17]) *)
Lemma t434 : p710 -> p709.
 intros h0.
 refine (abs_of_bnd_n r9 i545 i544 h0 _) ; finalize.
Qed.
Lemma l650 : p5 -> p690 -> s1 -> p709 (* ABS(-1 * (inv_b_hat - 1 / b), [5.55112e-17, 1]) *).
 intros h0 h1 h2.
 assert (h3 := l635 h0 h1 h2).
 apply t434. refine (subset r9 i532 i545 h3 _) ; finalize.
Qed.
Lemma t435 : p709 -> p14.
 intros h0.
 refine (nzr_of_abs r9 i544 h0 _) ; finalize.
Qed.
Lemma l649 : p5 -> p690 -> s1 -> p14 (* NZR(-1 * (inv_b_hat - 1 / b)) *).
 intros h0 h1 h2.
 assert (h3 := l650 h0 h1 h2).
 apply t435. exact h3.
Qed.
Notation p711 := (BND r19 i543). (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.16415e-10, 2.32831e-10]) *)
Definition i546 := makepairF f374 f210.
Notation p712 := (BND r9 i546). (* BND(-1 * (inv_b_hat - 1 / b), [-5.55112e-17, -5.55112e-17]) *)
Lemma l652 : p5 -> p690 -> s1 -> p712 (* BND(-1 * (inv_b_hat - 1 / b), [-5.55112e-17, -5.55112e-17]) *).
 intros h0 h1 h2.
 assert (h3 := l635 h0 h1 h2).
 assert (h4 := l642 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t436 : p712 -> p341 -> p711.
 intros h0 h1.
 refine (div_nn r9 r8 i546 i266 i543 h0 h1 _) ; finalize.
Qed.
Lemma l651 : p5 -> p690 -> s1 -> p711 (* BND(-1 * (inv_b_hat - 1 / b) / (-1 * (inv_b_hat - 1 / b) / (1 / b)), [1.16415e-10, 2.32831e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l652 h0 h1 h2).
 assert (h4 := l637 h0 h1 h2).
 apply t436. exact h3. exact h4.
Qed.
Lemma t437 : p14 -> p18 -> p711 -> p708.
 intros h0 h1 h2.
 refine (div_xiru _ r12 i543 h0 h1 h2) ; finalize.
Qed.
Lemma l648 : p5 -> p690 -> s1 -> p708 (* BND(1 / b, [1.16415e-10, 2.32831e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l649 h0 h1 h2).
 assert (h4 := l17 h2).
 assert (h5 := l651 h0 h1 h2).
 apply t437. exact h3. exact h4. exact h5.
Qed.
Lemma l647 : p5 -> p690 -> s1 -> p707 (* BND(1 / b, [2.32831e-10, 2.32831e-10]) *).
 intros h0 h1 h2.
 assert (h3 := l648 h0 h1 h2).
 assert (h4 := l644 h1 h2).
 apply intersect with (1 := h3) (2 := h4). finalize.
Qed.
Lemma t438 : p692 -> p707 -> p691.
 intros h0 h1.
 refine (div_pp r11 r12 i531 i542 i530 h0 h1 _) ; finalize.
Qed.
Lemma l632 : p5 -> p690 -> s1 -> p691 (* BND((inv_b_hat - 1 / b) / (1 / b), [2.38419e-07, 4.29497e+09]) *).
 intros h0 h1 h2.
 assert (h3 := l633 h0 h1 h2).
 assert (h4 := l647 h0 h1 h2).
 apply t438. exact h3. exact h4.
Qed.
Lemma l631 : p5 -> p690 -> s1 -> False.
 intros h0 h1 h2.
 assert (h3 := l57 h2).
 assert (h4 := l632 h0 h1 h2).
 apply absurd_intersect with (1 := h3) (2 := h4). finalize.
Qed.
Definition i547 := makepairF f3 f373.
Notation p713 := (BND _exact_alpha i547). (* BND(exact_alpha, [-2.38419e-07, 2.38419e-07]) *)
Lemma l655 : p365 -> p690 -> s1 -> p365 (* BND(exact_alpha, [-2.38419e-07, inf]) *).
 intros h0 h1 h2.
 assert (h3 := h0).
 exact (h3).
Qed.
Lemma l654 : p365 -> p690 -> s1 -> p713 (* BND(exact_alpha, [-2.38419e-07, 2.38419e-07]) *).
 intros h0 h1 h2.
 assert (h3 := l640 h1 h2).
 assert (h4 := l655 h0 h1 h2).
 apply intersect_bh with (1 := h3) (2 := h4). finalize.
Qed.
Lemma l653 : p365 -> p690 -> s1 -> False.
 intros h0 h1 h2.
 assert (h3 := l324 h2).
 assert (h4 := l654 h0 h1 h2).
 refine (simplify (Tatom false (Abnd 0%nat i2)) Tfalse (Abnd 0%nat i547) (List.cons _exact_alpha List.nil) h4 h3 _) ; finalize.
Qed.
Lemma l630 : p690 -> s1 -> False.
 intros h0 h1.
 apply (union _exact_alpha f3).
 intro h2. (* [-inf, -2.38419e-07] *)
 apply (l631 h2 h0 h1).
 intro h2. (* [-2.38419e-07, inf] *)
 apply (l653 h2 h0 h1).
Qed.
Lemma l1 : s1 -> False.
 intros h0.
 apply (union _b f5).
 intro h1. (* [-inf, 2.14748e+09] *)
 apply (l2 h1 h0).
 intro h1. (* [2.14748e+09, inf] *)
 apply (l630 h1 h0).
Qed.

(* added manually *)
Lemma gappa1_ccl : not (p1 /\ not p2).
  intro.
  apply l1.
  unfold s1. unfold s2.
  exact H.
Qed.

Theorem STEP1 : p1 -> p2.
  pose proof gappa1_ccl as CCL.
  unfold p1, p2 in *.
  lra.
Qed.

End Generated_by_Gappa1.

End Step_1.


Module Step_2.

Section Generated_by_Gappa2.
(* === Gappa proof:
{a in [0, 4294967295] /\ b in [1, 4294967295] /\ 
exact_alpha in [-1b-22,1b-22] -> remainder_over_b in [-0.6,0.6]} === *)

Variable _b : R.
Notation r7 := (Float1 (1)).
Notation r11 := ((rounding_float rndNE (24)%positive (-149)%Z) _b).
Notation r10 := ((r7 / r11)%R).
Notation _inv_b_hat := ((rounding_float rndNE (24)%positive (-149)%Z) r10).
Notation r8 := ((_b * _inv_b_hat)%R).
Notation _exact_alpha := ((r7 - r8)%R).
Notation _alpha := ((rounding_float rndNE (53)%positive (-1074)%Z) _exact_alpha).
Notation r12 := ((r7 / _b)%R).
Notation r4 := ((_alpha * r12)%R).
Notation r3 := ((r4 + _inv_b_hat)%R).
Notation r2 := ((r3 - r12)%R).
Notation r14 := ((_alpha - _exact_alpha)%R).
Notation r13 := ((r14 * r12)%R).
Hypothesis a1 : (_b <> 0)%R -> r2 = r13.
Lemma b1 : NZR _b -> r2 = r13.
 intros h0.
 apply a1.
 exact h0.
Qed.
Variable _a : R.
Notation r23 := ((_alpha * _inv_b_hat)%R).
Notation r22 := ((r23 + _inv_b_hat)%R).
Notation _inv_b_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) r22).
Notation r20 := ((_a * _inv_b_hat1)%R).
Notation _quotient_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) r20).
Notation _quotient_rnd := ((rounding_fixed rndNE (0)%Z) _quotient_hat1).
Notation r17 := ((_b * _quotient_rnd)%R).
Notation _remainder := ((_a - r17)%R).
Notation r26 := ((_a / _b)%R).
Notation r25 := ((r26 - _quotient_rnd)%R).
Notation r24 := ((r25 * _b)%R).
Hypothesis a2 : (_b <> 0)%R -> _remainder = r24.
Lemma b2 : NZR _b -> _remainder = r24.
 intros h0.
 apply a2.
 exact h0.
Qed.
Notation r27 := ((_quotient_rnd - r26)%R).
Notation r29 := ((_quotient_rnd - r20)%R).
Notation r31 := ((_inv_b_hat1 - r12)%R).
Notation r30 := ((_a * r31)%R).
Notation r28 := ((r29 + r30)%R).
Hypothesis a3 : (_b <> 0)%R -> r27 = r28.
Lemma b3 : NZR _b -> r27 = r28.
 intros h0.
 apply a3.
 exact h0.
Qed.
Definition f1 := Float2 (0) (0).
Definition f2 := Float2 (4294967295) (0).
Definition i1 := makepairF f1 f2.
Notation p1 := (BND _a i1). (* BND(a, [0, 4.29497e+09]) *)
Definition f3 := Float2 (1) (0).
Definition i2 := makepairF f3 f2.
Notation p2 := (BND _b i2). (* BND(b, [1, 4.29497e+09]) *)
Definition s3 := (p1 /\ p2).
Definition f4 := Float2 (-1) (-22).
Definition f5 := Float2 (1) (-22).
Definition i3 := makepairF f4 f5.
Notation p3 := (BND _exact_alpha i3). (* BND(exact_alpha, [-2.38419e-07, 2.38419e-07]) *)
Definition s2 := (s3 /\ p3).
Notation _remainder_over_b := ((_remainder / _b)%R).
Definition f6 := Float2 (-691752902764108185) (-60).
Definition f7 := Float2 (691752902764108185) (-60).
Definition i4 := makepairF f6 f7.
Notation p4 := (BND _remainder_over_b i4). (* BND(remainder_over_b, [-0.6, 0.6]) *)
Definition s4 := (not p4).
Definition s1 := (s2 /\ s4).
Lemma l2 : s1 -> s4.
 intros h0.
 assert (h1 := h0).
 exact (proj2 h1).
Qed.
Definition f8 := Float2 (-576567130057645953) (-60).
Definition f9 := Float2 (576567130057645953) (-60).
Definition i5 := makepairF f8 f9.
Notation p5 := (BND _remainder_over_b i5). (* BND(remainder_over_b, [-0.500092, 0.500092]) *)
Notation p6 := (NZR _b). (* NZR(b) *)
Definition f10 := Float2 (1) (32).
Definition i6 := makepairF f3 f10.
Notation p7 := (ABS _b i6). (* ABS(b, [1, 4.29497e+09]) *)
Lemma l8 : s1 -> s2.
 intros h0.
 assert (h1 := h0).
 exact (proj1 h1).
Qed.
Lemma l7 : s1 -> s3.
 intros h0.
 assert (h1 := l8 h0).
 exact (proj1 h1).
Qed.
Lemma l6 : s1 -> p2 (* BND(b, [1, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l7 h0).
 exact (proj2 h1).
Qed.
Notation p8 := (BND _b i6). (* BND(b, [1, 4.29497e+09]) *)
Lemma t1 : p8 -> p7.
 intros h0.
 refine (abs_of_bnd_p _b i6 i6 h0 _) ; finalize.
Qed.
Lemma l5 : s1 -> p7 (* ABS(b, [1, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l6 h0).
 apply t1. refine (subset _b i2 i6 h1 _) ; finalize.
Qed.
Lemma t2 : p7 -> p6.
 intros h0.
 refine (nzr_of_abs _b i6 h0 _) ; finalize.
Qed.
Lemma l4 : s1 -> p6 (* NZR(b) *).
 intros h0.
 assert (h1 := l5 h0).
 apply t2. exact h1.
Qed.
Notation r35 := ((_remainder - r24)%R).
Notation r34 := ((r35 / _b)%R).
Notation r36 := ((r24 / _b)%R).
Notation r33 := ((r34 + r36)%R).
Notation p9 := (BND r33 i5). (* BND((remainder - (a / b - quotient_rnd) * b) / b + (a / b - quotient_rnd) * b / b, [-0.500092, 0.500092]) *)
Definition i7 := makepairF f1 f1.
Notation p10 := (BND r34 i7). (* BND((remainder - (a / b - quotient_rnd) * b) / b, [0, 0]) *)
Notation p11 := (BND r35 i7). (* BND(remainder - (a / b - quotient_rnd) * b, [0, 0]) *)
Notation p12 := (_remainder = r24). (* EQL(remainder, (a / b - quotient_rnd) * b) *)
Lemma t3 : p6 -> p12.
 intros h0.
 refine (b2 h0) ; finalize.
Qed.
Lemma l12 : s1 -> p12 (* EQL(remainder, (a / b - quotient_rnd) * b) *).
 intros h0.
 assert (h1 := l4 h0).
 apply t3. exact h1.
Qed.
Lemma t4 : p12 -> p11.
 intros h0.
 refine (sub_of_eql _remainder r24 i7 h0 _) ; finalize.
Qed.
Lemma l11 : s1 -> p11 (* BND(remainder - (a / b - quotient_rnd) * b, [0, 0]) *).
 intros h0.
 assert (h1 := l12 h0).
 apply t4. exact h1.
Qed.
Lemma t5 : p11 -> p8 -> p10.
 intros h0 h1.
 refine (div_pp r35 _b i7 i6 i7 h0 h1 _) ; finalize.
Qed.
Lemma l10 : s1 -> p10 (* BND((remainder - (a / b - quotient_rnd) * b) / b, [0, 0]) *).
 intros h0.
 assert (h1 := l11 h0).
 assert (h2 := l6 h0).
 apply t5. exact h1. refine (subset _b i2 i6 h2 _) ; finalize.
Qed.
Notation p13 := (BND r36 i5). (* BND((a / b - quotient_rnd) * b / b, [-0.500092, 0.500092]) *)
Notation p14 := (BND r25 i5). (* BND(a / b - quotient_rnd, [-0.500092, 0.500092]) *)
Notation r38 := ((r26 - r26)%R).
Notation r37 := ((r38 - r27)%R).
Notation p15 := (BND r37 i5). (* BND(a / b - a / b - (quotient_rnd - a / b), [-0.500092, 0.500092]) *)
Notation p16 := (BND r38 i7). (* BND(a / b - a / b, [0, 0]) *)
Lemma t6 : p16.
 refine (sub_refl _ i7 _) ; finalize.
Qed.
Lemma l16 : s1 -> p16 (* BND(a / b - a / b, [0, 0]) *).
 intros h0.
 apply t6.
Qed.
Notation p17 := (BND r27 i5). (* BND(quotient_rnd - a / b, [-0.500092, 0.500092]) *)
Notation p18 := (BND r28 i5). (* BND(quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b), [-0.500092, 0.500092]) *)
Definition f11 := Float2 (-2097153) (-22).
Definition f12 := Float2 (2097153) (-22).
Definition i8 := makepairF f11 f12.
Notation p19 := (BND r29 i8). (* BND(quotient_rnd - a * inv_b_hat1, [-0.5, 0.5]) *)
Notation r40 := ((_quotient_rnd - _quotient_hat1)%R).
Notation r41 := ((_quotient_hat1 - r20)%R).
Notation r39 := ((r40 + r41)%R).
Notation p20 := (BND r39 i8). (* BND(quotient_rnd - quotient_hat1 + (quotient_hat1 - a * inv_b_hat1), [-0.5, 0.5]) *)
Definition f13 := Float2 (-1) (-1).
Definition f14 := Float2 (1) (-1).
Definition i9 := makepairF f13 f14.
Notation p21 := (BND r40 i9). (* BND(quotient_rnd - quotient_hat1, [-0.5, 0.5]) *)
Lemma t7 : p21.
 refine (fixed_error_ne _ _ i9 _) ; finalize.
Qed.
Lemma l21 : s1 -> p21 (* BND(quotient_rnd - quotient_hat1, [-0.5, 0.5]) *).
 intros h0.
 apply t7.
Qed.
Notation p22 := (BND r41 i3). (* BND(quotient_hat1 - a * inv_b_hat1, [-2.38419e-07, 2.38419e-07]) *)
Definition i10 := makepairF f1 f10.
Notation p23 := (ABS r20 i10). (* ABS(a * inv_b_hat1, [0, 4.29497e+09]) *)
Notation p24 := (BND r20 i10). (* BND(a * inv_b_hat1, [0, 4.29497e+09]) *)
Lemma l25 : s1 -> p1 (* BND(a, [0, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l7 h0).
 exact (proj1 h1).
Qed.
Definition f15 := Float2 (1) (-33).
Definition f16 := Float2 (4294967297) (-32).
Definition i11 := makepairF f15 f16.
Notation p25 := (BND _inv_b_hat1 i11). (* BND(inv_b_hat1, [1.16415e-10, 1]) *)
Notation p26 := (BND r22 i11). (* BND(alpha * inv_b_hat + inv_b_hat, [1.16415e-10, 1]) *)
Notation r43 := ((r23 - r4)%R).
Notation r42 := ((r43 + r3)%R).
Notation p27 := (BND r42 i11). (* BND(alpha * inv_b_hat - alpha * (1 / b) + (alpha * (1 / b) + inv_b_hat), [1.16415e-10, 1]) *)
Definition f17 := Float2 (-105553120460801) (-92).
Definition f18 := Float2 (105553120460801) (-92).
Definition i12 := makepairF f17 f18.
Notation p28 := (BND r43 i12). (* BND(alpha * inv_b_hat - alpha * (1 / b), [-2.13163e-14, 2.13163e-14]) *)
Notation r45 := ((_inv_b_hat - r12)%R).
Notation r44 := ((_alpha * r45)%R).
Notation p29 := (BND r44 i12). (* BND(alpha * (inv_b_hat - 1 / b), [-2.13163e-14, 2.13163e-14]) *)
Notation p30 := (BND _alpha i3). (* BND(alpha, [-2.38419e-07, 2.38419e-07]) *)
Lemma l32 : s1 -> p3 (* BND(exact_alpha, [-2.38419e-07, 2.38419e-07]) *).
 intros h0.
 assert (h1 := l8 h0).
 exact (proj2 h1).
Qed.
Lemma t8 : p3 -> p30.
 intros h0.
 refine (float_round_ne _ _ _exact_alpha i3 i3 h0 _) ; finalize.
Qed.
Lemma l31 : s1 -> p30 (* BND(alpha, [-2.38419e-07, 2.38419e-07]) *).
 intros h0.
 assert (h1 := l32 h0).
 apply t8. exact h1.
Qed.
Definition f19 := Float2 (-3) (-25).
Definition f20 := Float2 (105553120460801) (-70).
Definition i13 := makepairF f19 f20.
Notation p31 := (BND r45 i13). (* BND(inv_b_hat - 1 / b, [-8.9407e-08, 8.9407e-08]) *)
Notation r47 := ((_inv_b_hat - r10)%R).
Notation r48 := ((r10 - r12)%R).
Notation r46 := ((r47 + r48)%R).
Notation p32 := (BND r46 i13). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-8.9407e-08, 8.9407e-08]) *)
Definition f21 := Float2 (-1) (-25).
Definition f22 := Float2 (1) (-25).
Definition i14 := makepairF f21 f22.
Notation p33 := (BND r47 i14). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-2.98023e-08, 2.98023e-08]) *)
Definition f23 := Float2 (1) (-32).
Definition i15 := makepairF f23 f3.
Notation p34 := (ABS r10 i15). (* ABS(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *)
Notation p35 := (BND r10 i15). (* BND(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *)
Definition i16 := makepairF f3 f3.
Notation p36 := (BND r7 i16). (* BND(1, [1, 1]) *)
Lemma t9 : p36.
 refine (constant1 _ i16 _) ; finalize.
Qed.
Lemma l38 : s1 -> p36 (* BND(1, [1, 1]) *).
 intros h0.
 apply t9.
Qed.
Notation p37 := (BND r11 i6). (* BND(float<24,-149,ne>(b), [1, 4.29497e+09]) *)
Lemma t10 : p8 -> p37.
 intros h0.
 refine (float_round_ne _ _ _b i6 i6 h0 _) ; finalize.
Qed.
Lemma l39 : s1 -> p37 (* BND(float<24,-149,ne>(b), [1, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l6 h0).
 apply t10. refine (subset _b i2 i6 h1 _) ; finalize.
Qed.
Lemma t11 : p36 -> p37 -> p35.
 intros h0 h1.
 refine (div_pp r7 r11 i16 i6 i15 h0 h1 _) ; finalize.
Qed.
Lemma l37 : s1 -> p35 (* BND(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l38 h0).
 assert (h2 := l39 h0).
 apply t11. exact h1. exact h2.
Qed.
Lemma t12 : p35 -> p34.
 intros h0.
 refine (abs_of_bnd_p r10 i15 i15 h0 _) ; finalize.
Qed.
Lemma l36 : s1 -> p34 (* ABS(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l37 h0).
 apply t12. exact h1.
Qed.
Lemma t13 : p34 -> p33.
 intros h0.
 refine (float_absolute_wide_ne _ _ r10 i15 i14 h0 _) ; finalize.
Qed.
Lemma l35 : s1 -> p33 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-2.98023e-08, 2.98023e-08]) *).
 intros h0.
 assert (h1 := l36 h0).
 apply t13. exact h1.
Qed.
Definition f24 := Float2 (-1) (-24).
Definition f25 := Float2 (70368748371969) (-70).
Definition i17 := makepairF f24 f25.
Notation p38 := (BND r48 i17). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Notation p39 := (REL r10 r12 i17). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Definition f26 := Float2 (1) (-24).
Definition i18 := makepairF f24 f26.
Notation p40 := (REL r11 _b i18). (* REL(float<24,-149,ne>(b), b, [-5.96046e-08, 5.96046e-08]) *)
Lemma t14 : p7 -> p40.
 intros h0.
 refine (float_relative_ne _ _ _b i6 i18 h0 _) ; finalize.
Qed.
Lemma l42 : s1 -> p40 (* REL(float<24,-149,ne>(b), b, [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l5 h0).
 apply t14. exact h1.
Qed.
Lemma t15 : p40 -> p6 -> p39.
 intros h0 h1.
 refine (inv_r r11 _b _ i18 i17 h0 h1 _) ; finalize.
Qed.
Lemma l41 : s1 -> p39 (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l42 h0).
 assert (h2 := l4 h0).
 apply t15. exact h1. exact h2.
Qed.
Definition f27 := Float2 (4294967297) (-64).
Definition i19 := makepairF f27 f3.
Notation p41 := (BND r12 i19). (* BND(1 / b, [2.32831e-10, 1]) *)
Lemma t16 : p36 -> p2 -> p41.
 intros h0 h1.
 refine (div_pp r7 _b i16 i2 i19 h0 h1 _) ; finalize.
Qed.
Lemma l43 : s1 -> p41 (* BND(1 / b, [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l38 h0).
 assert (h2 := l6 h0).
 apply t16. exact h1. exact h2.
Qed.
Notation p42 := (BND r12 i15). (* BND(1 / b, [2.32831e-10, 1]) *)
Lemma t17 : p39 -> p42 -> p38.
 intros h0 h1.
 refine (error_of_rel_op r10 r12 i17 i15 i17 h0 h1 _) ; finalize.
Qed.
Lemma l40 : s1 -> p38 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l41 h0).
 assert (h2 := l43 h0).
 apply t17. exact h1. refine (subset r12 i19 i15 h2 _) ; finalize.
Qed.
Lemma t18 : p33 -> p38 -> p32.
 intros h0 h1.
 refine (add r47 r48 i14 i17 i13 h0 h1 _) ; finalize.
Qed.
Lemma l34 : s1 -> p32 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-8.9407e-08, 8.9407e-08]) *).
 intros h0.
 assert (h1 := l35 h0).
 assert (h2 := l40 h0).
 apply t18. exact h1. exact h2.
Qed.
Lemma t19 : p32 -> p31.
 intros h0.
 refine (sub_xals _ _ _ i13 h0) ; finalize.
Qed.
Lemma l33 : s1 -> p31 (* BND(inv_b_hat - 1 / b, [-8.9407e-08, 8.9407e-08]) *).
 intros h0.
 assert (h1 := l34 h0).
 apply t19. exact h1.
Qed.
Lemma t20 : p30 -> p31 -> p29.
 intros h0 h1.
 refine (mul_oo _alpha r45 i3 i13 i12 h0 h1 _) ; finalize.
Qed.
Lemma l30 : s1 -> p29 (* BND(alpha * (inv_b_hat - 1 / b), [-2.13163e-14, 2.13163e-14]) *).
 intros h0.
 assert (h1 := l31 h0).
 assert (h2 := l33 h0).
 apply t20. exact h1. exact h2.
Qed.
Lemma t21 : p29 -> p28.
 intros h0.
 refine (mul_fils _ _ _ i12 h0) ; finalize.
Qed.
Lemma l29 : s1 -> p28 (* BND(alpha * inv_b_hat - alpha * (1 / b), [-2.13163e-14, 2.13163e-14]) *).
 intros h0.
 assert (h1 := l30 h0).
 apply t21. exact h1.
Qed.
Definition f28 := Float2 (8589934593) (-33).
Definition i20 := makepairF f23 f28.
Notation p43 := (BND r3 i20). (* BND(alpha * (1 / b) + inv_b_hat, [2.32831e-10, 1]) *)
Notation r49 := ((r12 + r2)%R).
Notation p44 := (BND r49 i20). (* BND(1 / b + (alpha * (1 / b) + inv_b_hat - 1 / b), [2.32831e-10, 1]) *)
Definition f29 := Float2 (-1) (-76).
Definition f30 := Float2 (1) (-76).
Definition i21 := makepairF f29 f30.
Notation p45 := (BND r2 i21). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-1.32349e-23, 1.32349e-23]) *)
Notation p46 := (r2 = r13). (* EQL(alpha * (1 / b) + inv_b_hat - 1 / b, (alpha - exact_alpha) * (1 / b)) *)
Lemma t22 : p6 -> p46.
 intros h0.
 refine (b1 h0) ; finalize.
Qed.
Lemma l47 : s1 -> p46 (* EQL(alpha * (1 / b) + inv_b_hat - 1 / b, (alpha - exact_alpha) * (1 / b)) *).
 intros h0.
 assert (h1 := l4 h0).
 apply t22. exact h1.
Qed.
Notation p47 := (BND r13 i21). (* BND((alpha - exact_alpha) * (1 / b), [-1.32349e-23, 1.32349e-23]) *)
Notation p48 := (BND r14 i21). (* BND(alpha - exact_alpha, [-1.32349e-23, 1.32349e-23]) *)
Definition i22 := makepairF f1 f5.
Notation p49 := (ABS _exact_alpha i22). (* ABS(exact_alpha, [0, 2.38419e-07]) *)
Lemma t23 : p3 -> p49.
 intros h0.
 refine (abs_of_bnd_o _exact_alpha i3 i22 h0 _) ; finalize.
Qed.
Lemma l50 : s1 -> p49 (* ABS(exact_alpha, [0, 2.38419e-07]) *).
 intros h0.
 assert (h1 := l32 h0).
 apply t23. exact h1.
Qed.
Lemma t24 : p49 -> p48.
 intros h0.
 refine (float_absolute_wide_ne _ _ _exact_alpha i22 i21 h0 _) ; finalize.
Qed.
Lemma l49 : s1 -> p48 (* BND(alpha - exact_alpha, [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l50 h0).
 apply t24. exact h1.
Qed.
Lemma t25 : p48 -> p42 -> p47.
 intros h0 h1.
 refine (mul_op r14 r12 i21 i15 i21 h0 h1 _) ; finalize.
Qed.
Lemma l48 : s1 -> p47 (* BND((alpha - exact_alpha) * (1 / b), [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l49 h0).
 assert (h2 := l43 h0).
 apply t25. exact h1. refine (subset r12 i19 i15 h2 _) ; finalize.
Qed.
Lemma t26 : p46 -> p47 -> p45.
 intros h0 h1.
 refine (bnd_rewrite r2 r13 i21 h0 h1) ; finalize.
Qed.
Lemma l46 : s1 -> p45 (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l47 h0).
 assert (h2 := l48 h0).
 apply t26. exact h1. exact h2.
Qed.
Definition f31 := Float2 (-1) (-64).
Definition i23 := makepairF f31 f15.
Notation p50 := (BND r2 i23). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-5.42101e-20, 1.16415e-10]) *)
Lemma t27 : p41 -> p50 -> p44.
 intros h0 h1.
 refine (add r12 r2 i19 i23 i20 h0 h1 _) ; finalize.
Qed.
Lemma l45 : s1 -> p44 (* BND(1 / b + (alpha * (1 / b) + inv_b_hat - 1 / b), [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l43 h0).
 assert (h2 := l46 h0).
 apply t27. exact h1. refine (subset r2 i21 i23 h2 _) ; finalize.
Qed.
Lemma t28 : p44 -> p43.
 intros h0.
 refine (val_xabs _ r3 i20 h0) ; finalize.
Qed.
Lemma l44 : s1 -> p43 (* BND(alpha * (1 / b) + inv_b_hat, [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l45 h0).
 apply t28. exact h1.
Qed.
Definition f32 := Float2 (-1) (-33).
Definition i24 := makepairF f32 f15.
Notation p51 := (BND r43 i24). (* BND(alpha * inv_b_hat - alpha * (1 / b), [-1.16415e-10, 1.16415e-10]) *)
Lemma t29 : p51 -> p43 -> p27.
 intros h0 h1.
 refine (add r43 r3 i24 i20 i11 h0 h1 _) ; finalize.
Qed.
Lemma l28 : s1 -> p27 (* BND(alpha * inv_b_hat - alpha * (1 / b) + (alpha * (1 / b) + inv_b_hat), [1.16415e-10, 1]) *).
 intros h0.
 assert (h1 := l29 h0).
 assert (h2 := l44 h0).
 apply t29. refine (subset r43 i12 i24 h1 _) ; finalize. exact h2.
Qed.
Lemma t30 : p27 -> p26.
 intros h0.
 refine (add_xals _ _ _ i11 h0) ; finalize.
Qed.
Lemma l27 : s1 -> p26 (* BND(alpha * inv_b_hat + inv_b_hat, [1.16415e-10, 1]) *).
 intros h0.
 assert (h1 := l28 h0).
 apply t30. exact h1.
Qed.
Lemma t31 : p26 -> p25.
 intros h0.
 refine (float_round_ne _ _ r22 i11 i11 h0 _) ; finalize.
Qed.
Lemma l26 : s1 -> p25 (* BND(inv_b_hat1, [1.16415e-10, 1]) *).
 intros h0.
 assert (h1 := l27 h0).
 apply t31. exact h1.
Qed.
Lemma t32 : p1 -> p25 -> p24.
 intros h0 h1.
 refine (mul_pp _a _inv_b_hat1 i1 i11 i10 h0 h1 _) ; finalize.
Qed.
Lemma l24 : s1 -> p24 (* BND(a * inv_b_hat1, [0, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l25 h0).
 assert (h2 := l26 h0).
 apply t32. exact h1. exact h2.
Qed.
Lemma t33 : p24 -> p23.
 intros h0.
 refine (abs_of_bnd_p r20 i10 i10 h0 _) ; finalize.
Qed.
Lemma l23 : s1 -> p23 (* ABS(a * inv_b_hat1, [0, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l24 h0).
 apply t33. exact h1.
Qed.
Lemma t34 : p23 -> p22.
 intros h0.
 refine (float_absolute_wide_ne _ _ r20 i10 i3 h0 _) ; finalize.
Qed.
Lemma l22 : s1 -> p22 (* BND(quotient_hat1 - a * inv_b_hat1, [-2.38419e-07, 2.38419e-07]) *).
 intros h0.
 assert (h1 := l23 h0).
 apply t34. exact h1.
Qed.
Lemma t35 : p21 -> p22 -> p20.
 intros h0 h1.
 refine (add r40 r41 i9 i3 i8 h0 h1 _) ; finalize.
Qed.
Lemma l20 : s1 -> p20 (* BND(quotient_rnd - quotient_hat1 + (quotient_hat1 - a * inv_b_hat1), [-0.5, 0.5]) *).
 intros h0.
 assert (h1 := l21 h0).
 assert (h2 := l22 h0).
 apply t35. exact h1. exact h2.
Qed.
Lemma t36 : p20 -> p19.
 intros h0.
 refine (sub_xals _ _ _ i8 h0) ; finalize.
Qed.
Lemma l19 : s1 -> p19 (* BND(quotient_rnd - a * inv_b_hat1, [-0.5, 0.5]) *).
 intros h0.
 assert (h1 := l20 h0).
 apply t36. exact h1.
Qed.
Definition f33 := Float2 (-106102876315521) (-60).
Definition f34 := Float2 (106102876315521) (-60).
Definition i25 := makepairF f33 f34.
Notation p52 := (BND r30 i25). (* BND(a * (inv_b_hat1 - 1 / b), [-9.20296e-05, 9.20296e-05]) *)
Definition f35 := Float2 (-106102876340225) (-92).
Definition f36 := Float2 (106102876340225) (-92).
Definition i26 := makepairF f35 f36.
Notation p53 := (BND r31 i26). (* BND(inv_b_hat1 - 1 / b, [-2.14273e-14, 2.14273e-14]) *)
Notation r51 := ((_inv_b_hat1 - r22)%R).
Notation r52 := ((r22 - r12)%R).
Notation r50 := ((r51 + r52)%R).
Notation p54 := (BND r50 i26). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-2.14273e-14, 2.14273e-14]) *)
Definition f37 := Float2 (-1) (-53).
Definition f38 := Float2 (1) (-53).
Definition i27 := makepairF f37 f38.
Notation p55 := (BND r51 i27). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.11022e-16, 1.11022e-16]) *)
Definition f39 := Float2 (1) (1).
Definition i28 := makepairF f1 f39.
Notation p56 := (ABS r22 i28). (* ABS(alpha * inv_b_hat + inv_b_hat, [0, 2]) *)
Definition i29 := makepairF f1 f3.
Notation p57 := (ABS r23 i29). (* ABS(alpha * inv_b_hat, [0, 1]) *)
Notation p58 := (ABS _alpha i29). (* ABS(alpha, [0, 1]) *)
Definition f40 := Float2 (-1) (0).
Definition i30 := makepairF f40 f3.
Notation p59 := (BND _alpha i30). (* BND(alpha, [-1, 1]) *)
Lemma t37 : p59 -> p58.
 intros h0.
 refine (abs_of_bnd_o _alpha i30 i29 h0 _) ; finalize.
Qed.
Lemma l57 : s1 -> p58 (* ABS(alpha, [0, 1]) *).
 intros h0.
 assert (h1 := l31 h0).
 apply t37. refine (subset _alpha i3 i30 h1 _) ; finalize.
Qed.
Notation p60 := (ABS _inv_b_hat i15). (* ABS(inv_b_hat, [2.32831e-10, 1]) *)
Notation p61 := (BND _inv_b_hat i15). (* BND(inv_b_hat, [2.32831e-10, 1]) *)
Lemma t38 : p35 -> p61.
 intros h0.
 refine (float_round_ne _ _ r10 i15 i15 h0 _) ; finalize.
Qed.
Lemma l59 : s1 -> p61 (* BND(inv_b_hat, [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l37 h0).
 apply t38. exact h1.
Qed.
Lemma t39 : p61 -> p60.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat i15 i15 h0 _) ; finalize.
Qed.
Lemma l58 : s1 -> p60 (* ABS(inv_b_hat, [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l59 h0).
 apply t39. exact h1.
Qed.
Lemma t40 : p58 -> p60 -> p57.
 intros h0 h1.
 refine (mul_aa _alpha _inv_b_hat i29 i15 i29 h0 h1 _) ; finalize.
Qed.
Lemma l56 : s1 -> p57 (* ABS(alpha * inv_b_hat, [0, 1]) *).
 intros h0.
 assert (h1 := l57 h0).
 assert (h2 := l58 h0).
 apply t40. exact h1. exact h2.
Qed.
Lemma t41 : p57 -> p60 -> p56.
 intros h0 h1.
 refine (add_aa_o r23 _inv_b_hat i29 i15 i28 h0 h1 _) ; finalize.
Qed.
Lemma l55 : s1 -> p56 (* ABS(alpha * inv_b_hat + inv_b_hat, [0, 2]) *).
 intros h0.
 assert (h1 := l56 h0).
 assert (h2 := l58 h0).
 apply t41. exact h1. exact h2.
Qed.
Lemma t42 : p56 -> p55.
 intros h0.
 refine (float_absolute_wide_ne _ _ r22 i28 i27 h0 _) ; finalize.
Qed.
Lemma l54 : s1 -> p55 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.11022e-16, 1.11022e-16]) *).
 intros h0.
 assert (h1 := l55 h0).
 apply t42. exact h1.
Qed.
Definition f41 := Float2 (-105553120526337) (-92).
Definition f42 := Float2 (105553120526337) (-92).
Definition i31 := makepairF f41 f42.
Notation p62 := (BND r52 i31). (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-2.13163e-14, 2.13163e-14]) *)
Notation r54 := ((r22 - r3)%R).
Notation r53 := ((r54 + r2)%R).
Notation p63 := (BND r53 i31). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-2.13163e-14, 2.13163e-14]) *)
Notation p64 := (BND r54 i12). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-2.13163e-14, 2.13163e-14]) *)
Lemma t43 : p28 -> p64.
 intros h0.
 refine (add_firs _ _ _ i12 h0) ; finalize.
Qed.
Lemma l62 : s1 -> p64 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-2.13163e-14, 2.13163e-14]) *).
 intros h0.
 assert (h1 := l29 h0).
 apply t43. exact h1.
Qed.
Lemma t44 : p64 -> p45 -> p63.
 intros h0 h1.
 refine (add r54 r2 i12 i21 i31 h0 h1 _) ; finalize.
Qed.
Lemma l61 : s1 -> p63 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-2.13163e-14, 2.13163e-14]) *).
 intros h0.
 assert (h1 := l62 h0).
 assert (h2 := l46 h0).
 apply t44. exact h1. exact h2.
Qed.
Lemma t45 : p63 -> p62.
 intros h0.
 refine (sub_xals _ _ _ i31 h0) ; finalize.
Qed.
Lemma l60 : s1 -> p62 (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-2.13163e-14, 2.13163e-14]) *).
 intros h0.
 assert (h1 := l61 h0).
 apply t45. exact h1.
Qed.
Lemma t46 : p55 -> p62 -> p54.
 intros h0 h1.
 refine (add r51 r52 i27 i31 i26 h0 h1 _) ; finalize.
Qed.
Lemma l53 : s1 -> p54 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-2.14273e-14, 2.14273e-14]) *).
 intros h0.
 assert (h1 := l54 h0).
 assert (h2 := l60 h0).
 apply t46. exact h1. exact h2.
Qed.
Lemma t47 : p54 -> p53.
 intros h0.
 refine (sub_xals _ _ _ i26 h0) ; finalize.
Qed.
Lemma l52 : s1 -> p53 (* BND(inv_b_hat1 - 1 / b, [-2.14273e-14, 2.14273e-14]) *).
 intros h0.
 assert (h1 := l53 h0).
 apply t47. exact h1.
Qed.
Lemma t48 : p1 -> p53 -> p52.
 intros h0 h1.
 refine (mul_po _a r31 i1 i26 i25 h0 h1 _) ; finalize.
Qed.
Lemma l51 : s1 -> p52 (* BND(a * (inv_b_hat1 - 1 / b), [-9.20296e-05, 9.20296e-05]) *).
 intros h0.
 assert (h1 := l25 h0).
 assert (h2 := l52 h0).
 apply t48. exact h1. exact h2.
Qed.
Lemma t49 : p19 -> p52 -> p18.
 intros h0 h1.
 refine (add r29 r30 i8 i25 i5 h0 h1 _) ; finalize.
Qed.
Lemma l18 : s1 -> p18 (* BND(quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b), [-0.500092, 0.500092]) *).
 intros h0.
 assert (h1 := l19 h0).
 assert (h2 := l51 h0).
 apply t49. exact h1. exact h2.
Qed.
Notation p65 := (REL r27 r28 i7). (* REL(quotient_rnd - a / b, quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b), [0, 0]) *)
Notation p66 := (r27 = r28). (* EQL(quotient_rnd - a / b, quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b)) *)
Lemma t50 : p6 -> p66.
 intros h0.
 refine (b3 h0) ; finalize.
Qed.
Lemma l64 : s1 -> p66 (* EQL(quotient_rnd - a / b, quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b)) *).
 intros h0.
 assert (h1 := l4 h0).
 apply t50. exact h1.
Qed.
Notation p67 := (REL r28 r28 i7). (* REL(quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b), quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b), [0, 0]) *)
Lemma t51 : p67.
 refine (rel_refl r28 i7 _) ; finalize.
Qed.
Lemma l65 : s1 -> p67 (* REL(quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b), quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b), [0, 0]) *).
 intros h0.
 apply t51.
Qed.
Lemma t52 : p66 -> p67 -> p65.
 intros h0 h1.
 refine (rel_rewrite_1 r27 r28 r28 i7 h0 h1) ; finalize.
Qed.
Lemma l63 : s1 -> p65 (* REL(quotient_rnd - a / b, quotient_rnd - a * inv_b_hat1 + a * (inv_b_hat1 - 1 / b), [0, 0]) *).
 intros h0.
 assert (h1 := l64 h0).
 assert (h2 := l65 h0).
 apply t52. exact h1. exact h2.
Qed.
Lemma t53 : p18 -> p65 -> p17.
 intros h0 h1.
 refine (bnd_of_bnd_rel_o r27 r28 i5 i7 i5 h0 h1 _) ; finalize.
Qed.
Lemma l17 : s1 -> p17 (* BND(quotient_rnd - a / b, [-0.500092, 0.500092]) *).
 intros h0.
 assert (h1 := l18 h0).
 assert (h2 := l63 h0).
 apply t53. exact h1. exact h2.
Qed.
Lemma t54 : p16 -> p17 -> p15.
 intros h0 h1.
 refine (sub r38 r27 i7 i5 i5 h0 h1 _) ; finalize.
Qed.
Lemma l15 : s1 -> p15 (* BND(a / b - a / b - (quotient_rnd - a / b), [-0.500092, 0.500092]) *).
 intros h0.
 assert (h1 := l16 h0).
 assert (h2 := l17 h0).
 apply t54. exact h1. exact h2.
Qed.
Lemma t55 : p15 -> p14.
 intros h0.
 refine (sub_xars _ _ _ i5 h0) ; finalize.
Qed.
Lemma l14 : s1 -> p14 (* BND(a / b - quotient_rnd, [-0.500092, 0.500092]) *).
 intros h0.
 assert (h1 := l15 h0).
 apply t55. exact h1.
Qed.
Lemma t56 : p6 -> p14 -> p13.
 intros h0 h1.
 refine (div_fir r25 _b i5 h0 h1) ; finalize.
Qed.
Lemma l13 : s1 -> p13 (* BND((a / b - quotient_rnd) * b / b, [-0.500092, 0.500092]) *).
 intros h0.
 assert (h1 := l4 h0).
 assert (h2 := l14 h0).
 apply t56. exact h1. exact h2.
Qed.
Lemma t57 : p10 -> p13 -> p9.
 intros h0 h1.
 refine (add r34 r36 i7 i5 i5 h0 h1 _) ; finalize.
Qed.
Lemma l9 : s1 -> p9 (* BND((remainder - (a / b - quotient_rnd) * b) / b + (a / b - quotient_rnd) * b / b, [-0.500092, 0.500092]) *).
 intros h0.
 assert (h1 := l10 h0).
 assert (h2 := l13 h0).
 apply t57. exact h1. exact h2.
Qed.
Lemma t58 : p6 -> p9 -> p5.
 intros h0 h1.
 refine (div_xals _ _ _b i5 h0 h1) ; finalize.
Qed.
Lemma l3 : s1 -> p5 (* BND(remainder_over_b, [-0.500092, 0.500092]) *).
 intros h0.
 assert (h1 := l4 h0).
 assert (h2 := l9 h0).
 apply t58. exact h1. exact h2.
Qed.
Lemma l1 : s1 -> False.
 intros h0.
 assert (h1 := l2 h0).
 assert (h2 := l3 h0).
 refine (simplify (Tatom false (Abnd 0%nat i4)) Tfalse (Abnd 0%nat i5) (List.cons _remainder_over_b List.nil) h2 h1 _) ; finalize.
Qed.

(*added manually*)
Lemma gappa2_ccl : not ((p1 /\ p2 /\ 
p3) /\ not p4).
  intro.
  apply l1.
  unfold s1. unfold s2, s4. unfold s3.
  tauto.
Qed.

(* bound_a /\ bound_b /\ bound_exact_alpha -> bound_remainder_over_b *)
Theorem STEP2 : p1 /\ p2 /\ 
p3 ->
p4.
  pose proof gappa2_ccl as CCL.
  unfold p1, p2, p3, p4 in *.
  lra.
Qed.

End Generated_by_Gappa2.

End Step_2.


Section Division_algorithm.

Require Import Flocq.Core.Core.

(* === Joining STEP1 and STEP2 === *)

(* Rewriting rules from Gappa scripts *)

(* (alpha * (1/b) + inv_b_hat) - 1/b -> (alpha - (1 - b * inv_b_hat)) * (1/b) { b <> 0 } *)
Lemma rr1 : forall a b c : R, b <> 0%R -> ((a * (1/b) + c) - 1/b = (a - (1 - b * c)) * (1/b))%R.
  intros.
  field.
  exact H.
Qed.

(* (a - b * quotient_rnd -> (a/b - quotient_rnd) * b { b <> 0 } *)
Lemma rr2 : forall a b d : R, b <> 0%R -> (a - b * d = (a/b - d) * b)%R.
  intros.
  field.
  exact H.
Qed.

(* quotient_rnd - a/b -> (quotient_rnd - a * inv_b_hat1) + a * (inv_b_hat1 - 1/b) { b <> 0 }; *)
Lemma rr3 : forall a b c d : R, b <> 0%R -> ((d - a/b) = (d - a * c) + a * (c - 1/b))%R. 
  intros.
  field.
  exact H.
Qed.

(* 1 - b * inv_b_hat -> -1 * (inv_b_hat - 1/b) / (1/b) { b <> 0 } *)
Lemma rr4 : forall b c : R, b <> 0%R -> (1 - b * c = -1 * (c - 1/b) / (1/b))%R.
  intros.
  field.
  exact H.
Qed.

(* Variables as defined by Gappa *)
Variable a : Z.
Variable b : Z.
Definition aR := IZR a.
Definition bR := IZR b.
Notation float_b := ((rounding_float rndNE (24)%positive (-149)%Z) bR).
Notation float_one := (Float1 (1)).
Notation inv_b := ((float_one / float_b)%R).
Notation inv_b_hat := ((rounding_float rndNE (24)%positive (-149)%Z) inv_b).
Notation inv_b_hatb := ((bR * inv_b_hat)%R).
Notation exact_alpha := ((float_one - inv_b_hatb)%R).
Notation alpha := ((rounding_float rndNE (53)%positive (-1074)%Z) exact_alpha).
Notation inv_b_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) ((alpha * inv_b_hat)%R + inv_b_hat)%R).
Notation quotient_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) (aR * inv_b_hat1)%R).
Notation quotient_rnd := ((rounding_fixed rndNE (0)%Z) quotient_hat1).
Notation real_quotient := ((aR / bR)%R).
Notation remainderR := ((aR - (bR * quotient_rnd)%R)%R).
Notation remainder_over_bR := ((remainderR / bR)%R).

Definition lower_a := Float2 (0) (0).
Definition upper_ab := Float2 (4294967295) (0).
Definition bound_a0 := makepairF lower_a upper_ab.
Definition lower_b := Float2 (1) (0).
Definition bound_b0 := makepairF lower_b upper_ab.
Notation bound_a := (BND aR bound_a0).
Notation bound_b := (BND bR bound_b0).
Definition lower_rob := Float2 (-691752902764108185) (-60).
Definition upper_rob := Float2 (691752902764108185) (-60).
Definition bound_rob0 := makepairF lower_rob upper_rob.
Notation bound_rob := (BND remainder_over_bR bound_rob0).

Lemma STEPS_bound_remainder_over_b : bound_a /\ bound_b -> bound_rob.
  intros [H0 H1].
  apply Step_2.STEP2.
  - intro. apply rr1. exact H.
  - intro. apply rr2. exact H.
  - intro. apply rr3. exact H.
  - split. exact H0.
    split. exact H1.
    apply Step_1.STEP1.
      intro. apply rr4. exact H.
      exact H1.
Qed.

(* === Transforming Gappa bounds into Z bounds === *)
Notation bound_aZ := (0 <= a <= 4294967295)%Z.
Notation bound_bZ := (1 <= b <= 4294967295)%Z.
Notation bound_robR := (-0.6 <= remainder_over_bR <= 0.6)%R.

Lemma b0 : (float2R lower_a = 0)%R. 
  unfold lower_a.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma b1 : (float2R upper_ab = 4294967295)%R.
  unfold upper_ab.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma b2 : (float2R lower_b = 1)%R.
  unfold lower_b.
  unfold float2R, Defs.F2R.
  cbn. 
  lra.
Qed.

Lemma b3 : (-0.6 <= lower_rob)%R.
  unfold lower_rob.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma b4 : (upper_rob <= 0.6)%R.
  unfold upper_rob.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma b_aR : bound_a -> (0 <= aR <= 4294967295)%R.
  intro. destruct H. split. cbn in *.
  - rewrite <- b0. exact H.
  - rewrite <- b1. exact H0.
Qed.

Lemma b_aZ : bound_a -> bound_aZ.
  intro. apply b_aR in H.
  split. destruct H.
  - apply le_IZR. tauto.
  - apply le_IZR. tauto.
Qed.

Lemma b_bR : bound_b -> (1 <= bR <= 4294967295)%R.
  intro. destruct H. split. cbn in *.
  - rewrite <- b2. exact H.
  - rewrite <- b1. exact H0.
Qed.

Lemma b_bZ : bound_b -> bound_bZ.
  intro. apply b_bR in H.
  split. destruct H.
  - apply le_IZR. tauto.
  - apply le_IZR. tauto.
Qed.

Lemma b_robR : bound_rob -> bound_robR.
  intro. destruct H. split. cbn in *. 
  - apply Rle_trans with lower_rob.
    apply b3. exact H.
  - apply Rle_trans with upper_rob. 
    exact H0. apply b4.
Qed.

(* Variables in Z *)
Definition real_eucl_quotient := Z.div a b. (* floored division *)
Definition QUOTIENT_rounded_ne := ZnearestE quotient_hat1.
Definition QUOTIENT_minus_1 := (QUOTIENT_rounded_ne - 1)%Z.
Definition remainder := (a - b * QUOTIENT_rounded_ne)%Z.
Definition remainder_over_b := (IZR remainder / IZR b)%R.

(* === Equality of Gappa variables and self-defined variables === *)

Theorem t0 : forall a : R, ((rounding_fixed rndNE (0)%Z) a) 
= (IZR (rndNE (scaled_mantissa radix2 (FIX_exp 0) a)) * bpow radix2 (0))%R.
  tauto.
Qed.

Theorem t1 : forall a : R, ((rounding_fixed rndNE (0)%Z) a) 
= (IZR (rndNE (scaled_mantissa radix2 (FIX_exp 0) a))).
  intro.
  rewrite -> t0.
  unfold bpow. 
  field.
Qed.

Theorem t2 : forall a : R, (scaled_mantissa radix2 (FIX_exp 0) a) = a.
  unfold bpow.
  intro.
  unfold scaled_mantissa. unfold bpow.
  cbn.
  field.
Qed.

Theorem int_rnd : forall a : R, ((rounding_fixed rndNE (0)%Z) a) = IZR (ZnearestE a).
  intro.
  rewrite -> t1.
  rewrite -> t2.
  tauto.
Qed.
 
Lemma q_eq : (quotient_rnd = IZR QUOTIENT_rounded_ne)%R.
  unfold quotient_rnd, QUOTIENT_rounded_ne.
  apply int_rnd.
Qed.

Lemma rem_eq : (remainderR = IZR remainder)%R.
  unfold remainder. unfold remainderR.
  rewrite -> minus_IZR. rewrite -> mult_IZR. rewrite <- q_eq.
  tauto.
Qed.

(* === Bounding the remainder === *)

Lemma bound_remainder_over_b : bound_a /\ bound_b -> (-0.6 <= remainder_over_b <= 0.6)%R.
  intros.
  apply STEPS_bound_remainder_over_b in H. 
  apply b_robR in H.
  unfold remainder_over_b. rewrite <- rem_eq.
  tauto.
Qed.

Lemma b_gt_zero : (b > 0)%Z -> (IZR b > 0)%R.
  intro.
  apply IZR_lt.
  lia.
Qed.

Lemma e0 : (b > 0)%Z -> (remainder_over_b * IZR b = IZR remainder)%R.
  intro.
  unfold remainder_over_b.
  field.
  apply b_gt_zero in H.
  lra.
Qed.

Lemma bound_remainder : (b > 0)%Z /\ bound_a /\ bound_b -> (-0.6 * IZR b <= IZR remainder <= 0.6 * IZR b)%R.
  intros [H0 H1].
  apply bound_remainder_over_b in H1. rewrite <- e0.
  apply b_gt_zero in H0.
  nra.
  exact H0.
Qed.

Lemma bound_remainder_b : (b > 0)%Z /\ bound_a /\ bound_b -> (- IZR b < IZR remainder < IZR b)%R.
  intros. 
  apply bound_remainder in H as H0.
  destruct H.
  apply b_gt_zero in H.
  lra.
Qed.

Lemma bound_remainder_b_in_Z : (b > 0)%Z /\ bound_a /\ bound_b -> (- b < remainder < b)%Z.
  intros.
  apply bound_remainder_b in H.
  destruct H.
  split.
  - apply lt_IZR. rewrite -> opp_IZR. exact H.
  - apply lt_IZR. exact H0.
Qed.

(* === Two cases === *)
Lemma two_cases : (b > 0)%Z /\ bound_a /\ bound_b -> (- b < remainder < 0)%Z \/ (0 <= remainder < b)%Z.
  intros.
  apply bound_remainder_b_in_Z in H.
  lia.
Qed.

(* case #1: (0 <= remainder < b)%Z *)
Lemma a_eq_bqr : (a = b * QUOTIENT_rounded_ne + remainder)%Z.
  unfold remainder.
  lia.
Qed.

Lemma remainder_gt_0 : (0 <= remainder < b)%Z -> QUOTIENT_rounded_ne = real_eucl_quotient.
  intros.
  apply Zdiv_unique with remainder.
  - exact H.
  - apply a_eq_bqr.
Qed.

(* case #2 : (- b < remainder < 0)%Z *)
Definition real_remainder := (a - QUOTIENT_minus_1 * b)%Z.

Lemma rr_eq : (real_remainder = remainder + b)%Z.
  unfold real_remainder. unfold QUOTIENT_minus_1. unfold remainder.
  lia.
Qed.

Lemma bound_real_remainder : (- b < remainder < 0)%Z -> (0 < real_remainder < b)%Z.
  intros.
  rewrite -> rr_eq.
  lia.
Qed.

Lemma a_eq_bqirr : (a = b * QUOTIENT_minus_1 + real_remainder)%Z.
  unfold real_remainder.
  lia.
Qed.

Lemma remainder_lt_0 : (- b < remainder < 0)%Z -> QUOTIENT_minus_1 = real_eucl_quotient.
  intro.
  apply Zdiv_unique with real_remainder.
  apply bound_real_remainder in H.
  - lia.
  - apply bound_real_remainder in H. rewrite <- a_eq_bqirr. reflexivity.
Qed.

Lemma b_gt_0 : bound_b -> (b > 0)%Z. 
  intro.
  apply b_bZ in H. 
  lia.
Qed.

Lemma algorithm_correct : bound_a /\ bound_b -> 
  QUOTIENT_rounded_ne = real_eucl_quotient \/ QUOTIENT_minus_1 = real_eucl_quotient.
  intros.
  destruct H as [H0 H1].
  apply b_gt_0 in H1 as H2.
  assert (H := conj H2 (conj H0 H1)).
  apply two_cases in H.
  elim H.
  - intro. apply remainder_lt_0 in H3. auto.
  - intro. apply remainder_gt_0 in H3. auto.
Qed.

End Division_algorithm.
