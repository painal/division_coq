From Flocq Require Import Core Binary Bits Generic_fmt.

Section Operations_correct.

Arguments is_finite {prec emax}.
Arguments B2R {prec emax}.
Notation round64 := (round radix2 (FLT_exp (-1074) 53) ZnearestE).
Notation round32 := (round radix2 (FLT_exp (-149) 24) ZnearestE).


Variable A : binary64. (* of_int a *)
Variable B : binary32.
Variable fOne : binary32.
Definition inv_b_hat := b32_div mode_NE fOne B.
Variable B64 : binary64.
Variable fOne64 : binary64.
Variable inv_b_hat64 : binary64.
Variable minus_inv_b_hat64 : binary64.
(*convert binary32 to binary64!!*)
(*make Bopp work*)
Definition alpha := b64_fma mode_NE B64 minus_inv_b_hat64 fOne64.
Definition inv_b_hat1 := b64_fma mode_NE alpha inv_b_hat64 inv_b_hat64.
Definition quotient_hat1 := b64_mult mode_NE A inv_b_hat1.

Theorem b32_div_correct :
  forall x y : binary32, 
  B2R y <> 0%R ->
  is_finite x = true ->
  is_finite y = true ->
  (Rabs (round32 (B2R x / B2R y)) < bpow radix2 128)%R ->
  is_finite (b32_div mode_NE x y) = true /\
  B2R (b32_div mode_NE x y) = round32 (B2R x / B2R y).
Proof. 
  intros.
  generalize (Bdiv_correct _ _ (eq_refl Lt : 0 < 24)%Z (eq_refl Lt : 24 < 128)%Z binop_nan_pl32 mode_NE x y H).
  rewrite Rlt_bool_true by easy.
  intros [H3 [H4 _]].
  rewrite <- H0. 
  now split.
Qed.

Theorem inv_b_hat_correct : B2R B <> 0%R ->
  is_finite fOne = true ->
  is_finite B = true ->
  (Rabs (round32 (B2R fOne / B2R B)) < bpow radix2 128)%R ->
  is_finite inv_b_hat = true /\
  B2R inv_b_hat = round32 (B2R fOne / B2R B).
Proof.
  apply b32_div_correct.
Qed. 

Theorem fma64_correct : forall x y z : binary64,
  is_finite x = true ->
  is_finite y = true ->
  is_finite z = true ->
  (Rabs (round64 (B2R x * B2R y + B2R z)) < bpow radix2 1024)%R ->
  is_finite (b64_fma mode_NE x y z) = true /\
  B2R (b64_fma mode_NE x y z) = round64 (B2R x * B2R y + B2R z).
Proof.
  intros.
  generalize (Bfma_correct _ _ (eq_refl Lt : 0 < 53)%Z (eq_refl Lt : 53 < 1024)%Z ternop_nan_pl64 mode_NE x y z H H0 H1).
  rewrite Rlt_bool_true by easy.
  intros [H3 [H4 _]].
  now split.
Qed.

Theorem alpha_correct : is_finite B64 = true -> 
  is_finite minus_inv_b_hat64 = true ->
  is_finite fOne64 = true ->
  (Rabs (round64 (B2R B64 * B2R minus_inv_b_hat64 + B2R fOne64)) < bpow radix2 1024)%R ->
  is_finite alpha = true /\
  B2R alpha = round64 (B2R B64 * B2R minus_inv_b_hat64 + B2R fOne64).
Proof.
  apply fma64_correct.
Qed.

Theorem inv_b_hat1_correct : is_finite alpha = true ->
  is_finite inv_b_hat64 = true ->
  is_finite inv_b_hat64 = true ->
  (Rabs (round64 (B2R alpha * B2R inv_b_hat64 + B2R inv_b_hat64)) < bpow radix2 1024)%R ->
  is_finite inv_b_hat1 = true /\
  B2R inv_b_hat1 = round64 (B2R alpha * B2R inv_b_hat64 + B2R inv_b_hat64).
Proof.
  apply fma64_correct.
Qed.

Theorem b64_mult_correct : forall x y : binary64,
  is_finite x = true ->
  is_finite y = true ->
  (Rabs (round64 (B2R x * B2R y)) < bpow radix2 1024)%R ->
  is_finite (b64_mult mode_NE x y) = true /\
  B2R (b64_mult mode_NE x y) = round64 (B2R x * B2R y).
Proof.
  intros.
  generalize (Bmult_correct _ _ (eq_refl Lt : 0 < 53)%Z (eq_refl Lt : 53 < 1024)%Z binop_nan_pl64 mode_NE x y).
  rewrite Rlt_bool_true by easy.
  intros [H2 [H3 _]]. rewrite -> H in H3. rewrite -> H0 in H3.
  now split.
Qed.

Theorem quotient_hat1_correct : is_finite A = true ->
  is_finite inv_b_hat1 = true ->
  (Rabs (round64 (B2R A * B2R inv_b_hat1)) < bpow radix2 1024)%R ->
  is_finite (quotient_hat1) = true /\
  B2R (quotient_hat1) = round64 (B2R A * B2R inv_b_hat1).
Proof.
  apply b64_mult_correct.
Qed.
  
(*prove rounding correct? cd binary_round_correct in Binary*)

End Operations_correct.
