Require Import Gappa.Gappa_library.
Require Import Lra.
Require Import Psatz.
Require Import Reals.RIneq.

(*to prove

B2R b32_f <> 0%R

is_finite one32_f = true

is_finite b32_f = true

(Rabs (round32 (B2R one32_f / B2R b32_f)) < bpow radix2 128)%R : OK see below

same for alpha, inv_b_hat1, quotient_hat1


===

only assumptions should be 

bound_a /\ bound_b *)

Section Generated_by_Gappa.
Variable _b : R.
Definition f1 := Float2 (1) (0).
Definition f2 := Float2 (4294967295) (0).
Definition i1 := makepairF f1 f2.
Notation p1 := (BND _b i1). (* BND(b, [1, 4.29497e+09]) *)
Notation r4 := (Float1 (1)).
Notation r5 := ((rounding_float rndNE (24)%positive (-149)%Z) _b).
Notation r3 := ((r4 / r5)%R).
Notation _inv_b_hat := ((rounding_float rndNE (24)%positive (-149)%Z) r3).
Definition f3 := Float2 (-1152921504606846975) (68).
Definition f4 := Float2 (1152921504606846975) (68).
Definition i2 := makepairF f3 f4.
Notation p2 := (BND _inv_b_hat i2). (* BND(inv_b_hat, [-3.40282e+38, 3.40282e+38]) *)
Definition s2 := (not p2).
Definition s1 := (p1 /\ s2).
Lemma l2 : s1 -> s2.
 intros h0.
 assert (h1 := h0).
 exact (proj2 h1).
Qed.
Definition f5 := Float2 (1) (-32).
Definition i3 := makepairF f5 f1.
Notation p3 := (BND _inv_b_hat i3). (* BND(inv_b_hat, [2.32831e-10, 1]) *)
Notation p4 := (BND r3 i3). (* BND(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *)
Definition i4 := makepairF f1 f1.
Notation p5 := (BND r4 i4). (* BND(1, [1, 1]) *)
Lemma t1 : p5.
 refine (constant1 _ i4 _) ; finalize.
Qed.
Lemma l5 : s1 -> p5 (* BND(1, [1, 1]) *).
 intros h0.
 apply t1.
Qed.
Definition f6 := Float2 (1) (32).
Definition i5 := makepairF f1 f6.
Notation p6 := (BND r5 i5). (* BND(float<24,-149,ne>(b), [1, 4.29497e+09]) *)
Lemma l7 : s1 -> p1 (* BND(b, [1, 4.29497e+09]) *).
 intros h0.
 assert (h1 := h0).
 exact (proj1 h1).
Qed.
Notation p7 := (BND _b i5). (* BND(b, [1, 4.29497e+09]) *)
Lemma t2 : p7 -> p6.
 intros h0.
 refine (float_round_ne _ _ _b i5 i5 h0 _) ; finalize.
Qed.
Lemma l6 : s1 -> p6 (* BND(float<24,-149,ne>(b), [1, 4.29497e+09]) *).
 intros h0.
 assert (h1 := l7 h0).
 apply t2. refine (subset _b i1 i5 h1 _) ; finalize.
Qed.
Lemma t3 : p5 -> p6 -> p4.
 intros h0 h1.
 refine (div_pp r4 r5 i4 i5 i3 h0 h1 _) ; finalize.
Qed.
Lemma l4 : s1 -> p4 (* BND(1 / float<24,-149,ne>(b), [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l5 h0).
 assert (h2 := l6 h0).
 apply t3. exact h1. exact h2.
Qed.
Lemma t4 : p4 -> p3.
 intros h0.
 refine (float_round_ne _ _ r3 i3 i3 h0 _) ; finalize.
Qed.
Lemma l3 : s1 -> p3 (* BND(inv_b_hat, [2.32831e-10, 1]) *).
 intros h0.
 assert (h1 := l4 h0).
 apply t4. exact h1.
Qed.
Lemma l1 : s1 -> False.
 intros h0.
 assert (h1 := l2 h0).
 assert (h2 := l3 h0).
 refine (simplify (Tatom false (Abnd 0%nat i2)) Tfalse (Abnd 0%nat i3) (List.cons _inv_b_hat List.nil) h2 h1 _) ; finalize.
Qed.

Lemma gappa_ccl : not (p1 /\ not p2).
  intro. 
  apply l1.
  exact H.
Qed.

Lemma no_overflow_gappa : p1 -> p2.
  pose proof gappa_ccl as CCL.
  unfold p1, p2 in *.
  lra.
Qed.


Require Import Flocq.Core.Core.
From Flocq Require Import Core Binary Bits Generic_fmt.

Arguments is_finite {prec emax}.
Arguments B2R {prec emax}.
Notation round64 := (round radix2 (FLT_exp (-1074) 53) ZnearestE).
Notation round32 := (round radix2 (FLT_exp (-149) 24) ZnearestE).
Notation bound_b := (1 <= _b <= 4294967295)%Z.

(*Definition f3 := Float2 (-1152921504606846975) (68).
Definition f4 := Float2 (1152921504606846975) (68).*)

Lemma b0 : (float2R f4 < 340282366920938463463374607431768211456)%R. 
  unfold f4.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma b1 : (float2R f3 > - 340282366920938463463374607431768211456)%R. 
  unfold f3.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma b2 : p2 -> (Ropp (bpow radix2 128) < _inv_b_hat < bpow radix2 128)%R.
  intro. destruct H. split. 
  - pose proof b1. cbn. apply Rlt_le_trans with f3. lra. exact H.
  - pose proof b0. cbn. apply Rle_lt_trans with f4. exact H0. exact H1.  
Qed.

Lemma no_overflow_inv_b_hat : p1 -> (Rabs (_inv_b_hat) < bpow radix2 128)%R.
  intro.
  apply no_overflow_gappa in H.
  apply b2 in H.
  apply Rabs_lt in H.
  exact H.
Qed.

End Generated_by_Gappa.

(*raccord a CompCert when compatibility*)


