/* Seems to work */
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "cycles.h"

static uint32_t dm_random_uint32(void) {
  static uint32_t current=UINT32_C(0xDEADBEEF);
  current = ((uint64_t) current << 6) % UINT32_C(4294967291);
  return current;
}

static uint64_t dm_random_uint64(void) {
  uint32_t a = dm_random_uint32();
  uint32_t b = dm_random_uint32();
  
  return (((uint64_t) a) << 32) | b;
}


static uint64_t dm_biased_random_uint64(void) {
  uint32_t flags = dm_random_uint32();
  uint32_t r;
  switch (flags & 7) {
  case 0:
  case 5:
    r = dm_random_uint32() & 0x3FF;
    break;
  case 1:
    r = dm_random_uint32() & 0xFFFF;
    break;
  case 2:
    r = dm_random_uint32();
    break;
  case 3:
    r = dm_random_uint32() & 0xF;
    break;    
  case 4:
    r = dm_random_uint32() & 0xFF;
    break;    
  default:
    r = dm_random_uint64();
  }
  return r;
}

static inline int64_t my_llround(double x) {
#ifdef __KVX__
  int64_t r;
  asm("fixedud.rn %0 = %1, 0" : "=r" (r) : "r" (x));
  return r;
#else
  return llround((double) x);
#endif
}

static inline float my_invf(float x) {
#if defined(__KVX__) && !defined(__COMPCERT__)
  float r;
  asm("finvw %0 = %1" : "=r" (r) : "r" (x));
  return r;
#else
  return 1.0f / x;
#endif
}

uint64_t ulong_div6(const uint64_t a, const uint64_t b) {
  /* This test is necessary, otherwise the results are wrong
     for a=9223372036854775808 (2^63)
     b=18446744073709551615 (2^64-1) */
  //if (a < b) return 0;

  float inv_b_hat = my_invf(b); 
  uint64_t quotient_hat0 = my_llround(inv_b_hat * a);
  int64_t a1 = a - quotient_hat0 * b;
#if 0
  printf("q0=%lu a1=%ld\n", q0, a1);
#endif
  double alpha = fma(b, -inv_b_hat, 1.0);
  double inv_b_hat1 = fma(alpha, inv_b_hat, inv_b_hat);
  int64_t quotient_hat1 = my_llround(inv_b_hat1 * a1);
  int64_t remainder = a1 - quotient_hat1 * b;
  uint64_t quotient = quotient_hat0 + quotient_hat1;
  if (remainder < 0) {
    quotient--;
  }
  return quotient;
}

int main() {
#if 1
  uint64_t x = 0xFFFFFFFFFFFFFFFFULL;
  uint64_t y = 1ULL;
  uint64_t q1 = x/y, q2 = ulong_div6(x, y);
  if (q1 != q2) {
    printf("%" PRIu64 "/%" PRIu64 "\t%" PRIu64 "\t%" PRIu64 "\n",
	   x, y, q1, q2);
  }

  cycle_t native_time = 0, my_time = 0;
  uint64_t count = 0;
#if 0
  while(1) {
    if (count % 100000000 == 0) {
      printf("%" PRIu64 "\n", count);
    }
#else
  for(int i=0; i<5000000; i++) {
#endif
    count++;
    uint64_t x = dm_biased_random_uint64();
    uint64_t y = dm_biased_random_uint64();
    if (y==0) continue;
    uint64_t q1, q2;

    cycle_t cycle_a = get_cycle();
    q1 = x/y;
    cycle_t cycle_b = get_cycle();
    q2 = ulong_div6(x, y);
    cycle_t cycle_c = get_cycle();

    native_time += cycle_b - cycle_a;
    my_time += cycle_c - cycle_b;
    
    if (q1 != q2) {
      printf("%" PRIu64 "/%" PRIu64 "\t%" PRIu64 "\t%" PRIu64 "\n",
	     x, y, q1, q2);
    }
  }
  printf("native_time= %" PRcycle "\tmy_time= %" PRcycle "\n",
	 native_time, my_time);
#endif
  return 0;
}
