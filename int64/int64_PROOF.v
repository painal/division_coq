(* === Library imports === *)
From Coq Require Import Reals.
Require Import Coq.Numbers.NatInt.NZDiv.
Require Import Coq.Logic.ClassicalFacts.
Require Import Coq.QArith.QArith_base.
Require Import QArith.
Require Import BinInt.
Require Import Coq.ZArith.BinIntDef.
Require Import Gappa.Gappa_library.
From Gappa Require Import Gappa_tactic.
Require Import Psatz.
Require Import Lia.
Require Import Field.
Require Import Flocq.Core.Core.

(*
This is a draft.

Only the part of the proof in Z has been completed (unicity of quotient). 
However the same bounds have been proven as for the int32 division so a proof on the same model should work.

Variables must be converted to CompCert Ints and floats and floating-point divisions must be proven correct.

Also, Gappa proof should be regenerated changing rounding mode for a, b and inv_b_hat. See int32 gappa proofs for a model. 
Whenever doing a single-precision operation, convert all operands to singles (Gappa/Flocq round32) before rounding the result.
Whenever doing a double-precision operation, convert all operands to doubles (Gappa/Flocq round64) before rounding the result.
That will be crucial for the proof of correctness of floating-point operations.
*)

Module Step_1.

Section Generated_by_Gappa1.
Variable _b : R.
Variable _a : R.
Notation r10 := (Float1 (1)).
Notation r11 := ((rounding_float rndNE (24)%positive (-149)%Z) _b).
Notation r9 := ((r10 / r11)%R).
Notation _inv_b_hat := ((rounding_float rndNE (24)%positive (-149)%Z) r9).
Notation r7 := ((_a * _inv_b_hat)%R).
Notation _quotient_hat0 := ((rounding_float rndNE (53)%positive (-1074)%Z) r7).
Notation _q0 := ((rounding_fixed rndNE (0)%Z) _quotient_hat0).
Notation r4 := ((_b * _q0)%R).
Notation _r0 := ((_a - r4)%R).
Notation r14 := ((_a / _b)%R).
Notation r13 := ((r14 - _q0)%R).
Notation r12 := ((r13 * _b)%R).
Hypothesis a1 : (_b <> 0)%R -> _r0 = r12.
Lemma b1 : NZR _b -> _r0 = r12.
 intros h0.
 apply a1.
 exact h0.
Qed.
Notation r16 := ((_b * _inv_b_hat)%R).
Notation r15 := ((r16 - r10)%R).
Notation r19 := ((r10 / _b)%R).
Notation r18 := ((_inv_b_hat - r19)%R).
Notation r17 := ((r18 / r19)%R).
Hypothesis a2 : (_b <> 0)%R -> r15 = r17.
Lemma b2 : NZR _b -> r15 = r17.
 intros h0.
 apply a2.
 exact h0.
Qed.
Notation r20 := ((_quotient_hat0 - r14)%R).
Notation r22 := ((_quotient_hat0 - r7)%R).
Notation r23 := ((_a * r18)%R).
Notation r21 := ((r22 + r23)%R).
Hypothesis a3 : (_b <> 0)%R -> r20 = r21.
Lemma b3 : NZR _b -> r20 = r21.
 intros h0.
 apply a3.
 exact h0.
Qed.
Notation r28 := ((r10 - r16)%R).
Notation _alpha := ((rounding_float rndNE (53)%positive (-1074)%Z) r28).
Notation r26 := ((_alpha * r19)%R).
Notation r25 := ((r26 + _inv_b_hat)%R).
Notation r24 := ((r25 - r19)%R).
Notation r30 := ((_alpha - r28)%R).
Notation r29 := ((r30 * r19)%R).
Hypothesis a4 : (_b <> 0)%R -> r24 = r29.
Lemma b4 : NZR _b -> r24 = r29.
 intros h0.
 apply a4.
 exact h0.
Qed.
Notation r36 := ((_alpha * _inv_b_hat)%R).
Notation r35 := ((r36 + _inv_b_hat)%R).
Notation _inv_b_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) r35).
Notation r33 := ((_r0 * _inv_b_hat1)%R).
Notation _quotient_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) r33).
Notation r37 := ((_r0 / _b)%R).
Notation r31 := ((_quotient_hat1 - r37)%R).
Notation r39 := ((_quotient_hat1 - r33)%R).
Notation r41 := ((_inv_b_hat1 - r19)%R).
Notation r40 := ((_r0 * r41)%R).
Notation r38 := ((r39 + r40)%R).
Hypothesis a5 : (_b <> 0)%R -> r31 = r38.
Lemma b5 : NZR _b -> r31 = r38.
 intros h0.
 apply a5.
 exact h0.
Qed.
Definition f1 := Float2 (0) (0).
Definition f2 := Float2 (1) (64).
Definition i1 := makepairF f1 f2.
Notation p1 := (BND _a i1). (* BND(a, [0, 1.84467e+19]) *)
Definition f3 := Float2 (1) (0).
Definition i2 := makepairF f3 f2.
Notation p2 := (BND _b i2). (* BND(b, [1, 1.84467e+19]) *)
Definition s2 := (p1 /\ p2).
Notation _q1 := ((rounding_fixed rndNE (0)%Z) _quotient_hat1).
Notation r42 := ((_q1 - r37)%R).
Definition f4 := Float2 (-345876451382054093) (-59).
Definition f5 := Float2 (345876451382054093) (-59).
Definition i3 := makepairF f4 f5.
Notation p3 := (BND r42 i3). (* BND(q1 - r0 / b, [-0.6, 0.6]) *)
Definition s3 := (not p3).
Definition s1 := (s2 /\ s3).
Definition f6 := Float2 (720575940379279359) (-57).
Notation p4 := ((_b <= f6)%R). (* BND(b, [-inf, 5]) *)
Lemma l3 : s1 -> s3.
 intros h0.
 assert (h1 := h0).
 exact (proj2 h1).
Qed.
Definition f7 := Float2 (-679410231306517489) (-60).
Definition f8 := Float2 (679410239359581975) (-60).
Definition i4 := makepairF f7 f8.
Notation p5 := (BND r42 i4). (* BND(q1 - r0 / b, [-0.589294, 0.589294]) *)
Notation r45 := ((_q1 - _quotient_hat1)%R).
Notation r44 := ((r45 + r31)%R).
Notation p6 := (BND r44 i4). (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.589294, 0.589294]) *)
Definition f9 := Float2 (-1) (-1).
Definition f10 := Float2 (1) (-1).
Definition i5 := makepairF f9 f10.
Notation p7 := (BND r45 i5). (* BND(q1 - quotient_hat1, [-0.5, 0.5]) *)
Lemma t1 : p7.
 refine (fixed_error_ne _ _ i5 _) ; finalize.
Qed.
Lemma l6 : s1 -> p7 (* BND(q1 - quotient_hat1, [-0.5, 0.5]) *).
 intros h0.
 apply t1.
Qed.
Definition f11 := Float2 (-102949479003094001) (-60).
Definition f12 := Float2 (102949487056158487) (-60).
Definition i6 := makepairF f11 f12.
Notation p8 := (BND r31 i6). (* BND(quotient_hat1 - r0 / b, [-0.0892944, 0.0892944]) *)
Notation p9 := (r31 = r38). (* EQL(quotient_hat1 - r0 / b, quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b)) *)
Notation p10 := (NZR _b). (* NZR(b) *)
Notation p11 := (ABS _b i2). (* ABS(b, [1, 1.84467e+19]) *)
Lemma l12 : s1 -> s2.
 intros h0.
 assert (h1 := h0).
 exact (proj1 h1).
Qed.
Lemma l11 : s1 -> p2 (* BND(b, [1, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l12 h0).
 exact (proj2 h1).
Qed.
Lemma t2 : p2 -> p11.
 intros h0.
 refine (abs_of_bnd_p _b i2 i2 h0 _) ; finalize.
Qed.
Lemma l10 : s1 -> p11 (* ABS(b, [1, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l11 h0).
 apply t2. exact h1.
Qed.
Lemma t3 : p11 -> p10.
 intros h0.
 refine (nzr_of_abs _b i2 h0 _) ; finalize.
Qed.
Lemma l9 : s1 -> p10 (* NZR(b) *).
 intros h0.
 assert (h1 := l10 h0).
 apply t3. exact h1.
Qed.
Lemma t4 : p10 -> p9.
 intros h0.
 refine (b5 h0) ; finalize.
Qed.
Lemma l8 : s1 -> p9 (* EQL(quotient_hat1 - r0 / b, quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b)) *).
 intros h0.
 assert (h1 := l9 h0).
 apply t4. exact h1.
Qed.
Notation p12 := (BND r38 i6). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0892944, 0.0892944]) *)
Definition f13 := Float2 (-1) (-11).
Definition f14 := Float2 (1) (-11).
Definition i7 := makepairF f13 f14.
Notation p13 := (BND r39 i7). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.000488281, 0.000488281]) *)
Definition f15 := Float2 (1) (43).
Definition i8 := makepairF f1 f15.
Notation p14 := (ABS r33 i8). (* ABS(r0 * inv_b_hat1, [0, 8.79609e+12]) *)
Definition f16 := Float2 (-1) (43).
Definition i9 := makepairF f16 f15.
Notation p15 := (BND r33 i9). (* BND(r0 * inv_b_hat1, [-8.79609e+12, 8.79609e+12]) *)
Definition f17 := Float2 (-2111062410527365) (-8).
Definition f18 := Float2 (1006632921) (13).
Definition i10 := makepairF f17 f18.
Notation p16 := (BND _r0 i10). (* BND(r0, [-8.24634e+12, 8.24634e+12]) *)
Notation r46 := ((r37 * _b)%R).
Notation p17 := (BND r46 i10). (* BND(r0 / b * b, [-8.24634e+12, 8.24634e+12]) *)
Definition f19 := Float2 (-864691163352008705) (-19).
Definition f20 := Float2 (422212448551041) (-8).
Definition i11 := makepairF f19 f20.
Notation p18 := (BND r37 i11). (* BND(r0 / b, [-1.64927e+12, 1.64927e+12]) *)
Notation r49 := ((_r0 - r12)%R).
Notation r48 := ((r49 / _b)%R).
Notation r50 := ((r12 / _b)%R).
Notation r47 := ((r48 + r50)%R).
Notation p19 := (BND r47 i11). (* BND((r0 - (a / b - q0) * b) / b + (a / b - q0) * b / b, [-1.64927e+12, 1.64927e+12]) *)
Definition i12 := makepairF f1 f1.
Notation p20 := (BND r48 i12). (* BND((r0 - (a / b - q0) * b) / b, [0, 0]) *)
Notation p21 := (BND r49 i12). (* BND(r0 - (a / b - q0) * b, [0, 0]) *)
Notation p22 := (_r0 = r12). (* EQL(r0, (a / b - q0) * b) *)
Lemma t5 : p10 -> p22.
 intros h0.
 refine (b1 h0) ; finalize.
Qed.
Lemma l23 : s1 -> p22 (* EQL(r0, (a / b - q0) * b) *).
 intros h0.
 assert (h1 := l9 h0).
 apply t5. exact h1.
Qed.
Lemma t6 : p22 -> p21.
 intros h0.
 refine (sub_of_eql _r0 r12 i12 h0 _) ; finalize.
Qed.
Lemma l22 : s1 -> p21 (* BND(r0 - (a / b - q0) * b, [0, 0]) *).
 intros h0.
 assert (h1 := l23 h0).
 apply t6. exact h1.
Qed.
Lemma t7 : p21 -> p2 -> p20.
 intros h0 h1.
 refine (div_pp r49 _b i12 i2 i12 h0 h1 _) ; finalize.
Qed.
Lemma l21 : s1 -> p20 (* BND((r0 - (a / b - q0) * b) / b, [0, 0]) *).
 intros h0.
 assert (h1 := l22 h0).
 assert (h2 := l11 h0).
 apply t7. exact h1. exact h2.
Qed.
Notation p23 := (BND r50 i11). (* BND((a / b - q0) * b / b, [-1.64927e+12, 1.64927e+12]) *)
Notation p24 := (BND r13 i11). (* BND(a / b - q0, [-1.64927e+12, 1.64927e+12]) *)
Notation r52 := ((r14 - _quotient_hat0)%R).
Notation r53 := ((_q0 - _quotient_hat0)%R).
Notation r51 := ((r52 - r53)%R).
Notation p25 := (BND r51 i11). (* BND(a / b - quotient_hat0 - (q0 - quotient_hat0), [-1.64927e+12, 1.64927e+12]) *)
Definition f21 := Float2 (-864691163351746561) (-19).
Definition f22 := Float2 (422212448550913) (-8).
Definition i13 := makepairF f21 f22.
Notation p26 := (BND r52 i13). (* BND(a / b - quotient_hat0, [-1.64927e+12, 1.64927e+12]) *)
Notation r55 := ((r14 - r14)%R).
Notation r54 := ((r55 - r20)%R).
Notation p27 := (BND r54 i13). (* BND(a / b - a / b - (quotient_hat0 - a / b), [-1.64927e+12, 1.64927e+12]) *)
Notation p28 := (BND r55 i12). (* BND(a / b - a / b, [0, 0]) *)
Lemma t8 : p28.
 refine (sub_refl _ i12 _) ; finalize.
Qed.
Lemma l29 : s1 -> p28 (* BND(a / b - a / b, [0, 0]) *).
 intros h0.
 apply t8.
Qed.
Definition f23 := Float2 (-422212448550913) (-8).
Definition f24 := Float2 (864691163351746561) (-19).
Definition i14 := makepairF f23 f24.
Notation p29 := (BND r20 i14). (* BND(quotient_hat0 - a / b, [-1.64927e+12, 1.64927e+12]) *)
Notation p30 := (BND r21 i14). (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-1.64927e+12, 1.64927e+12]) *)
Definition f25 := Float2 (-1) (10).
Definition f26 := Float2 (1) (10).
Definition i15 := makepairF f25 f26.
Notation p31 := (BND r22 i15). (* BND(quotient_hat0 - a * inv_b_hat, [-1024, 1024]) *)
Notation p32 := (ABS r7 i1). (* ABS(a * inv_b_hat, [0, 1.84467e+19]) *)
Notation p33 := (ABS _a i1). (* ABS(a, [0, 1.84467e+19]) *)
Lemma l35 : s1 -> p1 (* BND(a, [0, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l12 h0).
 exact (proj1 h1).
Qed.
Lemma t9 : p1 -> p33.
 intros h0.
 refine (abs_of_bnd_p _a i1 i1 h0 _) ; finalize.
Qed.
Lemma l34 : s1 -> p33 (* ABS(a, [0, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l35 h0).
 apply t9. exact h1.
Qed.
Definition f27 := Float2 (1) (-64).
Definition i16 := makepairF f27 f3.
Notation p34 := (ABS _inv_b_hat i16). (* ABS(inv_b_hat, [5.42101e-20, 1]) *)
Notation p35 := (BND _inv_b_hat i16). (* BND(inv_b_hat, [5.42101e-20, 1]) *)
Notation p36 := (BND r9 i16). (* BND(1 / float<24,-149,ne>(b), [5.42101e-20, 1]) *)
Definition i17 := makepairF f3 f3.
Notation p37 := (BND r10 i17). (* BND(1, [1, 1]) *)
Lemma t10 : p37.
 refine (constant1 _ i17 _) ; finalize.
Qed.
Lemma l39 : s1 -> p37 (* BND(1, [1, 1]) *).
 intros h0.
 apply t10.
Qed.
Notation p38 := (BND r11 i2). (* BND(float<24,-149,ne>(b), [1, 1.84467e+19]) *)
Lemma t11 : p2 -> p38.
 intros h0.
 refine (float_round_ne _ _ _b i2 i2 h0 _) ; finalize.
Qed.
Lemma l40 : s1 -> p38 (* BND(float<24,-149,ne>(b), [1, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l11 h0).
 apply t11. exact h1.
Qed.
Lemma t12 : p37 -> p38 -> p36.
 intros h0 h1.
 refine (div_pp r10 r11 i17 i2 i16 h0 h1 _) ; finalize.
Qed.
Lemma l38 : s1 -> p36 (* BND(1 / float<24,-149,ne>(b), [5.42101e-20, 1]) *).
 intros h0.
 assert (h1 := l39 h0).
 assert (h2 := l40 h0).
 apply t12. exact h1. exact h2.
Qed.
Lemma t13 : p36 -> p35.
 intros h0.
 refine (float_round_ne _ _ r9 i16 i16 h0 _) ; finalize.
Qed.
Lemma l37 : s1 -> p35 (* BND(inv_b_hat, [5.42101e-20, 1]) *).
 intros h0.
 assert (h1 := l38 h0).
 apply t13. exact h1.
Qed.
Lemma t14 : p35 -> p34.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat i16 i16 h0 _) ; finalize.
Qed.
Lemma l36 : s1 -> p34 (* ABS(inv_b_hat, [5.42101e-20, 1]) *).
 intros h0.
 assert (h1 := l37 h0).
 apply t14. exact h1.
Qed.
Lemma t15 : p33 -> p34 -> p32.
 intros h0 h1.
 refine (mul_aa _a _inv_b_hat i1 i16 i1 h0 h1 _) ; finalize.
Qed.
Lemma l33 : s1 -> p32 (* ABS(a * inv_b_hat, [0, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l34 h0).
 assert (h2 := l36 h0).
 apply t15. exact h1. exact h2.
Qed.
Lemma t16 : p32 -> p31.
 intros h0.
 refine (float_absolute_wide_ne _ _ r7 i1 i15 h0 _) ; finalize.
Qed.
Lemma l32 : s1 -> p31 (* BND(quotient_hat0 - a * inv_b_hat, [-1024, 1024]) *).
 intros h0.
 assert (h1 := l33 h0).
 apply t16. exact h1.
Qed.
Definition f28 := Float2 (-422212448288769) (-8).
Definition f29 := Float2 (864691162814875649) (-19).
Definition i18 := makepairF f28 f29.
Notation p39 := (BND r23 i18). (* BND(a * (inv_b_hat - 1 / b), [-1.64927e+12, 1.64927e+12]) *)
Definition f30 := Float2 (-422212448288769) (-72).
Definition f31 := Float2 (864691162814875649) (-83).
Definition i19 := makepairF f30 f31.
Notation p40 := (BND r18 i19). (* BND(inv_b_hat - 1 / b, [-8.9407e-08, 8.9407e-08]) *)
Notation r57 := ((_inv_b_hat - r9)%R).
Notation r58 := ((r9 - r19)%R).
Notation r56 := ((r57 + r58)%R).
Notation p41 := (BND r56 i19). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-8.9407e-08, 8.9407e-08]) *)
Definition f32 := Float2 (-1) (-25).
Definition f33 := Float2 (1) (-25).
Definition i20 := makepairF f32 f33.
Notation p42 := (BND r57 i20). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-2.98023e-08, 2.98023e-08]) *)
Notation p43 := (ABS r9 i16). (* ABS(1 / float<24,-149,ne>(b), [5.42101e-20, 1]) *)
Lemma t17 : p36 -> p43.
 intros h0.
 refine (abs_of_bnd_p r9 i16 i16 h0 _) ; finalize.
Qed.
Lemma l45 : s1 -> p43 (* ABS(1 / float<24,-149,ne>(b), [5.42101e-20, 1]) *).
 intros h0.
 assert (h1 := l38 h0).
 apply t17. exact h1.
Qed.
Lemma t18 : p43 -> p42.
 intros h0.
 refine (float_absolute_wide_ne _ _ r9 i16 i20 h0 _) ; finalize.
Qed.
Lemma l44 : s1 -> p42 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-2.98023e-08, 2.98023e-08]) *).
 intros h0.
 assert (h1 := l45 h0).
 apply t18. exact h1.
Qed.
Definition f34 := Float2 (-281474959933441) (-72).
Definition f35 := Float2 (576460786663163905) (-83).
Definition i21 := makepairF f34 f35.
Notation p44 := (BND r58 i21). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Notation p45 := (REL r9 r19 i21). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Definition f36 := Float2 (-1) (-24).
Definition f37 := Float2 (1) (-24).
Definition i22 := makepairF f36 f37.
Notation p46 := (REL r11 _b i22). (* REL(float<24,-149,ne>(b), b, [-5.96046e-08, 5.96046e-08]) *)
Lemma t19 : p11 -> p46.
 intros h0.
 refine (float_relative_ne _ _ _b i2 i22 h0 _) ; finalize.
Qed.
Lemma l48 : s1 -> p46 (* REL(float<24,-149,ne>(b), b, [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l10 h0).
 apply t19. exact h1.
Qed.
Lemma t20 : p46 -> p10 -> p45.
 intros h0 h1.
 refine (inv_r r11 _b _ i22 i21 h0 h1 _) ; finalize.
Qed.
Lemma l47 : s1 -> p45 (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l48 h0).
 assert (h2 := l9 h0).
 apply t20. exact h1. exact h2.
Qed.
Notation p47 := (BND r19 i16). (* BND(1 / b, [5.42101e-20, 1]) *)
Lemma t21 : p37 -> p2 -> p47.
 intros h0 h1.
 refine (div_pp r10 _b i17 i2 i16 h0 h1 _) ; finalize.
Qed.
Lemma l49 : s1 -> p47 (* BND(1 / b, [5.42101e-20, 1]) *).
 intros h0.
 assert (h1 := l39 h0).
 assert (h2 := l11 h0).
 apply t21. exact h1. exact h2.
Qed.
Lemma t22 : p45 -> p47 -> p44.
 intros h0 h1.
 refine (error_of_rel_op r9 r19 i21 i16 i21 h0 h1 _) ; finalize.
Qed.
Lemma l46 : s1 -> p44 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l47 h0).
 assert (h2 := l49 h0).
 apply t22. exact h1. exact h2.
Qed.
Lemma t23 : p42 -> p44 -> p41.
 intros h0 h1.
 refine (add r57 r58 i20 i21 i19 h0 h1 _) ; finalize.
Qed.
Lemma l43 : s1 -> p41 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-8.9407e-08, 8.9407e-08]) *).
 intros h0.
 assert (h1 := l44 h0).
 assert (h2 := l46 h0).
 apply t23. exact h1. exact h2.
Qed.
Lemma t24 : p41 -> p40.
 intros h0.
 refine (sub_xals _ _ _ i19 h0) ; finalize.
Qed.
Lemma l42 : s1 -> p40 (* BND(inv_b_hat - 1 / b, [-8.9407e-08, 8.9407e-08]) *).
 intros h0.
 assert (h1 := l43 h0).
 apply t24. exact h1.
Qed.
Lemma t25 : p1 -> p40 -> p39.
 intros h0 h1.
 refine (mul_po _a r18 i1 i19 i18 h0 h1 _) ; finalize.
Qed.
Lemma l41 : s1 -> p39 (* BND(a * (inv_b_hat - 1 / b), [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l35 h0).
 assert (h2 := l42 h0).
 apply t25. exact h1. exact h2.
Qed.
Lemma t26 : p31 -> p39 -> p30.
 intros h0 h1.
 refine (add r22 r23 i15 i18 i14 h0 h1 _) ; finalize.
Qed.
Lemma l31 : s1 -> p30 (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l32 h0).
 assert (h2 := l41 h0).
 apply t26. exact h1. exact h2.
Qed.
Notation p48 := (REL r20 r21 i12). (* REL(quotient_hat0 - a / b, quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [0, 0]) *)
Notation p49 := (r20 = r21). (* EQL(quotient_hat0 - a / b, quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b)) *)
Lemma t27 : p10 -> p49.
 intros h0.
 refine (b3 h0) ; finalize.
Qed.
Lemma l51 : s1 -> p49 (* EQL(quotient_hat0 - a / b, quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b)) *).
 intros h0.
 assert (h1 := l9 h0).
 apply t27. exact h1.
Qed.
Notation p50 := (REL r21 r21 i12). (* REL(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [0, 0]) *)
Lemma t28 : p50.
 refine (rel_refl r21 i12 _) ; finalize.
Qed.
Lemma l52 : s1 -> p50 (* REL(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [0, 0]) *).
 intros h0.
 apply t28.
Qed.
Lemma t29 : p49 -> p50 -> p48.
 intros h0 h1.
 refine (rel_rewrite_1 r20 r21 r21 i12 h0 h1) ; finalize.
Qed.
Lemma l50 : s1 -> p48 (* REL(quotient_hat0 - a / b, quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [0, 0]) *).
 intros h0.
 assert (h1 := l51 h0).
 assert (h2 := l52 h0).
 apply t29. exact h1. exact h2.
Qed.
Lemma t30 : p30 -> p48 -> p29.
 intros h0 h1.
 refine (bnd_of_bnd_rel_o r20 r21 i14 i12 i14 h0 h1 _) ; finalize.
Qed.
Lemma l30 : s1 -> p29 (* BND(quotient_hat0 - a / b, [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l31 h0).
 assert (h2 := l50 h0).
 apply t30. exact h1. exact h2.
Qed.
Lemma t31 : p28 -> p29 -> p27.
 intros h0 h1.
 refine (sub r55 r20 i12 i14 i13 h0 h1 _) ; finalize.
Qed.
Lemma l28 : s1 -> p27 (* BND(a / b - a / b - (quotient_hat0 - a / b), [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l29 h0).
 assert (h2 := l30 h0).
 apply t31. exact h1. exact h2.
Qed.
Lemma t32 : p27 -> p26.
 intros h0.
 refine (sub_xars _ _ _ i13 h0) ; finalize.
Qed.
Lemma l27 : s1 -> p26 (* BND(a / b - quotient_hat0, [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l28 h0).
 apply t32. exact h1.
Qed.
Notation p51 := (BND r53 i5). (* BND(q0 - quotient_hat0, [-0.5, 0.5]) *)
Lemma t33 : p51.
 refine (fixed_error_ne _ _ i5 _) ; finalize.
Qed.
Lemma l53 : s1 -> p51 (* BND(q0 - quotient_hat0, [-0.5, 0.5]) *).
 intros h0.
 apply t33.
Qed.
Lemma t34 : p26 -> p51 -> p25.
 intros h0 h1.
 refine (sub r52 r53 i13 i5 i11 h0 h1 _) ; finalize.
Qed.
Lemma l26 : s1 -> p25 (* BND(a / b - quotient_hat0 - (q0 - quotient_hat0), [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l27 h0).
 assert (h2 := l53 h0).
 apply t34. exact h1. exact h2.
Qed.
Lemma t35 : p25 -> p24.
 intros h0.
 refine (sub_xars _ _ _ i11 h0) ; finalize.
Qed.
Lemma l25 : s1 -> p24 (* BND(a / b - q0, [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l26 h0).
 apply t35. exact h1.
Qed.
Lemma t36 : p10 -> p24 -> p23.
 intros h0 h1.
 refine (div_fir r13 _b i11 h0 h1) ; finalize.
Qed.
Lemma l24 : s1 -> p23 (* BND((a / b - q0) * b / b, [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l9 h0).
 assert (h2 := l25 h0).
 apply t36. exact h1. exact h2.
Qed.
Lemma t37 : p20 -> p23 -> p19.
 intros h0 h1.
 refine (add r48 r50 i12 i11 i11 h0 h1 _) ; finalize.
Qed.
Lemma l20 : s1 -> p19 (* BND((r0 - (a / b - q0) * b) / b + (a / b - q0) * b / b, [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l21 h0).
 assert (h2 := l24 h0).
 apply t37. exact h1. exact h2.
Qed.
Lemma t38 : p10 -> p19 -> p18.
 intros h0 h1.
 refine (div_xals _ _ _b i11 h0 h1) ; finalize.
Qed.
Lemma l19 : s1 -> p18 (* BND(r0 / b, [-1.64927e+12, 1.64927e+12]) *).
 intros h0.
 assert (h1 := l9 h0).
 assert (h2 := l20 h0).
 apply t38. exact h1. exact h2.
Qed.
Definition i23 := makepairF f3 f6.
Notation p52 := (BND _b i23). (* BND(b, [1, 5]) *)
Lemma l55 : p4 -> s1 -> p4 (* BND(b, [-inf, 5]) *).
 intros h0 h1.
 assert (h2 := h0).
 exact (h2).
Qed.
Lemma l54 : p4 -> s1 -> p52 (* BND(b, [1, 5]) *).
 intros h0 h1.
 assert (h2 := l55 h0 h1).
 assert (h3 := l11 h1).
 apply intersect_hb with (1 := h2) (2 := h3). finalize.
Qed.
Definition f38 := Float2 (3221225347) (9).
Definition i24 := makepairF f19 f38.
Notation p53 := (BND r37 i24). (* BND(r0 / b, [-1.64927e+12, 1.64927e+12]) *)
Lemma t39 : p53 -> p52 -> p17.
 intros h0 h1.
 refine (mul_op r37 _b i24 i23 i10 h0 h1 _) ; finalize.
Qed.
Lemma l18 : p4 -> s1 -> p17 (* BND(r0 / b * b, [-8.24634e+12, 8.24634e+12]) *).
 intros h0 h1.
 assert (h2 := l19 h1).
 assert (h3 := l54 h0 h1).
 apply t39. refine (subset r37 i11 i24 h2 _) ; finalize. exact h3.
Qed.
Lemma t40 : p10 -> p17 -> p16.
 intros h0 h1.
 refine (div_xilu _r0 _ i10 h0 h1) ; finalize.
Qed.
Lemma l17 : p4 -> s1 -> p16 (* BND(r0, [-8.24634e+12, 8.24634e+12]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l18 h0 h1).
 apply t40. exact h2. exact h3.
Qed.
Definition f39 := Float2 (-5) (-25).
Definition f40 := Float2 (140737505132543) (-47).
Definition i25 := makepairF f39 f40.
Notation p54 := (BND _inv_b_hat1 i25). (* BND(inv_b_hat1, [-1.49012e-07, 1]) *)
Definition f41 := Float2 (-432343408690859521) (-105).
Definition f42 := Float2 (9007199254741089) (-53).
Definition i26 := makepairF f41 f42.
Notation p55 := (BND r35 i26). (* BND(alpha * inv_b_hat + inv_b_hat, [-1.06581e-14, 1]) *)
Notation r60 := ((r36 - r26)%R).
Notation r59 := ((r60 + r25)%R).
Notation p56 := (BND r59 i26). (* BND(alpha * inv_b_hat - alpha * (1 / b) + (alpha * (1 / b) + inv_b_hat), [-1.06581e-14, 1]) *)
Definition f43 := Float2 (-432345607177244161) (-105).
Definition f44 := Float2 (864691145635007489) (-106).
Definition i27 := makepairF f43 f44.
Notation p57 := (BND r60 i27). (* BND(alpha * inv_b_hat - alpha * (1 / b), [-1.06581e-14, 1.06581e-14]) *)
Notation r61 := ((_alpha * r18)%R).
Notation p58 := (BND r61 i27). (* BND(alpha * (inv_b_hat - 1 / b), [-1.06581e-14, 1.06581e-14]) *)
Definition f45 := Float2 (-281474993487873) (-71).
Definition f46 := Float2 (281474959933441) (-71).
Definition i28 := makepairF f45 f46.
Notation p59 := (BND _alpha i28). (* BND(alpha, [-1.19209e-07, 1.19209e-07]) *)
Definition f47 := Float2 (-9007199791611937) (-76).
Definition i29 := makepairF f47 f46.
Notation p60 := (BND r28 i29). (* BND(1 - b * inv_b_hat, [-1.19209e-07, 1.19209e-07]) *)
Notation r63 := ((r10 - r10)%R).
Notation r62 := ((r63 - r15)%R).
Notation p61 := (BND r62 i29). (* BND(1 - 1 - (b * inv_b_hat - 1), [-1.19209e-07, 1.19209e-07]) *)
Notation p62 := (BND r63 i12). (* BND(1 - 1, [0, 0]) *)
Lemma t41 : p62.
 refine (sub_refl _ i12 _) ; finalize.
Qed.
Lemma l64 : s1 -> p62 (* BND(1 - 1, [0, 0]) *).
 intros h0.
 apply t41.
Qed.
Definition f48 := Float2 (-281474959933441) (-71).
Definition f49 := Float2 (9007199791611937) (-76).
Definition i30 := makepairF f48 f49.
Notation p63 := (BND r15 i30). (* BND(b * inv_b_hat - 1, [-1.19209e-07, 1.19209e-07]) *)
Definition f50 := Float2 (144115196665790977) (-80).
Definition i31 := makepairF f48 f50.
Notation p64 := (BND r17 i31). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, 1.19209e-07]) *)
Notation p65 := (NZR r19). (* NZR(1 / b) *)
Notation p66 := (NZR r10). (* NZR(1) *)
Notation p67 := (ABS r10 i17). (* ABS(1, [1, 1]) *)
Lemma t42 : p37 -> p67.
 intros h0.
 refine (abs_of_bnd_p r10 i17 i17 h0 _) ; finalize.
Qed.
Lemma l69 : s1 -> p67 (* ABS(1, [1, 1]) *).
 intros h0.
 assert (h1 := l39 h0).
 apply t42. exact h1.
Qed.
Lemma t43 : p67 -> p66.
 intros h0.
 refine (nzr_of_abs r10 i17 h0 _) ; finalize.
Qed.
Lemma l68 : s1 -> p66 (* NZR(1) *).
 intros h0.
 assert (h1 := l69 h0).
 apply t43. exact h1.
Qed.
Lemma t44 : p66 -> p10 -> p65.
 intros h0 h1.
 refine (div_nzr r10 _b h0 h1) ; finalize.
Qed.
Lemma l67 : s1 -> p65 (* NZR(1 / b) *).
 intros h0.
 assert (h1 := l68 h0).
 assert (h2 := l9 h0).
 apply t44. exact h1. exact h2.
Qed.
Definition f51 := Float2 (288230393331581953) (-81).
Definition i32 := makepairF f48 f51.
Notation p68 := (REL _inv_b_hat r19 i32). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, 1.19209e-07]) *)
Notation p69 := (REL _inv_b_hat r9 i22). (* REL(inv_b_hat, 1 / float<24,-149,ne>(b), [-5.96046e-08, 5.96046e-08]) *)
Lemma t45 : p43 -> p69.
 intros h0.
 refine (float_relative_ne _ _ r9 i16 i22 h0 _) ; finalize.
Qed.
Lemma l71 : s1 -> p69 (* REL(inv_b_hat, 1 / float<24,-149,ne>(b), [-5.96046e-08, 5.96046e-08]) *).
 intros h0.
 assert (h1 := l45 h0).
 apply t45. exact h1.
Qed.
Definition f52 := Float2 (288230393331581953) (-82).
Definition i33 := makepairF f34 f52.
Notation p70 := (REL r9 r19 i33). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Lemma t46 : p69 -> p70 -> p68.
 intros h0 h1.
 refine (compose _inv_b_hat r9 r19 i22 i33 i32 h0 h1 _) ; finalize.
Qed.
Lemma l70 : s1 -> p68 (* REL(inv_b_hat, 1 / b, [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l71 h0).
 assert (h2 := l47 h0).
 apply t46. exact h1. refine (rel_subset r9 r19 i21 i33 h2 _) ; finalize.
Qed.
Notation p71 := (REL _inv_b_hat r19 i31). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, 1.19209e-07]) *)
Lemma t47 : p65 -> p71 -> p64.
 intros h0 h1.
 refine (bnd_of_nzr_rel _inv_b_hat r19 i31 h0 h1) ; finalize.
Qed.
Lemma l66 : s1 -> p64 (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l67 h0).
 assert (h2 := l70 h0).
 apply t47. exact h1. refine (rel_subset _inv_b_hat r19 i32 i31 h2 _) ; finalize.
Qed.
Notation p72 := (REL r15 r17 i12). (* REL(b * inv_b_hat - 1, (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *)
Notation p73 := (r15 = r17). (* EQL(b * inv_b_hat - 1, (inv_b_hat - 1 / b) / (1 / b)) *)
Lemma t48 : p10 -> p73.
 intros h0.
 refine (b2 h0) ; finalize.
Qed.
Lemma l73 : s1 -> p73 (* EQL(b * inv_b_hat - 1, (inv_b_hat - 1 / b) / (1 / b)) *).
 intros h0.
 assert (h1 := l9 h0).
 apply t48. exact h1.
Qed.
Notation p74 := (REL r17 r17 i12). (* REL((inv_b_hat - 1 / b) / (1 / b), (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *)
Lemma t49 : p74.
 refine (rel_refl r17 i12 _) ; finalize.
Qed.
Lemma l74 : s1 -> p74 (* REL((inv_b_hat - 1 / b) / (1 / b), (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *).
 intros h0.
 apply t49.
Qed.
Lemma t50 : p73 -> p74 -> p72.
 intros h0 h1.
 refine (rel_rewrite_1 r15 r17 r17 i12 h0 h1) ; finalize.
Qed.
Lemma l72 : s1 -> p72 (* REL(b * inv_b_hat - 1, (inv_b_hat - 1 / b) / (1 / b), [0, 0]) *).
 intros h0.
 assert (h1 := l73 h0).
 assert (h2 := l74 h0).
 apply t50. exact h1. exact h2.
Qed.
Notation p75 := (BND r17 i30). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, 1.19209e-07]) *)
Lemma t51 : p75 -> p72 -> p63.
 intros h0 h1.
 refine (bnd_of_bnd_rel_o r15 r17 i30 i12 i30 h0 h1 _) ; finalize.
Qed.
Lemma l65 : s1 -> p63 (* BND(b * inv_b_hat - 1, [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l66 h0).
 assert (h2 := l72 h0).
 apply t51. refine (subset r17 i31 i30 h1 _) ; finalize. exact h2.
Qed.
Lemma t52 : p62 -> p63 -> p61.
 intros h0 h1.
 refine (sub r63 r15 i12 i30 i29 h0 h1 _) ; finalize.
Qed.
Lemma l63 : s1 -> p61 (* BND(1 - 1 - (b * inv_b_hat - 1), [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l64 h0).
 assert (h2 := l65 h0).
 apply t52. exact h1. exact h2.
Qed.
Lemma t53 : p61 -> p60.
 intros h0.
 refine (sub_xars _ _ _ i29 h0) ; finalize.
Qed.
Lemma l62 : s1 -> p60 (* BND(1 - b * inv_b_hat, [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l63 h0).
 apply t53. exact h1.
Qed.
Lemma t54 : p60 -> p59.
 intros h0.
 refine (float_round_ne _ _ r28 i29 i28 h0 _) ; finalize.
Qed.
Lemma l61 : s1 -> p59 (* BND(alpha, [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l62 h0).
 apply t54. exact h1.
Qed.
Definition f53 := Float2 (33554431) (-48).
Definition i34 := makepairF f45 f53.
Notation p76 := (BND _alpha i34). (* BND(alpha, [-1.19209e-07, 1.19209e-07]) *)
Lemma t55 : p76 -> p40 -> p58.
 intros h0 h1.
 refine (mul_oo _alpha r18 i34 i19 i27 h0 h1 _) ; finalize.
Qed.
Lemma l60 : s1 -> p58 (* BND(alpha * (inv_b_hat - 1 / b), [-1.06581e-14, 1.06581e-14]) *).
 intros h0.
 assert (h1 := l61 h0).
 assert (h2 := l42 h0).
 apply t55. refine (subset _alpha i28 i34 h1 _) ; finalize. exact h2.
Qed.
Lemma t56 : p58 -> p57.
 intros h0.
 refine (mul_fils _ _ _ i27 h0) ; finalize.
Qed.
Lemma l59 : s1 -> p57 (* BND(alpha * inv_b_hat - alpha * (1 / b), [-1.06581e-14, 1.06581e-14]) *).
 intros h0.
 assert (h1 := l60 h0).
 apply t56. exact h1.
Qed.
Definition f54 := Float2 (4095) (-76).
Definition f55 := Float2 (18014398509481985) (-54).
Definition i35 := makepairF f54 f55.
Notation p77 := (BND r25 i35). (* BND(alpha * (1 / b) + inv_b_hat, [5.41969e-20, 1]) *)
Notation r64 := ((r19 + r24)%R).
Notation p78 := (BND r64 i35). (* BND(1 / b + (alpha * (1 / b) + inv_b_hat - 1 / b), [5.41969e-20, 1]) *)
Definition f56 := Float2 (-1) (-76).
Definition f57 := Float2 (1) (-76).
Definition i36 := makepairF f56 f57.
Notation p79 := (BND r24 i36). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-1.32349e-23, 1.32349e-23]) *)
Notation p80 := (BND r29 i36). (* BND((alpha - (1 - b * inv_b_hat)) * (1 / b), [-1.32349e-23, 1.32349e-23]) *)
Notation p81 := (BND r30 i36). (* BND(alpha - (1 - b * inv_b_hat), [-1.32349e-23, 1.32349e-23]) *)
Definition f58 := Float2 (1) (-22).
Definition i37 := makepairF f1 f58.
Notation p82 := (ABS r28 i37). (* ABS(1 - b * inv_b_hat, [0, 2.38419e-07]) *)
Definition f59 := Float2 (-1) (-22).
Definition i38 := makepairF f59 f58.
Notation p83 := (BND r28 i38). (* BND(1 - b * inv_b_hat, [-2.38419e-07, 2.38419e-07]) *)
Lemma t57 : p83 -> p82.
 intros h0.
 refine (abs_of_bnd_o r28 i38 i37 h0 _) ; finalize.
Qed.
Lemma l80 : s1 -> p82 (* ABS(1 - b * inv_b_hat, [0, 2.38419e-07]) *).
 intros h0.
 assert (h1 := l62 h0).
 apply t57. refine (subset r28 i29 i38 h1 _) ; finalize.
Qed.
Lemma t58 : p82 -> p81.
 intros h0.
 refine (float_absolute_wide_ne _ _ r28 i37 i36 h0 _) ; finalize.
Qed.
Lemma l79 : s1 -> p81 (* BND(alpha - (1 - b * inv_b_hat), [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l80 h0).
 apply t58. exact h1.
Qed.
Lemma t59 : p81 -> p47 -> p80.
 intros h0 h1.
 refine (mul_op r30 r19 i36 i16 i36 h0 h1 _) ; finalize.
Qed.
Lemma l78 : s1 -> p80 (* BND((alpha - (1 - b * inv_b_hat)) * (1 / b), [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l79 h0).
 assert (h2 := l49 h0).
 apply t59. exact h1. exact h2.
Qed.
Notation p84 := (REL r24 r29 i12). (* REL(alpha * (1 / b) + inv_b_hat - 1 / b, (alpha - (1 - b * inv_b_hat)) * (1 / b), [0, 0]) *)
Notation p85 := (r24 = r29). (* EQL(alpha * (1 / b) + inv_b_hat - 1 / b, (alpha - (1 - b * inv_b_hat)) * (1 / b)) *)
Lemma t60 : p10 -> p85.
 intros h0.
 refine (b4 h0) ; finalize.
Qed.
Lemma l82 : s1 -> p85 (* EQL(alpha * (1 / b) + inv_b_hat - 1 / b, (alpha - (1 - b * inv_b_hat)) * (1 / b)) *).
 intros h0.
 assert (h1 := l9 h0).
 apply t60. exact h1.
Qed.
Notation p86 := (REL r29 r29 i12). (* REL((alpha - (1 - b * inv_b_hat)) * (1 / b), (alpha - (1 - b * inv_b_hat)) * (1 / b), [0, 0]) *)
Lemma t61 : p86.
 refine (rel_refl r29 i12 _) ; finalize.
Qed.
Lemma l83 : s1 -> p86 (* REL((alpha - (1 - b * inv_b_hat)) * (1 / b), (alpha - (1 - b * inv_b_hat)) * (1 / b), [0, 0]) *).
 intros h0.
 apply t61.
Qed.
Lemma t62 : p85 -> p86 -> p84.
 intros h0 h1.
 refine (rel_rewrite_1 r24 r29 r29 i12 h0 h1) ; finalize.
Qed.
Lemma l81 : s1 -> p84 (* REL(alpha * (1 / b) + inv_b_hat - 1 / b, (alpha - (1 - b * inv_b_hat)) * (1 / b), [0, 0]) *).
 intros h0.
 assert (h1 := l82 h0).
 assert (h2 := l83 h0).
 apply t62. exact h1. exact h2.
Qed.
Lemma t63 : p80 -> p84 -> p79.
 intros h0 h1.
 refine (bnd_of_bnd_rel_o r24 r29 i36 i12 i36 h0 h1 _) ; finalize.
Qed.
Lemma l77 : s1 -> p79 (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l78 h0).
 assert (h2 := l81 h0).
 apply t63. exact h1. exact h2.
Qed.
Definition f60 := Float2 (1) (-54).
Definition i39 := makepairF f56 f60.
Notation p87 := (BND r24 i39). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-1.32349e-23, 5.55112e-17]) *)
Lemma t64 : p47 -> p87 -> p78.
 intros h0 h1.
 refine (add r19 r24 i16 i39 i35 h0 h1 _) ; finalize.
Qed.
Lemma l76 : s1 -> p78 (* BND(1 / b + (alpha * (1 / b) + inv_b_hat - 1 / b), [5.41969e-20, 1]) *).
 intros h0.
 assert (h1 := l49 h0).
 assert (h2 := l77 h0).
 apply t64. exact h1. refine (subset r24 i36 i39 h2 _) ; finalize.
Qed.
Lemma t65 : p78 -> p77.
 intros h0.
 refine (val_xabs _ r25 i35 h0) ; finalize.
Qed.
Lemma l75 : s1 -> p77 (* BND(alpha * (1 / b) + inv_b_hat, [5.41969e-20, 1]) *).
 intros h0.
 assert (h1 := l76 h0).
 apply t65. exact h1.
Qed.
Definition f61 := Float2 (193) (-54).
Definition i40 := makepairF f43 f61.
Notation p88 := (BND r60 i40). (* BND(alpha * inv_b_hat - alpha * (1 / b), [-1.06581e-14, 1.07137e-14]) *)
Lemma t66 : p88 -> p77 -> p56.
 intros h0 h1.
 refine (add r60 r25 i40 i35 i26 h0 h1 _) ; finalize.
Qed.
Lemma l58 : s1 -> p56 (* BND(alpha * inv_b_hat - alpha * (1 / b) + (alpha * (1 / b) + inv_b_hat), [-1.06581e-14, 1]) *).
 intros h0.
 assert (h1 := l59 h0).
 assert (h2 := l75 h0).
 apply t66. refine (subset r60 i27 i40 h1 _) ; finalize. exact h2.
Qed.
Lemma t67 : p56 -> p55.
 intros h0.
 refine (add_xals _ _ _ i26 h0) ; finalize.
Qed.
Lemma l57 : s1 -> p55 (* BND(alpha * inv_b_hat + inv_b_hat, [-1.06581e-14, 1]) *).
 intros h0.
 assert (h1 := l58 h0).
 apply t67. exact h1.
Qed.
Definition f62 := Float2 (9007200328482753) (-53).
Definition i41 := makepairF f39 f62.
Notation p89 := (BND r35 i41). (* BND(alpha * inv_b_hat + inv_b_hat, [-1.49012e-07, 1]) *)
Lemma t68 : p89 -> p54.
 intros h0.
 refine (float_round_ne _ _ r35 i41 i25 h0 _) ; finalize.
Qed.
Lemma l56 : s1 -> p54 (* BND(inv_b_hat1, [-1.49012e-07, 1]) *).
 intros h0.
 assert (h1 := l57 h0).
 apply t68. refine (subset r35 i26 i41 h1 _) ; finalize.
Qed.
Definition f63 := Float2 (-31) (38).
Definition f64 := Float2 (15) (39).
Definition i42 := makepairF f63 f64.
Notation p90 := (BND _r0 i42). (* BND(r0, [-8.52122e+12, 8.24634e+12]) *)
Definition f65 := Float2 (-1) (0).
Definition f66 := Float2 (33) (-5).
Definition i43 := makepairF f65 f66.
Notation p91 := (BND _inv_b_hat1 i43). (* BND(inv_b_hat1, [-1, 1.03125]) *)
Lemma t69 : p90 -> p91 -> p15.
 intros h0 h1.
 refine (mul_oo _r0 _inv_b_hat1 i42 i43 i9 h0 h1 _) ; finalize.
Qed.
Lemma l16 : p4 -> s1 -> p15 (* BND(r0 * inv_b_hat1, [-8.79609e+12, 8.79609e+12]) *).
 intros h0 h1.
 assert (h2 := l17 h0 h1).
 assert (h3 := l56 h1).
 apply t69. refine (subset _r0 i10 i42 h2 _) ; finalize. refine (subset _inv_b_hat1 i25 i43 h3 _) ; finalize.
Qed.
Lemma t70 : p15 -> p14.
 intros h0.
 refine (abs_of_bnd_o r33 i9 i8 h0 _) ; finalize.
Qed.
Lemma l15 : p4 -> s1 -> p14 (* ABS(r0 * inv_b_hat1, [0, 8.79609e+12]) *).
 intros h0 h1.
 assert (h2 := l16 h0 h1).
 apply t70. exact h2.
Qed.
Lemma t71 : p14 -> p13.
 intros h0.
 refine (float_absolute_wide_ne _ _ r33 i8 i7 h0 _) ; finalize.
Qed.
Lemma l14 : p4 -> s1 -> p13 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.000488281, 0.000488281]) *).
 intros h0 h1.
 assert (h2 := l15 h0 h1).
 apply t71. exact h2.
Qed.
Definition f67 := Float2 (-102386529049672689) (-60).
Definition f68 := Float2 (102386537102737175) (-60).
Definition i44 := makepairF f67 f68.
Notation p92 := (BND r40 i44). (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.0888062, 0.0888062]) *)
Definition f69 := Float2 (-436849207341485569) (-105).
Definition f70 := Float2 (873698345963490305) (-106).
Definition i45 := makepairF f69 f70.
Notation p93 := (BND r41 i45). (* BND(inv_b_hat1 - 1 / b, [-1.07692e-14, 1.07692e-14]) *)
Notation r66 := ((_inv_b_hat1 - r35)%R).
Notation r67 := ((r35 - r19)%R).
Notation r65 := ((r66 + r67)%R).
Notation p94 := (BND r65 i45). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-1.07692e-14, 1.07692e-14]) *)
Definition f71 := Float2 (-1) (-53).
Definition f72 := Float2 (1) (-53).
Definition i46 := makepairF f71 f72.
Notation p95 := (BND r66 i46). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.11022e-16, 1.11022e-16]) *)
Definition f73 := Float2 (1) (1).
Definition i47 := makepairF f1 f73.
Notation p96 := (ABS r35 i47). (* ABS(alpha * inv_b_hat + inv_b_hat, [0, 2]) *)
Definition f74 := Float2 (1) (-16).
Definition i48 := makepairF f1 f74.
Notation p97 := (ABS r36 i48). (* ABS(alpha * inv_b_hat, [0, 1.52588e-05]) *)
Notation p98 := (ABS _alpha i48). (* ABS(alpha, [0, 1.52588e-05]) *)
Definition f75 := Float2 (-1) (-16).
Definition i49 := makepairF f75 f74.
Notation p99 := (BND _alpha i49). (* BND(alpha, [-1.52588e-05, 1.52588e-05]) *)
Lemma t72 : p99 -> p98.
 intros h0.
 refine (abs_of_bnd_o _alpha i49 i48 h0 _) ; finalize.
Qed.
Lemma l90 : s1 -> p98 (* ABS(alpha, [0, 1.52588e-05]) *).
 intros h0.
 assert (h1 := l61 h0).
 apply t72. refine (subset _alpha i28 i49 h1 _) ; finalize.
Qed.
Lemma t73 : p98 -> p34 -> p97.
 intros h0 h1.
 refine (mul_aa _alpha _inv_b_hat i48 i16 i48 h0 h1 _) ; finalize.
Qed.
Lemma l89 : s1 -> p97 (* ABS(alpha * inv_b_hat, [0, 1.52588e-05]) *).
 intros h0.
 assert (h1 := l90 h0).
 assert (h2 := l36 h0).
 apply t73. exact h1. exact h2.
Qed.
Definition i50 := makepairF f1 f3.
Notation p100 := (ABS r36 i50). (* ABS(alpha * inv_b_hat, [0, 1]) *)
Lemma t74 : p100 -> p34 -> p96.
 intros h0 h1.
 refine (add_aa_o r36 _inv_b_hat i50 i16 i47 h0 h1 _) ; finalize.
Qed.
Lemma l88 : s1 -> p96 (* ABS(alpha * inv_b_hat + inv_b_hat, [0, 2]) *).
 intros h0.
 assert (h1 := l89 h0).
 assert (h2 := l36 h0).
 apply t74. refine (abs_subset r36 i48 i50 h1 _) ; finalize. exact h2.
Qed.
Lemma t75 : p96 -> p95.
 intros h0.
 refine (float_absolute_wide_ne _ _ r35 i47 i46 h0 _) ; finalize.
Qed.
Lemma l87 : s1 -> p95 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.11022e-16, 1.11022e-16]) *).
 intros h0.
 assert (h1 := l88 h0).
 apply t75. exact h1.
Qed.
Definition f76 := Float2 (-432345607714115073) (-105).
Definition f77 := Float2 (864691146708749313) (-106).
Definition i51 := makepairF f76 f77.
Notation p101 := (BND r67 i51). (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-1.06581e-14, 1.06581e-14]) *)
Notation r69 := ((r35 - r25)%R).
Notation r68 := ((r69 + r24)%R).
Notation p102 := (BND r68 i51). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.06581e-14, 1.06581e-14]) *)
Notation p103 := (BND r69 i27). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-1.06581e-14, 1.06581e-14]) *)
Lemma t76 : p57 -> p103.
 intros h0.
 refine (add_firs _ _ _ i27 h0) ; finalize.
Qed.
Lemma l93 : s1 -> p103 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-1.06581e-14, 1.06581e-14]) *).
 intros h0.
 assert (h1 := l59 h0).
 apply t76. exact h1.
Qed.
Lemma t77 : p103 -> p79 -> p102.
 intros h0 h1.
 refine (add r69 r24 i27 i36 i51 h0 h1 _) ; finalize.
Qed.
Lemma l92 : s1 -> p102 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.06581e-14, 1.06581e-14]) *).
 intros h0.
 assert (h1 := l93 h0).
 assert (h2 := l77 h0).
 apply t77. exact h1. exact h2.
Qed.
Lemma t78 : p102 -> p101.
 intros h0.
 refine (sub_xals _ _ _ i51 h0) ; finalize.
Qed.
Lemma l91 : s1 -> p101 (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-1.06581e-14, 1.06581e-14]) *).
 intros h0.
 assert (h1 := l92 h0).
 apply t78. exact h1.
Qed.
Lemma t79 : p95 -> p101 -> p94.
 intros h0 h1.
 refine (add r66 r67 i46 i51 i45 h0 h1 _) ; finalize.
Qed.
Lemma l86 : s1 -> p94 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-1.07692e-14, 1.07692e-14]) *).
 intros h0.
 assert (h1 := l87 h0).
 assert (h2 := l91 h0).
 apply t79. exact h1. exact h2.
Qed.
Lemma t80 : p94 -> p93.
 intros h0.
 refine (sub_xals _ _ _ i45 h0) ; finalize.
Qed.
Lemma l85 : s1 -> p93 (* BND(inv_b_hat1 - 1 / b, [-1.07692e-14, 1.07692e-14]) *).
 intros h0.
 assert (h1 := l86 h0).
 apply t80. exact h1.
Qed.
Definition f78 := Float2 (-109212301835371393) (-103).
Definition i52 := makepairF f78 f70.
Notation p104 := (BND r41 i52). (* BND(inv_b_hat1 - 1 / b, [-1.07692e-14, 1.07692e-14]) *)
Lemma t81 : p16 -> p104 -> p92.
 intros h0 h1.
 refine (mul_oo _r0 r41 i10 i52 i44 h0 h1 _) ; finalize.
Qed.
Lemma l84 : p4 -> s1 -> p92 (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.0888062, 0.0888062]) *).
 intros h0 h1.
 assert (h2 := l17 h0 h1).
 assert (h3 := l85 h1).
 apply t81. exact h2. refine (subset r41 i45 i52 h3 _) ; finalize.
Qed.
Lemma t82 : p13 -> p92 -> p12.
 intros h0 h1.
 refine (add r39 r40 i7 i44 i6 h0 h1 _) ; finalize.
Qed.
Lemma l13 : p4 -> s1 -> p12 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0892944, 0.0892944]) *).
 intros h0 h1.
 assert (h2 := l14 h0 h1).
 assert (h3 := l84 h0 h1).
 apply t82. exact h2. exact h3.
Qed.
Lemma t83 : p9 -> p12 -> p8.
 intros h0 h1.
 refine (bnd_rewrite r31 r38 i6 h0 h1) ; finalize.
Qed.
Lemma l7 : p4 -> s1 -> p8 (* BND(quotient_hat1 - r0 / b, [-0.0892944, 0.0892944]) *).
 intros h0 h1.
 assert (h2 := l8 h1).
 assert (h3 := l13 h0 h1).
 apply t83. exact h2. exact h3.
Qed.
Lemma t84 : p7 -> p8 -> p6.
 intros h0 h1.
 refine (add r45 r31 i5 i6 i4 h0 h1 _) ; finalize.
Qed.
Lemma l5 : p4 -> s1 -> p6 (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.589294, 0.589294]) *).
 intros h0 h1.
 assert (h2 := l6 h1).
 assert (h3 := l7 h0 h1).
 apply t84. exact h2. exact h3.
Qed.
Lemma t85 : p6 -> p5.
 intros h0.
 refine (sub_xals _ _ _ i4 h0) ; finalize.
Qed.
Lemma l4 : p4 -> s1 -> p5 (* BND(q1 - r0 / b, [-0.589294, 0.589294]) *).
 intros h0 h1.
 assert (h2 := l5 h0 h1).
 apply t85. exact h2.
Qed.
Lemma l2 : p4 -> s1 -> False.
 intros h0 h1.
 assert (h2 := l3 h1).
 assert (h3 := l4 h0 h1).
 refine (simplify (Tatom false (Abnd 0%nat i3)) Tfalse (Abnd 0%nat i4) (List.cons r42 List.nil) h3 h2 _) ; finalize.
Qed.
Definition f79 := Float2 (585467951558164479) (-53).
Definition i53 := makepairF f6 f79.
Notation p105 := (BND _b i53). (* BND(b, [5, 65]) *)
Definition f80 := Float2 (-1279845302111003) (-51).
Definition f81 := Float2 (1279845302111003) (-51).
Definition i54 := makepairF f80 f81.
Notation p106 := (BND r42 i54). (* BND(q1 - r0 / b, [-0.568365, 0.568365]) *)
Notation p107 := (BND r44 i54). (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.568365, 0.568365]) *)
Definition f82 := Float2 (-153945395268379) (-51).
Definition f83 := Float2 (153945395268379) (-51).
Definition i55 := makepairF f82 f83.
Notation p108 := (BND r31 i55). (* BND(quotient_hat1 - r0 / b, [-0.0683655, 0.0683655]) *)
Definition i56 := makepairF f1 f83.
Notation p109 := (ABS r31 i56). (* ABS(quotient_hat1 - r0 / b, [0, 0.0683655]) *)
Notation p110 := (ABS r38 i56). (* ABS(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [0, 0.0683655]) *)
Definition f84 := Float2 (-2293965) (-25).
Definition i57 := makepairF f84 f83.
Notation p111 := (BND r38 i57). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0683655, 0.0683655]) *)
Definition f85 := Float2 (-1) (-9).
Definition f86 := Float2 (1) (-9).
Definition i58 := makepairF f85 f86.
Notation p112 := (BND r39 i58). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.00195312, 0.00195312]) *)
Definition f87 := Float2 (1) (45).
Definition i59 := makepairF f1 f87.
Notation p113 := (ABS r33 i59). (* ABS(r0 * inv_b_hat1, [0, 3.51844e+13]) *)
Definition f88 := Float2 (1) (47).
Definition i60 := makepairF f1 f88.
Notation p114 := (ABS _r0 i60). (* ABS(r0, [0, 1.40737e+14]) *)
Definition f89 := Float2 (-117093597359245729) (-12).
Definition f90 := Float2 (27262975) (20).
Definition i61 := makepairF f89 f90.
Notation p115 := (BND _r0 i61). (* BND(r0, [-2.85873e+13, 2.85873e+13]) *)
Notation p116 := (BND r46 i61). (* BND(r0 / b * b, [-2.85873e+13, 2.85873e+13]) *)
Definition f91 := Float2 (-230584314799745435) (-19).
Definition f92 := Float2 (53687089) (13).
Definition i62 := makepairF f91 f92.
Notation p117 := (BND r37 i62). (* BND(r0 / b, [-4.39805e+11, 4.39805e+11]) *)
Notation p118 := (BND r47 i62). (* BND((r0 - (a / b - q0) * b) / b + (a / b - q0) * b / b, [-4.39805e+11, 4.39805e+11]) *)
Notation p119 := (BND r50 i62). (* BND((a / b - q0) * b / b, [-4.39805e+11, 4.39805e+11]) *)
Notation p120 := (BND r13 i62). (* BND(a / b - q0, [-4.39805e+11, 4.39805e+11]) *)
Notation p121 := (BND r51 i62). (* BND(a / b - quotient_hat0 - (q0 - quotient_hat0), [-4.39805e+11, 4.39805e+11]) *)
Definition f93 := Float2 (-230584314799483291) (-19).
Definition f94 := Float2 (107374177) (12).
Definition i63 := makepairF f93 f94.
Notation p122 := (BND r52 i63). (* BND(a / b - quotient_hat0, [-4.39805e+11, 4.39805e+11]) *)
Notation p123 := (BND r54 i63). (* BND(a / b - a / b - (quotient_hat0 - a / b), [-4.39805e+11, 4.39805e+11]) *)
Definition f95 := Float2 (-107374177) (12).
Definition f96 := Float2 (230584314799483291) (-19).
Definition i64 := makepairF f95 f96.
Notation p124 := (BND r20 i64). (* BND(quotient_hat0 - a / b, [-4.39805e+11, 4.39805e+11]) *)
Notation r71 := ((r20 - r21)%R).
Notation r70 := ((r21 + r71)%R).
Notation p125 := (BND r70 i64). (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b) + (quotient_hat0 - a / b - (quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b))), [-4.39805e+11, 4.39805e+11]) *)
Notation p126 := (BND r21 i64). (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-4.39805e+11, 4.39805e+11]) *)
Definition f97 := Float2 (-1) (8).
Definition f98 := Float2 (1) (8).
Definition i65 := makepairF f97 f98.
Notation p127 := (BND r22 i65). (* BND(quotient_hat0 - a * inv_b_hat, [-256, 256]) *)
Definition f99 := Float2 (1) (62).
Definition i66 := makepairF f1 f99.
Notation p128 := (ABS r7 i66). (* ABS(a * inv_b_hat, [0, 4.61169e+18]) *)
Definition f100 := Float2 (3) (-8).
Definition f101 := Float2 (7) (-5).
Definition i67 := makepairF f100 f101.
Notation p129 := (ABS _inv_b_hat i67). (* ABS(inv_b_hat, [0.0117188, 0.21875]) *)
Notation p130 := (BND _inv_b_hat i67). (* BND(inv_b_hat, [0.0117188, 0.21875]) *)
Notation r72 := ((r16 / _b)%R).
Notation p131 := (BND r72 i67). (* BND(b * inv_b_hat / b, [0.0117188, 0.21875]) *)
Definition f102 := Float2 (8388607) (-23).
Definition f103 := Float2 (576460821022904321) (-59).
Definition i68 := makepairF f102 f103.
Notation p132 := (BND r16 i68). (* BND(b * inv_b_hat, [1, 1]) *)
Notation r73 := ((r10 + r15)%R).
Notation p133 := (BND r73 i68). (* BND(1 + (b * inv_b_hat - 1), [1, 1]) *)
Definition f104 := Float2 (-1) (-23).
Definition f105 := Float2 (68719480833) (-59).
Definition i69 := makepairF f104 f105.
Notation p134 := (BND r15 i69). (* BND(b * inv_b_hat - 1, [-1.19209e-07, 1.19209e-07]) *)
Lemma t86 : p37 -> p134 -> p133.
 intros h0 h1.
 refine (add r10 r15 i17 i69 i68 h0 h1 _) ; finalize.
Qed.
Lemma l122 : s1 -> p133 (* BND(1 + (b * inv_b_hat - 1), [1, 1]) *).
 intros h0.
 assert (h1 := l39 h0).
 assert (h2 := l65 h0).
 apply t86. exact h1. refine (subset r15 i30 i69 h2 _) ; finalize.
Qed.
Lemma t87 : p133 -> p132.
 intros h0.
 refine (val_xabs _ r16 i68 h0) ; finalize.
Qed.
Lemma l121 : s1 -> p132 (* BND(b * inv_b_hat, [1, 1]) *).
 intros h0.
 assert (h1 := l122 h0).
 apply t87. exact h1.
Qed.
Lemma l123 : p105 -> s1 -> p105 (* BND(b, [5, 65]) *).
 intros h0 h1.
 assert (h2 := h0).
 exact (h2).
Qed.
Definition f106 := Float2 (15) (-4).
Definition f107 := Float2 (17) (-4).
Definition i70 := makepairF f106 f107.
Notation p135 := (BND r16 i70). (* BND(b * inv_b_hat, [0.9375, 1.0625]) *)
Definition f108 := Float2 (39) (-3).
Definition f109 := Float2 (5) (4).
Definition i71 := makepairF f108 f109.
Notation p136 := (BND _b i71). (* BND(b, [4.875, 80]) *)
Lemma t88 : p135 -> p136 -> p131.
 intros h0 h1.
 refine (div_pp r16 _b i70 i71 i67 h0 h1 _) ; finalize.
Qed.
Lemma l120 : p105 -> s1 -> p131 (* BND(b * inv_b_hat / b, [0.0117188, 0.21875]) *).
 intros h0 h1.
 assert (h2 := l121 h1).
 assert (h3 := l123 h0 h1).
 apply t88. refine (subset r16 i68 i70 h2 _) ; finalize. refine (subset _b i53 i71 h3 _) ; finalize.
Qed.
Lemma t89 : p10 -> p131 -> p130.
 intros h0 h1.
 refine (mul_xiru _ _inv_b_hat i67 h0 h1) ; finalize.
Qed.
Lemma l119 : p105 -> s1 -> p130 (* BND(inv_b_hat, [0.0117188, 0.21875]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l120 h0 h1).
 apply t89. exact h2. exact h3.
Qed.
Lemma t90 : p130 -> p129.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat i67 i67 h0 _) ; finalize.
Qed.
Lemma l118 : p105 -> s1 -> p129 (* ABS(inv_b_hat, [0.0117188, 0.21875]) *).
 intros h0 h1.
 assert (h2 := l119 h0 h1).
 apply t90. exact h2.
Qed.
Definition f110 := Float2 (1) (-7).
Definition f111 := Float2 (1) (-2).
Definition i72 := makepairF f110 f111.
Notation p137 := (ABS _inv_b_hat i72). (* ABS(inv_b_hat, [0.0078125, 0.25]) *)
Lemma t91 : p33 -> p137 -> p128.
 intros h0 h1.
 refine (mul_aa _a _inv_b_hat i1 i72 i66 h0 h1 _) ; finalize.
Qed.
Lemma l117 : p105 -> s1 -> p128 (* ABS(a * inv_b_hat, [0, 4.61169e+18]) *).
 intros h0 h1.
 assert (h2 := l34 h1).
 assert (h3 := l118 h0 h1).
 apply t91. exact h2. refine (abs_subset _inv_b_hat i67 i72 h3 _) ; finalize.
Qed.
Lemma t92 : p128 -> p127.
 intros h0.
 refine (float_absolute_wide_ne _ _ r7 i66 i65 h0 _) ; finalize.
Qed.
Lemma l116 : p105 -> s1 -> p127 (* BND(quotient_hat0 - a * inv_b_hat, [-256, 256]) *).
 intros h0 h1.
 assert (h2 := l117 h0 h1).
 apply t92. exact h2.
Qed.
Definition f112 := Float2 (-214748353) (11).
Definition f113 := Float2 (230584314665265563) (-19).
Definition i73 := makepairF f112 f113.
Notation p138 := (BND r23 i73). (* BND(a * (inv_b_hat - 1 / b), [-4.39805e+11, 4.39805e+11]) *)
Definition f114 := Float2 (-2791728641) (-57).
Definition f115 := Float2 (93674875685280359) (-82).
Definition i74 := makepairF f114 f115.
Notation p139 := (BND r18 i74). (* BND(inv_b_hat - 1 / b, [-1.93715e-08, 1.93715e-08]) *)
Notation p140 := (BND r56 i74). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-1.93715e-08, 1.93715e-08]) *)
Definition f116 := Float2 (-1) (-27).
Definition f117 := Float2 (1) (-27).
Definition i75 := makepairF f116 f117.
Notation p141 := (BND r57 i75). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-7.45058e-09, 7.45058e-09]) *)
Definition i76 := makepairF f110 f101.
Notation p142 := (ABS _inv_b_hat i76). (* ABS(inv_b_hat, [0.0078125, 0.21875]) *)
Lemma t93 : p142 -> p141.
 intros h0.
 refine (float_absolute_inv_ne _ _ _ i76 i75 h0 _) ; finalize.
Qed.
Lemma l127 : p105 -> s1 -> p141 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-7.45058e-09, 7.45058e-09]) *).
 intros h0 h1.
 assert (h2 := l118 h0 h1).
 apply t93. refine (abs_subset _inv_b_hat i67 i76 h2 _) ; finalize.
Qed.
Definition f118 := Float2 (-1717986817) (-57).
Definition f119 := Float2 (57646078666316391) (-82).
Definition i77 := makepairF f118 f119.
Notation p143 := (BND r58 i77). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-1.19209e-08, 1.19209e-08]) *)
Definition f120 := Float2 (922337203685477583) (-62).
Definition i78 := makepairF f110 f120.
Notation p144 := (BND r19 i78). (* BND(1 / b, [0.0078125, 0.2]) *)
Definition f121 := Float2 (1) (7).
Definition i79 := makepairF f6 f121.
Notation p145 := (BND _b i79). (* BND(b, [5, 128]) *)
Lemma t94 : p37 -> p145 -> p144.
 intros h0 h1.
 refine (div_pp r10 _b i17 i79 i78 h0 h1 _) ; finalize.
Qed.
Lemma l129 : p105 -> s1 -> p144 (* BND(1 / b, [0.0078125, 0.2]) *).
 intros h0 h1.
 assert (h2 := l39 h1).
 assert (h3 := l123 h0 h1).
 apply t94. exact h2. refine (subset _b i53 i79 h3 _) ; finalize.
Qed.
Definition f122 := Float2 (-2147483521) (-55).
Definition i80 := makepairF f122 f52.
Notation p146 := (REL r9 r19 i80). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Definition f123 := Float2 (57646075230342349) (-58).
Definition i81 := makepairF f110 f123.
Notation p147 := (BND r19 i81). (* BND(1 / b, [0.0078125, 0.2]) *)
Lemma t95 : p146 -> p147 -> p143.
 intros h0 h1.
 refine (error_of_rel_op r9 r19 i80 i81 i77 h0 h1 _) ; finalize.
Qed.
Lemma l128 : p105 -> s1 -> p143 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-1.19209e-08, 1.19209e-08]) *).
 intros h0 h1.
 assert (h2 := l47 h1).
 assert (h3 := l129 h0 h1).
 apply t95. refine (rel_subset r9 r19 i21 i80 h2 _) ; finalize. refine (subset r19 i78 i81 h3 _) ; finalize.
Qed.
Lemma t96 : p141 -> p143 -> p140.
 intros h0 h1.
 refine (add r57 r58 i75 i77 i74 h0 h1 _) ; finalize.
Qed.
Lemma l126 : p105 -> s1 -> p140 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-1.93715e-08, 1.93715e-08]) *).
 intros h0 h1.
 assert (h2 := l127 h0 h1).
 assert (h3 := l128 h0 h1).
 apply t96. exact h2. exact h3.
Qed.
Lemma t97 : p140 -> p139.
 intros h0.
 refine (sub_xals _ _ _ i74 h0) ; finalize.
Qed.
Lemma l125 : p105 -> s1 -> p139 (* BND(inv_b_hat - 1 / b, [-1.93715e-08, 1.93715e-08]) *).
 intros h0 h1.
 assert (h2 := l126 h0 h1).
 apply t97. exact h2.
Qed.
Definition f124 := Float2 (-214748353) (-53).
Definition f125 := Float2 (230584314665265563) (-83).
Definition i82 := makepairF f124 f125.
Notation p148 := (BND r18 i82). (* BND(inv_b_hat - 1 / b, [-2.38419e-08, 2.38419e-08]) *)
Lemma t98 : p1 -> p148 -> p138.
 intros h0 h1.
 refine (mul_po _a r18 i1 i82 i73 h0 h1 _) ; finalize.
Qed.
Lemma l124 : p105 -> s1 -> p138 (* BND(a * (inv_b_hat - 1 / b), [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l35 h1).
 assert (h3 := l125 h0 h1).
 apply t98. exact h2. refine (subset r18 i74 i82 h3 _) ; finalize.
Qed.
Lemma t99 : p127 -> p138 -> p126.
 intros h0 h1.
 refine (add r22 r23 i65 i73 i64 h0 h1 _) ; finalize.
Qed.
Lemma l115 : p105 -> s1 -> p126 (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l116 h0 h1).
 assert (h3 := l124 h0 h1).
 apply t99. exact h2. exact h3.
Qed.
Notation p149 := (BND r71 i12). (* BND(quotient_hat0 - a / b - (quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b)), [0, 0]) *)
Lemma t100 : p49 -> p149.
 intros h0.
 refine (sub_of_eql r20 r21 i12 h0 _) ; finalize.
Qed.
Lemma l130 : s1 -> p149 (* BND(quotient_hat0 - a / b - (quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b)), [0, 0]) *).
 intros h0.
 assert (h1 := l51 h0).
 apply t100. exact h1.
Qed.
Lemma t101 : p126 -> p149 -> p125.
 intros h0 h1.
 refine (add r21 r71 i64 i12 i64 h0 h1 _) ; finalize.
Qed.
Lemma l114 : p105 -> s1 -> p125 (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b) + (quotient_hat0 - a / b - (quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b))), [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l115 h0 h1).
 assert (h3 := l130 h1).
 apply t101. exact h2. exact h3.
Qed.
Lemma t102 : p125 -> p124.
 intros h0.
 refine (val_xabs _ r20 i64 h0) ; finalize.
Qed.
Lemma l113 : p105 -> s1 -> p124 (* BND(quotient_hat0 - a / b, [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l114 h0 h1).
 apply t102. exact h2.
Qed.
Lemma t103 : p28 -> p124 -> p123.
 intros h0 h1.
 refine (sub r55 r20 i12 i64 i63 h0 h1 _) ; finalize.
Qed.
Lemma l112 : p105 -> s1 -> p123 (* BND(a / b - a / b - (quotient_hat0 - a / b), [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l29 h1).
 assert (h3 := l113 h0 h1).
 apply t103. exact h2. exact h3.
Qed.
Lemma t104 : p123 -> p122.
 intros h0.
 refine (sub_xars _ _ _ i63 h0) ; finalize.
Qed.
Lemma l111 : p105 -> s1 -> p122 (* BND(a / b - quotient_hat0, [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l112 h0 h1).
 apply t104. exact h2.
Qed.
Definition i83 := makepairF f65 f10.
Notation p150 := (BND r53 i83). (* BND(q0 - quotient_hat0, [-1, 0.5]) *)
Lemma t105 : p122 -> p150 -> p121.
 intros h0 h1.
 refine (sub r52 r53 i63 i83 i62 h0 h1 _) ; finalize.
Qed.
Lemma l110 : p105 -> s1 -> p121 (* BND(a / b - quotient_hat0 - (q0 - quotient_hat0), [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l111 h0 h1).
 assert (h3 := l53 h1).
 apply t105. exact h2. refine (subset r53 i5 i83 h3 _) ; finalize.
Qed.
Lemma t106 : p121 -> p120.
 intros h0.
 refine (sub_xars _ _ _ i62 h0) ; finalize.
Qed.
Lemma l109 : p105 -> s1 -> p120 (* BND(a / b - q0, [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l110 h0 h1).
 apply t106. exact h2.
Qed.
Lemma t107 : p10 -> p120 -> p119.
 intros h0 h1.
 refine (div_fir r13 _b i62 h0 h1) ; finalize.
Qed.
Lemma l108 : p105 -> s1 -> p119 (* BND((a / b - q0) * b / b, [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l109 h0 h1).
 apply t107. exact h2. exact h3.
Qed.
Lemma t108 : p20 -> p119 -> p118.
 intros h0 h1.
 refine (add r48 r50 i12 i62 i62 h0 h1 _) ; finalize.
Qed.
Lemma l107 : p105 -> s1 -> p118 (* BND((r0 - (a / b - q0) * b) / b + (a / b - q0) * b / b, [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l21 h1).
 assert (h3 := l108 h0 h1).
 apply t108. exact h2. exact h3.
Qed.
Lemma t109 : p10 -> p118 -> p117.
 intros h0 h1.
 refine (div_xals _ _ _b i62 h0 h1) ; finalize.
Qed.
Lemma l106 : p105 -> s1 -> p117 (* BND(r0 / b, [-4.39805e+11, 4.39805e+11]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l107 h0 h1).
 apply t109. exact h2. exact h3.
Qed.
Definition f126 := Float2 (65) (0).
Definition i84 := makepairF f3 f126.
Notation p151 := (BND _b i84). (* BND(b, [1, 65]) *)
Lemma t110 : p117 -> p151 -> p116.
 intros h0 h1.
 refine (mul_op r37 _b i62 i84 i61 h0 h1 _) ; finalize.
Qed.
Lemma l105 : p105 -> s1 -> p116 (* BND(r0 / b * b, [-2.85873e+13, 2.85873e+13]) *).
 intros h0 h1.
 assert (h2 := l106 h0 h1).
 assert (h3 := l123 h0 h1).
 apply t110. exact h2. refine (subset _b i53 i84 h3 _) ; finalize.
Qed.
Lemma t111 : p10 -> p116 -> p115.
 intros h0 h1.
 refine (div_xilu _r0 _ i61 h0 h1) ; finalize.
Qed.
Lemma l104 : p105 -> s1 -> p115 (* BND(r0, [-2.85873e+13, 2.85873e+13]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l105 h0 h1).
 apply t111. exact h2. exact h3.
Qed.
Definition f127 := Float2 (-1) (47).
Definition i85 := makepairF f127 f88.
Notation p152 := (BND _r0 i85). (* BND(r0, [-1.40737e+14, 1.40737e+14]) *)
Lemma t112 : p152 -> p114.
 intros h0.
 refine (abs_of_bnd_o _r0 i85 i60 h0 _) ; finalize.
Qed.
Lemma l103 : p105 -> s1 -> p114 (* ABS(r0, [0, 1.40737e+14]) *).
 intros h0 h1.
 assert (h2 := l104 h0 h1).
 apply t112. refine (subset _r0 i61 i85 h2 _) ; finalize.
Qed.
Notation p153 := (ABS _inv_b_hat1 i72). (* ABS(inv_b_hat1, [0.0078125, 0.25]) *)
Notation p154 := (BND _inv_b_hat1 i72). (* BND(inv_b_hat1, [0.0078125, 0.25]) *)
Notation p155 := (BND r35 i72). (* BND(alpha * inv_b_hat + inv_b_hat, [0.0078125, 0.25]) *)
Notation p156 := (BND r36 i28). (* BND(alpha * inv_b_hat, [-1.19209e-07, 1.19209e-07]) *)
Lemma t113 : p59 -> p35 -> p156.
 intros h0 h1.
 refine (mul_op _alpha _inv_b_hat i28 i16 i28 h0 h1 _) ; finalize.
Qed.
Lemma l134 : s1 -> p156 (* BND(alpha * inv_b_hat, [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l61 h0).
 assert (h2 := l37 h0).
 apply t113. exact h1. exact h2.
Qed.
Definition f128 := Float2 (-1) (-8).
Definition f129 := Float2 (1) (-5).
Definition i86 := makepairF f128 f129.
Notation p157 := (BND r36 i86). (* BND(alpha * inv_b_hat, [-0.00390625, 0.03125]) *)
Lemma t114 : p157 -> p130 -> p155.
 intros h0 h1.
 refine (add r36 _inv_b_hat i86 i67 i72 h0 h1 _) ; finalize.
Qed.
Lemma l133 : p105 -> s1 -> p155 (* BND(alpha * inv_b_hat + inv_b_hat, [0.0078125, 0.25]) *).
 intros h0 h1.
 assert (h2 := l134 h1).
 assert (h3 := l119 h0 h1).
 apply t114. refine (subset r36 i28 i86 h2 _) ; finalize. exact h3.
Qed.
Lemma t115 : p155 -> p154.
 intros h0.
 refine (float_round_ne _ _ r35 i72 i72 h0 _) ; finalize.
Qed.
Lemma l132 : p105 -> s1 -> p154 (* BND(inv_b_hat1, [0.0078125, 0.25]) *).
 intros h0 h1.
 assert (h2 := l133 h0 h1).
 apply t115. exact h2.
Qed.
Lemma t116 : p154 -> p153.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat1 i72 i72 h0 _) ; finalize.
Qed.
Lemma l131 : p105 -> s1 -> p153 (* ABS(inv_b_hat1, [0.0078125, 0.25]) *).
 intros h0 h1.
 assert (h2 := l132 h0 h1).
 apply t116. exact h2.
Qed.
Lemma t117 : p114 -> p153 -> p113.
 intros h0 h1.
 refine (mul_aa _r0 _inv_b_hat1 i60 i72 i59 h0 h1 _) ; finalize.
Qed.
Lemma l102 : p105 -> s1 -> p113 (* ABS(r0 * inv_b_hat1, [0, 3.51844e+13]) *).
 intros h0 h1.
 assert (h2 := l103 h0 h1).
 assert (h3 := l131 h0 h1).
 apply t117. exact h2. exact h3.
Qed.
Lemma t118 : p113 -> p112.
 intros h0.
 refine (float_absolute_wide_ne _ _ r33 i59 i58 h0 _) ; finalize.
Qed.
Lemma l101 : p105 -> s1 -> p112 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.00195312, 0.00195312]) *).
 intros h0 h1.
 assert (h2 := l102 h0 h1).
 apply t118. exact h2.
Qed.
Definition f130 := Float2 (-2228429) (-25).
Definition f131 := Float2 (149547348757275) (-51).
Definition i87 := makepairF f130 f131.
Notation p158 := (BND r40 i87). (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.0664124, 0.0664124]) *)
Definition f132 := Float2 (-23559457832383469) (-103).
Definition f133 := Float2 (175531627) (-76).
Definition i88 := makepairF f132 f133.
Notation p159 := (BND r41 i88). (* BND(inv_b_hat1 - 1 / b, [-2.32314e-15, 2.32314e-15]) *)
Notation p160 := (BND r65 i88). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-2.32314e-15, 2.32314e-15]) *)
Definition f134 := Float2 (-1) (-56).
Definition f135 := Float2 (1) (-56).
Definition i89 := makepairF f134 f135.
Notation p161 := (BND r66 i89). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.38778e-17, 1.38778e-17]) *)
Notation p162 := (ABS r35 i72). (* ABS(alpha * inv_b_hat + inv_b_hat, [0.0078125, 0.25]) *)
Definition f136 := Float2 (1) (-8).
Definition i90 := makepairF f1 f136.
Notation p163 := (ABS r36 i90). (* ABS(alpha * inv_b_hat, [0, 0.00390625]) *)
Lemma t119 : p163 -> p129 -> p162.
 intros h0 h1.
 refine (add_aa_n r36 _inv_b_hat i90 i67 i72 h0 h1 _) ; finalize.
Qed.
Lemma l139 : p105 -> s1 -> p162 (* ABS(alpha * inv_b_hat + inv_b_hat, [0.0078125, 0.25]) *).
 intros h0 h1.
 assert (h2 := l89 h1).
 assert (h3 := l118 h0 h1).
 apply t119. refine (abs_subset r36 i48 i90 h2 _) ; finalize. exact h3.
Qed.
Lemma t120 : p162 -> p161.
 intros h0.
 refine (float_absolute_wide_ne _ _ r35 i72 i89 h0 _) ; finalize.
Qed.
Lemma l138 : p105 -> s1 -> p161 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.38778e-17, 1.38778e-17]) *).
 intros h0 h1.
 assert (h2 := l139 h0 h1).
 apply t120. exact h2.
Qed.
Definition f137 := Float2 (-23418720344028141) (-103).
Definition f138 := Float2 (174483051) (-76).
Definition i91 := makepairF f137 f138.
Notation p164 := (BND r67 i91). (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-2.30926e-15, 2.30926e-15]) *)
Notation p165 := (BND r68 i91). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-2.30926e-15, 2.30926e-15]) *)
Definition f139 := Float2 (-187349762537476763) (-106).
Definition f140 := Float2 (348966101) (-77).
Definition i92 := makepairF f139 f140.
Notation p166 := (BND r69 i92). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-2.30926e-15, 2.30926e-15]) *)
Notation p167 := (BND r60 i92). (* BND(alpha * inv_b_hat - alpha * (1 / b), [-2.30926e-15, 2.30926e-15]) *)
Notation p168 := (BND r61 i92). (* BND(alpha * (inv_b_hat - 1 / b), [-2.30926e-15, 2.30926e-15]) *)
Lemma t121 : p76 -> p139 -> p168.
 intros h0 h1.
 refine (mul_oo _alpha r18 i34 i74 i92 h0 h1 _) ; finalize.
Qed.
Lemma l144 : p105 -> s1 -> p168 (* BND(alpha * (inv_b_hat - 1 / b), [-2.30926e-15, 2.30926e-15]) *).
 intros h0 h1.
 assert (h2 := l61 h1).
 assert (h3 := l125 h0 h1).
 apply t121. refine (subset _alpha i28 i34 h2 _) ; finalize. exact h3.
Qed.
Lemma t122 : p168 -> p167.
 intros h0.
 refine (mul_fils _ _ _ i92 h0) ; finalize.
Qed.
Lemma l143 : p105 -> s1 -> p167 (* BND(alpha * inv_b_hat - alpha * (1 / b), [-2.30926e-15, 2.30926e-15]) *).
 intros h0 h1.
 assert (h2 := l144 h0 h1).
 apply t122. exact h2.
Qed.
Lemma t123 : p167 -> p166.
 intros h0.
 refine (add_firs _ _ _ i92 h0) ; finalize.
Qed.
Lemma l142 : p105 -> s1 -> p166 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-2.30926e-15, 2.30926e-15]) *).
 intros h0 h1.
 assert (h2 := l143 h0 h1).
 apply t123. exact h2.
Qed.
Definition f141 := Float2 (-214748365) (-106).
Definition f142 := Float2 (1) (-77).
Definition i93 := makepairF f141 f142.
Notation p169 := (BND r24 i93). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-2.64698e-24, 6.61744e-24]) *)
Notation p170 := (REL r25 r19 i36). (* REL(alpha * (1 / b) + inv_b_hat, 1 / b, [-1.32349e-23, 1.32349e-23]) *)
Notation r74 := ((r24 / r19)%R).
Notation p171 := (BND r74 i36). (* BND((alpha * (1 / b) + inv_b_hat - 1 / b) / (1 / b), [-1.32349e-23, 1.32349e-23]) *)
Notation r77 := ((r24 - r29)%R).
Notation r76 := ((r77 / r19)%R).
Notation r78 := ((r29 / r19)%R).
Notation r75 := ((r76 + r78)%R).
Notation p172 := (BND r75 i36). (* BND((alpha * (1 / b) + inv_b_hat - 1 / b - (alpha - (1 - b * inv_b_hat)) * (1 / b)) / (1 / b) + (alpha - (1 - b * inv_b_hat)) * (1 / b) / (1 / b), [-1.32349e-23, 1.32349e-23]) *)
Notation p173 := (BND r76 i12). (* BND((alpha * (1 / b) + inv_b_hat - 1 / b - (alpha - (1 - b * inv_b_hat)) * (1 / b)) / (1 / b), [0, 0]) *)
Notation p174 := (BND r77 i12). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b - (alpha - (1 - b * inv_b_hat)) * (1 / b), [0, 0]) *)
Lemma t124 : p85 -> p174.
 intros h0.
 refine (sub_of_eql r24 r29 i12 h0 _) ; finalize.
Qed.
Lemma l150 : s1 -> p174 (* BND(alpha * (1 / b) + inv_b_hat - 1 / b - (alpha - (1 - b * inv_b_hat)) * (1 / b), [0, 0]) *).
 intros h0.
 assert (h1 := l82 h0).
 apply t124. exact h1.
Qed.
Lemma t125 : p174 -> p47 -> p173.
 intros h0 h1.
 refine (div_pp r77 r19 i12 i16 i12 h0 h1 _) ; finalize.
Qed.
Lemma l149 : s1 -> p173 (* BND((alpha * (1 / b) + inv_b_hat - 1 / b - (alpha - (1 - b * inv_b_hat)) * (1 / b)) / (1 / b), [0, 0]) *).
 intros h0.
 assert (h1 := l150 h0).
 assert (h2 := l49 h0).
 apply t125. exact h1. exact h2.
Qed.
Notation p175 := (BND r78 i36). (* BND((alpha - (1 - b * inv_b_hat)) * (1 / b) / (1 / b), [-1.32349e-23, 1.32349e-23]) *)
Lemma t126 : p65 -> p81 -> p175.
 intros h0 h1.
 refine (div_fir r30 r19 i36 h0 h1) ; finalize.
Qed.
Lemma l151 : s1 -> p175 (* BND((alpha - (1 - b * inv_b_hat)) * (1 / b) / (1 / b), [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l67 h0).
 assert (h2 := l79 h0).
 apply t126. exact h1. exact h2.
Qed.
Lemma t127 : p173 -> p175 -> p172.
 intros h0 h1.
 refine (add r76 r78 i12 i36 i36 h0 h1 _) ; finalize.
Qed.
Lemma l148 : s1 -> p172 (* BND((alpha * (1 / b) + inv_b_hat - 1 / b - (alpha - (1 - b * inv_b_hat)) * (1 / b)) / (1 / b) + (alpha - (1 - b * inv_b_hat)) * (1 / b) / (1 / b), [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l149 h0).
 assert (h2 := l151 h0).
 apply t127. exact h1. exact h2.
Qed.
Lemma t128 : p65 -> p172 -> p171.
 intros h0 h1.
 refine (div_xals _ _ r19 i36 h0 h1) ; finalize.
Qed.
Lemma l147 : s1 -> p171 (* BND((alpha * (1 / b) + inv_b_hat - 1 / b) / (1 / b), [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l67 h0).
 assert (h2 := l148 h0).
 apply t128. exact h1. exact h2.
Qed.
Lemma t129 : p65 -> p171 -> p170.
 intros h0 h1.
 refine (rel_of_nzr_bnd r25 r19 i36 h0 h1) ; finalize.
Qed.
Lemma l146 : s1 -> p170 (* REL(alpha * (1 / b) + inv_b_hat, 1 / b, [-1.32349e-23, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l67 h0).
 assert (h2 := l147 h0).
 apply t129. exact h1. exact h2.
Qed.
Definition f143 := Float2 (1) (-75).
Definition i94 := makepairF f56 f143.
Notation p176 := (REL r25 r19 i94). (* REL(alpha * (1 / b) + inv_b_hat, 1 / b, [-1.32349e-23, 2.64698e-23]) *)
Definition f144 := Float2 (214748365) (-30).
Definition i95 := makepairF f110 f144.
Notation p177 := (BND r19 i95). (* BND(1 / b, [0.0078125, 0.2]) *)
Lemma t130 : p176 -> p177 -> p169.
 intros h0 h1.
 refine (error_of_rel_op r25 r19 i94 i95 i93 h0 h1 _) ; finalize.
Qed.
Lemma l145 : p105 -> s1 -> p169 (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-2.64698e-24, 6.61744e-24]) *).
 intros h0 h1.
 assert (h2 := l146 h1).
 assert (h3 := l129 h0 h1).
 apply t130. refine (rel_subset r25 r19 i36 i94 h2 _) ; finalize. refine (subset r19 i78 i95 h3 _) ; finalize.
Qed.
Lemma t131 : p166 -> p169 -> p165.
 intros h0 h1.
 refine (add r69 r24 i92 i93 i91 h0 h1 _) ; finalize.
Qed.
Lemma l141 : p105 -> s1 -> p165 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-2.30926e-15, 2.30926e-15]) *).
 intros h0 h1.
 assert (h2 := l142 h0 h1).
 assert (h3 := l145 h0 h1).
 apply t131. exact h2. exact h3.
Qed.
Lemma t132 : p165 -> p164.
 intros h0.
 refine (sub_xals _ _ _ i91 h0) ; finalize.
Qed.
Lemma l140 : p105 -> s1 -> p164 (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-2.30926e-15, 2.30926e-15]) *).
 intros h0 h1.
 assert (h2 := l141 h0 h1).
 apply t132. exact h2.
Qed.
Lemma t133 : p161 -> p164 -> p160.
 intros h0 h1.
 refine (add r66 r67 i89 i91 i88 h0 h1 _) ; finalize.
Qed.
Lemma l137 : p105 -> s1 -> p160 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-2.32314e-15, 2.32314e-15]) *).
 intros h0 h1.
 assert (h2 := l138 h0 h1).
 assert (h3 := l140 h0 h1).
 apply t133. exact h2. exact h3.
Qed.
Lemma t134 : p160 -> p159.
 intros h0.
 refine (sub_xals _ _ _ i88 h0) ; finalize.
Qed.
Lemma l136 : p105 -> s1 -> p159 (* BND(inv_b_hat1 - 1 / b, [-2.32314e-15, 2.32314e-15]) *).
 intros h0 h1.
 assert (h2 := l137 h0 h1).
 apply t134. exact h2.
Qed.
Lemma t135 : p115 -> p159 -> p158.
 intros h0 h1.
 refine (mul_oo _r0 r41 i61 i88 i87 h0 h1 _) ; finalize.
Qed.
Lemma l135 : p105 -> s1 -> p158 (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.0664124, 0.0664124]) *).
 intros h0 h1.
 assert (h2 := l104 h0 h1).
 assert (h3 := l136 h0 h1).
 apply t135. exact h2. exact h3.
Qed.
Lemma t136 : p112 -> p158 -> p111.
 intros h0 h1.
 refine (add r39 r40 i58 i87 i57 h0 h1 _) ; finalize.
Qed.
Lemma l100 : p105 -> s1 -> p111 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0683655, 0.0683655]) *).
 intros h0 h1.
 assert (h2 := l101 h0 h1).
 assert (h3 := l135 h0 h1).
 apply t136. exact h2. exact h3.
Qed.
Lemma t137 : p111 -> p110.
 intros h0.
 refine (abs_of_bnd_o r38 i57 i56 h0 _) ; finalize.
Qed.
Lemma l99 : p105 -> s1 -> p110 (* ABS(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [0, 0.0683655]) *).
 intros h0 h1.
 assert (h2 := l100 h0 h1).
 apply t137. exact h2.
Qed.
Lemma t138 : p9 -> p110 -> p109.
 intros h0 h1.
 refine (abs_rewrite r31 r38 i56 h0 h1) ; finalize.
Qed.
Lemma l98 : p105 -> s1 -> p109 (* ABS(quotient_hat1 - r0 / b, [0, 0.0683655]) *).
 intros h0 h1.
 assert (h2 := l8 h1).
 assert (h3 := l99 h0 h1).
 apply t138. exact h2. exact h3.
Qed.
Lemma t139 : p109 -> p108.
 intros h0.
 refine (bnd_of_abs r31 i56 i55 h0 _) ; finalize.
Qed.
Lemma l97 : p105 -> s1 -> p108 (* BND(quotient_hat1 - r0 / b, [-0.0683655, 0.0683655]) *).
 intros h0 h1.
 assert (h2 := l98 h0 h1).
 apply t139. exact h2.
Qed.
Lemma t140 : p7 -> p108 -> p107.
 intros h0 h1.
 refine (add r45 r31 i5 i55 i54 h0 h1 _) ; finalize.
Qed.
Lemma l96 : p105 -> s1 -> p107 (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.568365, 0.568365]) *).
 intros h0 h1.
 assert (h2 := l6 h1).
 assert (h3 := l97 h0 h1).
 apply t140. exact h2. exact h3.
Qed.
Lemma t141 : p107 -> p106.
 intros h0.
 refine (sub_xals _ _ _ i54 h0) ; finalize.
Qed.
Lemma l95 : p105 -> s1 -> p106 (* BND(q1 - r0 / b, [-0.568365, 0.568365]) *).
 intros h0 h1.
 assert (h2 := l96 h0 h1).
 apply t141. exact h2.
Qed.
Lemma l94 : p105 -> s1 -> False.
 intros h0 h1.
 assert (h2 := l3 h1).
 assert (h3 := l95 h0 h1).
 refine (simplify (Tatom false (Abnd 0%nat i3)) Tfalse (Abnd 0%nat i54) (List.cons r42 List.nil) h3 h2 _) ; finalize.
Qed.
Definition f145 := Float2 (576495936675512319) (-45).
Definition i96 := makepairF f79 f145.
Notation p178 := (BND _b i96). (* BND(b, [65, 16385]) *)
Definition f146 := Float2 (-656435539616191235) (-60).
Definition f147 := Float2 (656435539616191235) (-60).
Definition i97 := makepairF f146 f147.
Notation p179 := (BND r42 i97). (* BND(q1 - r0 / b, [-0.569367, 0.569367]) *)
Notation p180 := (BND r44 i97). (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.569367, 0.569367]) *)
Definition f148 := Float2 (-79974787312767747) (-60).
Definition f149 := Float2 (79974787312767747) (-60).
Definition i98 := makepairF f148 f149.
Notation p181 := (BND r31 i98). (* BND(quotient_hat1 - r0 / b, [-0.0693671, 0.0693671]) *)
Notation p182 := (BND r38 i98). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0693671, 0.0693671]) *)
Definition f150 := Float2 (-1) (-13).
Definition f151 := Float2 (1) (-13).
Definition i99 := makepairF f150 f151.
Notation p183 := (BND r39 i99). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.00012207, 0.00012207]) *)
Definition f152 := Float2 (1) (41).
Definition i100 := makepairF f1 f152.
Notation p184 := (ABS r33 i100). (* ABS(r0 * inv_b_hat1, [0, 2.19902e+12]) *)
Definition f153 := Float2 (-1) (41).
Definition i101 := makepairF f153 f152.
Notation p185 := (BND r33 i101). (* BND(r0 * inv_b_hat1, [-2.19902e+12, 2.19902e+12]) *)
Notation r80 := ((_q1 - r33)%R).
Notation r79 := ((_q1 - r80)%R).
Notation p186 := (BND r79 i101). (* BND(q1 - (q1 - r0 * inv_b_hat1), [-2.19902e+12, 2.19902e+12]) *)
Definition f154 := Float2 (-7) (38).
Definition f155 := Float2 (3) (39).
Definition i102 := makepairF f154 f155.
Notation p187 := (BND _q1 i102). (* BND(q1, [-1.92415e+12, 1.64927e+12]) *)
Notation p188 := (BND _quotient_hat1 i102). (* BND(quotient_hat1, [-1.92415e+12, 1.64927e+12]) *)
Notation r81 := ((r37 + r31)%R).
Notation p189 := (BND r81 i102). (* BND(r0 / b + (quotient_hat1 - r0 / b), [-1.92415e+12, 1.64927e+12]) *)
Definition f156 := Float2 (-1) (9).
Definition f157 := Float2 (1) (9).
Definition i103 := makepairF f156 f157.
Notation p190 := (BND r31 i103). (* BND(quotient_hat1 - r0 / b, [-512, 512]) *)
Notation r83 := ((r31 - r38)%R).
Notation r82 := ((r38 + r83)%R).
Notation p191 := (BND r82 i103). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b) + (quotient_hat1 - r0 / b - (quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b))), [-512, 512]) *)
Notation p192 := (BND r38 i103). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-512, 512]) *)
Definition i104 := makepairF f1 f157.
Notation p193 := (ABS r38 i104). (* ABS(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [0, 512]) *)
Notation p194 := (ABS r39 i47). (* ABS(quotient_hat1 - r0 * inv_b_hat1, [0, 2]) *)
Definition f158 := Float2 (-1) (1).
Definition i105 := makepairF f158 f73.
Notation p195 := (BND r39 i105). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-2, 2]) *)
Definition f159 := Float2 (1) (49).
Definition i106 := makepairF f1 f159.
Notation p196 := (ABS r33 i106). (* ABS(r0 * inv_b_hat1, [0, 5.6295e+14]) *)
Definition f160 := Float2 (213969809281753447) (-9).
Definition i107 := makepairF f1 f160.
Notation p197 := (ABS _r0 i107). (* ABS(r0, [0, 4.1791e+14]) *)
Notation p198 := (ABS r12 i107). (* ABS((a / b - q0) * b, [0, 4.1791e+14]) *)
Definition f161 := Float2 (213956750398062159) (-23).
Definition i108 := makepairF f1 f161.
Notation p199 := (ABS r13 i108). (* ABS(a / b - q0, [0, 2.55056e+10]) *)
Definition f162 := Float2 (-213956750398062159) (-23).
Definition f163 := Float2 (12453921) (11).
Definition i109 := makepairF f162 f163.
Notation p200 := (BND r13 i109). (* BND(a / b - q0, [-2.55056e+10, 2.55056e+10]) *)
Notation r85 := ((_q0 - r14)%R).
Notation r84 := ((r55 - r85)%R).
Notation p201 := (BND r84 i109). (* BND(a / b - a / b - (q0 - a / b), [-2.55056e+10, 2.55056e+10]) *)
Definition f164 := Float2 (-12453921) (11).
Definition i110 := makepairF f164 f161.
Notation p202 := (BND r85 i110). (* BND(q0 - a / b, [-2.55056e+10, 2.55056e+10]) *)
Notation r86 := ((r53 + r20)%R).
Notation p203 := (BND r86 i110). (* BND(q0 - quotient_hat0 + (quotient_hat0 - a / b), [-2.55056e+10, 2.55056e+10]) *)
Definition f165 := Float2 (-3188203775) (3).
Definition f166 := Float2 (213956750393867855) (-23).
Definition i111 := makepairF f165 f166.
Notation p204 := (BND r20 i111). (* BND(quotient_hat0 - a / b, [-2.55056e+10, 2.55056e+10]) *)
Notation p205 := (BND r21 i111). (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-2.55056e+10, 2.55056e+10]) *)
Definition f167 := Float2 (-1) (4).
Definition f168 := Float2 (1) (4).
Definition i112 := makepairF f167 f168.
Notation p206 := (BND r22 i112). (* BND(quotient_hat0 - a * inv_b_hat, [-16, 16]) *)
Definition f169 := Float2 (1) (58).
Definition i113 := makepairF f1 f169.
Notation p207 := (ABS r7 i113). (* ABS(a * inv_b_hat, [0, 2.8823e+17]) *)
Definition f170 := Float2 (3) (-16).
Definition f171 := Float2 (127) (-13).
Definition i114 := makepairF f170 f171.
Notation p208 := (ABS _inv_b_hat i114). (* ABS(inv_b_hat, [4.57764e-05, 0.0155029]) *)
Definition f172 := Float2 (268419041) (-42).
Definition f173 := Float2 (567592193007167333) (-65).
Definition i115 := makepairF f172 f173.
Notation p209 := (BND _inv_b_hat i115). (* BND(inv_b_hat, [6.10314e-05, 0.0153846]) *)
Notation p210 := (BND r72 i115). (* BND(b * inv_b_hat / b, [6.10314e-05, 0.0153846]) *)
Lemma l185 : p178 -> s1 -> p178 (* BND(b, [65, 16385]) *).
 intros h0 h1.
 assert (h2 := h0).
 exact (h2).
Qed.
Definition f174 := Float2 (16385) (0).
Definition i116 := makepairF f79 f174.
Notation p211 := (BND _b i116). (* BND(b, [65, 16385]) *)
Lemma t142 : p132 -> p211 -> p210.
 intros h0 h1.
 refine (div_pp r16 _b i68 i116 i115 h0 h1 _) ; finalize.
Qed.
Lemma l184 : p178 -> s1 -> p210 (* BND(b * inv_b_hat / b, [6.10314e-05, 0.0153846]) *).
 intros h0 h1.
 assert (h2 := l121 h1).
 assert (h3 := l185 h0 h1).
 apply t142. exact h2. refine (subset _b i96 i116 h3 _) ; finalize.
Qed.
Lemma t143 : p10 -> p210 -> p209.
 intros h0 h1.
 refine (mul_xiru _ _inv_b_hat i115 h0 h1) ; finalize.
Qed.
Lemma l183 : p178 -> s1 -> p209 (* BND(inv_b_hat, [6.10314e-05, 0.0153846]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l184 h0 h1).
 apply t143. exact h2. exact h3.
Qed.
Notation p212 := (BND _inv_b_hat i114). (* BND(inv_b_hat, [4.57764e-05, 0.0155029]) *)
Lemma t144 : p212 -> p208.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat i114 i114 h0 _) ; finalize.
Qed.
Lemma l182 : p178 -> s1 -> p208 (* ABS(inv_b_hat, [4.57764e-05, 0.0155029]) *).
 intros h0 h1.
 assert (h2 := l183 h0 h1).
 apply t144. refine (subset _inv_b_hat i115 i114 h2 _) ; finalize.
Qed.
Definition f175 := Float2 (1) (-15).
Definition f176 := Float2 (1) (-6).
Definition i117 := makepairF f175 f176.
Notation p213 := (ABS _inv_b_hat i117). (* ABS(inv_b_hat, [3.05176e-05, 0.015625]) *)
Lemma t145 : p33 -> p213 -> p207.
 intros h0 h1.
 refine (mul_aa _a _inv_b_hat i1 i117 i113 h0 h1 _) ; finalize.
Qed.
Lemma l181 : p178 -> s1 -> p207 (* ABS(a * inv_b_hat, [0, 2.8823e+17]) *).
 intros h0 h1.
 assert (h2 := l34 h1).
 assert (h3 := l182 h0 h1).
 apply t145. exact h2. refine (abs_subset _inv_b_hat i114 i117 h3 _) ; finalize.
Qed.
Lemma t146 : p207 -> p206.
 intros h0.
 refine (float_absolute_wide_ne _ _ r7 i113 i112 h0 _) ; finalize.
Qed.
Lemma l180 : p178 -> s1 -> p206 (* BND(quotient_hat0 - a * inv_b_hat, [-16, 16]) *).
 intros h0 h1.
 assert (h2 := l181 h0 h1).
 apply t146. exact h2.
Qed.
Definition f177 := Float2 (-3188203773) (3).
Definition f178 := Float2 (213956750259650127) (-23).
Definition i118 := makepairF f177 f178.
Notation p214 := (BND r23 i118). (* BND(a * (inv_b_hat - 1 / b), [-2.55056e+10, 2.55056e+10]) *)
Definition f179 := Float2 (-3188203773) (-61).
Definition f180 := Float2 (213956750259650127) (-87).
Definition i119 := makepairF f179 f180.
Notation p215 := (BND r18 i119). (* BND(inv_b_hat - 1 / b, [-1.38266e-09, 1.38266e-09]) *)
Notation r88 := ((_inv_b_hat - r25)%R).
Notation r87 := ((r88 + r24)%R).
Notation p216 := (BND r87 i119). (* BND(inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.38266e-09, 1.38266e-09]) *)
Definition f181 := Float2 (-6376407545) (-62).
Definition f182 := Float2 (213956750259648079) (-87).
Definition i120 := makepairF f181 f182.
Notation p217 := (BND r88 i120). (* BND(inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-1.38266e-09, 1.38266e-09]) *)
Notation r90 := ((r9 - r25)%R).
Notation r89 := ((r57 + r90)%R).
Notation p218 := (BND r89 i120). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - (alpha * (1 / b) + inv_b_hat)), [-1.38266e-09, 1.38266e-09]) *)
Definition f183 := Float2 (-1) (-31).
Definition f184 := Float2 (1) (-31).
Definition i121 := makepairF f183 f184.
Notation p219 := (BND r57 i121). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-4.65661e-10, 4.65661e-10]) *)
Definition i122 := makepairF f175 f171.
Notation p220 := (ABS _inv_b_hat i122). (* ABS(inv_b_hat, [3.05176e-05, 0.0155029]) *)
Lemma t147 : p220 -> p219.
 intros h0.
 refine (float_absolute_inv_ne _ _ _ i122 i121 h0 _) ; finalize.
Qed.
Lemma l191 : p178 -> s1 -> p219 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-4.65661e-10, 4.65661e-10]) *).
 intros h0 h1.
 assert (h2 := l182 h0 h1).
 apply t147. refine (abs_subset _inv_b_hat i114 i122 h2 _) ; finalize.
Qed.
Definition f185 := Float2 (-4228923897) (-62).
Definition f186 := Float2 (141899156221720143) (-87).
Definition i123 := makepairF f185 f186.
Notation p221 := (BND r90 i123). (* BND(1 / float<24,-149,ne>(b) - (alpha * (1 / b) + inv_b_hat), [-9.17002e-10, 9.17002e-10]) *)
Definition f187 := Float2 (-8589934081) (-57).
Definition f188 := Float2 (144115196666316323) (-81).
Definition i124 := makepairF f187 f188.
Notation p222 := (REL r9 r25 i124). (* REL(1 / float<24,-149,ne>(b), alpha * (1 / b) + inv_b_hat, [-5.96046e-08, 5.96046e-08]) *)
Definition f189 := Float2 (-1) (-58).
Definition f190 := Float2 (2101385) (-83).
Definition i125 := makepairF f189 f190.
Notation p223 := (REL r19 r25 i125). (* REL(1 / b, alpha * (1 / b) + inv_b_hat, [-3.46945e-18, 2.17278e-19]) *)
Notation p224 := (NZR r25). (* NZR(alpha * (1 / b) + inv_b_hat) *)
Definition i126 := makepairF f9 f3.
Notation p225 := (REL r25 r19 i126). (* REL(alpha * (1 / b) + inv_b_hat, 1 / b, [-0.5, 1]) *)
Lemma t148 : p65 -> p225 -> p224.
 intros h0 h1.
 refine (nzr_of_nzr_rel r25 r19 i126 h0 h1 _) ; finalize.
Qed.
Lemma l195 : s1 -> p224 (* NZR(alpha * (1 / b) + inv_b_hat) *).
 intros h0.
 assert (h1 := l67 h0).
 assert (h2 := l146 h0).
 apply t148. exact h1. refine (rel_subset r25 r19 i36 i126 h2 _) ; finalize.
Qed.
Notation r92 := ((r19 - r25)%R).
Notation r91 := ((r92 / r25)%R).
Notation p226 := (BND r91 i125). (* BND((1 / b - (alpha * (1 / b) + inv_b_hat)) / (alpha * (1 / b) + inv_b_hat), [-3.46945e-18, 2.17278e-19]) *)
Definition f191 := Float2 (-1) (-73).
Definition i127 := makepairF f191 f57.
Notation p227 := (BND r92 i127). (* BND(1 / b - (alpha * (1 / b) + inv_b_hat), [-1.05879e-22, 1.32349e-23]) *)
Notation r94 := ((r19 - r19)%R).
Notation r93 := ((r94 - r24)%R).
Notation p228 := (BND r93 i127). (* BND(1 / b - 1 / b - (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.05879e-22, 1.32349e-23]) *)
Notation p229 := (BND r94 i12). (* BND(1 / b - 1 / b, [0, 0]) *)
Lemma t149 : p229.
 refine (sub_refl _ i12 _) ; finalize.
Qed.
Lemma l199 : s1 -> p229 (* BND(1 / b - 1 / b, [0, 0]) *).
 intros h0.
 apply t149.
Qed.
Definition f192 := Float2 (1) (-73).
Definition i128 := makepairF f56 f192.
Notation p230 := (BND r24 i128). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-1.32349e-23, 1.05879e-22]) *)
Lemma t150 : p229 -> p230 -> p228.
 intros h0 h1.
 refine (sub r94 r24 i12 i128 i127 h0 h1 _) ; finalize.
Qed.
Lemma l198 : s1 -> p228 (* BND(1 / b - 1 / b - (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.05879e-22, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l199 h0).
 assert (h2 := l77 h0).
 apply t150. exact h1. refine (subset r24 i36 i128 h2 _) ; finalize.
Qed.
Lemma t151 : p228 -> p227.
 intros h0.
 refine (sub_xars _ _ _ i127 h0) ; finalize.
Qed.
Lemma l197 : s1 -> p227 (* BND(1 / b - (alpha * (1 / b) + inv_b_hat), [-1.05879e-22, 1.32349e-23]) *).
 intros h0.
 assert (h1 := l198 h0).
 apply t151. exact h1.
Qed.
Definition f193 := Float2 (8371711) (-37).
Definition f194 := Float2 (283798295526708147) (-64).
Definition i129 := makepairF f193 f194.
Notation p231 := (BND r25 i129). (* BND(alpha * (1 / b) + inv_b_hat, [6.09122e-05, 0.0153847]) *)
Definition f195 := Float2 (-524289) (-42).
Definition f196 := Float2 (4398046248961) (-65).
Definition i130 := makepairF f195 f196.
Notation p232 := (BND r26 i130). (* BND(alpha * (1 / b), [-1.1921e-07, 1.19209e-07]) *)
Notation p233 := (BND _alpha i130). (* BND(alpha, [-1.1921e-07, 1.19209e-07]) *)
Lemma t152 : p233 -> p47 -> p232.
 intros h0 h1.
 refine (mul_op _alpha r19 i130 i16 i130 h0 h1 _) ; finalize.
Qed.
Lemma l201 : s1 -> p232 (* BND(alpha * (1 / b), [-1.1921e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l61 h0).
 assert (h2 := l49 h0).
 apply t152. refine (subset _alpha i28 i130 h1 _) ; finalize. exact h2.
Qed.
Lemma t153 : p232 -> p209 -> p231.
 intros h0 h1.
 refine (add r26 _inv_b_hat i130 i115 i129 h0 h1 _) ; finalize.
Qed.
Lemma l200 : p178 -> s1 -> p231 (* BND(alpha * (1 / b) + inv_b_hat, [6.09122e-05, 0.0153847]) *).
 intros h0 h1.
 assert (h2 := l201 h1).
 assert (h3 := l183 h0 h1).
 apply t153. exact h2. exact h3.
Qed.
Definition i131 := makepairF f193 f3.
Notation p234 := (BND r25 i131). (* BND(alpha * (1 / b) + inv_b_hat, [6.09122e-05, 1]) *)
Lemma t154 : p227 -> p234 -> p226.
 intros h0 h1.
 refine (div_op r92 r25 i127 i131 i125 h0 h1 _) ; finalize.
Qed.
Lemma l196 : p178 -> s1 -> p226 (* BND((1 / b - (alpha * (1 / b) + inv_b_hat)) / (alpha * (1 / b) + inv_b_hat), [-3.46945e-18, 2.17278e-19]) *).
 intros h0 h1.
 assert (h2 := l197 h1).
 assert (h3 := l200 h0 h1).
 apply t154. exact h2. refine (subset r25 i129 i131 h3 _) ; finalize.
Qed.
Lemma t155 : p224 -> p226 -> p223.
 intros h0 h1.
 refine (rel_of_nzr_bnd r19 r25 i125 h0 h1) ; finalize.
Qed.
Lemma l194 : p178 -> s1 -> p223 (* REL(1 / b, alpha * (1 / b) + inv_b_hat, [-3.46945e-18, 2.17278e-19]) *).
 intros h0 h1.
 assert (h2 := l195 h1).
 assert (h3 := l196 h0 h1).
 apply t155. exact h2. exact h3.
Qed.
Definition f197 := Float2 (-17179868161) (-58).
Definition i132 := makepairF f197 f52.
Notation p235 := (REL r9 r19 i132). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Lemma t156 : p235 -> p223 -> p222.
 intros h0 h1.
 refine (compose r9 r19 r25 i132 i125 i124 h0 h1 _) ; finalize.
Qed.
Lemma l193 : p178 -> s1 -> p222 (* REL(1 / float<24,-149,ne>(b), alpha * (1 / b) + inv_b_hat, [-5.96046e-08, 5.96046e-08]) *).
 intros h0 h1.
 assert (h2 := l47 h1).
 assert (h3 := l194 h0 h1).
 apply t156. refine (rel_subset r9 r19 i21 i132 h2 _) ; finalize. exact h3.
Qed.
Definition i133 := makepairF f175 f194.
Notation p236 := (BND r25 i133). (* BND(alpha * (1 / b) + inv_b_hat, [3.05176e-05, 0.0153847]) *)
Lemma t157 : p222 -> p236 -> p221.
 intros h0 h1.
 refine (error_of_rel_op r9 r25 i124 i133 i123 h0 h1 _) ; finalize.
Qed.
Lemma l192 : p178 -> s1 -> p221 (* BND(1 / float<24,-149,ne>(b) - (alpha * (1 / b) + inv_b_hat), [-9.17002e-10, 9.17002e-10]) *).
 intros h0 h1.
 assert (h2 := l193 h0 h1).
 assert (h3 := l200 h0 h1).
 apply t157. exact h2. refine (subset r25 i129 i133 h3 _) ; finalize.
Qed.
Lemma t158 : p219 -> p221 -> p218.
 intros h0 h1.
 refine (add r57 r90 i121 i123 i120 h0 h1 _) ; finalize.
Qed.
Lemma l190 : p178 -> s1 -> p218 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - (alpha * (1 / b) + inv_b_hat)), [-1.38266e-09, 1.38266e-09]) *).
 intros h0 h1.
 assert (h2 := l191 h0 h1).
 assert (h3 := l192 h0 h1).
 apply t158. exact h2. exact h3.
Qed.
Lemma t159 : p218 -> p217.
 intros h0.
 refine (sub_xals _ _ _ i120 h0) ; finalize.
Qed.
Lemma l189 : p178 -> s1 -> p217 (* BND(inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-1.38266e-09, 1.38266e-09]) *).
 intros h0 h1.
 assert (h2 := l190 h0 h1).
 apply t159. exact h2.
Qed.
Definition f198 := Float2 (-1) (-62).
Definition i134 := makepairF f198 f57.
Notation p237 := (BND r24 i134). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-2.1684e-19, 1.32349e-23]) *)
Lemma t160 : p217 -> p237 -> p216.
 intros h0 h1.
 refine (add r88 r24 i120 i134 i119 h0 h1 _) ; finalize.
Qed.
Lemma l188 : p178 -> s1 -> p216 (* BND(inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.38266e-09, 1.38266e-09]) *).
 intros h0 h1.
 assert (h2 := l189 h0 h1).
 assert (h3 := l77 h1).
 apply t160. exact h2. refine (subset r24 i36 i134 h3 _) ; finalize.
Qed.
Lemma t161 : p216 -> p215.
 intros h0.
 refine (sub_xals _ _ _ i119 h0) ; finalize.
Qed.
Lemma l187 : p178 -> s1 -> p215 (* BND(inv_b_hat - 1 / b, [-1.38266e-09, 1.38266e-09]) *).
 intros h0 h1.
 assert (h2 := l188 h0 h1).
 apply t161. exact h2.
Qed.
Lemma t162 : p1 -> p215 -> p214.
 intros h0 h1.
 refine (mul_po _a r18 i1 i119 i118 h0 h1 _) ; finalize.
Qed.
Lemma l186 : p178 -> s1 -> p214 (* BND(a * (inv_b_hat - 1 / b), [-2.55056e+10, 2.55056e+10]) *).
 intros h0 h1.
 assert (h2 := l35 h1).
 assert (h3 := l187 h0 h1).
 apply t162. exact h2. exact h3.
Qed.
Lemma t163 : p206 -> p214 -> p205.
 intros h0 h1.
 refine (add r22 r23 i112 i118 i111 h0 h1 _) ; finalize.
Qed.
Lemma l179 : p178 -> s1 -> p205 (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-2.55056e+10, 2.55056e+10]) *).
 intros h0 h1.
 assert (h2 := l180 h0 h1).
 assert (h3 := l186 h0 h1).
 apply t163. exact h2. exact h3.
Qed.
Lemma t164 : p205 -> p48 -> p204.
 intros h0 h1.
 refine (bnd_of_bnd_rel_o r20 r21 i111 i12 i111 h0 h1 _) ; finalize.
Qed.
Lemma l178 : p178 -> s1 -> p204 (* BND(quotient_hat0 - a / b, [-2.55056e+10, 2.55056e+10]) *).
 intros h0 h1.
 assert (h2 := l179 h0 h1).
 assert (h3 := l50 h1).
 apply t164. exact h2. exact h3.
Qed.
Lemma t165 : p150 -> p204 -> p203.
 intros h0 h1.
 refine (add r53 r20 i83 i111 i110 h0 h1 _) ; finalize.
Qed.
Lemma l177 : p178 -> s1 -> p203 (* BND(q0 - quotient_hat0 + (quotient_hat0 - a / b), [-2.55056e+10, 2.55056e+10]) *).
 intros h0 h1.
 assert (h2 := l53 h1).
 assert (h3 := l178 h0 h1).
 apply t165. refine (subset r53 i5 i83 h2 _) ; finalize. exact h3.
Qed.
Lemma t166 : p203 -> p202.
 intros h0.
 refine (sub_xals _ _ _ i110 h0) ; finalize.
Qed.
Lemma l176 : p178 -> s1 -> p202 (* BND(q0 - a / b, [-2.55056e+10, 2.55056e+10]) *).
 intros h0 h1.
 assert (h2 := l177 h0 h1).
 apply t166. exact h2.
Qed.
Lemma t167 : p28 -> p202 -> p201.
 intros h0 h1.
 refine (sub r55 r85 i12 i110 i109 h0 h1 _) ; finalize.
Qed.
Lemma l175 : p178 -> s1 -> p201 (* BND(a / b - a / b - (q0 - a / b), [-2.55056e+10, 2.55056e+10]) *).
 intros h0 h1.
 assert (h2 := l29 h1).
 assert (h3 := l176 h0 h1).
 apply t167. exact h2. exact h3.
Qed.
Lemma t168 : p201 -> p200.
 intros h0.
 refine (sub_xars _ _ _ i109 h0) ; finalize.
Qed.
Lemma l174 : p178 -> s1 -> p200 (* BND(a / b - q0, [-2.55056e+10, 2.55056e+10]) *).
 intros h0 h1.
 assert (h2 := l175 h0 h1).
 apply t168. exact h2.
Qed.
Lemma t169 : p200 -> p199.
 intros h0.
 refine (abs_of_bnd_o r13 i109 i108 h0 _) ; finalize.
Qed.
Lemma l173 : p178 -> s1 -> p199 (* ABS(a / b - q0, [0, 2.55056e+10]) *).
 intros h0 h1.
 assert (h2 := l174 h0 h1).
 apply t169. exact h2.
Qed.
Definition i135 := makepairF f3 f145.
Notation p238 := (ABS _b i135). (* ABS(b, [1, 16385]) *)
Notation p239 := (BND _b i135). (* BND(b, [1, 16385]) *)
Lemma t170 : p239 -> p238.
 intros h0.
 refine (abs_of_bnd_p _b i135 i135 h0 _) ; finalize.
Qed.
Lemma l202 : p178 -> s1 -> p238 (* ABS(b, [1, 16385]) *).
 intros h0 h1.
 assert (h2 := l185 h0 h1).
 apply t170. refine (subset _b i96 i135 h2 _) ; finalize.
Qed.
Definition i136 := makepairF f3 f174.
Notation p240 := (ABS _b i136). (* ABS(b, [1, 16385]) *)
Lemma t171 : p199 -> p240 -> p198.
 intros h0 h1.
 refine (mul_aa r13 _b i108 i136 i107 h0 h1 _) ; finalize.
Qed.
Lemma l172 : p178 -> s1 -> p198 (* ABS((a / b - q0) * b, [0, 4.1791e+14]) *).
 intros h0 h1.
 assert (h2 := l173 h0 h1).
 assert (h3 := l202 h0 h1).
 apply t171. exact h2. refine (abs_subset _b i135 i136 h3 _) ; finalize.
Qed.
Lemma t172 : p22 -> p198 -> p197.
 intros h0 h1.
 refine (abs_rewrite _r0 r12 i107 h0 h1) ; finalize.
Qed.
Lemma l171 : p178 -> s1 -> p197 (* ABS(r0, [0, 4.1791e+14]) *).
 intros h0 h1.
 assert (h2 := l23 h1).
 assert (h3 := l172 h0 h1).
 apply t172. exact h2. exact h3.
Qed.
Notation p241 := (ABS _inv_b_hat1 i117). (* ABS(inv_b_hat1, [3.05176e-05, 0.015625]) *)
Notation p242 := (BND _inv_b_hat1 i117). (* BND(inv_b_hat1, [3.05176e-05, 0.015625]) *)
Notation p243 := (BND r35 i114). (* BND(alpha * inv_b_hat + inv_b_hat, [4.57764e-05, 0.0155029]) *)
Definition f199 := Float2 (-1) (-17).
Definition f200 := Float2 (1) (-14).
Definition i137 := makepairF f199 f200.
Notation p244 := (BND r36 i137). (* BND(alpha * inv_b_hat, [-7.62939e-06, 6.10352e-05]) *)
Definition f201 := Float2 (7) (-17).
Definition f202 := Float2 (253) (-14).
Definition i138 := makepairF f201 f202.
Notation p245 := (BND _inv_b_hat i138). (* BND(inv_b_hat, [5.34058e-05, 0.0154419]) *)
Lemma t173 : p244 -> p245 -> p243.
 intros h0 h1.
 refine (add r36 _inv_b_hat i137 i138 i114 h0 h1 _) ; finalize.
Qed.
Lemma l205 : p178 -> s1 -> p243 (* BND(alpha * inv_b_hat + inv_b_hat, [4.57764e-05, 0.0155029]) *).
 intros h0 h1.
 assert (h2 := l134 h1).
 assert (h3 := l183 h0 h1).
 apply t173. refine (subset r36 i28 i137 h2 _) ; finalize. refine (subset _inv_b_hat i115 i138 h3 _) ; finalize.
Qed.
Definition f203 := Float2 (-1) (-2).
Definition i139 := makepairF f203 f110.
Notation p246 := (REL _inv_b_hat1 r35 i139). (* REL(inv_b_hat1, alpha * inv_b_hat + inv_b_hat, [-0.25, 0.0078125]) *)
Notation p247 := (ABS r35 i117). (* ABS(alpha * inv_b_hat + inv_b_hat, [3.05176e-05, 0.015625]) *)
Lemma t174 : p97 -> p208 -> p247.
 intros h0 h1.
 refine (add_aa_n r36 _inv_b_hat i48 i114 i117 h0 h1 _) ; finalize.
Qed.
Lemma l207 : p178 -> s1 -> p247 (* ABS(alpha * inv_b_hat + inv_b_hat, [3.05176e-05, 0.015625]) *).
 intros h0 h1.
 assert (h2 := l89 h1).
 assert (h3 := l182 h0 h1).
 apply t174. exact h2. exact h3.
Qed.
Definition i140 := makepairF f175 f3.
Notation p248 := (ABS r35 i140). (* ABS(alpha * inv_b_hat + inv_b_hat, [3.05176e-05, 1]) *)
Lemma t175 : p248 -> p246.
 intros h0.
 refine (float_relative_ne _ _ r35 i140 i139 h0 _) ; finalize.
Qed.
Lemma l206 : p178 -> s1 -> p246 (* REL(inv_b_hat1, alpha * inv_b_hat + inv_b_hat, [-0.25, 0.0078125]) *).
 intros h0 h1.
 assert (h2 := l207 h0 h1).
 apply t175. refine (abs_subset r35 i117 i140 h2 _) ; finalize.
Qed.
Lemma t176 : p243 -> p246 -> p242.
 intros h0 h1.
 refine (bnd_of_bnd_rel_p _inv_b_hat1 r35 i114 i139 i117 h0 h1 _) ; finalize.
Qed.
Lemma l204 : p178 -> s1 -> p242 (* BND(inv_b_hat1, [3.05176e-05, 0.015625]) *).
 intros h0 h1.
 assert (h2 := l205 h0 h1).
 assert (h3 := l206 h0 h1).
 apply t176. exact h2. exact h3.
Qed.
Lemma t177 : p242 -> p241.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat1 i117 i117 h0 _) ; finalize.
Qed.
Lemma l203 : p178 -> s1 -> p241 (* ABS(inv_b_hat1, [3.05176e-05, 0.015625]) *).
 intros h0 h1.
 assert (h2 := l204 h0 h1).
 apply t177. exact h2.
Qed.
Definition f204 := Float2 (1) (55).
Definition i141 := makepairF f1 f204.
Notation p249 := (ABS _r0 i141). (* ABS(r0, [0, 3.60288e+16]) *)
Lemma t178 : p249 -> p241 -> p196.
 intros h0 h1.
 refine (mul_aa _r0 _inv_b_hat1 i141 i117 i106 h0 h1 _) ; finalize.
Qed.
Lemma l170 : p178 -> s1 -> p196 (* ABS(r0 * inv_b_hat1, [0, 5.6295e+14]) *).
 intros h0 h1.
 assert (h2 := l171 h0 h1).
 assert (h3 := l203 h0 h1).
 apply t178. refine (abs_subset _r0 i107 i141 h2 _) ; finalize. exact h3.
Qed.
Notation p250 := (ABS r33 i141). (* ABS(r0 * inv_b_hat1, [0, 3.60288e+16]) *)
Lemma t179 : p250 -> p195.
 intros h0.
 refine (float_absolute_wide_ne _ _ r33 i141 i105 h0 _) ; finalize.
Qed.
Lemma l169 : p178 -> s1 -> p195 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-2, 2]) *).
 intros h0 h1.
 assert (h2 := l170 h0 h1).
 apply t179. refine (abs_subset r33 i106 i141 h2 _) ; finalize.
Qed.
Lemma t180 : p195 -> p194.
 intros h0.
 refine (abs_of_bnd_o r39 i105 i47 h0 _) ; finalize.
Qed.
Lemma l168 : p178 -> s1 -> p194 (* ABS(quotient_hat1 - r0 * inv_b_hat1, [0, 2]) *).
 intros h0 h1.
 assert (h2 := l169 h0 h1).
 apply t180. exact h2.
Qed.
Definition f205 := Float2 (79834049824412419) (-60).
Definition i142 := makepairF f1 f205.
Notation p251 := (ABS r40 i142). (* ABS(r0 * (inv_b_hat1 - 1 / b), [0, 0.069245]) *)
Definition f206 := Float2 (26885332897951911) (-107).
Definition i143 := makepairF f1 f206.
Notation p252 := (ABS r41 i143). (* ABS(inv_b_hat1 - 1 / b, [0, 1.65694e-16]) *)
Definition f207 := Float2 (-26885332897951911) (-107).
Definition f208 := Float2 (6259729) (-75).
Definition i144 := makepairF f207 f208.
Notation p253 := (BND r41 i144). (* BND(inv_b_hat1 - 1 / b, [-1.65694e-16, 1.65694e-16]) *)
Notation r96 := ((_inv_b_hat1 - r25)%R).
Notation r95 := ((r96 + r24)%R).
Notation p254 := (BND r95 i144). (* BND(inv_b_hat1 - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.65694e-16, 1.65694e-16]) *)
Definition f209 := Float2 (-26885332864913701) (-107).
Definition f210 := Float2 (50077831) (-78).
Definition i145 := makepairF f209 f210.
Notation p255 := (BND r96 i145). (* BND(inv_b_hat1 - (alpha * (1 / b) + inv_b_hat), [-1.65694e-16, 1.65694e-16]) *)
Notation r97 := ((r66 + r69)%R).
Notation p256 := (BND r97 i145). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat)), [-1.65694e-16, 1.65694e-16]) *)
Definition f211 := Float2 (-1) (-60).
Definition f212 := Float2 (1) (-60).
Definition i146 := makepairF f211 f212.
Notation p257 := (BND r66 i146). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-8.67362e-19, 8.67362e-19]) *)
Lemma t181 : p247 -> p257.
 intros h0.
 refine (float_absolute_wide_ne _ _ r35 i117 i146 h0 _) ; finalize.
Qed.
Lemma l214 : p178 -> s1 -> p257 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-8.67362e-19, 8.67362e-19]) *).
 intros h0 h1.
 assert (h2 := l207 h0 h1).
 apply t181. exact h2.
Qed.
Definition f213 := Float2 (-26744595376558373) (-107).
Definition f214 := Float2 (49815687) (-78).
Definition i147 := makepairF f213 f214.
Notation p258 := (BND r69 i147). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-1.64826e-16, 1.64826e-16]) *)
Notation p259 := (BND r60 i147). (* BND(alpha * inv_b_hat - alpha * (1 / b), [-1.64826e-16, 1.64826e-16]) *)
Notation p260 := (BND r61 i147). (* BND(alpha * (inv_b_hat - 1 / b), [-1.64826e-16, 1.64826e-16]) *)
Definition f215 := Float2 (-12453921) (-53).
Definition i148 := makepairF f215 f180.
Notation p261 := (BND r18 i148). (* BND(inv_b_hat - 1 / b, [-1.38266e-09, 1.38266e-09]) *)
Lemma t182 : p76 -> p261 -> p260.
 intros h0 h1.
 refine (mul_oo _alpha r18 i34 i148 i147 h0 h1 _) ; finalize.
Qed.
Lemma l217 : p178 -> s1 -> p260 (* BND(alpha * (inv_b_hat - 1 / b), [-1.64826e-16, 1.64826e-16]) *).
 intros h0 h1.
 assert (h2 := l61 h1).
 assert (h3 := l187 h0 h1).
 apply t182. refine (subset _alpha i28 i34 h2 _) ; finalize. refine (subset r18 i119 i148 h3 _) ; finalize.
Qed.
Lemma t183 : p260 -> p259.
 intros h0.
 refine (mul_fils _ _ _ i147 h0) ; finalize.
Qed.
Lemma l216 : p178 -> s1 -> p259 (* BND(alpha * inv_b_hat - alpha * (1 / b), [-1.64826e-16, 1.64826e-16]) *).
 intros h0 h1.
 assert (h2 := l217 h0 h1).
 apply t183. exact h2.
Qed.
Lemma t184 : p259 -> p258.
 intros h0.
 refine (add_firs _ _ _ i147 h0) ; finalize.
Qed.
Lemma l215 : p178 -> s1 -> p258 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-1.64826e-16, 1.64826e-16]) *).
 intros h0 h1.
 assert (h2 := l216 h0 h1).
 apply t184. exact h2.
Qed.
Lemma t185 : p257 -> p258 -> p256.
 intros h0 h1.
 refine (add r66 r69 i146 i147 i145 h0 h1 _) ; finalize.
Qed.
Lemma l213 : p178 -> s1 -> p256 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat)), [-1.65694e-16, 1.65694e-16]) *).
 intros h0 h1.
 assert (h2 := l214 h0 h1).
 assert (h3 := l215 h0 h1).
 apply t185. exact h2. exact h3.
Qed.
Lemma t186 : p256 -> p255.
 intros h0.
 refine (sub_xals _ _ _ i145 h0) ; finalize.
Qed.
Lemma l212 : p178 -> s1 -> p255 (* BND(inv_b_hat1 - (alpha * (1 / b) + inv_b_hat), [-1.65694e-16, 1.65694e-16]) *).
 intros h0 h1.
 assert (h2 := l213 h0 h1).
 apply t186. exact h2.
Qed.
Definition f216 := Float2 (-16519105) (-106).
Definition f217 := Float2 (1) (-78).
Definition i149 := makepairF f216 f217.
Notation p262 := (BND r24 i149). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-2.03614e-25, 3.30872e-24]) *)
Definition f218 := Float2 (16519105) (-30).
Definition i150 := makepairF f175 f218.
Notation p263 := (BND r19 i150). (* BND(1 / b, [3.05176e-05, 0.0153846]) *)
Definition f219 := Float2 (1090519039) (-24).
Definition f220 := Float2 (1) (15).
Definition i151 := makepairF f219 f220.
Notation p264 := (BND _b i151). (* BND(b, [65, 32768]) *)
Lemma t187 : p37 -> p264 -> p263.
 intros h0 h1.
 refine (div_pp r10 _b i17 i151 i150 h0 h1 _) ; finalize.
Qed.
Lemma l219 : p178 -> s1 -> p263 (* BND(1 / b, [3.05176e-05, 0.0153846]) *).
 intros h0 h1.
 assert (h2 := l39 h1).
 assert (h3 := l185 h0 h1).
 apply t187. exact h2. refine (subset _b i96 i151 h3 _) ; finalize.
Qed.
Definition f221 := Float2 (1) (-72).
Definition i152 := makepairF f56 f221.
Notation p265 := (REL r25 r19 i152). (* REL(alpha * (1 / b) + inv_b_hat, 1 / b, [-1.32349e-23, 2.11758e-22]) *)
Lemma t188 : p265 -> p263 -> p262.
 intros h0 h1.
 refine (error_of_rel_op r25 r19 i152 i150 i149 h0 h1 _) ; finalize.
Qed.
Lemma l218 : p178 -> s1 -> p262 (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-2.03614e-25, 3.30872e-24]) *).
 intros h0 h1.
 assert (h2 := l146 h1).
 assert (h3 := l219 h0 h1).
 apply t188. refine (rel_subset r25 r19 i36 i152 h2 _) ; finalize. exact h3.
Qed.
Lemma t189 : p255 -> p262 -> p254.
 intros h0 h1.
 refine (add r96 r24 i145 i149 i144 h0 h1 _) ; finalize.
Qed.
Lemma l211 : p178 -> s1 -> p254 (* BND(inv_b_hat1 - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.65694e-16, 1.65694e-16]) *).
 intros h0 h1.
 assert (h2 := l212 h0 h1).
 assert (h3 := l218 h0 h1).
 apply t189. exact h2. exact h3.
Qed.
Lemma t190 : p254 -> p253.
 intros h0.
 refine (sub_xals _ _ _ i144 h0) ; finalize.
Qed.
Lemma l210 : p178 -> s1 -> p253 (* BND(inv_b_hat1 - 1 / b, [-1.65694e-16, 1.65694e-16]) *).
 intros h0 h1.
 assert (h2 := l211 h0 h1).
 apply t190. exact h2.
Qed.
Lemma t191 : p253 -> p252.
 intros h0.
 refine (abs_of_bnd_o r41 i144 i143 h0 _) ; finalize.
Qed.
Lemma l209 : p178 -> s1 -> p252 (* ABS(inv_b_hat1 - 1 / b, [0, 1.65694e-16]) *).
 intros h0 h1.
 assert (h2 := l210 h0 h1).
 apply t191. exact h2.
Qed.
Lemma t192 : p197 -> p252 -> p251.
 intros h0 h1.
 refine (mul_aa _r0 r41 i107 i143 i142 h0 h1 _) ; finalize.
Qed.
Lemma l208 : p178 -> s1 -> p251 (* ABS(r0 * (inv_b_hat1 - 1 / b), [0, 0.069245]) *).
 intros h0 h1.
 assert (h2 := l171 h0 h1).
 assert (h3 := l209 h0 h1).
 apply t192. exact h2. exact h3.
Qed.
Definition f222 := Float2 (3) (7).
Definition i153 := makepairF f1 f222.
Notation p266 := (ABS r40 i153). (* ABS(r0 * (inv_b_hat1 - 1 / b), [0, 384]) *)
Lemma t193 : p194 -> p266 -> p193.
 intros h0 h1.
 refine (add_aa_o r39 r40 i47 i153 i104 h0 h1 _) ; finalize.
Qed.
Lemma l167 : p178 -> s1 -> p193 (* ABS(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [0, 512]) *).
 intros h0 h1.
 assert (h2 := l168 h0 h1).
 assert (h3 := l208 h0 h1).
 apply t193. exact h2. refine (abs_subset r40 i142 i153 h3 _) ; finalize.
Qed.
Lemma t194 : p193 -> p192.
 intros h0.
 refine (bnd_of_abs r38 i104 i103 h0 _) ; finalize.
Qed.
Lemma l166 : p178 -> s1 -> p192 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-512, 512]) *).
 intros h0 h1.
 assert (h2 := l167 h0 h1).
 apply t194. exact h2.
Qed.
Notation p267 := (BND r83 i12). (* BND(quotient_hat1 - r0 / b - (quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b)), [0, 0]) *)
Lemma t195 : p9 -> p267.
 intros h0.
 refine (sub_of_eql r31 r38 i12 h0 _) ; finalize.
Qed.
Lemma l220 : s1 -> p267 (* BND(quotient_hat1 - r0 / b - (quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b)), [0, 0]) *).
 intros h0.
 assert (h1 := l8 h0).
 apply t195. exact h1.
Qed.
Lemma t196 : p192 -> p267 -> p191.
 intros h0 h1.
 refine (add r38 r83 i103 i12 i103 h0 h1 _) ; finalize.
Qed.
Lemma l165 : p178 -> s1 -> p191 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b) + (quotient_hat1 - r0 / b - (quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b))), [-512, 512]) *).
 intros h0 h1.
 assert (h2 := l166 h0 h1).
 assert (h3 := l220 h1).
 apply t196. exact h2. exact h3.
Qed.
Lemma t197 : p191 -> p190.
 intros h0.
 refine (val_xabs _ r31 i103 h0) ; finalize.
Qed.
Lemma l164 : p178 -> s1 -> p190 (* BND(quotient_hat1 - r0 / b, [-512, 512]) *).
 intros h0 h1.
 assert (h2 := l165 h0 h1).
 apply t197. exact h2.
Qed.
Definition f223 := Float2 (-13) (37).
Definition f224 := Float2 (50331647) (15).
Definition i154 := makepairF f223 f224.
Notation p268 := (BND r37 i154). (* BND(r0 / b, [-1.78671e+12, 1.64927e+12]) *)
Lemma t198 : p268 -> p190 -> p189.
 intros h0 h1.
 refine (add r37 r31 i154 i103 i102 h0 h1 _) ; finalize.
Qed.
Lemma l163 : p178 -> s1 -> p189 (* BND(r0 / b + (quotient_hat1 - r0 / b), [-1.92415e+12, 1.64927e+12]) *).
 intros h0 h1.
 assert (h2 := l19 h1).
 assert (h3 := l164 h0 h1).
 apply t198. refine (subset r37 i11 i154 h2 _) ; finalize. exact h3.
Qed.
Lemma t199 : p189 -> p188.
 intros h0.
 refine (val_xabs _ _quotient_hat1 i102 h0) ; finalize.
Qed.
Lemma l162 : p178 -> s1 -> p188 (* BND(quotient_hat1, [-1.92415e+12, 1.64927e+12]) *).
 intros h0 h1.
 assert (h2 := l163 h0 h1).
 apply t199. exact h2.
Qed.
Lemma t200 : p188 -> p187.
 intros h0.
 refine (fixed_round_ne _ _quotient_hat1 i102 i102 h0 _) ; finalize.
Qed.
Lemma l161 : p178 -> s1 -> p187 (* BND(q1, [-1.92415e+12, 1.64927e+12]) *).
 intros h0 h1.
 assert (h2 := l162 h0 h1).
 apply t200. exact h2.
Qed.
Definition i155 := makepairF f65 f3.
Notation p269 := (BND r80 i155). (* BND(q1 - r0 * inv_b_hat1, [-1, 1]) *)
Notation r98 := ((r45 + r39)%R).
Notation p270 := (BND r98 i155). (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 * inv_b_hat1), [-1, 1]) *)
Notation p271 := (BND r39 i5). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.5, 0.5]) *)
Lemma t201 : p196 -> p271.
 intros h0.
 refine (float_absolute_wide_ne _ _ r33 i106 i5 h0 _) ; finalize.
Qed.
Lemma l223 : p178 -> s1 -> p271 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.5, 0.5]) *).
 intros h0 h1.
 assert (h2 := l170 h0 h1).
 apply t201. exact h2.
Qed.
Lemma t202 : p7 -> p271 -> p270.
 intros h0 h1.
 refine (add r45 r39 i5 i5 i155 h0 h1 _) ; finalize.
Qed.
Lemma l222 : p178 -> s1 -> p270 (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 * inv_b_hat1), [-1, 1]) *).
 intros h0 h1.
 assert (h2 := l6 h1).
 assert (h3 := l223 h0 h1).
 apply t202. exact h2. exact h3.
Qed.
Lemma t203 : p270 -> p269.
 intros h0.
 refine (sub_xals _ _ _ i155 h0) ; finalize.
Qed.
Lemma l221 : p178 -> s1 -> p269 (* BND(q1 - r0 * inv_b_hat1, [-1, 1]) *).
 intros h0 h1.
 assert (h2 := l222 h0 h1).
 apply t203. exact h2.
Qed.
Lemma t204 : p187 -> p269 -> p186.
 intros h0 h1.
 refine (sub _q1 r80 i102 i155 i101 h0 h1 _) ; finalize.
Qed.
Lemma l160 : p178 -> s1 -> p186 (* BND(q1 - (q1 - r0 * inv_b_hat1), [-2.19902e+12, 2.19902e+12]) *).
 intros h0 h1.
 assert (h2 := l161 h0 h1).
 assert (h3 := l221 h0 h1).
 apply t204. exact h2. exact h3.
Qed.
Lemma t205 : p186 -> p185.
 intros h0.
 refine (val_xebs r33 _ i101 h0) ; finalize.
Qed.
Lemma l159 : p178 -> s1 -> p185 (* BND(r0 * inv_b_hat1, [-2.19902e+12, 2.19902e+12]) *).
 intros h0 h1.
 assert (h2 := l160 h0 h1).
 apply t205. exact h2.
Qed.
Lemma t206 : p185 -> p184.
 intros h0.
 refine (abs_of_bnd_o r33 i101 i100 h0 _) ; finalize.
Qed.
Lemma l158 : p178 -> s1 -> p184 (* ABS(r0 * inv_b_hat1, [0, 2.19902e+12]) *).
 intros h0 h1.
 assert (h2 := l159 h0 h1).
 apply t206. exact h2.
Qed.
Lemma t207 : p184 -> p183.
 intros h0.
 refine (float_absolute_wide_ne _ _ r33 i100 i99 h0 _) ; finalize.
Qed.
Lemma l157 : p178 -> s1 -> p183 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.00012207, 0.00012207]) *).
 intros h0 h1.
 assert (h2 := l158 h0 h1).
 apply t207. exact h2.
Qed.
Definition f225 := Float2 (-79834049824412419) (-60).
Definition i156 := makepairF f225 f205.
Notation p272 := (BND r40 i156). (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.069245, 0.069245]) *)
Lemma t208 : p251 -> p272.
 intros h0.
 refine (bnd_of_abs r40 i142 i156 h0 _) ; finalize.
Qed.
Lemma l224 : p178 -> s1 -> p272 (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.069245, 0.069245]) *).
 intros h0 h1.
 assert (h2 := l208 h0 h1).
 apply t208. exact h2.
Qed.
Lemma t209 : p183 -> p272 -> p182.
 intros h0 h1.
 refine (add r39 r40 i99 i156 i98 h0 h1 _) ; finalize.
Qed.
Lemma l156 : p178 -> s1 -> p182 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0693671, 0.0693671]) *).
 intros h0 h1.
 assert (h2 := l157 h0 h1).
 assert (h3 := l224 h0 h1).
 apply t209. exact h2. exact h3.
Qed.
Lemma t210 : p9 -> p182 -> p181.
 intros h0 h1.
 refine (bnd_rewrite r31 r38 i98 h0 h1) ; finalize.
Qed.
Lemma l155 : p178 -> s1 -> p181 (* BND(quotient_hat1 - r0 / b, [-0.0693671, 0.0693671]) *).
 intros h0 h1.
 assert (h2 := l8 h1).
 assert (h3 := l156 h0 h1).
 apply t210. exact h2. exact h3.
Qed.
Lemma t211 : p7 -> p181 -> p180.
 intros h0 h1.
 refine (add r45 r31 i5 i98 i97 h0 h1 _) ; finalize.
Qed.
Lemma l154 : p178 -> s1 -> p180 (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.569367, 0.569367]) *).
 intros h0 h1.
 assert (h2 := l6 h1).
 assert (h3 := l155 h0 h1).
 apply t211. exact h2. exact h3.
Qed.
Lemma t212 : p180 -> p179.
 intros h0.
 refine (sub_xals _ _ _ i97 h0) ; finalize.
Qed.
Lemma l153 : p178 -> s1 -> p179 (* BND(q1 - r0 / b, [-0.569367, 0.569367]) *).
 intros h0 h1.
 assert (h2 := l154 h0 h1).
 apply t212. exact h2.
Qed.
Lemma l152 : p178 -> s1 -> False.
 intros h0 h1.
 assert (h2 := l3 h1).
 assert (h3 := l153 h0 h1).
 refine (simplify (Tatom false (Abnd 0%nat i3)) Tfalse (Abnd 0%nat i97) (List.cons r42 List.nil) h3 h2 _) ; finalize.
Qed.
Definition f226 := Float2 (576460752840294399) (-29).
Definition i157 := makepairF f145 f226.
Notation p273 := (BND _b i157). (* BND(b, [16385, 1.07374e+09]) *)
Definition f227 := Float2 (-342549545635747817) (-59).
Definition f228 := Float2 (342549545635747817) (-59).
Definition i158 := makepairF f227 f228.
Notation p274 := (BND r42 i158). (* BND(q1 - r0 / b, [-0.594229, 0.594229]) *)
Notation p275 := (BND r44 i158). (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.594229, 0.594229]) *)
Definition f229 := Float2 (-54319169484036073) (-59).
Definition f230 := Float2 (54319169484036073) (-59).
Definition i159 := makepairF f229 f230.
Notation p276 := (BND r31 i159). (* BND(quotient_hat1 - r0 / b, [-0.0942287, 0.0942287]) *)
Notation p277 := (BND r38 i159). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0942287, 0.0942287]) *)
Notation p278 := (BND r39 i75). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-7.45058e-09, 7.45058e-09]) *)
Definition f231 := Float2 (32767) (12).
Definition i160 := makepairF f1 f231.
Notation p279 := (ABS _quotient_hat1 i160). (* ABS(quotient_hat1, [0, 1.34214e+08]) *)
Definition f232 := Float2 (-32767) (12).
Definition i161 := makepairF f232 f231.
Notation p280 := (BND _quotient_hat1 i161). (* BND(quotient_hat1, [-1.34214e+08, 1.34214e+08]) *)
Notation p281 := (BND r81 i161). (* BND(r0 / b + (quotient_hat1 - r0 / b), [-1.34214e+08, 1.34214e+08]) *)
Definition f233 := Float2 (-65533) (11).
Definition f234 := Float2 (16383) (13).
Definition i162 := makepairF f233 f234.
Notation p282 := (BND r37 i162). (* BND(r0 / b, [-1.34212e+08, 1.3421e+08]) *)
Notation p283 := (BND r47 i162). (* BND((r0 - (a / b - q0) * b) / b + (a / b - q0) * b / b, [-1.34212e+08, 1.3421e+08]) *)
Notation p284 := (BND r50 i162). (* BND((a / b - q0) * b / b, [-1.34212e+08, 1.3421e+08]) *)
Definition f235 := Float2 (-576425606852249739) (-32).
Definition i163 := makepairF f235 f234.
Notation p285 := (BND r13 i163). (* BND(a / b - q0, [-1.3421e+08, 1.3421e+08]) *)
Notation p286 := (BND r51 i163). (* BND(a / b - quotient_hat0 - (q0 - quotient_hat0), [-1.3421e+08, 1.3421e+08]) *)
Definition f236 := Float2 (-576425604704766091) (-32).
Definition f237 := Float2 (33552383) (2).
Definition i164 := makepairF f236 f237.
Notation p287 := (BND r52 i164). (* BND(a / b - quotient_hat0, [-1.3421e+08, 1.3421e+08]) *)
Notation p288 := (BND r54 i164). (* BND(a / b - a / b - (quotient_hat0 - a / b), [-1.3421e+08, 1.3421e+08]) *)
Definition f238 := Float2 (-33552383) (2).
Definition f239 := Float2 (576425604704766091) (-32).
Definition i165 := makepairF f238 f239.
Notation p289 := (BND r20 i165). (* BND(quotient_hat0 - a / b, [-1.3421e+08, 1.3421e+08]) *)
Notation p290 := (BND r21 i165). (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-1.3421e+08, 1.3421e+08]) *)
Definition f240 := Float2 (1) (-4).
Definition i166 := makepairF f65 f240.
Notation p291 := (BND r22 i166). (* BND(quotient_hat0 - a * inv_b_hat, [-1, 0.0625]) *)
Definition f241 := Float2 (1) (50).
Definition i167 := makepairF f1 f241.
Notation p292 := (ABS r7 i167). (* ABS(a * inv_b_hat, [0, 1.1259e+15]) *)
Definition i168 := makepairF f184 f200.
Notation p293 := (ABS _inv_b_hat i168). (* ABS(inv_b_hat, [4.65661e-10, 6.10352e-05]) *)
Definition f242 := Float2 (3) (-32).
Definition f243 := Float2 (288212819396987013) (-72).
Definition i169 := makepairF f242 f243.
Notation p294 := (BND _inv_b_hat i169). (* BND(inv_b_hat, [6.98492e-10, 6.10314e-05]) *)
Notation p295 := (BND r72 i169). (* BND(b * inv_b_hat / b, [6.98492e-10, 6.10314e-05]) *)
Lemma l248 : p273 -> s1 -> p273 (* BND(b, [16385, 1.07374e+09]) *).
 intros h0 h1.
 assert (h2 := h0).
 exact (h2).
Qed.
Definition i170 := makepairF f106 f103.
Notation p296 := (BND r16 i170). (* BND(b * inv_b_hat, [0.9375, 1]) *)
Definition f244 := Float2 (5) (28).
Definition i171 := makepairF f145 f244.
Notation p297 := (BND _b i171). (* BND(b, [16385, 1.34218e+09]) *)
Lemma t213 : p296 -> p297 -> p295.
 intros h0 h1.
 refine (div_pp r16 _b i170 i171 i169 h0 h1 _) ; finalize.
Qed.
Lemma l247 : p273 -> s1 -> p295 (* BND(b * inv_b_hat / b, [6.98492e-10, 6.10314e-05]) *).
 intros h0 h1.
 assert (h2 := l121 h1).
 assert (h3 := l248 h0 h1).
 apply t213. refine (subset r16 i68 i170 h2 _) ; finalize. refine (subset _b i157 i171 h3 _) ; finalize.
Qed.
Lemma t214 : p10 -> p295 -> p294.
 intros h0 h1.
 refine (mul_xiru _ _inv_b_hat i169 h0 h1) ; finalize.
Qed.
Lemma l246 : p273 -> s1 -> p294 (* BND(inv_b_hat, [6.98492e-10, 6.10314e-05]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l247 h0 h1).
 apply t214. exact h2. exact h3.
Qed.
Notation p298 := (BND _inv_b_hat i168). (* BND(inv_b_hat, [4.65661e-10, 6.10352e-05]) *)
Lemma t215 : p298 -> p293.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat i168 i168 h0 _) ; finalize.
Qed.
Lemma l245 : p273 -> s1 -> p293 (* ABS(inv_b_hat, [4.65661e-10, 6.10352e-05]) *).
 intros h0 h1.
 assert (h2 := l246 h0 h1).
 apply t215. refine (subset _inv_b_hat i169 i168 h2 _) ; finalize.
Qed.
Lemma t216 : p33 -> p293 -> p292.
 intros h0 h1.
 refine (mul_aa _a _inv_b_hat i1 i168 i167 h0 h1 _) ; finalize.
Qed.
Lemma l244 : p273 -> s1 -> p292 (* ABS(a * inv_b_hat, [0, 1.1259e+15]) *).
 intros h0 h1.
 assert (h2 := l34 h1).
 assert (h3 := l245 h0 h1).
 apply t216. exact h2. exact h3.
Qed.
Lemma t217 : p292 -> p291.
 intros h0.
 refine (float_absolute_wide_ne _ _ r7 i167 i166 h0 _) ; finalize.
Qed.
Lemma l243 : p273 -> s1 -> p291 (* BND(quotient_hat0 - a * inv_b_hat, [-1, 0.0625]) *).
 intros h0 h1.
 assert (h2 := l244 h0 h1).
 apply t217. exact h2.
Qed.
Definition f245 := Float2 (-67104765) (1).
Definition f246 := Float2 (576425604436330635) (-32).
Definition i172 := makepairF f245 f246.
Notation p299 := (BND r23 i172). (* BND(a * (inv_b_hat - 1 / b), [-1.3421e+08, 1.3421e+08]) *)
Definition f247 := Float2 (-50329599) (-63).
Definition f248 := Float2 (432327990294021189) (-96).
Definition i173 := makepairF f247 f248.
Notation p300 := (BND r18 i173). (* BND(inv_b_hat - 1 / b, [-5.45675e-12, 5.45675e-12]) *)
Notation p301 := (BND r56 i173). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-5.45675e-12, 5.45675e-12]) *)
Definition f249 := Float2 (-1) (-39).
Definition f250 := Float2 (1) (-39).
Definition i174 := makepairF f249 f250.
Notation p302 := (BND r57 i174). (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-1.81899e-12, 1.81899e-12]) *)
Notation p303 := (ABS r9 i168). (* ABS(1 / float<24,-149,ne>(b), [4.65661e-10, 6.10352e-05]) *)
Notation p304 := (BND r9 i168). (* BND(1 / float<24,-149,ne>(b), [4.65661e-10, 6.10352e-05]) *)
Definition f251 := Float2 (32767) (-29).
Definition i175 := makepairF f242 f251.
Notation p305 := (BND _inv_b_hat i175). (* BND(inv_b_hat, [6.98492e-10, 6.10333e-05]) *)
Definition f252 := Float2 (-1) (-15).
Definition i176 := makepairF f252 f10.
Notation p306 := (REL _inv_b_hat r9 i176). (* REL(inv_b_hat, 1 / float<24,-149,ne>(b), [-3.05176e-05, 0.5]) *)
Lemma t218 : p305 -> p306 -> p304.
 intros h0 h1.
 refine (bnd_of_rel_bnd_p _inv_b_hat r9 i175 i176 i168 h0 h1 _) ; finalize.
Qed.
Lemma l254 : p273 -> s1 -> p304 (* BND(1 / float<24,-149,ne>(b), [4.65661e-10, 6.10352e-05]) *).
 intros h0 h1.
 assert (h2 := l246 h0 h1).
 assert (h3 := l71 h1).
 apply t218. refine (subset _inv_b_hat i169 i175 h2 _) ; finalize. refine (rel_subset _inv_b_hat r9 i22 i176 h3 _) ; finalize.
Qed.
Lemma t219 : p304 -> p303.
 intros h0.
 refine (abs_of_bnd_p r9 i168 i168 h0 _) ; finalize.
Qed.
Lemma l253 : p273 -> s1 -> p303 (* ABS(1 / float<24,-149,ne>(b), [4.65661e-10, 6.10352e-05]) *).
 intros h0 h1.
 assert (h2 := l254 h0 h1).
 apply t219. exact h2.
Qed.
Lemma t220 : p303 -> p302.
 intros h0.
 refine (float_absolute_wide_ne _ _ r9 i168 i174 h0 _) ; finalize.
Qed.
Lemma l252 : p273 -> s1 -> p302 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b), [-1.81899e-12, 1.81899e-12]) *).
 intros h0 h1.
 assert (h2 := l253 h0 h1).
 apply t220. exact h2.
Qed.
Definition f253 := Float2 (-33552383) (-63).
Definition f254 := Float2 (288212802218165317) (-96).
Definition i177 := makepairF f253 f254.
Notation p307 := (BND r58 i177). (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-3.63776e-12, 3.63776e-12]) *)
Definition f255 := Float2 (576425570078687241) (-73).
Definition i178 := makepairF f184 f255.
Notation p308 := (BND r19 i178). (* BND(1 / b, [4.65661e-10, 6.10314e-05]) *)
Definition f256 := Float2 (1) (31).
Definition i179 := makepairF f145 f256.
Notation p309 := (BND _b i179). (* BND(b, [16385, 2.14748e+09]) *)
Lemma t221 : p37 -> p309 -> p308.
 intros h0 h1.
 refine (div_pp r10 _b i17 i179 i178 h0 h1 _) ; finalize.
Qed.
Lemma l256 : p273 -> s1 -> p308 (* BND(1 / b, [4.65661e-10, 6.10314e-05]) *).
 intros h0 h1.
 assert (h2 := l39 h1).
 assert (h3 := l248 h0 h1).
 apply t221. exact h2. refine (subset _b i157 i179 h3 _) ; finalize.
Qed.
Definition f257 := Float2 (-67108861) (-50).
Definition i180 := makepairF f257 f35.
Notation p310 := (REL r9 r19 i180). (* REL(1 / float<24,-149,ne>(b), 1 / b, [-5.96046e-08, 5.96046e-08]) *)
Lemma t222 : p310 -> p308 -> p307.
 intros h0 h1.
 refine (error_of_rel_op r9 r19 i180 i178 i177 h0 h1 _) ; finalize.
Qed.
Lemma l255 : p273 -> s1 -> p307 (* BND(1 / float<24,-149,ne>(b) - 1 / b, [-3.63776e-12, 3.63776e-12]) *).
 intros h0 h1.
 assert (h2 := l47 h1).
 assert (h3 := l256 h0 h1).
 apply t222. refine (rel_subset r9 r19 i21 i180 h2 _) ; finalize. exact h3.
Qed.
Lemma t223 : p302 -> p307 -> p301.
 intros h0 h1.
 refine (add r57 r58 i174 i177 i173 h0 h1 _) ; finalize.
Qed.
Lemma l251 : p273 -> s1 -> p301 (* BND(inv_b_hat - 1 / float<24,-149,ne>(b) + (1 / float<24,-149,ne>(b) - 1 / b), [-5.45675e-12, 5.45675e-12]) *).
 intros h0 h1.
 assert (h2 := l252 h0 h1).
 assert (h3 := l255 h0 h1).
 apply t223. exact h2. exact h3.
Qed.
Lemma t224 : p301 -> p300.
 intros h0.
 refine (sub_xals _ _ _ i173 h0) ; finalize.
Qed.
Lemma l250 : p273 -> s1 -> p300 (* BND(inv_b_hat - 1 / b, [-5.45675e-12, 5.45675e-12]) *).
 intros h0 h1.
 assert (h2 := l251 h0 h1).
 apply t224. exact h2.
Qed.
Definition f258 := Float2 (-67104765) (-63).
Definition f259 := Float2 (576425604436330635) (-96).
Definition i181 := makepairF f258 f259.
Notation p311 := (BND r18 i181). (* BND(inv_b_hat - 1 / b, [-7.27551e-12, 7.27551e-12]) *)
Lemma t225 : p1 -> p311 -> p299.
 intros h0 h1.
 refine (mul_po _a r18 i1 i181 i172 h0 h1 _) ; finalize.
Qed.
Lemma l249 : p273 -> s1 -> p299 (* BND(a * (inv_b_hat - 1 / b), [-1.3421e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l35 h1).
 assert (h3 := l250 h0 h1).
 apply t225. exact h2. refine (subset r18 i173 i181 h3 _) ; finalize.
Qed.
Lemma t226 : p291 -> p299 -> p290.
 intros h0 h1.
 refine (add r22 r23 i166 i172 i165 h0 h1 _) ; finalize.
Qed.
Lemma l242 : p273 -> s1 -> p290 (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-1.3421e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l243 h0 h1).
 assert (h3 := l249 h0 h1).
 apply t226. exact h2. exact h3.
Qed.
Lemma t227 : p49 -> p290 -> p289.
 intros h0 h1.
 refine (bnd_rewrite r20 r21 i165 h0 h1) ; finalize.
Qed.
Lemma l241 : p273 -> s1 -> p289 (* BND(quotient_hat0 - a / b, [-1.3421e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l51 h1).
 assert (h3 := l242 h0 h1).
 apply t227. exact h2. exact h3.
Qed.
Lemma t228 : p28 -> p289 -> p288.
 intros h0 h1.
 refine (sub r55 r20 i12 i165 i164 h0 h1 _) ; finalize.
Qed.
Lemma l240 : p273 -> s1 -> p288 (* BND(a / b - a / b - (quotient_hat0 - a / b), [-1.3421e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l29 h1).
 assert (h3 := l241 h0 h1).
 apply t228. exact h2. exact h3.
Qed.
Lemma t229 : p288 -> p287.
 intros h0.
 refine (sub_xars _ _ _ i164 h0) ; finalize.
Qed.
Lemma l239 : p273 -> s1 -> p287 (* BND(a / b - quotient_hat0, [-1.3421e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l240 h0 h1).
 apply t229. exact h2.
Qed.
Lemma t230 : p287 -> p150 -> p286.
 intros h0 h1.
 refine (sub r52 r53 i164 i83 i163 h0 h1 _) ; finalize.
Qed.
Lemma l238 : p273 -> s1 -> p286 (* BND(a / b - quotient_hat0 - (q0 - quotient_hat0), [-1.3421e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l239 h0 h1).
 assert (h3 := l53 h1).
 apply t230. exact h2. refine (subset r53 i5 i83 h3 _) ; finalize.
Qed.
Lemma t231 : p286 -> p285.
 intros h0.
 refine (sub_xars _ _ _ i163 h0) ; finalize.
Qed.
Lemma l237 : p273 -> s1 -> p285 (* BND(a / b - q0, [-1.3421e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l238 h0 h1).
 apply t231. exact h2.
Qed.
Notation p312 := (BND r13 i162). (* BND(a / b - q0, [-1.34212e+08, 1.3421e+08]) *)
Lemma t232 : p10 -> p312 -> p284.
 intros h0 h1.
 refine (div_fir r13 _b i162 h0 h1) ; finalize.
Qed.
Lemma l236 : p273 -> s1 -> p284 (* BND((a / b - q0) * b / b, [-1.34212e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l237 h0 h1).
 apply t232. exact h2. refine (subset r13 i163 i162 h3 _) ; finalize.
Qed.
Lemma t233 : p20 -> p284 -> p283.
 intros h0 h1.
 refine (add r48 r50 i12 i162 i162 h0 h1 _) ; finalize.
Qed.
Lemma l235 : p273 -> s1 -> p283 (* BND((r0 - (a / b - q0) * b) / b + (a / b - q0) * b / b, [-1.34212e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l21 h1).
 assert (h3 := l236 h0 h1).
 apply t233. exact h2. exact h3.
Qed.
Lemma t234 : p10 -> p283 -> p282.
 intros h0 h1.
 refine (div_xals _ _ _b i162 h0 h1) ; finalize.
Qed.
Lemma l234 : p273 -> s1 -> p282 (* BND(r0 / b, [-1.34212e+08, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l235 h0 h1).
 apply t234. exact h2. exact h3.
Qed.
Definition f260 := Float2 (-1) (11).
Definition f261 := Float2 (1) (11).
Definition i182 := makepairF f260 f261.
Notation p313 := (BND r31 i182). (* BND(quotient_hat1 - r0 / b, [-2048, 2048]) *)
Notation p314 := (BND r38 i182). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-2048, 2048]) *)
Notation p315 := (BND r39 i155). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-1, 1]) *)
Notation p316 := (ABS _quotient_hat1 i100). (* ABS(quotient_hat1, [0, 2.19902e+12]) *)
Notation p317 := (BND _quotient_hat1 i101). (* BND(quotient_hat1, [-2.19902e+12, 2.19902e+12]) *)
Notation p318 := (BND r81 i101). (* BND(r0 / b + (quotient_hat1 - r0 / b), [-2.19902e+12, 2.19902e+12]) *)
Definition f262 := Float2 (-1) (25).
Definition f263 := Float2 (1) (25).
Definition i183 := makepairF f262 f263.
Notation p319 := (BND r31 i183). (* BND(quotient_hat1 - r0 / b, [-3.35544e+07, 3.35544e+07]) *)
Notation p320 := (BND r38 i183). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-3.35544e+07, 3.35544e+07]) *)
Definition f264 := Float2 (-1) (5).
Definition f265 := Float2 (1) (5).
Definition i184 := makepairF f264 f265.
Notation p321 := (BND r39 i184). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-32, 32]) *)
Definition f266 := Float2 (3) (57).
Definition i185 := makepairF f1 f266.
Notation p322 := (ABS _quotient_hat1 i185). (* ABS(quotient_hat1, [0, 4.32346e+17]) *)
Definition f267 := Float2 (-659780684080292157) (-1).
Definition i186 := makepairF f267 f266.
Notation p323 := (BND _quotient_hat1 i186). (* BND(quotient_hat1, [-3.2989e+17, 4.32346e+17]) *)
Notation p324 := (BND r81 i186). (* BND(r0 / b + (quotient_hat1 - r0 / b), [-3.2989e+17, 4.32346e+17]) *)
Definition f268 := Float2 (-659777385545275707) (-1).
Definition f269 := Float2 (5) (56).
Definition i187 := makepairF f268 f269.
Notation p325 := (BND r31 i187). (* BND(quotient_hat1 - r0 / b, [-3.29889e+17, 3.60288e+17]) *)
Notation p326 := (BND r38 i187). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-3.29889e+17, 3.60288e+17]) *)
Definition f270 := Float2 (-1) (51).
Definition f271 := Float2 (1) (51).
Definition i188 := makepairF f270 f271.
Notation p327 := (BND r39 i188). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-2.2518e+15, 2.2518e+15]) *)
Definition f272 := Float2 (1) (105).
Definition i189 := makepairF f1 f272.
Notation p328 := (ABS r33 i189). (* ABS(r0 * inv_b_hat1, [0, 4.05648e+31]) *)
Definition f273 := Float2 (-1) (105).
Definition f274 := Float2 (1) (82).
Definition i190 := makepairF f273 f274.
Notation p329 := (BND r33 i190). (* BND(r0 * inv_b_hat1, [-4.05648e+31, 4.8357e+24]) *)
Definition f275 := Float2 (-432345581676004353) (46).
Definition i191 := makepairF f275 f2.
Notation p330 := (BND _r0 i191). (* BND(r0, [-3.04236e+31, 1.84467e+19]) *)
Definition f276 := Float2 (-1) (128).
Definition i192 := makepairF f276 f2.
Notation p331 := (BND _r0 i192). (* BND(r0, [-3.40282e+38, 1.84467e+19]) *)
Definition f277 := Float2 (1) (128).
Definition i193 := makepairF f1 f277.
Notation p332 := (BND r4 i193). (* BND(b * q0, [0, 3.40282e+38]) *)
Notation p333 := (BND _q0 i1). (* BND(q0, [0, 1.84467e+19]) *)
Notation p334 := (BND _quotient_hat0 i1). (* BND(quotient_hat0, [0, 1.84467e+19]) *)
Notation p335 := (BND r7 i1). (* BND(a * inv_b_hat, [0, 1.84467e+19]) *)
Lemma t235 : p1 -> p35 -> p335.
 intros h0 h1.
 refine (mul_pp _a _inv_b_hat i1 i16 i1 h0 h1 _) ; finalize.
Qed.
Lemma l279 : s1 -> p335 (* BND(a * inv_b_hat, [0, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l35 h0).
 assert (h2 := l37 h0).
 apply t235. exact h1. exact h2.
Qed.
Lemma t236 : p335 -> p334.
 intros h0.
 refine (float_round_ne _ _ r7 i1 i1 h0 _) ; finalize.
Qed.
Lemma l278 : s1 -> p334 (* BND(quotient_hat0, [0, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l279 h0).
 apply t236. exact h1.
Qed.
Lemma t237 : p334 -> p333.
 intros h0.
 refine (fixed_round_ne _ _quotient_hat0 i1 i1 h0 _) ; finalize.
Qed.
Lemma l277 : s1 -> p333 (* BND(q0, [0, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l278 h0).
 apply t237. exact h1.
Qed.
Lemma t238 : p2 -> p333 -> p332.
 intros h0 h1.
 refine (mul_pp _b _q0 i2 i1 i193 h0 h1 _) ; finalize.
Qed.
Lemma l276 : s1 -> p332 (* BND(b * q0, [0, 3.40282e+38]) *).
 intros h0.
 assert (h1 := l11 h0).
 assert (h2 := l277 h0).
 apply t238. exact h1. exact h2.
Qed.
Lemma t239 : p1 -> p332 -> p331.
 intros h0 h1.
 refine (sub _a r4 i1 i193 i192 h0 h1 _) ; finalize.
Qed.
Lemma l275 : s1 -> p331 (* BND(r0, [-3.40282e+38, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l35 h0).
 assert (h2 := l276 h0).
 apply t239. exact h1. exact h2.
Qed.
Definition i194 := makepairF f275 f272.
Notation p336 := (BND _r0 i194). (* BND(r0, [-3.04236e+31, 4.05648e+31]) *)
Notation p337 := (BND r46 i194). (* BND(r0 / b * b, [-3.04236e+31, 4.05648e+31]) *)
Definition f278 := Float2 (-432345581676004353) (-18).
Definition i195 := makepairF f278 f152.
Notation p338 := (BND r37 i195). (* BND(r0 / b, [-1.64927e+12, 2.19902e+12]) *)
Lemma t240 : p338 -> p2 -> p337.
 intros h0 h1.
 refine (mul_op r37 _b i195 i2 i194 h0 h1 _) ; finalize.
Qed.
Lemma l281 : s1 -> p337 (* BND(r0 / b * b, [-3.04236e+31, 4.05648e+31]) *).
 intros h0.
 assert (h1 := l19 h0).
 assert (h2 := l11 h0).
 apply t240. refine (subset r37 i11 i195 h1 _) ; finalize. exact h2.
Qed.
Lemma t241 : p10 -> p337 -> p336.
 intros h0 h1.
 refine (div_xilu _r0 _ i194 h0 h1) ; finalize.
Qed.
Lemma l280 : s1 -> p336 (* BND(r0, [-3.04236e+31, 4.05648e+31]) *).
 intros h0.
 assert (h1 := l9 h0).
 assert (h2 := l281 h0).
 apply t241. exact h1. exact h2.
Qed.
Lemma l274 : s1 -> p330 (* BND(r0, [-3.04236e+31, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l275 h0).
 assert (h2 := l280 h0).
 apply intersect with (1 := h1) (2 := h2). finalize.
Qed.
Definition f279 := Float2 (-25) (100).
Definition i196 := makepairF f279 f2.
Notation p339 := (BND _r0 i196). (* BND(r0, [-3.16913e+31, 1.84467e+19]) *)
Definition f280 := Float2 (5) (-2).
Definition i197 := makepairF f39 f280.
Notation p340 := (BND _inv_b_hat1 i197). (* BND(inv_b_hat1, [-1.49012e-07, 1.25]) *)
Lemma t242 : p339 -> p340 -> p329.
 intros h0 h1.
 refine (mul_oo _r0 _inv_b_hat1 i196 i197 i190 h0 h1 _) ; finalize.
Qed.
Lemma l273 : s1 -> p329 (* BND(r0 * inv_b_hat1, [-4.05648e+31, 4.8357e+24]) *).
 intros h0.
 assert (h1 := l274 h0).
 assert (h2 := l56 h0).
 apply t242. refine (subset _r0 i191 i196 h1 _) ; finalize. refine (subset _inv_b_hat1 i25 i197 h2 _) ; finalize.
Qed.
Lemma t243 : p329 -> p328.
 intros h0.
 refine (abs_of_bnd_o r33 i190 i189 h0 _) ; finalize.
Qed.
Lemma l272 : s1 -> p328 (* ABS(r0 * inv_b_hat1, [0, 4.05648e+31]) *).
 intros h0.
 assert (h1 := l273 h0).
 apply t243. exact h1.
Qed.
Lemma t244 : p328 -> p327.
 intros h0.
 refine (float_absolute_wide_ne _ _ r33 i189 i188 h0 _) ; finalize.
Qed.
Lemma l271 : s1 -> p327 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-2.2518e+15, 2.2518e+15]) *).
 intros h0.
 assert (h1 := l272 h0).
 apply t244. exact h1.
Qed.
Definition f281 := Float2 (-655273785917905211) (-1).
Definition f282 := Float2 (19) (54).
Definition i198 := makepairF f281 f282.
Notation p341 := (BND r40 i198). (* BND(r0 * (inv_b_hat1 - 1 / b), [-3.27637e+17, 3.42274e+17]) *)
Definition f283 := Float2 (-25) (-51).
Definition i199 := makepairF f283 f70.
Notation p342 := (BND r41 i199). (* BND(inv_b_hat1 - 1 / b, [-1.11022e-14, 1.07692e-14]) *)
Lemma t245 : p330 -> p342 -> p341.
 intros h0 h1.
 refine (mul_oo _r0 r41 i191 i199 i198 h0 h1 _) ; finalize.
Qed.
Lemma l282 : s1 -> p341 (* BND(r0 * (inv_b_hat1 - 1 / b), [-3.27637e+17, 3.42274e+17]) *).
 intros h0.
 assert (h1 := l274 h0).
 assert (h2 := l85 h0).
 apply t245. exact h1. refine (subset r41 i45 i199 h2 _) ; finalize.
Qed.
Lemma t246 : p327 -> p341 -> p326.
 intros h0 h1.
 refine (add r39 r40 i188 i198 i187 h0 h1 _) ; finalize.
Qed.
Lemma l270 : s1 -> p326 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-3.29889e+17, 3.60288e+17]) *).
 intros h0.
 assert (h1 := l271 h0).
 assert (h2 := l282 h0).
 apply t246. exact h1. exact h2.
Qed.
Notation p343 := (REL r31 r38 i12). (* REL(quotient_hat1 - r0 / b, quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [0, 0]) *)
Notation p344 := (REL r38 r38 i12). (* REL(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [0, 0]) *)
Lemma t247 : p344.
 refine (rel_refl r38 i12 _) ; finalize.
Qed.
Lemma l284 : s1 -> p344 (* REL(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [0, 0]) *).
 intros h0.
 apply t247.
Qed.
Lemma t248 : p9 -> p344 -> p343.
 intros h0 h1.
 refine (rel_rewrite_1 r31 r38 r38 i12 h0 h1) ; finalize.
Qed.
Lemma l283 : s1 -> p343 (* REL(quotient_hat1 - r0 / b, quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [0, 0]) *).
 intros h0.
 assert (h1 := l8 h0).
 assert (h2 := l284 h0).
 apply t248. exact h1. exact h2.
Qed.
Lemma t249 : p326 -> p343 -> p325.
 intros h0 h1.
 refine (bnd_of_bnd_rel_o r31 r38 i187 i12 i187 h0 h1 _) ; finalize.
Qed.
Lemma l269 : s1 -> p325 (* BND(quotient_hat1 - r0 / b, [-3.29889e+17, 3.60288e+17]) *).
 intros h0.
 assert (h1 := l270 h0).
 assert (h2 := l283 h0).
 apply t249. exact h1. exact h2.
Qed.
Definition f284 := Float2 (-1649267508225) (0).
Definition i200 := makepairF f284 f152.
Notation p345 := (BND r37 i200). (* BND(r0 / b, [-1.64927e+12, 2.19902e+12]) *)
Lemma t250 : p345 -> p325 -> p324.
 intros h0 h1.
 refine (add r37 r31 i200 i187 i186 h0 h1 _) ; finalize.
Qed.
Lemma l268 : s1 -> p324 (* BND(r0 / b + (quotient_hat1 - r0 / b), [-3.2989e+17, 4.32346e+17]) *).
 intros h0.
 assert (h1 := l19 h0).
 assert (h2 := l269 h0).
 apply t250. refine (subset r37 i11 i200 h1 _) ; finalize. exact h2.
Qed.
Lemma t251 : p324 -> p323.
 intros h0.
 refine (val_xabs _ _quotient_hat1 i186 h0) ; finalize.
Qed.
Lemma l267 : s1 -> p323 (* BND(quotient_hat1, [-3.2989e+17, 4.32346e+17]) *).
 intros h0.
 assert (h1 := l268 h0).
 apply t251. exact h1.
Qed.
Definition f285 := Float2 (-3) (57).
Definition i201 := makepairF f285 f266.
Notation p346 := (BND _quotient_hat1 i201). (* BND(quotient_hat1, [-4.32346e+17, 4.32346e+17]) *)
Lemma t252 : p346 -> p322.
 intros h0.
 refine (abs_of_bnd_o _quotient_hat1 i201 i185 h0 _) ; finalize.
Qed.
Lemma l266 : s1 -> p322 (* ABS(quotient_hat1, [0, 4.32346e+17]) *).
 intros h0.
 assert (h1 := l267 h0).
 apply t252. refine (subset _quotient_hat1 i186 i201 h1 _) ; finalize.
Qed.
Lemma t253 : p322 -> p321.
 intros h0.
 refine (float_absolute_inv_ne _ _ _ i185 i184 h0 _) ; finalize.
Qed.
Lemma l265 : s1 -> p321 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-32, 32]) *).
 intros h0.
 assert (h1 := l266 h0).
 apply t253. exact h1.
Qed.
Definition f286 := Float2 (-54319165189068777) (-59).
Definition f287 := Float2 (54319165189068777) (-59).
Definition i202 := makepairF f286 f287.
Notation p347 := (BND r40 i202). (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.0942287, 0.0942287]) *)
Definition f288 := Float2 (-576425607389087919) (-2).
Definition f289 := Float2 (576425607389087919) (-2).
Definition i203 := makepairF f288 f289.
Notation p348 := (BND _r0 i203). (* BND(r0, [-1.44106e+17, 1.44106e+17]) *)
Definition i204 := makepairF f1 f289.
Notation p349 := (ABS _r0 i204). (* ABS(r0, [0, 1.44106e+17]) *)
Notation p350 := (ABS r12 i204). (* ABS((a / b - q0) * b, [0, 1.44106e+17]) *)
Definition f290 := Float2 (576425606852249739) (-32).
Definition i205 := makepairF f1 f290.
Notation p351 := (ABS r13 i205). (* ABS(a / b - q0, [0, 1.3421e+08]) *)
Lemma t254 : p285 -> p351.
 intros h0.
 refine (abs_of_bnd_o r13 i163 i205 h0 _) ; finalize.
Qed.
Lemma l289 : p273 -> s1 -> p351 (* ABS(a / b - q0, [0, 1.3421e+08]) *).
 intros h0 h1.
 assert (h2 := l237 h0 h1).
 apply t254. exact h2.
Qed.
Definition i206 := makepairF f3 f226.
Notation p352 := (ABS _b i206). (* ABS(b, [1, 1.07374e+09]) *)
Notation p353 := (BND _b i206). (* BND(b, [1, 1.07374e+09]) *)
Lemma t255 : p353 -> p352.
 intros h0.
 refine (abs_of_bnd_p _b i206 i206 h0 _) ; finalize.
Qed.
Lemma l290 : p273 -> s1 -> p352 (* ABS(b, [1, 1.07374e+09]) *).
 intros h0 h1.
 assert (h2 := l248 h0 h1).
 apply t255. refine (subset _b i157 i206 h2 _) ; finalize.
Qed.
Lemma t256 : p351 -> p352 -> p350.
 intros h0 h1.
 refine (mul_aa r13 _b i205 i206 i204 h0 h1 _) ; finalize.
Qed.
Lemma l288 : p273 -> s1 -> p350 (* ABS((a / b - q0) * b, [0, 1.44106e+17]) *).
 intros h0 h1.
 assert (h2 := l289 h0 h1).
 assert (h3 := l290 h0 h1).
 apply t256. exact h2. exact h3.
Qed.
Lemma t257 : p22 -> p350 -> p349.
 intros h0 h1.
 refine (abs_rewrite _r0 r12 i204 h0 h1) ; finalize.
Qed.
Lemma l287 : p273 -> s1 -> p349 (* ABS(r0, [0, 1.44106e+17]) *).
 intros h0 h1.
 assert (h2 := l23 h1).
 assert (h3 := l288 h0 h1).
 apply t257. exact h2. exact h3.
Qed.
Lemma t258 : p349 -> p348.
 intros h0.
 refine (bnd_of_abs _r0 i204 i203 h0 _) ; finalize.
Qed.
Lemma l286 : p273 -> s1 -> p348 (* BND(r0, [-1.44106e+17, 1.44106e+17]) *).
 intros h0 h1.
 assert (h2 := l287 h0 h1).
 apply t258. exact h2.
Qed.
Definition f291 := Float2 (-434579816413302407) (-119).
Definition f292 := Float2 (12647937) (-84).
Definition i207 := makepairF f291 f292.
Notation p354 := (BND r41 i207). (* BND(inv_b_hat1 - 1 / b, [-6.53883e-19, 6.53883e-19]) *)
Notation p355 := (BND r65 i207). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-6.53883e-19, 6.53883e-19]) *)
Definition f293 := Float2 (-1) (-68).
Definition f294 := Float2 (1) (-68).
Definition i208 := makepairF f293 f294.
Notation p356 := (BND r66 i208). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-3.38813e-21, 3.38813e-21]) *)
Notation p357 := (ABS r35 i168). (* ABS(alpha * inv_b_hat + inv_b_hat, [4.65661e-10, 6.10352e-05]) *)
Notation p358 := (BND r35 i168). (* BND(alpha * inv_b_hat + inv_b_hat, [4.65661e-10, 6.10352e-05]) *)
Definition f295 := Float2 (-288212836575810757) (-95).
Definition f296 := Float2 (1152851208872661269) (-97).
Definition i209 := makepairF f295 f296.
Notation p359 := (BND r36 i209). (* BND(alpha * inv_b_hat, [-7.27551e-12, 7.27551e-12]) *)
Definition i210 := makepairF f184 f243.
Notation p360 := (BND _inv_b_hat i210). (* BND(inv_b_hat, [4.65661e-10, 6.10314e-05]) *)
Lemma t259 : p59 -> p360 -> p359.
 intros h0 h1.
 refine (mul_op _alpha _inv_b_hat i28 i210 i209 h0 h1 _) ; finalize.
Qed.
Lemma l296 : p273 -> s1 -> p359 (* BND(alpha * inv_b_hat, [-7.27551e-12, 7.27551e-12]) *).
 intros h0 h1.
 assert (h2 := l61 h1).
 assert (h3 := l246 h0 h1).
 apply t259. exact h2. refine (subset _inv_b_hat i169 i210 h3 _) ; finalize.
Qed.
Definition f297 := Float2 (-1) (-32).
Definition f298 := Float2 (1) (-29).
Definition i211 := makepairF f297 f298.
Notation p361 := (BND r36 i211). (* BND(alpha * inv_b_hat, [-2.32831e-10, 1.86265e-09]) *)
Lemma t260 : p361 -> p305 -> p358.
 intros h0 h1.
 refine (add r36 _inv_b_hat i211 i175 i168 h0 h1 _) ; finalize.
Qed.
Lemma l295 : p273 -> s1 -> p358 (* BND(alpha * inv_b_hat + inv_b_hat, [4.65661e-10, 6.10352e-05]) *).
 intros h0 h1.
 assert (h2 := l296 h0 h1).
 assert (h3 := l246 h0 h1).
 apply t260. refine (subset r36 i209 i211 h2 _) ; finalize. refine (subset _inv_b_hat i169 i175 h3 _) ; finalize.
Qed.
Lemma t261 : p358 -> p357.
 intros h0.
 refine (abs_of_bnd_p r35 i168 i168 h0 _) ; finalize.
Qed.
Lemma l294 : p273 -> s1 -> p357 (* ABS(alpha * inv_b_hat + inv_b_hat, [4.65661e-10, 6.10352e-05]) *).
 intros h0 h1.
 assert (h2 := l295 h0 h1).
 apply t261. exact h2.
Qed.
Lemma t262 : p357 -> p356.
 intros h0.
 refine (float_absolute_wide_ne _ _ r35 i168 i208 h0 _) ; finalize.
Qed.
Lemma l293 : p273 -> s1 -> p356 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-3.38813e-21, 3.38813e-21]) *).
 intros h0 h1.
 assert (h2 := l294 h0 h1).
 apply t262. exact h2.
Qed.
Definition f299 := Float2 (-432328016599617159) (-119).
Definition f300 := Float2 (12582401) (-84).
Definition i212 := makepairF f299 f300.
Notation p362 := (BND r67 i212). (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-6.50495e-19, 6.50495e-19]) *)
Notation p363 := (BND r68 i212). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-6.50495e-19, 6.50495e-19]) *)
Definition f301 := Float2 (-432328016062779013) (-119).
Definition f302 := Float2 (25164801) (-85).
Definition i213 := makepairF f301 f302.
Notation p364 := (BND r69 i213). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-6.50495e-19, 6.50495e-19]) *)
Notation p365 := (BND r60 i213). (* BND(alpha * inv_b_hat - alpha * (1 / b), [-6.50495e-19, 6.50495e-19]) *)
Notation p366 := (BND r61 i213). (* BND(alpha * (inv_b_hat - 1 / b), [-6.50495e-19, 6.50495e-19]) *)
Lemma t263 : p76 -> p300 -> p366.
 intros h0 h1.
 refine (mul_oo _alpha r18 i34 i173 i213 h0 h1 _) ; finalize.
Qed.
Lemma l301 : p273 -> s1 -> p366 (* BND(alpha * (inv_b_hat - 1 / b), [-6.50495e-19, 6.50495e-19]) *).
 intros h0 h1.
 assert (h2 := l61 h1).
 assert (h3 := l250 h0 h1).
 apply t263. refine (subset _alpha i28 i34 h2 _) ; finalize. exact h3.
Qed.
Lemma t264 : p366 -> p365.
 intros h0.
 refine (mul_fils _ _ _ i213 h0) ; finalize.
Qed.
Lemma l300 : p273 -> s1 -> p365 (* BND(alpha * inv_b_hat - alpha * (1 / b), [-6.50495e-19, 6.50495e-19]) *).
 intros h0 h1.
 assert (h2 := l301 h0 h1).
 apply t264. exact h2.
Qed.
Lemma t265 : p365 -> p364.
 intros h0.
 refine (add_firs _ _ _ i213 h0) ; finalize.
Qed.
Lemma l299 : p273 -> s1 -> p364 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-6.50495e-19, 6.50495e-19]) *).
 intros h0 h1.
 assert (h2 := l300 h0 h1).
 apply t265. exact h2.
Qed.
Definition f303 := Float2 (-268419073) (-118).
Definition f304 := Float2 (268419073) (-118).
Definition i214 := makepairF f303 f304.
Notation p367 := (BND r24 i214). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-8.07744e-28, 8.07744e-28]) *)
Definition f305 := Float2 (268419073) (-42).
Definition i215 := makepairF f184 f305.
Notation p368 := (BND r19 i215). (* BND(1 / b, [4.65661e-10, 6.10314e-05]) *)
Lemma t266 : p170 -> p368 -> p367.
 intros h0 h1.
 refine (error_of_rel_op r25 r19 i36 i215 i214 h0 h1 _) ; finalize.
Qed.
Lemma l302 : p273 -> s1 -> p367 (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-8.07744e-28, 8.07744e-28]) *).
 intros h0 h1.
 assert (h2 := l146 h1).
 assert (h3 := l256 h0 h1).
 apply t266. exact h2. refine (subset r19 i178 i215 h3 _) ; finalize.
Qed.
Definition f306 := Float2 (1) (-85).
Definition i216 := makepairF f303 f306.
Notation p369 := (BND r24 i216). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-8.07744e-28, 2.58494e-26]) *)
Lemma t267 : p364 -> p369 -> p363.
 intros h0 h1.
 refine (add r69 r24 i213 i216 i212 h0 h1 _) ; finalize.
Qed.
Lemma l298 : p273 -> s1 -> p363 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-6.50495e-19, 6.50495e-19]) *).
 intros h0 h1.
 assert (h2 := l299 h0 h1).
 assert (h3 := l302 h0 h1).
 apply t267. exact h2. refine (subset r24 i214 i216 h3 _) ; finalize.
Qed.
Lemma t268 : p363 -> p362.
 intros h0.
 refine (sub_xals _ _ _ i212 h0) ; finalize.
Qed.
Lemma l297 : p273 -> s1 -> p362 (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-6.50495e-19, 6.50495e-19]) *).
 intros h0 h1.
 assert (h2 := l298 h0 h1).
 apply t268. exact h2.
Qed.
Lemma t269 : p356 -> p362 -> p355.
 intros h0 h1.
 refine (add r66 r67 i208 i212 i207 h0 h1 _) ; finalize.
Qed.
Lemma l292 : p273 -> s1 -> p355 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-6.53883e-19, 6.53883e-19]) *).
 intros h0 h1.
 assert (h2 := l293 h0 h1).
 assert (h3 := l297 h0 h1).
 apply t269. exact h2. exact h3.
Qed.
Lemma t270 : p355 -> p354.
 intros h0.
 refine (sub_xals _ _ _ i207 h0) ; finalize.
Qed.
Lemma l291 : p273 -> s1 -> p354 (* BND(inv_b_hat1 - 1 / b, [-6.53883e-19, 6.53883e-19]) *).
 intros h0 h1.
 assert (h2 := l292 h0 h1).
 apply t270. exact h2.
Qed.
Lemma t271 : p348 -> p354 -> p347.
 intros h0 h1.
 refine (mul_oo _r0 r41 i203 i207 i202 h0 h1 _) ; finalize.
Qed.
Lemma l285 : p273 -> s1 -> p347 (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.0942287, 0.0942287]) *).
 intros h0 h1.
 assert (h2 := l286 h0 h1).
 assert (h3 := l291 h0 h1).
 apply t271. exact h2. exact h3.
Qed.
Definition f307 := Float2 (-3) (23).
Definition f308 := Float2 (3) (23).
Definition i217 := makepairF f307 f308.
Notation p370 := (BND r40 i217). (* BND(r0 * (inv_b_hat1 - 1 / b), [-2.51658e+07, 2.51658e+07]) *)
Lemma t272 : p321 -> p370 -> p320.
 intros h0 h1.
 refine (add r39 r40 i184 i217 i183 h0 h1 _) ; finalize.
Qed.
Lemma l264 : p273 -> s1 -> p320 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-3.35544e+07, 3.35544e+07]) *).
 intros h0 h1.
 assert (h2 := l265 h1).
 assert (h3 := l285 h0 h1).
 apply t272. exact h2. refine (subset r40 i202 i217 h3 _) ; finalize.
Qed.
Lemma t273 : p9 -> p320 -> p319.
 intros h0 h1.
 refine (bnd_rewrite r31 r38 i183 h0 h1) ; finalize.
Qed.
Lemma l263 : p273 -> s1 -> p319 (* BND(quotient_hat1 - r0 / b, [-3.35544e+07, 3.35544e+07]) *).
 intros h0 h1.
 assert (h2 := l8 h1).
 assert (h3 := l264 h0 h1).
 apply t273. exact h2. exact h3.
Qed.
Notation p371 := (BND r37 i102). (* BND(r0 / b, [-1.92415e+12, 1.64927e+12]) *)
Lemma t274 : p371 -> p319 -> p318.
 intros h0 h1.
 refine (add r37 r31 i102 i183 i101 h0 h1 _) ; finalize.
Qed.
Lemma l262 : p273 -> s1 -> p318 (* BND(r0 / b + (quotient_hat1 - r0 / b), [-2.19902e+12, 2.19902e+12]) *).
 intros h0 h1.
 assert (h2 := l19 h1).
 assert (h3 := l263 h0 h1).
 apply t274. refine (subset r37 i11 i102 h2 _) ; finalize. exact h3.
Qed.
Lemma t275 : p318 -> p317.
 intros h0.
 refine (val_xabs _ _quotient_hat1 i101 h0) ; finalize.
Qed.
Lemma l261 : p273 -> s1 -> p317 (* BND(quotient_hat1, [-2.19902e+12, 2.19902e+12]) *).
 intros h0 h1.
 assert (h2 := l262 h0 h1).
 apply t275. exact h2.
Qed.
Lemma t276 : p317 -> p316.
 intros h0.
 refine (abs_of_bnd_o _quotient_hat1 i101 i100 h0 _) ; finalize.
Qed.
Lemma l260 : p273 -> s1 -> p316 (* ABS(quotient_hat1, [0, 2.19902e+12]) *).
 intros h0 h1.
 assert (h2 := l261 h0 h1).
 apply t276. exact h2.
Qed.
Lemma t277 : p316 -> p315.
 intros h0.
 refine (float_absolute_inv_ne _ _ _ i100 i155 h0 _) ; finalize.
Qed.
Lemma l259 : p273 -> s1 -> p315 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-1, 1]) *).
 intros h0 h1.
 assert (h2 := l260 h0 h1).
 apply t277. exact h2.
Qed.
Definition f309 := Float2 (-7) (8).
Definition f310 := Float2 (7) (8).
Definition i218 := makepairF f309 f310.
Notation p372 := (BND r40 i218). (* BND(r0 * (inv_b_hat1 - 1 / b), [-1792, 1792]) *)
Lemma t278 : p315 -> p372 -> p314.
 intros h0 h1.
 refine (add r39 r40 i155 i218 i182 h0 h1 _) ; finalize.
Qed.
Lemma l258 : p273 -> s1 -> p314 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-2048, 2048]) *).
 intros h0 h1.
 assert (h2 := l259 h0 h1).
 assert (h3 := l285 h0 h1).
 apply t278. exact h2. refine (subset r40 i202 i218 h3 _) ; finalize.
Qed.
Lemma t279 : p9 -> p314 -> p313.
 intros h0 h1.
 refine (bnd_rewrite r31 r38 i182 h0 h1) ; finalize.
Qed.
Lemma l257 : p273 -> s1 -> p313 (* BND(quotient_hat1 - r0 / b, [-2048, 2048]) *).
 intros h0 h1.
 assert (h2 := l8 h1).
 assert (h3 := l258 h0 h1).
 apply t279. exact h2. exact h3.
Qed.
Lemma t280 : p282 -> p313 -> p281.
 intros h0 h1.
 refine (add r37 r31 i162 i182 i161 h0 h1 _) ; finalize.
Qed.
Lemma l233 : p273 -> s1 -> p281 (* BND(r0 / b + (quotient_hat1 - r0 / b), [-1.34214e+08, 1.34214e+08]) *).
 intros h0 h1.
 assert (h2 := l234 h0 h1).
 assert (h3 := l257 h0 h1).
 apply t280. exact h2. exact h3.
Qed.
Lemma t281 : p281 -> p280.
 intros h0.
 refine (val_xabs _ _quotient_hat1 i161 h0) ; finalize.
Qed.
Lemma l232 : p273 -> s1 -> p280 (* BND(quotient_hat1, [-1.34214e+08, 1.34214e+08]) *).
 intros h0 h1.
 assert (h2 := l233 h0 h1).
 apply t281. exact h2.
Qed.
Lemma t282 : p280 -> p279.
 intros h0.
 refine (abs_of_bnd_o _quotient_hat1 i161 i160 h0 _) ; finalize.
Qed.
Lemma l231 : p273 -> s1 -> p279 (* ABS(quotient_hat1, [0, 1.34214e+08]) *).
 intros h0 h1.
 assert (h2 := l232 h0 h1).
 apply t282. exact h2.
Qed.
Lemma t283 : p279 -> p278.
 intros h0.
 refine (float_absolute_inv_ne _ _ _ i160 i75 h0 _) ; finalize.
Qed.
Lemma l230 : p273 -> s1 -> p278 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-7.45058e-09, 7.45058e-09]) *).
 intros h0 h1.
 assert (h2 := l231 h0 h1).
 apply t283. exact h2.
Qed.
Lemma t284 : p278 -> p347 -> p277.
 intros h0 h1.
 refine (add r39 r40 i75 i202 i159 h0 h1 _) ; finalize.
Qed.
Lemma l229 : p273 -> s1 -> p277 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0942287, 0.0942287]) *).
 intros h0 h1.
 assert (h2 := l230 h0 h1).
 assert (h3 := l285 h0 h1).
 apply t284. exact h2. exact h3.
Qed.
Lemma t285 : p9 -> p277 -> p276.
 intros h0 h1.
 refine (bnd_rewrite r31 r38 i159 h0 h1) ; finalize.
Qed.
Lemma l228 : p273 -> s1 -> p276 (* BND(quotient_hat1 - r0 / b, [-0.0942287, 0.0942287]) *).
 intros h0 h1.
 assert (h2 := l8 h1).
 assert (h3 := l229 h0 h1).
 apply t285. exact h2. exact h3.
Qed.
Lemma t286 : p7 -> p276 -> p275.
 intros h0 h1.
 refine (add r45 r31 i5 i159 i158 h0 h1 _) ; finalize.
Qed.
Lemma l227 : p273 -> s1 -> p275 (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.594229, 0.594229]) *).
 intros h0 h1.
 assert (h2 := l6 h1).
 assert (h3 := l228 h0 h1).
 apply t286. exact h2. exact h3.
Qed.
Lemma t287 : p275 -> p274.
 intros h0.
 refine (sub_xals _ _ _ i158 h0) ; finalize.
Qed.
Lemma l226 : p273 -> s1 -> p274 (* BND(q1 - r0 / b, [-0.594229, 0.594229]) *).
 intros h0 h1.
 assert (h2 := l227 h0 h1).
 apply t287. exact h2.
Qed.
Lemma l225 : p273 -> s1 -> False.
 intros h0 h1.
 assert (h2 := l3 h1).
 assert (h3 := l226 h0 h1).
 refine (simplify (Tatom false (Abnd 0%nat i3)) Tfalse (Abnd 0%nat i158) (List.cons r42 List.nil) h3 h2 _) ; finalize.
Qed.
Definition f311 := Float2 (1) (61).
Definition i219 := makepairF f226 f311.
Notation p373 := (BND _b i219). (* BND(b, [1.07374e+09, 2.30584e+18]) *)
Definition f312 := Float2 (-1338140287631361) (-51).
Definition f313 := Float2 (85640979482411199) (-57).
Definition i220 := makepairF f312 f313.
Notation p374 := (BND r42 i220). (* BND(q1 - r0 / b, [-0.594254, 0.594254]) *)
Notation r100 := ((r42 - r31)%R).
Notation r99 := ((r31 + r100)%R).
Notation p375 := (BND r99 i220). (* BND(quotient_hat1 - r0 / b + (q1 - r0 / b - (quotient_hat1 - r0 / b)), [-0.594254, 0.594254]) *)
Definition f314 := Float2 (-212240380788737) (-51).
Definition f315 := Float2 (13583385444483263) (-57).
Definition i221 := makepairF f314 f315.
Notation p376 := (BND r31 i221). (* BND(quotient_hat1 - r0 / b, [-0.0942537, 0.0942537]) *)
Notation p377 := (BND r82 i221). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b) + (quotient_hat1 - r0 / b - (quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b))), [-0.0942537, 0.0942537]) *)
Notation p378 := (BND r38 i221). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0942537, 0.0942537]) *)
Definition f316 := Float2 (-1) (-5).
Definition i222 := makepairF f316 f129.
Notation p379 := (BND r39 i222). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.03125, 0.03125]) *)
Definition f317 := Float2 (7) (46).
Definition i223 := makepairF f1 f317.
Notation p380 := (ABS _quotient_hat1 i223). (* ABS(quotient_hat1, [0, 4.92581e+14]) *)
Definition f318 := Float2 (-7) (46).
Definition f319 := Float2 (1) (35).
Definition i224 := makepairF f318 f319.
Notation p381 := (BND _quotient_hat1 i224). (* BND(quotient_hat1, [-4.92581e+14, 3.43597e+10]) *)
Definition f320 := Float2 (-1) (56).
Definition i225 := makepairF f320 f319.
Notation p382 := (BND _quotient_hat1 i225). (* BND(quotient_hat1, [-7.20576e+16, 3.43597e+10]) *)
Notation p383 := (BND r33 i225). (* BND(r0 * inv_b_hat1, [-7.20576e+16, 3.43597e+10]) *)
Definition f321 := Float2 (-1) (66).
Definition i226 := makepairF f321 f319.
Notation p384 := (BND r33 i226). (* BND(r0 * inv_b_hat1, [-7.3787e+19, 3.43597e+10]) *)
Notation r102 := ((_r0 * r66)%R).
Notation r103 := ((_r0 * r35)%R).
Notation r101 := ((r102 + r103)%R).
Notation p385 := (BND r101 i226). (* BND(r0 * (inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat)) + r0 * (alpha * inv_b_hat + inv_b_hat), [-7.3787e+19, 3.43597e+10]) *)
Definition f322 := Float2 (-1) (13).
Definition f323 := Float2 (1) (13).
Definition i227 := makepairF f322 f323.
Notation p386 := (BND r102 i227). (* BND(r0 * (inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat)), [-8192, 8192]) *)
Definition f324 := Float2 (-72075190518939901) (16).
Definition i228 := makepairF f324 f2.
Notation p387 := (BND _r0 i228). (* BND(r0, [-4.72352e+21, 1.84467e+19]) *)
Notation p388 := (BND r12 i228). (* BND((a / b - q0) * b, [-4.72352e+21, 1.84467e+19]) *)
Notation p389 := (BND r12 i192). (* BND((a / b - q0) * b, [-3.40282e+38, 1.84467e+19]) *)
Notation p390 := (REL _r0 r12 i12). (* REL(r0, (a / b - q0) * b, [0, 0]) *)
Notation p391 := (REL r12 r12 i12). (* REL((a / b - q0) * b, (a / b - q0) * b, [0, 0]) *)
Lemma t288 : p391.
 refine (rel_refl r12 i12 _) ; finalize.
Qed.
Lemma l321 : s1 -> p391 (* REL((a / b - q0) * b, (a / b - q0) * b, [0, 0]) *).
 intros h0.
 apply t288.
Qed.
Lemma t289 : p22 -> p391 -> p390.
 intros h0 h1.
 refine (rel_rewrite_1 _r0 r12 r12 i12 h0 h1) ; finalize.
Qed.
Lemma l320 : s1 -> p390 (* REL(r0, (a / b - q0) * b, [0, 0]) *).
 intros h0.
 assert (h1 := l23 h0).
 assert (h2 := l321 h0).
 apply t289. exact h1. exact h2.
Qed.
Lemma t290 : p331 -> p390 -> p389.
 intros h0 h1.
 refine (bnd_of_rel_bnd_o _r0 r12 i192 i12 i192 h0 h1 _) ; finalize.
Qed.
Lemma l319 : s1 -> p389 (* BND((a / b - q0) * b, [-3.40282e+38, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l275 h0).
 assert (h2 := l320 h0).
 apply t290. exact h1. exact h2.
Qed.
Definition f325 := Float2 (1) (73).
Definition i229 := makepairF f324 f325.
Notation p392 := (BND r12 i229). (* BND((a / b - q0) * b, [-4.72352e+21, 9.44473e+21]) *)
Definition f326 := Float2 (-72075190518939901) (-45).
Definition f327 := Float2 (1) (12).
Definition i230 := makepairF f326 f327.
Notation p393 := (BND r13 i230). (* BND(a / b - q0, [-2048.5, 4096]) *)
Notation p394 := (BND r51 i230). (* BND(a / b - quotient_hat0 - (q0 - quotient_hat0), [-2048.5, 4096]) *)
Definition f328 := Float2 (-72057598332895485) (-45).
Definition i231 := makepairF f328 f261.
Notation p395 := (BND r52 i231). (* BND(a / b - quotient_hat0, [-2048, 2048]) *)
Notation p396 := (BND r54 i231). (* BND(a / b - a / b - (quotient_hat0 - a / b), [-2048, 2048]) *)
Definition f329 := Float2 (72057598332895485) (-45).
Definition i232 := makepairF f260 f329.
Notation p397 := (BND r20 i232). (* BND(quotient_hat0 - a / b, [-2048, 2048]) *)
Notation p398 := (BND r21 i232). (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-2048, 2048]) *)
Definition f330 := Float2 (1) (-19).
Definition i233 := makepairF f150 f330.
Notation p399 := (BND r22 i233). (* BND(quotient_hat0 - a * inv_b_hat, [-0.00012207, 1.90735e-06]) *)
Definition i234 := makepairF f1 f319.
Notation p400 := (ABS r7 i234). (* ABS(a * inv_b_hat, [0, 3.43597e+10]) *)
Definition f331 := Float2 (1) (-62).
Definition i235 := makepairF f331 f298.
Notation p401 := (ABS _inv_b_hat i235). (* ABS(inv_b_hat, [2.1684e-19, 1.86265e-09]) *)
Definition f332 := Float2 (2097151) (-82).
Definition f333 := Float2 (576460820486033347) (-89).
Definition i236 := makepairF f332 f333.
Notation p402 := (BND _inv_b_hat i236). (* BND(inv_b_hat, [4.33681e-19, 9.31323e-10]) *)
Notation p403 := (BND r72 i236). (* BND(b * inv_b_hat / b, [4.33681e-19, 9.31323e-10]) *)
Lemma l334 : p373 -> s1 -> p373 (* BND(b, [1.07374e+09, 2.30584e+18]) *).
 intros h0 h1.
 assert (h2 := h0).
 exact (h2).
Qed.
Definition f334 := Float2 (2097151) (-21).
Definition i237 := makepairF f334 f103.
Notation p404 := (BND r16 i237). (* BND(b * inv_b_hat, [1, 1]) *)
Lemma t291 : p404 -> p373 -> p403.
 intros h0 h1.
 refine (div_pp r16 _b i237 i219 i236 h0 h1 _) ; finalize.
Qed.
Lemma l333 : p373 -> s1 -> p403 (* BND(b * inv_b_hat / b, [4.33681e-19, 9.31323e-10]) *).
 intros h0 h1.
 assert (h2 := l121 h1).
 assert (h3 := l334 h0 h1).
 apply t291. refine (subset r16 i68 i237 h2 _) ; finalize. exact h3.
Qed.
Lemma t292 : p10 -> p403 -> p402.
 intros h0 h1.
 refine (mul_xiru _ _inv_b_hat i236 h0 h1) ; finalize.
Qed.
Lemma l332 : p373 -> s1 -> p402 (* BND(inv_b_hat, [4.33681e-19, 9.31323e-10]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l333 h0 h1).
 apply t292. exact h2. exact h3.
Qed.
Notation p405 := (BND _inv_b_hat i235). (* BND(inv_b_hat, [2.1684e-19, 1.86265e-09]) *)
Lemma t293 : p405 -> p401.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat i235 i235 h0 _) ; finalize.
Qed.
Lemma l331 : p373 -> s1 -> p401 (* ABS(inv_b_hat, [2.1684e-19, 1.86265e-09]) *).
 intros h0 h1.
 assert (h2 := l332 h0 h1).
 apply t293. refine (subset _inv_b_hat i236 i235 h2 _) ; finalize.
Qed.
Lemma t294 : p33 -> p401 -> p400.
 intros h0 h1.
 refine (mul_aa _a _inv_b_hat i1 i235 i234 h0 h1 _) ; finalize.
Qed.
Lemma l330 : p373 -> s1 -> p400 (* ABS(a * inv_b_hat, [0, 3.43597e+10]) *).
 intros h0 h1.
 assert (h2 := l34 h1).
 assert (h3 := l331 h0 h1).
 apply t294. exact h2. exact h3.
Qed.
Lemma t295 : p400 -> p399.
 intros h0.
 refine (float_absolute_wide_ne _ _ r7 i234 i233 h0 _) ; finalize.
Qed.
Lemma l329 : p373 -> s1 -> p399 (* BND(quotient_hat0 - a * inv_b_hat, [-0.00012207, 1.90735e-06]) *).
 intros h0 h1.
 assert (h2 := l330 h0 h1).
 apply t295. exact h2.
Qed.
Definition f335 := Float2 (-16777215) (-13).
Definition f336 := Float2 (72057598265786621) (-45).
Definition i238 := makepairF f335 f336.
Notation p406 := (BND r23 i238). (* BND(a * (inv_b_hat - 1 / b), [-2048, 2048]) *)
Definition f337 := Float2 (-16777215) (-77).
Definition f338 := Float2 (72057598265786621) (-109).
Definition i239 := makepairF f337 f338.
Notation p407 := (BND r18 i239). (* BND(inv_b_hat - 1 / b, [-1.11022e-16, 1.11022e-16]) *)
Notation r104 := ((r17 * r19)%R).
Notation p408 := (BND r104 i239). (* BND((inv_b_hat - 1 / b) / (1 / b) * (1 / b), [-1.11022e-16, 1.11022e-16]) *)
Definition f339 := Float2 (1) (-61).
Definition f340 := Float2 (288230375883276289) (-88).
Definition i240 := makepairF f339 f340.
Notation p409 := (BND r19 i240). (* BND(1 / b, [4.33681e-19, 9.31323e-10]) *)
Lemma t296 : p37 -> p373 -> p409.
 intros h0 h1.
 refine (div_pp r10 _b i17 i219 i240 h0 h1 _) ; finalize.
Qed.
Lemma l338 : p373 -> s1 -> p409 (* BND(1 / b, [4.33681e-19, 9.31323e-10]) *).
 intros h0 h1.
 assert (h2 := l39 h1).
 assert (h3 := l334 h0 h1).
 apply t296. exact h2. exact h3.
Qed.
Definition f341 := Float2 (-2147483521) (-54).
Definition i241 := makepairF f341 f50.
Notation p410 := (BND r17 i241). (* BND((inv_b_hat - 1 / b) / (1 / b), [-1.19209e-07, 1.19209e-07]) *)
Definition f342 := Float2 (144115187941638145) (-87).
Definition i242 := makepairF f339 f342.
Notation p411 := (BND r19 i242). (* BND(1 / b, [4.33681e-19, 9.31323e-10]) *)
Lemma t297 : p410 -> p411 -> p408.
 intros h0 h1.
 refine (mul_op r17 r19 i241 i242 i239 h0 h1 _) ; finalize.
Qed.
Lemma l337 : p373 -> s1 -> p408 (* BND((inv_b_hat - 1 / b) / (1 / b) * (1 / b), [-1.11022e-16, 1.11022e-16]) *).
 intros h0 h1.
 assert (h2 := l66 h1).
 assert (h3 := l338 h0 h1).
 apply t297. refine (subset r17 i31 i241 h2 _) ; finalize. refine (subset r19 i240 i242 h3 _) ; finalize.
Qed.
Lemma t298 : p65 -> p408 -> p407.
 intros h0 h1.
 refine (div_xilu r18 _ i239 h0 h1) ; finalize.
Qed.
Lemma l336 : p373 -> s1 -> p407 (* BND(inv_b_hat - 1 / b, [-1.11022e-16, 1.11022e-16]) *).
 intros h0 h1.
 assert (h2 := l67 h1).
 assert (h3 := l337 h0 h1).
 apply t298. exact h2. exact h3.
Qed.
Lemma t299 : p1 -> p407 -> p406.
 intros h0 h1.
 refine (mul_po _a r18 i1 i239 i238 h0 h1 _) ; finalize.
Qed.
Lemma l335 : p373 -> s1 -> p406 (* BND(a * (inv_b_hat - 1 / b), [-2048, 2048]) *).
 intros h0 h1.
 assert (h2 := l35 h1).
 assert (h3 := l336 h0 h1).
 apply t299. exact h2. exact h3.
Qed.
Lemma t300 : p399 -> p406 -> p398.
 intros h0 h1.
 refine (add r22 r23 i233 i238 i232 h0 h1 _) ; finalize.
Qed.
Lemma l328 : p373 -> s1 -> p398 (* BND(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [-2048, 2048]) *).
 intros h0 h1.
 assert (h2 := l329 h0 h1).
 assert (h3 := l335 h0 h1).
 apply t300. exact h2. exact h3.
Qed.
Lemma t301 : p49 -> p398 -> p397.
 intros h0 h1.
 refine (bnd_rewrite r20 r21 i232 h0 h1) ; finalize.
Qed.
Lemma l327 : p373 -> s1 -> p397 (* BND(quotient_hat0 - a / b, [-2048, 2048]) *).
 intros h0 h1.
 assert (h2 := l51 h1).
 assert (h3 := l328 h0 h1).
 apply t301. exact h2. exact h3.
Qed.
Lemma t302 : p28 -> p397 -> p396.
 intros h0 h1.
 refine (sub r55 r20 i12 i232 i231 h0 h1 _) ; finalize.
Qed.
Lemma l326 : p373 -> s1 -> p396 (* BND(a / b - a / b - (quotient_hat0 - a / b), [-2048, 2048]) *).
 intros h0 h1.
 assert (h2 := l29 h1).
 assert (h3 := l327 h0 h1).
 apply t302. exact h2. exact h3.
Qed.
Lemma t303 : p396 -> p395.
 intros h0.
 refine (sub_xars _ _ _ i231 h0) ; finalize.
Qed.
Lemma l325 : p373 -> s1 -> p395 (* BND(a / b - quotient_hat0, [-2048, 2048]) *).
 intros h0 h1.
 assert (h2 := l326 h0 h1).
 apply t303. exact h2.
Qed.
Lemma t304 : p395 -> p150 -> p394.
 intros h0 h1.
 refine (sub r52 r53 i231 i83 i230 h0 h1 _) ; finalize.
Qed.
Lemma l324 : p373 -> s1 -> p394 (* BND(a / b - quotient_hat0 - (q0 - quotient_hat0), [-2048.5, 4096]) *).
 intros h0 h1.
 assert (h2 := l325 h0 h1).
 assert (h3 := l53 h1).
 apply t304. exact h2. refine (subset r53 i5 i83 h3 _) ; finalize.
Qed.
Lemma t305 : p394 -> p393.
 intros h0.
 refine (sub_xars _ _ _ i230 h0) ; finalize.
Qed.
Lemma l323 : p373 -> s1 -> p393 (* BND(a / b - q0, [-2048.5, 4096]) *).
 intros h0 h1.
 assert (h2 := l324 h0 h1).
 apply t305. exact h2.
Qed.
Definition i243 := makepairF f3 f311.
Notation p412 := (BND _b i243). (* BND(b, [1, 2.30584e+18]) *)
Lemma t306 : p393 -> p412 -> p392.
 intros h0 h1.
 refine (mul_op r13 _b i230 i243 i229 h0 h1 _) ; finalize.
Qed.
Lemma l322 : p373 -> s1 -> p392 (* BND((a / b - q0) * b, [-4.72352e+21, 9.44473e+21]) *).
 intros h0 h1.
 assert (h2 := l323 h0 h1).
 assert (h3 := l334 h0 h1).
 apply t306. exact h2. refine (subset _b i219 i243 h3 _) ; finalize.
Qed.
Lemma l318 : p373 -> s1 -> p388 (* BND((a / b - q0) * b, [-4.72352e+21, 1.84467e+19]) *).
 intros h0 h1.
 assert (h2 := l319 h1).
 assert (h3 := l322 h0 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Lemma t307 : p22 -> p388 -> p387.
 intros h0 h1.
 refine (bnd_rewrite _r0 r12 i228 h0 h1) ; finalize.
Qed.
Lemma l317 : p373 -> s1 -> p387 (* BND(r0, [-4.72352e+21, 1.84467e+19]) *).
 intros h0 h1.
 assert (h2 := l23 h1).
 assert (h3 := l318 h0 h1).
 apply t307. exact h2. exact h3.
Qed.
Definition f343 := Float2 (-1) (-83).
Definition f344 := Float2 (1) (-83).
Definition i244 := makepairF f343 f344.
Notation p413 := (BND r66 i244). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.03398e-25, 1.03398e-25]) *)
Definition i245 := makepairF f1 f298.
Notation p414 := (ABS r35 i245). (* ABS(alpha * inv_b_hat + inv_b_hat, [0, 1.86265e-09]) *)
Definition f345 := Float2 (-261121) (-71).
Definition f346 := Float2 (144115222301378529) (-87).
Definition i246 := makepairF f345 f346.
Notation p415 := (BND r35 i246). (* BND(alpha * inv_b_hat + inv_b_hat, [-1.10589e-16, 9.31323e-10]) *)
Definition f347 := Float2 (-524289) (-72).
Definition f348 := Float2 (68719480769) (-89).
Definition i247 := makepairF f347 f348.
Notation p416 := (BND r36 i247). (* BND(alpha * inv_b_hat, [-1.11023e-16, 1.11022e-16]) *)
Definition f349 := Float2 (-1048577) (-43).
Definition f350 := Float2 (137438945281) (-60).
Definition i248 := makepairF f349 f350.
Notation p417 := (BND _alpha i248). (* BND(alpha, [-1.19209e-07, 1.19209e-07]) *)
Definition f351 := Float2 (137438969729) (-67).
Definition i249 := makepairF f331 f351.
Notation p418 := (BND _inv_b_hat i249). (* BND(inv_b_hat, [2.1684e-19, 9.31323e-10]) *)
Lemma t308 : p417 -> p418 -> p416.
 intros h0 h1.
 refine (mul_op _alpha _inv_b_hat i248 i249 i247 h0 h1 _) ; finalize.
Qed.
Lemma l342 : p373 -> s1 -> p416 (* BND(alpha * inv_b_hat, [-1.11023e-16, 1.11022e-16]) *).
 intros h0 h1.
 assert (h2 := l61 h1).
 assert (h3 := l332 h0 h1).
 apply t308. refine (subset _alpha i28 i248 h2 _) ; finalize. refine (subset _inv_b_hat i236 i249 h3 _) ; finalize.
Qed.
Definition f352 := Float2 (2047) (-72).
Definition i250 := makepairF f352 f333.
Notation p419 := (BND _inv_b_hat i250). (* BND(inv_b_hat, [4.33469e-19, 9.31323e-10]) *)
Lemma t309 : p416 -> p419 -> p415.
 intros h0 h1.
 refine (add r36 _inv_b_hat i247 i250 i246 h0 h1 _) ; finalize.
Qed.
Lemma l341 : p373 -> s1 -> p415 (* BND(alpha * inv_b_hat + inv_b_hat, [-1.10589e-16, 9.31323e-10]) *).
 intros h0 h1.
 assert (h2 := l342 h0 h1).
 assert (h3 := l332 h0 h1).
 apply t309. exact h2. refine (subset _inv_b_hat i236 i250 h3 _) ; finalize.
Qed.
Definition f353 := Float2 (-1) (-29).
Definition i251 := makepairF f353 f298.
Notation p420 := (BND r35 i251). (* BND(alpha * inv_b_hat + inv_b_hat, [-1.86265e-09, 1.86265e-09]) *)
Lemma t310 : p420 -> p414.
 intros h0.
 refine (abs_of_bnd_o r35 i251 i245 h0 _) ; finalize.
Qed.
Lemma l340 : p373 -> s1 -> p414 (* ABS(alpha * inv_b_hat + inv_b_hat, [0, 1.86265e-09]) *).
 intros h0 h1.
 assert (h2 := l341 h0 h1).
 apply t310. refine (subset r35 i246 i251 h2 _) ; finalize.
Qed.
Lemma t311 : p414 -> p413.
 intros h0.
 refine (float_absolute_wide_ne _ _ r35 i245 i244 h0 _) ; finalize.
Qed.
Lemma l339 : p373 -> s1 -> p413 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.03398e-25, 1.03398e-25]) *).
 intros h0 h1.
 assert (h2 := l340 h0 h1).
 apply t311. exact h2.
Qed.
Definition f354 := Float2 (-1) (96).
Definition i252 := makepairF f354 f2.
Notation p421 := (BND _r0 i252). (* BND(r0, [-7.92282e+28, 1.84467e+19]) *)
Lemma t312 : p421 -> p413 -> p386.
 intros h0 h1.
 refine (mul_oo _r0 r66 i252 i244 i227 h0 h1 _) ; finalize.
Qed.
Lemma l316 : p373 -> s1 -> p386 (* BND(r0 * (inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat)), [-8192, 8192]) *).
 intros h0 h1.
 assert (h2 := l317 h0 h1).
 assert (h3 := l339 h0 h1).
 apply t312. refine (subset _r0 i228 i252 h2 _) ; finalize. exact h3.
Qed.
Definition f355 := Float2 (-3) (64).
Definition f356 := Float2 (3) (33).
Definition i253 := makepairF f355 f356.
Notation p422 := (BND r103 i253). (* BND(r0 * (alpha * inv_b_hat + inv_b_hat), [-5.53402e+19, 2.57698e+10]) *)
Definition f357 := Float2 (288089638655033343) (-122).
Definition f358 := Float2 (9) (-33).
Definition i254 := makepairF f357 f358.
Notation p423 := (BND r35 i254). (* BND(alpha * inv_b_hat + inv_b_hat, [5.41836e-20, 1.04774e-09]) *)
Definition f359 := Float2 (17) (-34).
Definition i255 := makepairF f54 f359.
Notation p424 := (BND r25 i255). (* BND(alpha * (1 / b) + inv_b_hat, [5.41969e-20, 9.8953e-10]) *)
Definition i256 := makepairF f65 f359.
Notation p425 := (BND r25 i256). (* BND(alpha * (1 / b) + inv_b_hat, [-1, 9.8953e-10]) *)
Definition f360 := Float2 (-288230393063146481) (-111).
Definition f361 := Float2 (288230358703408145) (-111).
Definition i257 := makepairF f360 f361.
Notation p426 := (BND r26 i257). (* BND(alpha * (1 / b), [-1.11022e-16, 1.11022e-16]) *)
Lemma t313 : p59 -> p409 -> p426.
 intros h0 h1.
 refine (mul_op _alpha r19 i28 i240 i257 h0 h1 _) ; finalize.
Qed.
Lemma l347 : p373 -> s1 -> p426 (* BND(alpha * (1 / b), [-1.11022e-16, 1.11022e-16]) *).
 intros h0 h1.
 assert (h2 := l61 h1).
 assert (h3 := l338 h0 h1).
 apply t313. exact h2. exact h3.
Qed.
Definition f362 := Float2 (1) (-35).
Definition i258 := makepairF f65 f362.
Notation p427 := (BND r26 i258). (* BND(alpha * (1 / b), [-1, 2.91038e-11]) *)
Definition f363 := Float2 (33) (-35).
Definition i259 := makepairF f331 f363.
Notation p428 := (BND _inv_b_hat i259). (* BND(inv_b_hat, [2.1684e-19, 9.60426e-10]) *)
Lemma t314 : p427 -> p428 -> p425.
 intros h0 h1.
 refine (add r26 _inv_b_hat i258 i259 i256 h0 h1 _) ; finalize.
Qed.
Lemma l346 : p373 -> s1 -> p425 (* BND(alpha * (1 / b) + inv_b_hat, [-1, 9.8953e-10]) *).
 intros h0 h1.
 assert (h2 := l347 h0 h1).
 assert (h3 := l332 h0 h1).
 apply t314. refine (subset r26 i257 i258 h2 _) ; finalize. refine (subset _inv_b_hat i236 i259 h3 _) ; finalize.
Qed.
Lemma l345 : p373 -> s1 -> p424 (* BND(alpha * (1 / b) + inv_b_hat, [5.41969e-20, 9.8953e-10]) *).
 intros h0 h1.
 assert (h2 := l346 h0 h1).
 assert (h3 := l75 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Definition f364 := Float2 (-140771873134563) (-59).
Definition i260 := makepairF f364 f129.
Notation p429 := (REL r35 r25 i260). (* REL(alpha * inv_b_hat + inv_b_hat, alpha * (1 / b) + inv_b_hat, [-0.0002442, 0.03125]) *)
Notation p430 := (REL r36 r26 i32). (* REL(alpha * inv_b_hat, alpha * (1 / b), [-1.19209e-07, 1.19209e-07]) *)
Lemma t315 : p68 -> p430.
 intros h0.
 refine (mul_filq _ _inv_b_hat r19 i32 h0) ; finalize.
Qed.
Lemma l349 : s1 -> p430 (* REL(alpha * inv_b_hat, alpha * (1 / b), [-1.19209e-07, 1.19209e-07]) *).
 intros h0.
 assert (h1 := l70 h0).
 apply t315. exact h1.
Qed.
Notation p431 := (REL _inv_b_hat _inv_b_hat i12). (* REL(inv_b_hat, inv_b_hat, [0, 0]) *)
Lemma t316 : p431.
 refine (rel_refl _inv_b_hat i12 _) ; finalize.
Qed.
Lemma l350 : s1 -> p431 (* REL(inv_b_hat, inv_b_hat, [0, 0]) *).
 intros h0.
 apply t316.
Qed.
Notation r105 := ((r26 / r25)%R).
Definition f365 := Float2 (-18018798687219903) (-43).
Definition i261 := makepairF f365 f3.
Notation p432 := (BND r105 i261). (* BND(alpha * (1 / b) / (alpha * (1 / b) + inv_b_hat), [-2048.5, 1]) *)
Definition f366 := Float2 (-1) (65).
Definition i262 := makepairF f366 f3.
Notation p433 := (BND r105 i262). (* BND(alpha * (1 / b) / (alpha * (1 / b) + inv_b_hat), [-3.68935e+19, 1]) *)
Notation r107 := ((_inv_b_hat / r25)%R).
Notation r106 := ((r10 - r107)%R).
Notation p434 := (BND r106 i262). (* BND(1 - inv_b_hat / (alpha * (1 / b) + inv_b_hat), [-3.68935e+19, 1]) *)
Definition f367 := Float2 (1) (-65).
Definition f368 := Float2 (1) (65).
Definition i263 := makepairF f367 f368.
Notation p435 := (BND r107 i263). (* BND(inv_b_hat / (alpha * (1 / b) + inv_b_hat), [2.71051e-20, 3.68935e+19]) *)
Definition i264 := makepairF f367 f73.
Notation p436 := (BND r25 i264). (* BND(alpha * (1 / b) + inv_b_hat, [2.71051e-20, 2]) *)
Lemma t317 : p35 -> p436 -> p435.
 intros h0 h1.
 refine (div_pp _inv_b_hat r25 i16 i264 i263 h0 h1 _) ; finalize.
Qed.
Lemma l354 : s1 -> p435 (* BND(inv_b_hat / (alpha * (1 / b) + inv_b_hat), [2.71051e-20, 3.68935e+19]) *).
 intros h0.
 assert (h1 := l37 h0).
 assert (h2 := l75 h0).
 apply t317. exact h1. refine (subset r25 i35 i264 h2 _) ; finalize.
Qed.
Lemma t318 : p37 -> p435 -> p434.
 intros h0 h1.
 refine (sub r10 r107 i17 i263 i262 h0 h1 _) ; finalize.
Qed.
Lemma l353 : s1 -> p434 (* BND(1 - inv_b_hat / (alpha * (1 / b) + inv_b_hat), [-3.68935e+19, 1]) *).
 intros h0.
 assert (h1 := l39 h0).
 assert (h2 := l354 h0).
 apply t318. exact h1. exact h2.
Qed.
Lemma t319 : p224 -> p434 -> p433.
 intros h0 h1.
 refine (addf_2 _ _ i262 h0 h1) ; finalize.
Qed.
Lemma l352 : s1 -> p433 (* BND(alpha * (1 / b) / (alpha * (1 / b) + inv_b_hat), [-3.68935e+19, 1]) *).
 intros h0.
 assert (h1 := l195 h0).
 assert (h2 := l353 h0).
 apply t319. exact h1. exact h2.
Qed.
Definition f369 := Float2 (576601524159907841) (5).
Definition i265 := makepairF f365 f369.
Notation p437 := (BND r105 i265). (* BND(alpha * (1 / b) / (alpha * (1 / b) + inv_b_hat), [-2048.5, 1.84512e+19]) *)
Definition i266 := makepairF f54 f3.
Notation p438 := (BND r25 i266). (* BND(alpha * (1 / b) + inv_b_hat, [5.41969e-20, 1]) *)
Notation p439 := (BND r25 i155). (* BND(alpha * (1 / b) + inv_b_hat, [-1, 1]) *)
Notation p440 := (BND r26 i83). (* BND(alpha * (1 / b), [-1, 0.5]) *)
Definition i267 := makepairF f331 f10.
Notation p441 := (BND _inv_b_hat i267). (* BND(inv_b_hat, [2.1684e-19, 0.5]) *)
Lemma t320 : p440 -> p441 -> p439.
 intros h0 h1.
 refine (add r26 _inv_b_hat i83 i267 i155 h0 h1 _) ; finalize.
Qed.
Lemma l357 : p373 -> s1 -> p439 (* BND(alpha * (1 / b) + inv_b_hat, [-1, 1]) *).
 intros h0 h1.
 assert (h2 := l201 h1).
 assert (h3 := l332 h0 h1).
 apply t320. refine (subset r26 i130 i83 h2 _) ; finalize. refine (subset _inv_b_hat i236 i267 h3 _) ; finalize.
Qed.
Lemma l356 : p373 -> s1 -> p438 (* BND(alpha * (1 / b) + inv_b_hat, [5.41969e-20, 1]) *).
 intros h0 h1.
 assert (h2 := l357 h0 h1).
 assert (h3 := l75 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Definition f370 := Float2 (-36028799132893311) (-108).
Definition i268 := makepairF f370 f3.
Notation p442 := (BND r26 i268). (* BND(alpha * (1 / b), [-1.11022e-16, 1]) *)
Lemma t321 : p442 -> p438 -> p437.
 intros h0 h1.
 refine (div_op r26 r25 i268 i266 i265 h0 h1 _) ; finalize.
Qed.
Lemma l355 : p373 -> s1 -> p437 (* BND(alpha * (1 / b) / (alpha * (1 / b) + inv_b_hat), [-2048.5, 1.84512e+19]) *).
 intros h0 h1.
 assert (h2 := l347 h0 h1).
 assert (h3 := l356 h0 h1).
 apply t321. refine (subset r26 i257 i268 h2 _) ; finalize. exact h3.
Qed.
Lemma l351 : p373 -> s1 -> p432 (* BND(alpha * (1 / b) / (alpha * (1 / b) + inv_b_hat), [-2048.5, 1]) *).
 intros h0 h1.
 assert (h2 := l352 h1).
 assert (h3 := l355 h0 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Definition f371 := Float2 (18014399583223873) (-77).
Definition i269 := makepairF f199 f371.
Notation p443 := (REL r36 r26 i269). (* REL(alpha * inv_b_hat, alpha * (1 / b), [-7.62939e-06, 1.19209e-07]) *)
Lemma t322 : p443 -> p431 -> p432 -> p224 -> p429.
 intros h0 h1 h2 h3.
 refine (add_rr r36 r26 _inv_b_hat _inv_b_hat i269 i12 i261 i260 h0 h1 h2 h3 _) ; finalize.
Qed.
Lemma l348 : p373 -> s1 -> p429 (* REL(alpha * inv_b_hat + inv_b_hat, alpha * (1 / b) + inv_b_hat, [-0.0002442, 0.03125]) *).
 intros h0 h1.
 assert (h2 := l349 h1).
 assert (h3 := l350 h1).
 assert (h4 := l351 h0 h1).
 assert (h5 := l195 h1).
 apply t322. refine (rel_subset r36 r26 i32 i269 h2 _) ; finalize. exact h3. exact h4. exact h5.
Qed.
Lemma t323 : p424 -> p429 -> p423.
 intros h0 h1.
 refine (bnd_of_bnd_rel_p r35 r25 i255 i260 i254 h0 h1 _) ; finalize.
Qed.
Lemma l344 : p373 -> s1 -> p423 (* BND(alpha * inv_b_hat + inv_b_hat, [5.41836e-20, 1.04774e-09]) *).
 intros h0 h1.
 assert (h2 := l345 h0 h1).
 assert (h3 := l348 h0 h1).
 apply t323. exact h2. exact h3.
Qed.
Definition f372 := Float2 (-5) (93).
Definition i270 := makepairF f372 f2.
Notation p444 := (BND _r0 i270). (* BND(r0, [-4.95176e+28, 1.84467e+19]) *)
Definition i271 := makepairF f367 f358.
Notation p445 := (BND r35 i271). (* BND(alpha * inv_b_hat + inv_b_hat, [2.71051e-20, 1.04774e-09]) *)
Lemma t324 : p444 -> p445 -> p422.
 intros h0 h1.
 refine (mul_op _r0 r35 i270 i271 i253 h0 h1 _) ; finalize.
Qed.
Lemma l343 : p373 -> s1 -> p422 (* BND(r0 * (alpha * inv_b_hat + inv_b_hat), [-5.53402e+19, 2.57698e+10]) *).
 intros h0 h1.
 assert (h2 := l317 h0 h1).
 assert (h3 := l344 h0 h1).
 apply t324. refine (subset _r0 i228 i270 h2 _) ; finalize. refine (subset r35 i254 i271 h3 _) ; finalize.
Qed.
Lemma t325 : p386 -> p422 -> p385.
 intros h0 h1.
 refine (add r102 r103 i227 i253 i226 h0 h1 _) ; finalize.
Qed.
Lemma l315 : p373 -> s1 -> p385 (* BND(r0 * (inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat)) + r0 * (alpha * inv_b_hat + inv_b_hat), [-7.3787e+19, 3.43597e+10]) *).
 intros h0 h1.
 assert (h2 := l316 h0 h1).
 assert (h3 := l343 h0 h1).
 apply t325. exact h2. exact h3.
Qed.
Lemma t326 : p385 -> p384.
 intros h0.
 refine (mul_xars _ _ _ i226 h0) ; finalize.
Qed.
Lemma l314 : p373 -> s1 -> p384 (* BND(r0 * inv_b_hat1, [-7.3787e+19, 3.43597e+10]) *).
 intros h0 h1.
 assert (h2 := l315 h0 h1).
 apply t326. exact h2.
Qed.
Definition f373 := Float2 (1) (56).
Definition i272 := makepairF f320 f373.
Notation p446 := (BND r33 i272). (* BND(r0 * inv_b_hat1, [-7.20576e+16, 7.20576e+16]) *)
Notation r108 := ((_quotient_hat1 - r39)%R).
Notation p447 := (BND r108 i272). (* BND(quotient_hat1 - (quotient_hat1 - r0 * inv_b_hat1), [-7.20576e+16, 7.20576e+16]) *)
Definition f374 := Float2 (-163825043549509329) (-2).
Definition f375 := Float2 (3) (54).
Definition i273 := makepairF f374 f375.
Notation p448 := (BND _quotient_hat1 i273). (* BND(quotient_hat1, [-4.09563e+16, 5.40432e+16]) *)
Notation p449 := (BND r81 i273). (* BND(r0 / b + (quotient_hat1 - r0 / b), [-4.09563e+16, 5.40432e+16]) *)
Definition f376 := Float2 (-1) (35).
Definition f377 := Float2 (1) (34).
Definition i274 := makepairF f376 f377.
Notation p450 := (BND r37 i274). (* BND(r0 / b, [-3.43597e+10, 1.71799e+10]) *)
Notation p451 := (BND r47 i274). (* BND((r0 - (a / b - q0) * b) / b + (a / b - q0) * b / b, [-3.43597e+10, 1.71799e+10]) *)
Notation p452 := (BND r50 i274). (* BND((a / b - q0) * b / b, [-3.43597e+10, 1.71799e+10]) *)
Definition f378 := Float2 (-1152921503533105155) (12).
Definition f379 := Float2 (1152921503533105155) (-26).
Definition i275 := makepairF f378 f379.
Notation p453 := (BND r50 i275). (* BND((a / b - q0) * b / b, [-4.72237e+21, 1.71799e+10]) *)
Definition f380 := Float2 (-1) (102).
Definition i276 := makepairF f380 f2.
Notation p454 := (BND r12 i276). (* BND((a / b - q0) * b, [-5.0706e+30, 1.84467e+19]) *)
Lemma t327 : p454 -> p373 -> p453.
 intros h0 h1.
 refine (div_op r12 _b i276 i219 i275 h0 h1 _) ; finalize.
Qed.
Lemma l365 : p373 -> s1 -> p453 (* BND((a / b - q0) * b / b, [-4.72237e+21, 1.71799e+10]) *).
 intros h0 h1.
 assert (h2 := l318 h0 h1).
 assert (h3 := l334 h0 h1).
 apply t327. refine (subset r12 i228 i276 h2 _) ; finalize. exact h3.
Qed.
Definition i277 := makepairF f376 f319.
Notation p455 := (BND r50 i277). (* BND((a / b - q0) * b / b, [-3.43597e+10, 3.43597e+10]) *)
Notation p456 := (BND r13 i277). (* BND(a / b - q0, [-3.43597e+10, 3.43597e+10]) *)
Lemma t328 : p10 -> p456 -> p455.
 intros h0 h1.
 refine (div_fir r13 _b i277 h0 h1) ; finalize.
Qed.
Lemma l366 : p373 -> s1 -> p455 (* BND((a / b - q0) * b / b, [-3.43597e+10, 3.43597e+10]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l323 h0 h1).
 apply t328. exact h2. refine (subset r13 i230 i277 h3 _) ; finalize.
Qed.
Lemma l364 : p373 -> s1 -> p452 (* BND((a / b - q0) * b / b, [-3.43597e+10, 1.71799e+10]) *).
 intros h0 h1.
 assert (h2 := l365 h0 h1).
 assert (h3 := l366 h0 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Lemma t329 : p20 -> p452 -> p451.
 intros h0 h1.
 refine (add r48 r50 i12 i274 i274 h0 h1 _) ; finalize.
Qed.
Lemma l363 : p373 -> s1 -> p451 (* BND((r0 - (a / b - q0) * b) / b + (a / b - q0) * b / b, [-3.43597e+10, 1.71799e+10]) *).
 intros h0 h1.
 assert (h2 := l21 h1).
 assert (h3 := l364 h0 h1).
 apply t329. exact h2. exact h3.
Qed.
Lemma t330 : p10 -> p451 -> p450.
 intros h0 h1.
 refine (div_xals _ _ _b i274 h0 h1) ; finalize.
Qed.
Lemma l362 : p373 -> s1 -> p450 (* BND(r0 / b, [-3.43597e+10, 1.71799e+10]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l363 h0 h1).
 apply t330. exact h2. exact h3.
Qed.
Definition f381 := Float2 (-13) (45).
Definition f382 := Float2 (5) (53).
Definition i278 := makepairF f381 f382.
Notation p457 := (BND r31 i278). (* BND(quotient_hat1 - r0 / b, [-4.57397e+14, 4.5036e+16]) *)
Notation p458 := (BND r82 i278). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b) + (quotient_hat1 - r0 / b - (quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b))), [-4.57397e+14, 4.5036e+16]) *)
Definition i279 := makepairF f381 f159.
Notation p459 := (BND r38 i279). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-4.57397e+14, 5.6295e+14]) *)
Definition f383 := Float2 (-141871636611073) (-51).
Definition f384 := Float2 (9079785817112767) (-57).
Definition i280 := makepairF f383 f384.
Notation p460 := (BND r40 i280). (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.0630037, 0.0630037]) *)
Definition f385 := Float2 (-290482210325138403) (-134).
Definition f386 := Float2 (145241087982699009) (-133).
Definition i281 := makepairF f385 f386.
Notation p461 := (BND r41 i281). (* BND(inv_b_hat1 - 1 / b, [-1.33383e-23, 1.33383e-23]) *)
Notation p462 := (BND r95 i281). (* BND(inv_b_hat1 - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.33383e-23, 1.33383e-23]) *)
Definition f387 := Float2 (-290482210056702947) (-134).
Definition f388 := Float2 (145241087848481281) (-133).
Definition i282 := makepairF f387 f388.
Notation p463 := (BND r96 i282). (* BND(inv_b_hat1 - (alpha * (1 / b) + inv_b_hat), [-1.33383e-23, 1.33383e-23]) *)
Notation p464 := (BND r97 i282). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat)), [-1.33383e-23, 1.33383e-23]) *)
Definition f389 := Float2 (-288230410243017699) (-134).
Definition f390 := Float2 (144115187941638657) (-133).
Definition i283 := makepairF f389 f390.
Notation p465 := (BND r69 i283). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-1.32349e-23, 1.32349e-23]) *)
Notation p466 := (BND r60 i283). (* BND(alpha * inv_b_hat - alpha * (1 / b), [-1.32349e-23, 1.32349e-23]) *)
Lemma t331 : p430 -> p426 -> p466.
 intros h0 h1.
 refine (error_of_rel_oo r36 r26 i32 i257 i283 h0 h1 _) ; finalize.
Qed.
Lemma l376 : p373 -> s1 -> p466 (* BND(alpha * inv_b_hat - alpha * (1 / b), [-1.32349e-23, 1.32349e-23]) *).
 intros h0 h1.
 assert (h2 := l349 h1).
 assert (h3 := l347 h0 h1).
 apply t331. exact h2. exact h3.
Qed.
Lemma t332 : p466 -> p465.
 intros h0.
 refine (add_firs _ _ _ i283 h0) ; finalize.
Qed.
Lemma l375 : p373 -> s1 -> p465 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-1.32349e-23, 1.32349e-23]) *).
 intros h0 h1.
 assert (h2 := l376 h0 h1).
 apply t332. exact h2.
Qed.
Lemma t333 : p413 -> p465 -> p464.
 intros h0 h1.
 refine (add r66 r69 i244 i283 i282 h0 h1 _) ; finalize.
Qed.
Lemma l374 : p373 -> s1 -> p464 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat)), [-1.33383e-23, 1.33383e-23]) *).
 intros h0 h1.
 assert (h2 := l339 h0 h1).
 assert (h3 := l375 h0 h1).
 apply t333. exact h2. exact h3.
Qed.
Lemma t334 : p464 -> p463.
 intros h0.
 refine (sub_xals _ _ _ i282 h0) ; finalize.
Qed.
Lemma l373 : p373 -> s1 -> p463 (* BND(inv_b_hat1 - (alpha * (1 / b) + inv_b_hat), [-1.33383e-23, 1.33383e-23]) *).
 intros h0 h1.
 assert (h2 := l374 h0 h1).
 apply t334. exact h2.
Qed.
Definition f391 := Float2 (-1) (-106).
Definition f392 := Float2 (1) (-106).
Definition i284 := makepairF f391 f392.
Notation p467 := (BND r24 i284). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-1.2326e-32, 1.2326e-32]) *)
Definition f393 := Float2 (1) (-30).
Definition i285 := makepairF f339 f393.
Notation p468 := (BND r19 i285). (* BND(1 / b, [4.33681e-19, 9.31323e-10]) *)
Lemma t335 : p170 -> p468 -> p467.
 intros h0 h1.
 refine (error_of_rel_op r25 r19 i36 i285 i284 h0 h1 _) ; finalize.
Qed.
Lemma l377 : p373 -> s1 -> p467 (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-1.2326e-32, 1.2326e-32]) *).
 intros h0 h1.
 assert (h2 := l146 h1).
 assert (h3 := l338 h0 h1).
 apply t335. exact h2. refine (subset r19 i240 i285 h3 _) ; finalize.
Qed.
Lemma t336 : p463 -> p467 -> p462.
 intros h0 h1.
 refine (add r96 r24 i282 i284 i281 h0 h1 _) ; finalize.
Qed.
Lemma l372 : p373 -> s1 -> p462 (* BND(inv_b_hat1 - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-1.33383e-23, 1.33383e-23]) *).
 intros h0 h1.
 assert (h2 := l373 h0 h1).
 assert (h3 := l377 h0 h1).
 apply t336. exact h2. exact h3.
Qed.
Lemma t337 : p462 -> p461.
 intros h0.
 refine (sub_xals _ _ _ i281 h0) ; finalize.
Qed.
Lemma l371 : p373 -> s1 -> p461 (* BND(inv_b_hat1 - 1 / b, [-1.33383e-23, 1.33383e-23]) *).
 intros h0 h1.
 assert (h2 := l372 h0 h1).
 apply t337. exact h2.
Qed.
Lemma t338 : p387 -> p461 -> p460.
 intros h0 h1.
 refine (mul_oo _r0 r41 i228 i281 i280 h0 h1 _) ; finalize.
Qed.
Lemma l370 : p373 -> s1 -> p460 (* BND(r0 * (inv_b_hat1 - 1 / b), [-0.0630037, 0.0630037]) *).
 intros h0 h1.
 assert (h2 := l317 h0 h1).
 assert (h3 := l371 h0 h1).
 apply t338. exact h2. exact h3.
Qed.
Definition f394 := Float2 (-25) (44).
Definition i286 := makepairF f394 f317.
Notation p469 := (BND r40 i286). (* BND(r0 * (inv_b_hat1 - 1 / b), [-4.39805e+14, 4.92581e+14]) *)
Lemma t339 : p321 -> p469 -> p459.
 intros h0 h1.
 refine (add r39 r40 i184 i286 i279 h0 h1 _) ; finalize.
Qed.
Lemma l369 : p373 -> s1 -> p459 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-4.57397e+14, 5.6295e+14]) *).
 intros h0 h1.
 assert (h2 := l265 h1).
 assert (h3 := l370 h0 h1).
 apply t339. exact h2. refine (subset r40 i280 i286 h3 _) ; finalize.
Qed.
Lemma t340 : p459 -> p267 -> p458.
 intros h0 h1.
 refine (add r38 r83 i279 i12 i278 h0 h1 _) ; finalize.
Qed.
Lemma l368 : p373 -> s1 -> p458 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b) + (quotient_hat1 - r0 / b - (quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b))), [-4.57397e+14, 4.5036e+16]) *).
 intros h0 h1.
 assert (h2 := l369 h0 h1).
 assert (h3 := l220 h1).
 apply t340. exact h2. exact h3.
Qed.
Lemma t341 : p458 -> p457.
 intros h0.
 refine (val_xabs _ r31 i278 h0) ; finalize.
Qed.
Lemma l367 : p373 -> s1 -> p457 (* BND(quotient_hat1 - r0 / b, [-4.57397e+14, 4.5036e+16]) *).
 intros h0 h1.
 assert (h2 := l368 h0 h1).
 apply t341. exact h2.
Qed.
Definition f395 := Float2 (-26388280131593) (-4).
Definition i287 := makepairF f395 f377.
Notation p470 := (BND r37 i287). (* BND(r0 / b, [-1.64927e+12, 1.71799e+10]) *)
Definition f396 := Float2 (-655273785917905723) (-4).
Definition i288 := makepairF f396 f382.
Notation p471 := (BND r31 i288). (* BND(quotient_hat1 - r0 / b, [-4.09546e+16, 4.5036e+16]) *)
Lemma t342 : p470 -> p471 -> p449.
 intros h0 h1.
 refine (add r37 r31 i287 i288 i273 h0 h1 _) ; finalize.
Qed.
Lemma l361 : p373 -> s1 -> p449 (* BND(r0 / b + (quotient_hat1 - r0 / b), [-4.09563e+16, 5.40432e+16]) *).
 intros h0 h1.
 assert (h2 := l362 h0 h1).
 assert (h3 := l367 h0 h1).
 apply t342. refine (subset r37 i274 i287 h2 _) ; finalize. refine (subset r31 i278 i288 h3 _) ; finalize.
Qed.
Lemma t343 : p449 -> p448.
 intros h0.
 refine (val_xabs _ _quotient_hat1 i273 h0) ; finalize.
Qed.
Lemma l360 : p373 -> s1 -> p448 (* BND(quotient_hat1, [-4.09563e+16, 5.40432e+16]) *).
 intros h0 h1.
 assert (h2 := l361 h0 h1).
 apply t343. exact h2.
Qed.
Definition f397 := Float2 (-3) (54).
Definition i289 := makepairF f397 f375.
Notation p472 := (BND _quotient_hat1 i289). (* BND(quotient_hat1, [-5.40432e+16, 5.40432e+16]) *)
Lemma t344 : p472 -> p321 -> p447.
 intros h0 h1.
 refine (sub _quotient_hat1 r39 i289 i184 i272 h0 h1 _) ; finalize.
Qed.
Lemma l359 : p373 -> s1 -> p447 (* BND(quotient_hat1 - (quotient_hat1 - r0 * inv_b_hat1), [-7.20576e+16, 7.20576e+16]) *).
 intros h0 h1.
 assert (h2 := l360 h0 h1).
 assert (h3 := l265 h1).
 apply t344. refine (subset _quotient_hat1 i273 i289 h2 _) ; finalize. exact h3.
Qed.
Lemma t345 : p447 -> p446.
 intros h0.
 refine (val_xebs r33 _ i272 h0) ; finalize.
Qed.
Lemma l358 : p373 -> s1 -> p446 (* BND(r0 * inv_b_hat1, [-7.20576e+16, 7.20576e+16]) *).
 intros h0 h1.
 assert (h2 := l359 h0 h1).
 apply t345. exact h2.
Qed.
Lemma l313 : p373 -> s1 -> p383 (* BND(r0 * inv_b_hat1, [-7.20576e+16, 3.43597e+10]) *).
 intros h0 h1.
 assert (h2 := l314 h0 h1).
 assert (h3 := l358 h0 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Lemma t346 : p383 -> p382.
 intros h0.
 refine (float_round_ne _ _ r33 i225 i225 h0 _) ; finalize.
Qed.
Lemma l312 : p373 -> s1 -> p382 (* BND(quotient_hat1, [-7.20576e+16, 3.43597e+10]) *).
 intros h0 h1.
 assert (h2 := l313 h0 h1).
 apply t346. exact h2.
Qed.
Definition i290 := makepairF f318 f15.
Notation p473 := (BND _quotient_hat1 i290). (* BND(quotient_hat1, [-4.92581e+14, 8.79609e+12]) *)
Notation p474 := (BND r81 i290). (* BND(r0 / b + (quotient_hat1 - r0 / b), [-4.92581e+14, 8.79609e+12]) *)
Definition f398 := Float2 (3) (41).
Definition i291 := makepairF f381 f398.
Notation p475 := (BND r31 i291). (* BND(quotient_hat1 - r0 / b, [-4.57397e+14, 6.59707e+12]) *)
Definition f399 := Float2 (-4194305) (34).
Definition i292 := makepairF f399 f398.
Notation p476 := (BND r31 i292). (* BND(quotient_hat1 - r0 / b, [-7.20576e+16, 6.59707e+12]) *)
Definition f400 := Float2 (1) (42).
Definition i293 := makepairF f320 f400.
Notation p477 := (BND _quotient_hat1 i293). (* BND(quotient_hat1, [-7.20576e+16, 4.39805e+12]) *)
Definition f401 := Float2 (-1) (59).
Definition i294 := makepairF f401 f400.
Notation p478 := (BND _quotient_hat1 i294). (* BND(quotient_hat1, [-5.76461e+17, 4.39805e+12]) *)
Definition f402 := Float2 (-10379441932932229) (5).
Definition i295 := makepairF f402 f400.
Notation p479 := (BND r33 i295). (* BND(r0 * inv_b_hat1, [-3.32142e+17, 4.39805e+12]) *)
Definition i296 := makepairF f321 f400.
Notation p480 := (BND r33 i296). (* BND(r0 * inv_b_hat1, [-7.3787e+19, 4.39805e+12]) *)
Notation p481 := (BND r101 i296). (* BND(r0 * (inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat)) + r0 * (alpha * inv_b_hat + inv_b_hat), [-7.3787e+19, 4.39805e+12]) *)
Definition f403 := Float2 (-1) (26).
Definition f404 := Float2 (1) (26).
Definition i297 := makepairF f403 f404.
Notation p482 := (BND r102 i297). (* BND(r0 * (inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat)), [-6.71089e+07, 6.71089e+07]) *)
Notation p483 := (BND r66 i36). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.32349e-23, 1.32349e-23]) *)
Notation p484 := (ABS r35 i37). (* ABS(alpha * inv_b_hat + inv_b_hat, [0, 2.38419e-07]) *)
Lemma t347 : p484 -> p483.
 intros h0.
 refine (float_absolute_wide_ne _ _ r35 i37 i36 h0 _) ; finalize.
Qed.
Lemma l388 : p373 -> s1 -> p483 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-1.32349e-23, 1.32349e-23]) *).
 intros h0 h1.
 assert (h2 := l340 h0 h1).
 apply t347. refine (abs_subset r35 i245 i37 h2 _) ; finalize.
Qed.
Notation p485 := (BND _r0 i276). (* BND(r0, [-5.0706e+30, 1.84467e+19]) *)
Lemma t348 : p485 -> p483 -> p482.
 intros h0 h1.
 refine (mul_oo _r0 r66 i276 i36 i297 h0 h1 _) ; finalize.
Qed.
Lemma l387 : p373 -> s1 -> p482 (* BND(r0 * (inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat)), [-6.71089e+07, 6.71089e+07]) *).
 intros h0 h1.
 assert (h2 := l317 h0 h1).
 assert (h3 := l388 h0 h1).
 apply t348. refine (subset _r0 i228 i276 h2 _) ; finalize. exact h3.
Qed.
Definition f405 := Float2 (511) (33).
Definition i298 := makepairF f355 f405.
Notation p486 := (BND r103 i298). (* BND(r0 * (alpha * inv_b_hat + inv_b_hat), [-5.53402e+19, 4.38946e+12]) *)
Definition f406 := Float2 (-513) (86).
Definition i299 := makepairF f406 f2.
Notation p487 := (BND _r0 i299). (* BND(r0, [-3.96915e+28, 1.84467e+19]) *)
Definition f407 := Float2 (5) (-32).
Definition i300 := makepairF f345 f407.
Notation p488 := (BND r35 i300). (* BND(alpha * inv_b_hat + inv_b_hat, [-1.10589e-16, 1.16415e-09]) *)
Lemma t349 : p487 -> p488 -> p486.
 intros h0 h1.
 refine (mul_oo _r0 r35 i299 i300 i298 h0 h1 _) ; finalize.
Qed.
Lemma l389 : p373 -> s1 -> p486 (* BND(r0 * (alpha * inv_b_hat + inv_b_hat), [-5.53402e+19, 4.38946e+12]) *).
 intros h0 h1.
 assert (h2 := l317 h0 h1).
 assert (h3 := l341 h0 h1).
 apply t349. refine (subset _r0 i228 i299 h2 _) ; finalize. refine (subset r35 i246 i300 h3 _) ; finalize.
Qed.
Lemma t350 : p482 -> p486 -> p481.
 intros h0 h1.
 refine (add r102 r103 i297 i298 i296 h0 h1 _) ; finalize.
Qed.
Lemma l386 : p373 -> s1 -> p481 (* BND(r0 * (inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat)) + r0 * (alpha * inv_b_hat + inv_b_hat), [-7.3787e+19, 4.39805e+12]) *).
 intros h0 h1.
 assert (h2 := l387 h0 h1).
 assert (h3 := l389 h0 h1).
 apply t350. exact h2. exact h3.
Qed.
Lemma t351 : p481 -> p480.
 intros h0.
 refine (mul_xars _ _ _ i296 h0) ; finalize.
Qed.
Lemma l385 : p373 -> s1 -> p480 (* BND(r0 * inv_b_hat1, [-7.3787e+19, 4.39805e+12]) *).
 intros h0 h1.
 assert (h2 := l386 h0 h1).
 apply t351. exact h2.
Qed.
Definition f408 := Float2 (-664284283707662653) (-1).
Definition f409 := Float2 (1) (59).
Definition i301 := makepairF f408 f409.
Notation p489 := (BND r33 i301). (* BND(r0 * inv_b_hat1, [-3.32142e+17, 5.76461e+17]) *)
Notation p490 := (BND r108 i301). (* BND(quotient_hat1 - (quotient_hat1 - r0 * inv_b_hat1), [-3.32142e+17, 5.76461e+17]) *)
Lemma t352 : p323 -> p327 -> p490.
 intros h0 h1.
 refine (sub _quotient_hat1 r39 i186 i188 i301 h0 h1 _) ; finalize.
Qed.
Lemma l391 : s1 -> p490 (* BND(quotient_hat1 - (quotient_hat1 - r0 * inv_b_hat1), [-3.32142e+17, 5.76461e+17]) *).
 intros h0.
 assert (h1 := l267 h0).
 assert (h2 := l271 h0).
 apply t352. exact h1. exact h2.
Qed.
Lemma t353 : p490 -> p489.
 intros h0.
 refine (val_xebs r33 _ i301 h0) ; finalize.
Qed.
Lemma l390 : s1 -> p489 (* BND(r0 * inv_b_hat1, [-3.32142e+17, 5.76461e+17]) *).
 intros h0.
 assert (h1 := l391 h0).
 apply t353. exact h1.
Qed.
Lemma l384 : p373 -> s1 -> p479 (* BND(r0 * inv_b_hat1, [-3.32142e+17, 4.39805e+12]) *).
 intros h0 h1.
 assert (h2 := l385 h0 h1).
 assert (h3 := l390 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Notation p491 := (BND r33 i294). (* BND(r0 * inv_b_hat1, [-5.76461e+17, 4.39805e+12]) *)
Lemma t354 : p491 -> p478.
 intros h0.
 refine (float_round_ne _ _ r33 i294 i294 h0 _) ; finalize.
Qed.
Lemma l383 : p373 -> s1 -> p478 (* BND(quotient_hat1, [-5.76461e+17, 4.39805e+12]) *).
 intros h0 h1.
 assert (h2 := l384 h0 h1).
 apply t354. refine (subset r33 i295 i294 h2 _) ; finalize.
Qed.
Lemma l382 : p373 -> s1 -> p477 (* BND(quotient_hat1, [-7.20576e+16, 4.39805e+12]) *).
 intros h0 h1.
 assert (h2 := l383 h0 h1).
 assert (h3 := l360 h0 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Lemma t355 : p477 -> p450 -> p476.
 intros h0 h1.
 refine (sub _quotient_hat1 r37 i293 i274 i292 h0 h1 _) ; finalize.
Qed.
Lemma l381 : p373 -> s1 -> p476 (* BND(quotient_hat1 - r0 / b, [-7.20576e+16, 6.59707e+12]) *).
 intros h0 h1.
 assert (h2 := l382 h0 h1).
 assert (h3 := l362 h0 h1).
 apply t355. exact h2. exact h3.
Qed.
Lemma l380 : p373 -> s1 -> p475 (* BND(quotient_hat1 - r0 / b, [-4.57397e+14, 6.59707e+12]) *).
 intros h0 h1.
 assert (h2 := l381 h0 h1).
 assert (h3 := l367 h0 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Lemma t356 : p450 -> p475 -> p474.
 intros h0 h1.
 refine (add r37 r31 i274 i291 i290 h0 h1 _) ; finalize.
Qed.
Lemma l379 : p373 -> s1 -> p474 (* BND(r0 / b + (quotient_hat1 - r0 / b), [-4.92581e+14, 8.79609e+12]) *).
 intros h0 h1.
 assert (h2 := l362 h0 h1).
 assert (h3 := l380 h0 h1).
 apply t356. exact h2. exact h3.
Qed.
Lemma t357 : p474 -> p473.
 intros h0.
 refine (val_xabs _ _quotient_hat1 i290 h0) ; finalize.
Qed.
Lemma l378 : p373 -> s1 -> p473 (* BND(quotient_hat1, [-4.92581e+14, 8.79609e+12]) *).
 intros h0 h1.
 assert (h2 := l379 h0 h1).
 apply t357. exact h2.
Qed.
Lemma l311 : p373 -> s1 -> p381 (* BND(quotient_hat1, [-4.92581e+14, 3.43597e+10]) *).
 intros h0 h1.
 assert (h2 := l312 h0 h1).
 assert (h3 := l378 h0 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Lemma t358 : p381 -> p380.
 intros h0.
 refine (abs_of_bnd_o _quotient_hat1 i224 i223 h0 _) ; finalize.
Qed.
Lemma l310 : p373 -> s1 -> p380 (* ABS(quotient_hat1, [0, 4.92581e+14]) *).
 intros h0 h1.
 assert (h2 := l311 h0 h1).
 apply t358. exact h2.
Qed.
Lemma t359 : p380 -> p379.
 intros h0.
 refine (float_absolute_inv_ne _ _ _ i223 i222 h0 _) ; finalize.
Qed.
Lemma l309 : p373 -> s1 -> p379 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-0.03125, 0.03125]) *).
 intros h0 h1.
 assert (h2 := l310 h0 h1).
 apply t359. exact h2.
Qed.
Lemma t360 : p379 -> p460 -> p378.
 intros h0 h1.
 refine (add r39 r40 i222 i280 i221 h0 h1 _) ; finalize.
Qed.
Lemma l308 : p373 -> s1 -> p378 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-0.0942537, 0.0942537]) *).
 intros h0 h1.
 assert (h2 := l309 h0 h1).
 assert (h3 := l370 h0 h1).
 apply t360. exact h2. exact h3.
Qed.
Lemma t361 : p378 -> p267 -> p377.
 intros h0 h1.
 refine (add r38 r83 i221 i12 i221 h0 h1 _) ; finalize.
Qed.
Lemma l307 : p373 -> s1 -> p377 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b) + (quotient_hat1 - r0 / b - (quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b))), [-0.0942537, 0.0942537]) *).
 intros h0 h1.
 assert (h2 := l308 h0 h1).
 assert (h3 := l220 h1).
 apply t361. exact h2. exact h3.
Qed.
Lemma t362 : p377 -> p376.
 intros h0.
 refine (val_xabs _ r31 i221 h0) ; finalize.
Qed.
Lemma l306 : p373 -> s1 -> p376 (* BND(quotient_hat1 - r0 / b, [-0.0942537, 0.0942537]) *).
 intros h0 h1.
 assert (h2 := l307 h0 h1).
 apply t362. exact h2.
Qed.
Notation p492 := (BND r100 i5). (* BND(q1 - r0 / b - (quotient_hat1 - r0 / b), [-0.5, 0.5]) *)
Lemma t363 : p7 -> p492.
 intros h0.
 refine (sub_firs _ _ _ i5 h0) ; finalize.
Qed.
Lemma l392 : s1 -> p492 (* BND(q1 - r0 / b - (quotient_hat1 - r0 / b), [-0.5, 0.5]) *).
 intros h0.
 assert (h1 := l6 h0).
 apply t363. exact h1.
Qed.
Lemma t364 : p376 -> p492 -> p375.
 intros h0 h1.
 refine (add r31 r100 i221 i5 i220 h0 h1 _) ; finalize.
Qed.
Lemma l305 : p373 -> s1 -> p375 (* BND(quotient_hat1 - r0 / b + (q1 - r0 / b - (quotient_hat1 - r0 / b)), [-0.594254, 0.594254]) *).
 intros h0 h1.
 assert (h2 := l306 h0 h1).
 assert (h3 := l392 h1).
 apply t364. exact h2. exact h3.
Qed.
Lemma t365 : p375 -> p374.
 intros h0.
 refine (val_xabs _ r42 i220 h0) ; finalize.
Qed.
Lemma l304 : p373 -> s1 -> p374 (* BND(q1 - r0 / b, [-0.594254, 0.594254]) *).
 intros h0 h1.
 assert (h2 := l305 h0 h1).
 apply t365. exact h2.
Qed.
Lemma l303 : p373 -> s1 -> False.
 intros h0 h1.
 assert (h2 := l3 h1).
 assert (h3 := l304 h0 h1).
 refine (simplify (Tatom false (Abnd 0%nat i3)) Tfalse (Abnd 0%nat i220) (List.cons r42 List.nil) h3 h2 _) ; finalize.
Qed.
Notation p493 := ((f311 <= _b)%R). (* BND(b, [2.30584e+18, inf]) *)
Definition f410 := Float2 (-576460752303490049) (-60).
Definition f411 := Float2 (576460752303490049) (-60).
Definition i302 := makepairF f410 f411.
Notation p494 := (BND r42 i302). (* BND(q1 - r0 / b, [-0.5, 0.5]) *)
Notation p495 := (BND r44 i302). (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.5, 0.5]) *)
Definition f412 := Float2 (-66561) (-60).
Definition f413 := Float2 (66561) (-60).
Definition i303 := makepairF f412 f413.
Notation p496 := (BND r31 i303). (* BND(quotient_hat1 - r0 / b, [-5.77325e-14, 5.77325e-14]) *)
Notation p497 := (BND r38 i303). (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-5.77325e-14, 5.77325e-14]) *)
Definition f414 := Float2 (-1) (-51).
Definition f415 := Float2 (1) (-51).
Definition i304 := makepairF f414 f415.
Notation p498 := (BND r39 i304). (* BND(quotient_hat1 - r0 * inv_b_hat1, [-4.44089e-16, 4.44089e-16]) *)
Definition f416 := Float2 (1) (3).
Definition i305 := makepairF f1 f416.
Notation p499 := (ABS r33 i305). (* ABS(r0 * inv_b_hat1, [0, 8]) *)
Definition f417 := Float2 (262145) (45).
Definition i306 := makepairF f1 f417.
Notation p500 := (ABS _r0 i306). (* ABS(r0, [0, 9.22341e+18]) *)
Notation p501 := (ABS r12 i306). (* ABS((a / b - q0) * b, [0, 9.22341e+18]) *)
Definition f418 := Float2 (262145) (-19).
Definition i307 := makepairF f1 f418.
Notation p502 := (ABS r13 i307). (* ABS(a / b - q0, [0, 0.500002]) *)
Definition f419 := Float2 (-262145) (-19).
Definition i308 := makepairF f419 f418.
Notation p503 := (BND r13 i308). (* BND(a / b - q0, [-0.500002, 0.500002]) *)
Notation p504 := (BND r84 i308). (* BND(a / b - a / b - (q0 - a / b), [-0.500002, 0.500002]) *)
Notation p505 := (BND r85 i308). (* BND(q0 - a / b, [-0.500002, 0.500002]) *)
Notation r110 := ((_q0 - r7)%R).
Notation r111 := ((r7 - r14)%R).
Notation r109 := ((r110 + r111)%R).
Notation p506 := (BND r109 i308). (* BND(q0 - a * inv_b_hat + (a * inv_b_hat - a / b), [-0.500002, 0.500002]) *)
Definition f420 := Float2 (-1048577) (-21).
Definition f421 := Float2 (1048577) (-21).
Definition i309 := makepairF f420 f421.
Notation p507 := (BND r110 i309). (* BND(q0 - a * inv_b_hat, [-0.5, 0.5]) *)
Notation r112 := ((r53 + r22)%R).
Notation p508 := (BND r112 i309). (* BND(q0 - quotient_hat0 + (quotient_hat0 - a * inv_b_hat), [-0.5, 0.5]) *)
Definition f422 := Float2 (1) (-23).
Definition i310 := makepairF f104 f422.
Notation p509 := (BND r22 i310). (* BND(quotient_hat0 - a * inv_b_hat, [-1.19209e-07, 1.19209e-07]) *)
Definition i311 := makepairF f1 f168.
Notation p510 := (ABS r7 i311). (* ABS(a * inv_b_hat, [0, 16]) *)
Definition f423 := Float2 (3) (-62).
Definition i312 := makepairF f27 f423.
Notation p511 := (ABS _inv_b_hat i312). (* ABS(inv_b_hat, [5.42101e-20, 6.50521e-19]) *)
Definition f424 := Float2 (5) (-63).
Definition i313 := makepairF f27 f424.
Notation p512 := (BND _inv_b_hat i313). (* BND(inv_b_hat, [5.42101e-20, 5.42101e-19]) *)
Definition i314 := makepairF f367 f424.
Notation p513 := (BND _inv_b_hat i314). (* BND(inv_b_hat, [2.71051e-20, 5.42101e-19]) *)
Notation p514 := (BND r72 i314). (* BND(b * inv_b_hat / b, [2.71051e-20, 5.42101e-19]) *)
Definition i315 := makepairF f311 f2.
Notation p515 := (BND _b i315). (* BND(b, [2.30584e+18, 1.84467e+19]) *)
Lemma l416 : p493 -> s1 -> p493 (* BND(b, [2.30584e+18, inf]) *).
 intros h0 h1.
 assert (h2 := h0).
 exact (h2).
Qed.
Lemma l415 : p493 -> s1 -> p515 (* BND(b, [2.30584e+18, 1.84467e+19]) *).
 intros h0 h1.
 assert (h2 := l11 h1).
 assert (h3 := l416 h0 h1).
 apply intersect_bh with (1 := h2) (2 := h3). finalize.
Qed.
Definition i316 := makepairF f10 f280.
Notation p516 := (BND r16 i316). (* BND(b * inv_b_hat, [0.5, 1.25]) *)
Lemma t366 : p516 -> p515 -> p514.
 intros h0 h1.
 refine (div_pp r16 _b i316 i315 i314 h0 h1 _) ; finalize.
Qed.
Lemma l414 : p493 -> s1 -> p514 (* BND(b * inv_b_hat / b, [2.71051e-20, 5.42101e-19]) *).
 intros h0 h1.
 assert (h2 := l121 h1).
 assert (h3 := l415 h0 h1).
 apply t366. refine (subset r16 i68 i316 h2 _) ; finalize. exact h3.
Qed.
Lemma t367 : p10 -> p514 -> p513.
 intros h0 h1.
 refine (mul_xiru _ _inv_b_hat i314 h0 h1) ; finalize.
Qed.
Lemma l413 : p493 -> s1 -> p513 (* BND(inv_b_hat, [2.71051e-20, 5.42101e-19]) *).
 intros h0 h1.
 assert (h2 := l9 h1).
 assert (h3 := l414 h0 h1).
 apply t367. exact h2. exact h3.
Qed.
Lemma l412 : p493 -> s1 -> p512 (* BND(inv_b_hat, [5.42101e-20, 5.42101e-19]) *).
 intros h0 h1.
 assert (h2 := l413 h0 h1).
 assert (h3 := l37 h1).
 apply intersect with (1 := h2) (2 := h3). finalize.
Qed.
Notation p517 := (BND _inv_b_hat i312). (* BND(inv_b_hat, [5.42101e-20, 6.50521e-19]) *)
Lemma t368 : p517 -> p511.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat i312 i312 h0 _) ; finalize.
Qed.
Lemma l411 : p493 -> s1 -> p511 (* ABS(inv_b_hat, [5.42101e-20, 6.50521e-19]) *).
 intros h0 h1.
 assert (h2 := l412 h0 h1).
 apply t368. refine (subset _inv_b_hat i313 i312 h2 _) ; finalize.
Qed.
Definition i317 := makepairF f27 f212.
Notation p518 := (ABS _inv_b_hat i317). (* ABS(inv_b_hat, [5.42101e-20, 8.67362e-19]) *)
Lemma t369 : p33 -> p518 -> p510.
 intros h0 h1.
 refine (mul_aa _a _inv_b_hat i1 i317 i311 h0 h1 _) ; finalize.
Qed.
Lemma l410 : p493 -> s1 -> p510 (* ABS(a * inv_b_hat, [0, 16]) *).
 intros h0 h1.
 assert (h2 := l34 h1).
 assert (h3 := l411 h0 h1).
 apply t369. exact h2. refine (abs_subset _inv_b_hat i312 i317 h3 _) ; finalize.
Qed.
Lemma t370 : p510 -> p509.
 intros h0.
 refine (float_absolute_wide_ne _ _ r7 i311 i310 h0 _) ; finalize.
Qed.
Lemma l409 : p493 -> s1 -> p509 (* BND(quotient_hat0 - a * inv_b_hat, [-1.19209e-07, 1.19209e-07]) *).
 intros h0 h1.
 assert (h2 := l410 h0 h1).
 apply t370. exact h2.
Qed.
Definition f425 := Float2 (-1) (-21).
Definition f426 := Float2 (1) (-21).
Definition i318 := makepairF f425 f426.
Notation p519 := (BND r22 i318). (* BND(quotient_hat0 - a * inv_b_hat, [-4.76837e-07, 4.76837e-07]) *)
Lemma t371 : p51 -> p519 -> p508.
 intros h0 h1.
 refine (add r53 r22 i5 i318 i309 h0 h1 _) ; finalize.
Qed.
Lemma l408 : p493 -> s1 -> p508 (* BND(q0 - quotient_hat0 + (quotient_hat0 - a * inv_b_hat), [-0.5, 0.5]) *).
 intros h0 h1.
 assert (h2 := l53 h1).
 assert (h3 := l409 h0 h1).
 apply t371. exact h2. refine (subset r22 i310 i318 h3 _) ; finalize.
Qed.
Lemma t372 : p508 -> p507.
 intros h0.
 refine (sub_xals _ _ _ i309 h0) ; finalize.
Qed.
Lemma l407 : p493 -> s1 -> p507 (* BND(q0 - a * inv_b_hat, [-0.5, 0.5]) *).
 intros h0 h1.
 assert (h2 := l408 h0 h1).
 apply t372. exact h2.
Qed.
Definition f427 := Float2 (-3) (-21).
Definition f428 := Float2 (3) (-21).
Definition i319 := makepairF f427 f428.
Notation p520 := (BND r111 i319). (* BND(a * inv_b_hat - a / b, [-1.43051e-06, 1.43051e-06]) *)
Notation r114 := ((r7 - _quotient_hat0)%R).
Notation r113 := ((r114 + r20)%R).
Notation p521 := (BND r113 i319). (* BND(a * inv_b_hat - quotient_hat0 + (quotient_hat0 - a / b), [-1.43051e-06, 1.43051e-06]) *)
Notation p522 := (BND r114 i38). (* BND(a * inv_b_hat - quotient_hat0, [-2.38419e-07, 2.38419e-07]) *)
Notation r116 := ((r7 - r7)%R).
Notation r115 := ((r116 - r22)%R).
Notation p523 := (BND r115 i38). (* BND(a * inv_b_hat - a * inv_b_hat - (quotient_hat0 - a * inv_b_hat), [-2.38419e-07, 2.38419e-07]) *)
Notation p524 := (BND r116 i12). (* BND(a * inv_b_hat - a * inv_b_hat, [0, 0]) *)
Lemma t373 : p524.
 refine (sub_refl _ i12 _) ; finalize.
Qed.
Lemma l421 : s1 -> p524 (* BND(a * inv_b_hat - a * inv_b_hat, [0, 0]) *).
 intros h0.
 apply t373.
Qed.
Notation p525 := (BND r22 i38). (* BND(quotient_hat0 - a * inv_b_hat, [-2.38419e-07, 2.38419e-07]) *)
Lemma t374 : p524 -> p525 -> p523.
 intros h0 h1.
 refine (sub r116 r22 i12 i38 i38 h0 h1 _) ; finalize.
Qed.
Lemma l420 : p493 -> s1 -> p523 (* BND(a * inv_b_hat - a * inv_b_hat - (quotient_hat0 - a * inv_b_hat), [-2.38419e-07, 2.38419e-07]) *).
 intros h0 h1.
 assert (h2 := l421 h1).
 assert (h3 := l409 h0 h1).
 apply t374. exact h2. refine (subset r22 i310 i38 h3 _) ; finalize.
Qed.
Lemma t375 : p523 -> p522.
 intros h0.
 refine (sub_xars _ _ _ i38 h0) ; finalize.
Qed.
Lemma l419 : p493 -> s1 -> p522 (* BND(a * inv_b_hat - quotient_hat0, [-2.38419e-07, 2.38419e-07]) *).
 intros h0 h1.
 assert (h2 := l420 h0 h1).
 apply t375. exact h2.
Qed.
Definition f429 := Float2 (-5) (-22).
Definition f430 := Float2 (5) (-22).
Definition i320 := makepairF f429 f430.
Notation p526 := (BND r20 i320). (* BND(quotient_hat0 - a / b, [-1.19209e-06, 1.19209e-06]) *)
Definition i321 := makepairF f1 f430.
Notation p527 := (ABS r20 i321). (* ABS(quotient_hat0 - a / b, [0, 1.19209e-06]) *)
Notation p528 := (ABS r21 i321). (* ABS(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [0, 1.19209e-06]) *)
Definition i322 := makepairF f1 f422.
Notation p529 := (ABS r22 i322). (* ABS(quotient_hat0 - a * inv_b_hat, [0, 1.19209e-07]) *)
Lemma t376 : p509 -> p529.
 intros h0.
 refine (abs_of_bnd_o r22 i310 i322 h0 _) ; finalize.
Qed.
Lemma l425 : p493 -> s1 -> p529 (* ABS(quotient_hat0 - a * inv_b_hat, [0, 1.19209e-07]) *).
 intros h0 h1.
 assert (h2 := l409 h0 h1).
 apply t376. exact h2.
Qed.
Definition f431 := Float2 (9) (-23).
Definition i323 := makepairF f1 f431.
Notation p530 := (ABS r23 i323). (* ABS(a * (inv_b_hat - 1 / b), [0, 1.07288e-06]) *)
Definition f432 := Float2 (9) (-87).
Definition i324 := makepairF f1 f432.
Notation p531 := (ABS r18 i324). (* ABS(inv_b_hat - 1 / b, [0, 5.81611e-26]) *)
Definition f433 := Float2 (-1) (-84).
Definition i325 := makepairF f433 f432.
Notation p532 := (BND r18 i325). (* BND(inv_b_hat - 1 / b, [-5.16988e-26, 5.81611e-26]) *)
Definition i326 := makepairF f27 f339.
Notation p533 := (BND r19 i326). (* BND(1 / b, [5.42101e-20, 4.33681e-19]) *)
Lemma t377 : p37 -> p515 -> p533.
 intros h0 h1.
 refine (div_pp r10 _b i17 i315 i326 h0 h1 _) ; finalize.
Qed.
Lemma l429 : p493 -> s1 -> p533 (* BND(1 / b, [5.42101e-20, 4.33681e-19]) *).
 intros h0 h1.
 assert (h2 := l39 h1).
 assert (h3 := l415 h0 h1).
 apply t377. exact h2. exact h3.
Qed.
Definition f434 := Float2 (9) (-26).
Definition i327 := makepairF f104 f434.
Notation p534 := (REL _inv_b_hat r19 i327). (* REL(inv_b_hat, 1 / b, [-1.19209e-07, 1.3411e-07]) *)
Lemma t378 : p534 -> p533 -> p532.
 intros h0 h1.
 refine (error_of_rel_op _inv_b_hat r19 i327 i326 i325 h0 h1 _) ; finalize.
Qed.
Lemma l428 : p493 -> s1 -> p532 (* BND(inv_b_hat - 1 / b, [-5.16988e-26, 5.81611e-26]) *).
 intros h0 h1.
 assert (h2 := l70 h1).
 assert (h3 := l429 h0 h1).
 apply t378. refine (rel_subset _inv_b_hat r19 i32 i327 h2 _) ; finalize. exact h3.
Qed.
Lemma t379 : p532 -> p531.
 intros h0.
 refine (abs_of_bnd_o r18 i325 i324 h0 _) ; finalize.
Qed.
Lemma l427 : p493 -> s1 -> p531 (* ABS(inv_b_hat - 1 / b, [0, 5.81611e-26]) *).
 intros h0 h1.
 assert (h2 := l428 h0 h1).
 apply t379. exact h2.
Qed.
Lemma t380 : p33 -> p531 -> p530.
 intros h0 h1.
 refine (mul_aa _a r18 i1 i324 i323 h0 h1 _) ; finalize.
Qed.
Lemma l426 : p493 -> s1 -> p530 (* ABS(a * (inv_b_hat - 1 / b), [0, 1.07288e-06]) *).
 intros h0 h1.
 assert (h2 := l34 h1).
 assert (h3 := l427 h0 h1).
 apply t380. exact h2. exact h3.
Qed.
Lemma t381 : p529 -> p530 -> p528.
 intros h0 h1.
 refine (add_aa_o r22 r23 i322 i323 i321 h0 h1 _) ; finalize.
Qed.
Lemma l424 : p493 -> s1 -> p528 (* ABS(quotient_hat0 - a * inv_b_hat + a * (inv_b_hat - 1 / b), [0, 1.19209e-06]) *).
 intros h0 h1.
 assert (h2 := l425 h0 h1).
 assert (h3 := l426 h0 h1).
 apply t381. exact h2. exact h3.
Qed.
Lemma t382 : p49 -> p528 -> p527.
 intros h0 h1.
 refine (abs_rewrite r20 r21 i321 h0 h1) ; finalize.
Qed.
Lemma l423 : p493 -> s1 -> p527 (* ABS(quotient_hat0 - a / b, [0, 1.19209e-06]) *).
 intros h0 h1.
 assert (h2 := l51 h1).
 assert (h3 := l424 h0 h1).
 apply t382. exact h2. exact h3.
Qed.
Lemma t383 : p527 -> p526.
 intros h0.
 refine (bnd_of_abs r20 i321 i320 h0 _) ; finalize.
Qed.
Lemma l422 : p493 -> s1 -> p526 (* BND(quotient_hat0 - a / b, [-1.19209e-06, 1.19209e-06]) *).
 intros h0 h1.
 assert (h2 := l423 h0 h1).
 apply t383. exact h2.
Qed.
Lemma t384 : p522 -> p526 -> p521.
 intros h0 h1.
 refine (add r114 r20 i38 i320 i319 h0 h1 _) ; finalize.
Qed.
Lemma l418 : p493 -> s1 -> p521 (* BND(a * inv_b_hat - quotient_hat0 + (quotient_hat0 - a / b), [-1.43051e-06, 1.43051e-06]) *).
 intros h0 h1.
 assert (h2 := l419 h0 h1).
 assert (h3 := l422 h0 h1).
 apply t384. exact h2. exact h3.
Qed.
Lemma t385 : p521 -> p520.
 intros h0.
 refine (sub_xals _ _ _ i319 h0) ; finalize.
Qed.
Lemma l417 : p493 -> s1 -> p520 (* BND(a * inv_b_hat - a / b, [-1.43051e-06, 1.43051e-06]) *).
 intros h0 h1.
 assert (h2 := l418 h0 h1).
 apply t385. exact h2.
Qed.
Lemma t386 : p507 -> p520 -> p506.
 intros h0 h1.
 refine (add r110 r111 i309 i319 i308 h0 h1 _) ; finalize.
Qed.
Lemma l406 : p493 -> s1 -> p506 (* BND(q0 - a * inv_b_hat + (a * inv_b_hat - a / b), [-0.500002, 0.500002]) *).
 intros h0 h1.
 assert (h2 := l407 h0 h1).
 assert (h3 := l417 h0 h1).
 apply t386. exact h2. exact h3.
Qed.
Lemma t387 : p506 -> p505.
 intros h0.
 refine (sub_xals _ _ _ i308 h0) ; finalize.
Qed.
Lemma l405 : p493 -> s1 -> p505 (* BND(q0 - a / b, [-0.500002, 0.500002]) *).
 intros h0 h1.
 assert (h2 := l406 h0 h1).
 apply t387. exact h2.
Qed.
Lemma t388 : p28 -> p505 -> p504.
 intros h0 h1.
 refine (sub r55 r85 i12 i308 i308 h0 h1 _) ; finalize.
Qed.
Lemma l404 : p493 -> s1 -> p504 (* BND(a / b - a / b - (q0 - a / b), [-0.500002, 0.500002]) *).
 intros h0 h1.
 assert (h2 := l29 h1).
 assert (h3 := l405 h0 h1).
 apply t388. exact h2. exact h3.
Qed.
Lemma t389 : p504 -> p503.
 intros h0.
 refine (sub_xars _ _ _ i308 h0) ; finalize.
Qed.
Lemma l403 : p493 -> s1 -> p503 (* BND(a / b - q0, [-0.500002, 0.500002]) *).
 intros h0 h1.
 assert (h2 := l404 h0 h1).
 apply t389. exact h2.
Qed.
Lemma t390 : p503 -> p502.
 intros h0.
 refine (abs_of_bnd_o r13 i308 i307 h0 _) ; finalize.
Qed.
Lemma l402 : p493 -> s1 -> p502 (* ABS(a / b - q0, [0, 0.500002]) *).
 intros h0 h1.
 assert (h2 := l403 h0 h1).
 apply t390. exact h2.
Qed.
Lemma t391 : p502 -> p11 -> p501.
 intros h0 h1.
 refine (mul_aa r13 _b i307 i2 i306 h0 h1 _) ; finalize.
Qed.
Lemma l401 : p493 -> s1 -> p501 (* ABS((a / b - q0) * b, [0, 9.22341e+18]) *).
 intros h0 h1.
 assert (h2 := l402 h0 h1).
 assert (h3 := l10 h1).
 apply t391. exact h2. exact h3.
Qed.
Lemma t392 : p22 -> p501 -> p500.
 intros h0 h1.
 refine (abs_rewrite _r0 r12 i306 h0 h1) ; finalize.
Qed.
Lemma l400 : p493 -> s1 -> p500 (* ABS(r0, [0, 9.22341e+18]) *).
 intros h0 h1.
 assert (h2 := l23 h1).
 assert (h3 := l401 h0 h1).
 apply t392. exact h2. exact h3.
Qed.
Definition i328 := makepairF f367 f423.
Notation p535 := (ABS _inv_b_hat1 i328). (* ABS(inv_b_hat1, [2.71051e-20, 6.50521e-19]) *)
Notation p536 := (BND _inv_b_hat1 i328). (* BND(inv_b_hat1, [2.71051e-20, 6.50521e-19]) *)
Notation p537 := (BND r35 i328). (* BND(alpha * inv_b_hat + inv_b_hat, [2.71051e-20, 6.50521e-19]) *)
Definition f435 := Float2 (-1) (-65).
Definition f436 := Float2 (1) (-63).
Definition i329 := makepairF f435 f436.
Notation p538 := (BND r36 i329). (* BND(alpha * inv_b_hat, [-2.71051e-20, 1.0842e-19]) *)
Definition f437 := Float2 (1) (-3).
Definition i330 := makepairF f316 f437.
Notation p539 := (BND _alpha i330). (* BND(alpha, [-0.03125, 0.125]) *)
Notation p540 := (BND _inv_b_hat i317). (* BND(inv_b_hat, [5.42101e-20, 8.67362e-19]) *)
Lemma t393 : p539 -> p540 -> p538.
 intros h0 h1.
 refine (mul_op _alpha _inv_b_hat i330 i317 i329 h0 h1 _) ; finalize.
Qed.
Lemma l433 : p493 -> s1 -> p538 (* BND(alpha * inv_b_hat, [-2.71051e-20, 1.0842e-19]) *).
 intros h0 h1.
 assert (h2 := l61 h1).
 assert (h3 := l412 h0 h1).
 apply t393. refine (subset _alpha i28 i330 h2 _) ; finalize. refine (subset _inv_b_hat i313 i317 h3 _) ; finalize.
Qed.
Lemma t394 : p538 -> p512 -> p537.
 intros h0 h1.
 refine (add r36 _inv_b_hat i329 i313 i328 h0 h1 _) ; finalize.
Qed.
Lemma l432 : p493 -> s1 -> p537 (* BND(alpha * inv_b_hat + inv_b_hat, [2.71051e-20, 6.50521e-19]) *).
 intros h0 h1.
 assert (h2 := l433 h0 h1).
 assert (h3 := l412 h0 h1).
 apply t394. exact h2. exact h3.
Qed.
Lemma t395 : p537 -> p536.
 intros h0.
 refine (float_round_ne _ _ r35 i328 i328 h0 _) ; finalize.
Qed.
Lemma l431 : p493 -> s1 -> p536 (* BND(inv_b_hat1, [2.71051e-20, 6.50521e-19]) *).
 intros h0 h1.
 assert (h2 := l432 h0 h1).
 apply t395. exact h2.
Qed.
Lemma t396 : p536 -> p535.
 intros h0.
 refine (abs_of_bnd_p _inv_b_hat1 i328 i328 h0 _) ; finalize.
Qed.
Lemma l430 : p493 -> s1 -> p535 (* ABS(inv_b_hat1, [2.71051e-20, 6.50521e-19]) *).
 intros h0 h1.
 assert (h2 := l431 h0 h1).
 apply t396. exact h2.
Qed.
Definition f438 := Float2 (5) (61).
Definition i331 := makepairF f1 f438.
Notation p541 := (ABS _r0 i331). (* ABS(r0, [0, 1.15292e+19]) *)
Lemma t397 : p541 -> p535 -> p499.
 intros h0 h1.
 refine (mul_aa _r0 _inv_b_hat1 i331 i328 i305 h0 h1 _) ; finalize.
Qed.
Lemma l399 : p493 -> s1 -> p499 (* ABS(r0 * inv_b_hat1, [0, 8]) *).
 intros h0 h1.
 assert (h2 := l400 h0 h1).
 assert (h3 := l430 h0 h1).
 apply t397. refine (abs_subset _r0 i306 i331 h2 _) ; finalize. exact h3.
Qed.
Lemma t398 : p499 -> p498.
 intros h0.
 refine (float_absolute_wide_ne _ _ r33 i305 i304 h0 _) ; finalize.
Qed.
Lemma l398 : p493 -> s1 -> p498 (* BND(quotient_hat1 - r0 * inv_b_hat1, [-4.44089e-16, 4.44089e-16]) *).
 intros h0 h1.
 assert (h2 := l399 h0 h1).
 apply t398. exact h2.
Qed.
Definition f439 := Float2 (-66049) (-60).
Definition f440 := Float2 (66049) (-60).
Definition i332 := makepairF f439 f440.
Notation p542 := (BND r40 i332). (* BND(r0 * (inv_b_hat1 - 1 / b), [-5.72884e-14, 5.72884e-14]) *)
Definition f441 := Float2 (-262145) (45).
Definition i333 := makepairF f441 f417.
Notation p543 := (BND _r0 i333). (* BND(r0, [-9.22341e+18, 9.22341e+18]) *)
Lemma t399 : p500 -> p543.
 intros h0.
 refine (bnd_of_abs _r0 i306 i333 h0 _) ; finalize.
Qed.
Lemma l435 : p493 -> s1 -> p543 (* BND(r0, [-9.22341e+18, 9.22341e+18]) *).
 intros h0 h1.
 assert (h2 := l400 h0 h1).
 apply t399. exact h2.
Qed.
Definition f442 := Float2 (-132097) (-124).
Definition f443 := Float2 (132097) (-124).
Definition i334 := makepairF f442 f443.
Notation p544 := (BND r41 i334). (* BND(inv_b_hat1 - 1 / b, [-6.21117e-33, 6.21117e-33]) *)
Notation p545 := (BND r65 i334). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-6.21117e-33, 6.21117e-33]) *)
Definition f444 := Float2 (-1) (-114).
Definition f445 := Float2 (1) (-114).
Definition i335 := makepairF f444 f445.
Notation p546 := (BND r66 i335). (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-4.81482e-35, 4.81482e-35]) *)
Definition i336 := makepairF f367 f212.
Notation p547 := (ABS r35 i336). (* ABS(alpha * inv_b_hat + inv_b_hat, [2.71051e-20, 8.67362e-19]) *)
Definition i337 := makepairF f1 f367.
Notation p548 := (ABS r36 i337). (* ABS(alpha * inv_b_hat, [0, 2.71051e-20]) *)
Notation p549 := (ABS _alpha i90). (* ABS(alpha, [0, 0.00390625]) *)
Definition f446 := Float2 (1) (-57).
Definition i338 := makepairF f27 f446.
Notation p550 := (ABS _inv_b_hat i338). (* ABS(inv_b_hat, [5.42101e-20, 6.93889e-18]) *)
Lemma t400 : p549 -> p550 -> p548.
 intros h0 h1.
 refine (mul_aa _alpha _inv_b_hat i90 i338 i337 h0 h1 _) ; finalize.
Qed.
Lemma l440 : p493 -> s1 -> p548 (* ABS(alpha * inv_b_hat, [0, 2.71051e-20]) *).
 intros h0 h1.
 assert (h2 := l90 h1).
 assert (h3 := l411 h0 h1).
 apply t400. refine (abs_subset _alpha i48 i90 h2 _) ; finalize. refine (abs_subset _inv_b_hat i312 i338 h3 _) ; finalize.
Qed.
Lemma t401 : p548 -> p511 -> p547.
 intros h0 h1.
 refine (add_aa_n r36 _inv_b_hat i337 i312 i336 h0 h1 _) ; finalize.
Qed.
Lemma l439 : p493 -> s1 -> p547 (* ABS(alpha * inv_b_hat + inv_b_hat, [2.71051e-20, 8.67362e-19]) *).
 intros h0 h1.
 assert (h2 := l440 h0 h1).
 assert (h3 := l411 h0 h1).
 apply t401. exact h2. exact h3.
Qed.
Lemma t402 : p547 -> p546.
 intros h0.
 refine (float_absolute_wide_ne _ _ r35 i336 i335 h0 _) ; finalize.
Qed.
Lemma l438 : p493 -> s1 -> p546 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat), [-4.81482e-35, 4.81482e-35]) *).
 intros h0 h1.
 assert (h2 := l439 h0 h1).
 apply t402. exact h2.
Qed.
Definition f447 := Float2 (-131073) (-124).
Definition f448 := Float2 (131073) (-124).
Definition i339 := makepairF f447 f448.
Notation p551 := (BND r67 i339). (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-6.16302e-33, 6.16302e-33]) *)
Notation p552 := (BND r68 i339). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-6.16302e-33, 6.16302e-33]) *)
Definition f449 := Float2 (-262145) (-125).
Definition f450 := Float2 (262145) (-125).
Definition i340 := makepairF f449 f450.
Notation p553 := (BND r69 i340). (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-6.163e-33, 6.163e-33]) *)
Notation p554 := (BND r60 i340). (* BND(alpha * inv_b_hat - alpha * (1 / b), [-6.163e-33, 6.163e-33]) *)
Definition f451 := Float2 (-524289) (-103).
Definition f452 := Float2 (1) (-84).
Definition i341 := makepairF f451 f452.
Notation p555 := (BND r26 i341). (* BND(alpha * (1 / b), [-5.16989e-26, 5.16988e-26]) *)
Definition i342 := makepairF f195 f422.
Notation p556 := (BND _alpha i342). (* BND(alpha, [-1.1921e-07, 1.19209e-07]) *)
Lemma t403 : p556 -> p533 -> p555.
 intros h0 h1.
 refine (mul_op _alpha r19 i342 i326 i341 h0 h1 _) ; finalize.
Qed.
Lemma l445 : p493 -> s1 -> p555 (* BND(alpha * (1 / b), [-5.16989e-26, 5.16988e-26]) *).
 intros h0 h1.
 assert (h2 := l61 h1).
 assert (h3 := l429 h0 h1).
 apply t403. refine (subset _alpha i28 i342 h2 _) ; finalize. exact h3.
Qed.
Definition f453 := Float2 (1048577) (-43).
Definition i343 := makepairF f104 f453.
Notation p557 := (REL r36 r26 i343). (* REL(alpha * inv_b_hat, alpha * (1 / b), [-1.19209e-07, 1.19209e-07]) *)
Lemma t404 : p557 -> p555 -> p554.
 intros h0 h1.
 refine (error_of_rel_oo r36 r26 i343 i341 i340 h0 h1 _) ; finalize.
Qed.
Lemma l444 : p493 -> s1 -> p554 (* BND(alpha * inv_b_hat - alpha * (1 / b), [-6.163e-33, 6.163e-33]) *).
 intros h0 h1.
 assert (h2 := l349 h1).
 assert (h3 := l445 h0 h1).
 apply t404. refine (rel_subset r36 r26 i32 i343 h2 _) ; finalize. exact h3.
Qed.
Lemma t405 : p554 -> p553.
 intros h0.
 refine (add_firs _ _ _ i340 h0) ; finalize.
Qed.
Lemma l443 : p493 -> s1 -> p553 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat), [-6.163e-33, 6.163e-33]) *).
 intros h0 h1.
 assert (h2 := l444 h0 h1).
 apply t405. exact h2.
Qed.
Definition f454 := Float2 (-1) (-125).
Definition f455 := Float2 (1) (-125).
Definition i344 := makepairF f454 f455.
Notation p558 := (BND r24 i344). (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-2.35099e-38, 2.35099e-38]) *)
Definition f456 := Float2 (-1) (-70).
Definition f457 := Float2 (1) (-70).
Definition i345 := makepairF f456 f457.
Notation p559 := (REL r25 r19 i345). (* REL(alpha * (1 / b) + inv_b_hat, 1 / b, [-8.47033e-22, 8.47033e-22]) *)
Definition f458 := Float2 (1) (-55).
Definition i346 := makepairF f27 f458.
Notation p560 := (BND r19 i346). (* BND(1 / b, [5.42101e-20, 2.77556e-17]) *)
Lemma t406 : p559 -> p560 -> p558.
 intros h0 h1.
 refine (error_of_rel_op r25 r19 i345 i346 i344 h0 h1 _) ; finalize.
Qed.
Lemma l446 : p493 -> s1 -> p558 (* BND(alpha * (1 / b) + inv_b_hat - 1 / b, [-2.35099e-38, 2.35099e-38]) *).
 intros h0 h1.
 assert (h2 := l146 h1).
 assert (h3 := l429 h0 h1).
 apply t406. refine (rel_subset r25 r19 i36 i345 h2 _) ; finalize. refine (subset r19 i326 i346 h3 _) ; finalize.
Qed.
Lemma t407 : p553 -> p558 -> p552.
 intros h0 h1.
 refine (add r69 r24 i340 i344 i339 h0 h1 _) ; finalize.
Qed.
Lemma l442 : p493 -> s1 -> p552 (* BND(alpha * inv_b_hat + inv_b_hat - (alpha * (1 / b) + inv_b_hat) + (alpha * (1 / b) + inv_b_hat - 1 / b), [-6.16302e-33, 6.16302e-33]) *).
 intros h0 h1.
 assert (h2 := l443 h0 h1).
 assert (h3 := l446 h0 h1).
 apply t407. exact h2. exact h3.
Qed.
Lemma t408 : p552 -> p551.
 intros h0.
 refine (sub_xals _ _ _ i339 h0) ; finalize.
Qed.
Lemma l441 : p493 -> s1 -> p551 (* BND(alpha * inv_b_hat + inv_b_hat - 1 / b, [-6.16302e-33, 6.16302e-33]) *).
 intros h0 h1.
 assert (h2 := l442 h0 h1).
 apply t408. exact h2.
Qed.
Lemma t409 : p546 -> p551 -> p545.
 intros h0 h1.
 refine (add r66 r67 i335 i339 i334 h0 h1 _) ; finalize.
Qed.
Lemma l437 : p493 -> s1 -> p545 (* BND(inv_b_hat1 - (alpha * inv_b_hat + inv_b_hat) + (alpha * inv_b_hat + inv_b_hat - 1 / b), [-6.21117e-33, 6.21117e-33]) *).
 intros h0 h1.
 assert (h2 := l438 h0 h1).
 assert (h3 := l441 h0 h1).
 apply t409. exact h2. exact h3.
Qed.
Lemma t410 : p545 -> p544.
 intros h0.
 refine (sub_xals _ _ _ i334 h0) ; finalize.
Qed.
Lemma l436 : p493 -> s1 -> p544 (* BND(inv_b_hat1 - 1 / b, [-6.21117e-33, 6.21117e-33]) *).
 intros h0 h1.
 assert (h2 := l437 h0 h1).
 apply t410. exact h2.
Qed.
Lemma t411 : p543 -> p544 -> p542.
 intros h0 h1.
 refine (mul_oo _r0 r41 i333 i334 i332 h0 h1 _) ; finalize.
Qed.
Lemma l434 : p493 -> s1 -> p542 (* BND(r0 * (inv_b_hat1 - 1 / b), [-5.72884e-14, 5.72884e-14]) *).
 intros h0 h1.
 assert (h2 := l435 h0 h1).
 assert (h3 := l436 h0 h1).
 apply t411. exact h2. exact h3.
Qed.
Lemma t412 : p498 -> p542 -> p497.
 intros h0 h1.
 refine (add r39 r40 i304 i332 i303 h0 h1 _) ; finalize.
Qed.
Lemma l397 : p493 -> s1 -> p497 (* BND(quotient_hat1 - r0 * inv_b_hat1 + r0 * (inv_b_hat1 - 1 / b), [-5.77325e-14, 5.77325e-14]) *).
 intros h0 h1.
 assert (h2 := l398 h0 h1).
 assert (h3 := l434 h0 h1).
 apply t412. exact h2. exact h3.
Qed.
Lemma t413 : p497 -> p343 -> p496.
 intros h0 h1.
 refine (bnd_of_bnd_rel_o r31 r38 i303 i12 i303 h0 h1 _) ; finalize.
Qed.
Lemma l396 : p493 -> s1 -> p496 (* BND(quotient_hat1 - r0 / b, [-5.77325e-14, 5.77325e-14]) *).
 intros h0 h1.
 assert (h2 := l397 h0 h1).
 assert (h3 := l283 h1).
 apply t413. exact h2. exact h3.
Qed.
Lemma t414 : p7 -> p496 -> p495.
 intros h0 h1.
 refine (add r45 r31 i5 i303 i302 h0 h1 _) ; finalize.
Qed.
Lemma l395 : p493 -> s1 -> p495 (* BND(q1 - quotient_hat1 + (quotient_hat1 - r0 / b), [-0.5, 0.5]) *).
 intros h0 h1.
 assert (h2 := l6 h1).
 assert (h3 := l396 h0 h1).
 apply t414. exact h2. exact h3.
Qed.
Lemma t415 : p495 -> p494.
 intros h0.
 refine (sub_xals _ _ _ i302 h0) ; finalize.
Qed.
Lemma l394 : p493 -> s1 -> p494 (* BND(q1 - r0 / b, [-0.5, 0.5]) *).
 intros h0 h1.
 assert (h2 := l395 h0 h1).
 apply t415. exact h2.
Qed.
Lemma l393 : p493 -> s1 -> False.
 intros h0 h1.
 assert (h2 := l3 h1).
 assert (h3 := l394 h0 h1).
 refine (simplify (Tatom false (Abnd 0%nat i3)) Tfalse (Abnd 0%nat i302) (List.cons r42 List.nil) h3 h2 _) ; finalize.
Qed.
Lemma l1 : s1 -> False.
 intros h0.
 apply (union _b f6).
 intro h1. (* [-inf, 5] *)
 apply (l2 h1 h0).
 apply (union' _b i53).
 intro h1. (* [5, 65] *)
 apply (l94 h1 h0).
 apply (union' _b i96).
 intro h1. (* [65, 16385] *)
 apply (l152 h1 h0).
 apply (union' _b i157).
 intro h1. (* [16385, 1.07374e+09] *)
 apply (l225 h1 h0).
 apply (union' _b i219).
 intro h1. (* [1.07374e+09, 2.30584e+18] *)
 apply (l303 h1 h0).
 intro h1. (* [2.30584e+18, inf] *)
 apply (l393 h1 h0).
Qed.

(* added manually *)
Lemma gappa1_ccl : not ((p1 /\ p2) /\ not p3).
  intro.
  apply l1.
  unfold s1. unfold s2, s3.
  tauto.
Qed.

Theorem STEP1 : p1 /\ p2 -> p3.
  pose proof gappa1_ccl as CCL.
  unfold p1, p2, p3 in *.
  lra.
Qed.

End Generated_by_Gappa1.

End Step_1.


Module Step_2.

Section Generated_by_Gappa2.
Variable _b : R.
Variable _a : R.
Notation r11 := (Float1 (1)).
Notation r12 := ((rounding_float rndNE (24)%positive (-149)%Z) _b).
Notation r10 := ((r11 / r12)%R).
Notation _inv_b_hat := ((rounding_float rndNE (24)%positive (-149)%Z) r10).
Notation r8 := ((_a * _inv_b_hat)%R).
Notation _quotient_hat0 := ((rounding_float rndNE (53)%positive (-1074)%Z) r8).
Notation _q0 := ((rounding_fixed rndNE (0)%Z) _quotient_hat0).
Notation r5 := ((_b * _q0)%R).
Notation _r0 := ((_a - r5)%R).
Notation r22 := ((_b * _inv_b_hat)%R).
Notation r21 := ((r11 - r22)%R).
Notation _alpha := ((rounding_float rndNE (53)%positive (-1074)%Z) r21).
Notation r19 := ((_alpha * _inv_b_hat)%R).
Notation r18 := ((r19 + _inv_b_hat)%R).
Notation _inv_b_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) r18).
Notation r16 := ((_r0 * _inv_b_hat1)%R).
Notation _quotient_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) r16).
Notation _q1 := ((rounding_fixed rndNE (0)%Z) _quotient_hat1).
Notation r13 := ((_q1 * _b)%R).
Notation _remainder := ((_r0 - r13)%R).
Notation r25 := ((_r0 / _b)%R).
Notation r24 := ((r25 - _q1)%R).
Notation r23 := ((r24 * _b)%R).
Hypothesis a1 : (_b <> 0)%R -> _remainder = r23.
Lemma b1 : NZR _b -> _remainder = r23.
 intros h0.
 apply a1.
 exact h0.
Qed.
Definition f1 := Float2 (0) (0).
Definition f2 := Float2 (1) (64).
Definition i1 := makepairF f1 f2.
Notation p1 := (BND _a i1). (* BND(a, [0, 1.84467e+19]) *)
Definition f3 := Float2 (1) (0).
Definition i2 := makepairF f3 f2.
Notation p2 := (BND _b i2). (* BND(b, [1, 1.84467e+19]) *)
Definition s3 := (p1 /\ p2).
Notation r26 := ((_q1 - r25)%R).
Definition f4 := Float2 (-345876451382054093) (-59).
Definition f5 := Float2 (345876451382054093) (-59).
Definition i3 := makepairF f4 f5.
Notation p3 := (BND r26 i3). (* BND(q1 - r0 / b, [-0.6, 0.6]) *)
Definition s2 := (s3 /\ p3).
Notation _remainder_over_b := ((_remainder / _b)%R).
Definition f6 := Float2 (-807045053224792883) (-60).
Definition f7 := Float2 (807045053224792883) (-60).
Definition i4 := makepairF f6 f7.
Notation p4 := (BND _remainder_over_b i4). (* BND(remainder_over_b, [-0.7, 0.7]) *)
Definition s4 := (not p4).
Definition s1 := (s2 /\ s4).
Lemma l2 : s1 -> s4.
 intros h0.
 assert (h1 := h0).
 exact (proj2 h1).
Qed.
Notation p5 := (BND _remainder_over_b i3). (* BND(remainder_over_b, [-0.6, 0.6]) *)
Notation p6 := (NZR _b). (* NZR(b) *)
Notation p7 := (ABS _b i2). (* ABS(b, [1, 1.84467e+19]) *)
Lemma l8 : s1 -> s2.
 intros h0.
 assert (h1 := h0).
 exact (proj1 h1).
Qed.
Lemma l7 : s1 -> s3.
 intros h0.
 assert (h1 := l8 h0).
 exact (proj1 h1).
Qed.
Lemma l6 : s1 -> p2 (* BND(b, [1, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l7 h0).
 exact (proj2 h1).
Qed.
Lemma t1 : p2 -> p7.
 intros h0.
 refine (abs_of_bnd_p _b i2 i2 h0 _) ; finalize.
Qed.
Lemma l5 : s1 -> p7 (* ABS(b, [1, 1.84467e+19]) *).
 intros h0.
 assert (h1 := l6 h0).
 apply t1. exact h1.
Qed.
Lemma t2 : p7 -> p6.
 intros h0.
 refine (nzr_of_abs _b i2 h0 _) ; finalize.
Qed.
Lemma l4 : s1 -> p6 (* NZR(b) *).
 intros h0.
 assert (h1 := l5 h0).
 apply t2. exact h1.
Qed.
Notation r30 := ((_remainder - r23)%R).
Notation r29 := ((r30 / _b)%R).
Notation r31 := ((r23 / _b)%R).
Notation r28 := ((r29 + r31)%R).
Notation p8 := (BND r28 i3). (* BND((remainder - (r0 / b - q1) * b) / b + (r0 / b - q1) * b / b, [-0.6, 0.6]) *)
Definition i5 := makepairF f1 f1.
Notation p9 := (BND r29 i5). (* BND((remainder - (r0 / b - q1) * b) / b, [0, 0]) *)
Notation p10 := (BND r30 i5). (* BND(remainder - (r0 / b - q1) * b, [0, 0]) *)
Notation p11 := (_remainder = r23). (* EQL(remainder, (r0 / b - q1) * b) *)
Lemma t3 : p6 -> p11.
 intros h0.
 refine (b1 h0) ; finalize.
Qed.
Lemma l12 : s1 -> p11 (* EQL(remainder, (r0 / b - q1) * b) *).
 intros h0.
 assert (h1 := l4 h0).
 apply t3. exact h1.
Qed.
Lemma t4 : p11 -> p10.
 intros h0.
 refine (sub_of_eql _remainder r23 i5 h0 _) ; finalize.
Qed.
Lemma l11 : s1 -> p10 (* BND(remainder - (r0 / b - q1) * b, [0, 0]) *).
 intros h0.
 assert (h1 := l12 h0).
 apply t4. exact h1.
Qed.
Lemma t5 : p10 -> p2 -> p9.
 intros h0 h1.
 refine (div_pp r30 _b i5 i2 i5 h0 h1 _) ; finalize.
Qed.
Lemma l10 : s1 -> p9 (* BND((remainder - (r0 / b - q1) * b) / b, [0, 0]) *).
 intros h0.
 assert (h1 := l11 h0).
 assert (h2 := l6 h0).
 apply t5. exact h1. exact h2.
Qed.
Notation p12 := (BND r31 i3). (* BND((r0 / b - q1) * b / b, [-0.6, 0.6]) *)
Notation p13 := (BND r24 i3). (* BND(r0 / b - q1, [-0.6, 0.6]) *)
Notation r33 := ((r25 - r25)%R).
Notation r32 := ((r33 - r26)%R).
Notation p14 := (BND r32 i3). (* BND(r0 / b - r0 / b - (q1 - r0 / b), [-0.6, 0.6]) *)
Notation p15 := (BND r33 i5). (* BND(r0 / b - r0 / b, [0, 0]) *)
Lemma t6 : p15.
 refine (sub_refl _ i5 _) ; finalize.
Qed.
Lemma l16 : s1 -> p15 (* BND(r0 / b - r0 / b, [0, 0]) *).
 intros h0.
 apply t6.
Qed.
Lemma l17 : s1 -> p3 (* BND(q1 - r0 / b, [-0.6, 0.6]) *).
 intros h0.
 assert (h1 := l8 h0).
 exact (proj2 h1).
Qed.
Lemma t7 : p15 -> p3 -> p14.
 intros h0 h1.
 refine (sub r33 r26 i5 i3 i3 h0 h1 _) ; finalize.
Qed.
Lemma l15 : s1 -> p14 (* BND(r0 / b - r0 / b - (q1 - r0 / b), [-0.6, 0.6]) *).
 intros h0.
 assert (h1 := l16 h0).
 assert (h2 := l17 h0).
 apply t7. exact h1. exact h2.
Qed.
Lemma t8 : p14 -> p13.
 intros h0.
 refine (sub_xars _ _ _ i3 h0) ; finalize.
Qed.
Lemma l14 : s1 -> p13 (* BND(r0 / b - q1, [-0.6, 0.6]) *).
 intros h0.
 assert (h1 := l15 h0).
 apply t8. exact h1.
Qed.
Lemma t9 : p6 -> p13 -> p12.
 intros h0 h1.
 refine (div_fir r24 _b i3 h0 h1) ; finalize.
Qed.
Lemma l13 : s1 -> p12 (* BND((r0 / b - q1) * b / b, [-0.6, 0.6]) *).
 intros h0.
 assert (h1 := l4 h0).
 assert (h2 := l14 h0).
 apply t9. exact h1. exact h2.
Qed.
Lemma t10 : p9 -> p12 -> p8.
 intros h0 h1.
 refine (add r29 r31 i5 i3 i3 h0 h1 _) ; finalize.
Qed.
Lemma l9 : s1 -> p8 (* BND((remainder - (r0 / b - q1) * b) / b + (r0 / b - q1) * b / b, [-0.6, 0.6]) *).
 intros h0.
 assert (h1 := l10 h0).
 assert (h2 := l13 h0).
 apply t10. exact h1. exact h2.
Qed.
Lemma t11 : p6 -> p8 -> p5.
 intros h0 h1.
 refine (div_xals _ _ _b i3 h0 h1) ; finalize.
Qed.
Lemma l3 : s1 -> p5 (* BND(remainder_over_b, [-0.6, 0.6]) *).
 intros h0.
 assert (h1 := l4 h0).
 assert (h2 := l9 h0).
 apply t11. exact h1. exact h2.
Qed.
Lemma l1 : s1 -> False.
 intros h0.
 assert (h1 := l2 h0).
 assert (h2 := l3 h0).
 refine (simplify (Tatom false (Abnd 0%nat i4)) Tfalse (Abnd 0%nat i3) (List.cons _remainder_over_b List.nil) h2 h1 _) ; finalize.
Qed.

(* added manually *)
Lemma gappa2_ccl : not ((p1 /\ p2 /\ 
p3) /\ not p4).
  intro.
  apply l1.
  unfold s1. unfold s2, s4. unfold s3.
  tauto.
Qed.

(* bound_a /\ bound_b /\ bound_exact_alpha -> bound_remainder_over_b *)
Theorem STEP2 : p1 /\ p2 /\ 
p3 ->
p4.
  pose proof gappa2_ccl as CCL.
  unfold p1, p2, p3, p4 in *.
  lra.
Qed.
End Generated_by_Gappa2.

End Step_2.


Section Division_algorithm.

(* === Joining STEP1 and STEP2 === *)

(* Rewriting rules from Gappa scripts *)

(* a - b * q0 -> (a/b - q0) * b *)
Lemma rr1 : forall a b c : R, b <> 0%R -> (a - b * c = (a/b - c) * b)%R.
  intros.
  field.
  exact H.
Qed.

(* b * inv_b_hat - 1 -> (inv_b_hat - 1/b) / (1/b) { b <> 0 } *)
Lemma rr2 : forall b c : R, b <> 0%R -> (b * c - 1 = (c - 1/b) / (1/b))%R.
  intros.
  field.
  exact H.
Qed.

(* quotient_hat0 - a/b -> (quotient_hat0 - a * inv_b_hat1) + a * (inv_b_hat1 - 1/b) { b <> 0 }; *)
Lemma rr3 : forall a b c d : R, b <> 0%R -> ((d - a/b) = (d - a * c) + a * (c - 1/b))%R. 
  intros.
  field.
  exact H.
Qed.


(* (alpha * (1/b) + inv_b_hat) - 1/b -> (alpha - (1 - b * inv_b_hat)) * (1/b) { b <> 0 } *)
Lemma rr4 : forall a b c : R, b <> 0%R -> ((a * (1/b) + c) - 1/b = (a - (1 - b * c)) * (1/b))%R.
  intros.
  field.
  exact H.
Qed.

(* a - b * q0 -> (a/b - q0) * b *)
Lemma rr5 : forall a b c : R, b <> 0%R -> (a - c * b = (a/b - c) * b)%R.
  intros.
  field.
  exact H.
Qed.

(* Variables as defined by Gappa *)
Variable a : Z.
Variable b : Z.
Definition aR := IZR a.
Definition bR := IZR b.
Notation float_b := ((rounding_float rndNE (24)%positive (-149)%Z) bR).
Notation float_one := (Float1 (1)).
Notation inv_b := ((float_one / float_b)%R).
Notation inv_b_hat := ((rounding_float rndNE (24)%positive (-149)%Z) inv_b).
Notation inv_b_hatb := ((bR * inv_b_hat)%R).
Notation exact_alpha := ((float_one - inv_b_hatb)%R).
Notation alpha := ((rounding_float rndNE (53)%positive (-1074)%Z) exact_alpha).
Notation inv_b_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) ((alpha * inv_b_hat)%R + inv_b_hat)%R).
Notation quotient_hat0 := ((rounding_float rndNE (53)%positive (-1074)%Z) (aR * inv_b_hat)%R).
Notation q0 := ((rounding_fixed rndNE (0)%Z) quotient_hat0).
Notation r0 := ((aR - (bR * q0)%R)%R).
Notation quotient_hat1 := ((rounding_float rndNE (53)%positive (-1074)%Z) ((r0 * inv_b_hat1)%R)).
Notation q1 := ((rounding_fixed rndNE (0)%Z) quotient_hat1).
Notation qR := (q0 + q1)%R.
Notation remainderR := ((r0 - (q1 * bR)%R)%R).
Notation remainder_over_bR := ((remainderR / bR)%R).

Definition lower_a := Float2 (0) (0).
Definition upper_ab := Float2 (1) (64).
Definition bound_a0 := makepairF lower_a upper_ab.
Definition lower_b := Float2 (1) (0).
Definition bound_b0 := makepairF lower_b upper_ab.
Notation bound_a := (BND aR bound_a0).
Notation bound_b := (BND bR bound_b0).
Definition lower_rob := Float2 (-807045053224792883) (-60).
Definition upper_rob := Float2 (807045053224792883) (-60).
Definition bound_rob0 := makepairF lower_rob upper_rob.
Notation bound_rob := (BND remainder_over_bR bound_rob0).

Lemma STEPS_bound_remainder_over_b : bound_a /\ bound_b -> bound_rob.
  intros [H0 H1].
  apply Step_2.STEP2. 
  - intro. apply rr5. exact H.
  - split. exact H0.
    split. exact H1. 
    apply Step_1.STEP1. 
    apply rr1.
    apply rr2. 
    apply rr3.
    apply rr4. 
    apply rr3.
    split. exact H0. exact H1.
Qed.

(* === Transforming Gappa bounds into Z bounds === *)
(* From 2**64 to 2**64-1 *)
Notation bound_aZ := (0 <= a <= 18446744073709551616)%Z.
Notation bound_bZ := (1 <= b <= 18446744073709551616)%Z.
Notation bound_robR := (-0.7 <= remainder_over_bR <= 0.7)%R.

Lemma bb0 : (float2R lower_a = 0)%R. 
  unfold lower_a.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma bb1 : (float2R upper_ab = 18446744073709551616)%R.
  unfold upper_ab.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma bb2 : (float2R lower_b = 1)%R.
  unfold lower_b.
  unfold float2R, Defs.F2R.
  cbn. 
  lra.
Qed.

Lemma bb3 : (-0.7 <= lower_rob)%R.
  unfold lower_rob.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma bb4 : (upper_rob <= 0.7)%R.
  unfold upper_rob.
  unfold float2R, Defs.F2R.
  cbn.
  lra.
Qed.

Lemma b_aR : bound_a -> (0 <= aR <= 18446744073709551616)%R.
  intro. destruct H. split. cbn in *.
  - rewrite <- bb0. exact H.
  - rewrite <- bb1. exact H0.
Qed.

Lemma b_aZ : bound_a -> bound_aZ.
  intro. apply b_aR in H.
  split. destruct H.
  - apply le_IZR. tauto.
  - apply le_IZR. tauto.
Qed.

Lemma b_bR : bound_b -> (1 <= bR <= 18446744073709551616)%R.
  intro. destruct H. split. cbn in *.
  - rewrite <- bb2. exact H.
  - rewrite <- bb1. exact H0.
Qed.

Lemma b_bZ : bound_b -> bound_bZ.
  intro. apply b_bR in H.
  split. destruct H.
  - apply le_IZR. tauto.
  - apply le_IZR. tauto.
Qed.

Lemma b_robR : bound_rob -> bound_robR.
  intro. destruct H. split. cbn in *. 
  - apply Rle_trans with lower_rob.
    apply bb3. exact H.
  - apply Rle_trans with upper_rob. 
    exact H0. apply bb4.
Qed.

(* Variables in Z *)
Definition real_eucl_quotient := Z.div a b. (* floored division *)
Definition Q0 := ZnearestE (quotient_hat0).
Definition R0 := (a - b * Q0)%Z.
Definition Q1 := ZnearestE (quotient_hat1).
Definition QUOTIENT_rounded_ne := (Q0 + Q1)%Z.
Definition QUOTIENT_minus_1 := (QUOTIENT_rounded_ne - 1)%Z.
Definition remainder := (R0 - Q1 * b)%Z.
Definition remainder_over_b := (IZR remainder / IZR b)%R.

(* === Equality of Gappa variables and self-defined variables === *)

Theorem t0 : forall a : R, ((rounding_fixed rndNE (0)%Z) a) 
= (IZR (rndNE (scaled_mantissa radix2 (FIX_exp 0) a)) * bpow radix2 (0))%R.
  tauto.
Qed.

Theorem t1 : forall a : R, ((rounding_fixed rndNE (0)%Z) a) 
= (IZR (rndNE (scaled_mantissa radix2 (FIX_exp 0) a))).
  intro.
  rewrite -> t0.
  unfold bpow. 
  field.
Qed.

Theorem t2 : forall a : R, (scaled_mantissa radix2 (FIX_exp 0) a) = a.
  unfold bpow.
  intro.
  unfold scaled_mantissa. unfold bpow.
  cbn.
  field.
Qed.

Theorem int_rnd : forall a : R, ((rounding_fixed rndNE (0)%Z) a) = IZR (ZnearestE a).
  intro.
  rewrite -> t1.
  rewrite -> t2.
  tauto.
Qed.
 
Lemma q0_eq : (q0 = IZR Q0)%R.
  apply int_rnd.
Qed.

Lemma q1_eq : (q1 = IZR Q1)%R.
  apply int_rnd.
Qed.

Lemma qR_eq : (qR = IZR QUOTIENT_rounded_ne)%R.
  unfold QUOTIENT_rounded_ne. rewrite -> plus_IZR.  
  rewrite <- q0_eq. rewrite <- q1_eq. 
  reflexivity.
Qed.

Lemma rem_eq : (remainderR = IZR remainder)%R. 
  unfold remainder. unfold R0.  
  rewrite -> minus_IZR. rewrite -> minus_IZR. rewrite -> mult_IZR. rewrite -> mult_IZR. 
  rewrite <- q1_eq. rewrite <- q0_eq. 
  tauto.
Qed.

(* === Bounding the remainder === *)

Lemma bound_remainder_over_b : bound_a /\ bound_b -> (-0.7 <= remainder_over_b <= 0.7)%R.
  intros.
  apply STEPS_bound_remainder_over_b in H. 
  apply b_robR in H.
  unfold remainder_over_b. rewrite <- rem_eq.
  tauto.
Qed.

Lemma b_gt_zero : (b > 0)%Z -> (IZR b > 0)%R.
  intro.
  apply IZR_lt.
  lia.
Qed.

Lemma e0 : (b > 0)%Z -> (remainder_over_b * IZR b = IZR remainder)%R.
  intro.
  unfold remainder_over_b.
  field.
  apply b_gt_zero in H.
  lra.
Qed.

Lemma bound_remainder : (b > 0)%Z /\ bound_a /\ bound_b -> (-0.7 * IZR b <= IZR remainder <= 0.7 * IZR b)%R.
  intros [H0 H1].
  apply bound_remainder_over_b in H1. rewrite <- e0.
  apply b_gt_zero in H0.
  nra.
  exact H0.
Qed.

Lemma bound_remainder_b : (b > 0)%Z /\ bound_a /\ bound_b -> (- IZR b < IZR remainder < IZR b)%R.
  intros. 
  apply bound_remainder in H as H0.
  destruct H.
  apply b_gt_zero in H.
  lra.
Qed.

Lemma bound_remainder_b_in_Z : (b > 0)%Z /\ bound_a /\ bound_b -> (- b < remainder < b)%Z.
  intros.
  apply bound_remainder_b in H.
  destruct H.
  split.
  - apply lt_IZR. rewrite -> opp_IZR. exact H.
  - apply lt_IZR. exact H0.
Qed.

(* === Two cases === *)
Lemma two_cases : (b > 0)%Z /\ bound_a /\ bound_b -> (- b < remainder < 0)%Z \/ (0 <= remainder < b)%Z.
  intros.
  apply bound_remainder_b_in_Z in H.
  lia.
Qed.

(* case #1: (0 <= remainder < b)%Z *)
Lemma a_eq_bqr : (a = b * QUOTIENT_rounded_ne + remainder)%Z.
  unfold remainder, QUOTIENT_rounded_ne. unfold R0.
  lia.
Qed.

Lemma remainder_gt_0 : (0 <= remainder < b)%Z -> QUOTIENT_rounded_ne = real_eucl_quotient.
  intros.
  apply Zdiv_unique with remainder.
  - exact H.
  - apply a_eq_bqr.
Qed.

(* case #2 : (- b < remainder < 0)%Z *)
Definition real_remainder := (a - QUOTIENT_minus_1 * b)%Z.

Lemma rr_eq : (real_remainder = remainder + b)%Z.
  unfold real_remainder. unfold QUOTIENT_minus_1. unfold remainder. unfold R0, QUOTIENT_rounded_ne.
  lia.
Qed.

Lemma bound_real_remainder : (- b < remainder < 0)%Z -> (0 < real_remainder < b)%Z.
  intros.
  rewrite -> rr_eq.
  lia.
Qed.

Lemma a_eq_bqirr : (a = b * QUOTIENT_minus_1 + real_remainder)%Z.
  unfold real_remainder.
  lia.
Qed.

Lemma remainder_lt_0 : (- b < remainder < 0)%Z -> QUOTIENT_minus_1 = real_eucl_quotient.
  intro.
  apply Zdiv_unique with real_remainder.
  apply bound_real_remainder in H.
  - lia.
  - apply bound_real_remainder in H. rewrite <- a_eq_bqirr. reflexivity.
Qed.

Lemma algorithm_correct : (b > 0)%Z /\ bound_a /\ bound_b -> 
  QUOTIENT_rounded_ne = real_eucl_quotient \/ QUOTIENT_minus_1 = real_eucl_quotient.
  intros.
  apply two_cases in H.
  elim H.
  - intro. apply remainder_lt_0 in H0. auto.
  - intro. apply remainder_gt_0 in H0. auto.
Qed.

End Division_algorithm.




